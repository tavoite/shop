import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ClientManagerComponent} from './client-manager.component';
import {ClientProvider} from '../../provider/client.provider';
import {SharedModule} from '../../../shared/shared.module';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {Client} from '../../models/client';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RouterTestingModule} from '@angular/router/testing';

describe('ClientManagerComponent', () => {
  let component: ClientManagerComponent;
  let fixture: ComponentFixture<ClientManagerComponent>;
  let native: HTMLElement;

  const client = new Client('c1', 'test@test.pl');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClientManagerComponent
      ],
      imports: [
        SharedModule,
        FontAwesomeModule,
        NgbDropdownModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        ClientProvider
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientManagerComponent);

    component = fixture.componentInstance;
    component.client = client;

    native = fixture.debugElement.nativeElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display client email', () => {
    expect(native.querySelectorAll('.item')[0].textContent).toEqual(client.email);
  });

  it('should have profile button', () => {
    expect(native.querySelectorAll('.item')[1].querySelector('a').textContent).toEqual('Profile');
  });

  it('should have logout button', () => {
    expect(native.querySelectorAll('.item')[2].querySelector('a').textContent).toEqual('Logout');
  });
});
