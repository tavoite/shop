import {Address} from './address';

export class Client {
  public constructor(private _id: string, private _email: string, private _address?: Address) {
  }

  public get id(): string {
    return this._id;
  }

  public get email(): string {
    return this._email;
  }

  public get address(): Address | undefined {
    return this._address;
  }
}
