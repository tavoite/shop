import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {ClientManagerComponent} from './components/client-manager/client-manager.component';
import {ClientProvider} from './provider/client.provider';
import {AddressFactory} from './factory/address.factory';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FontAwesomeModule
  ],
  exports: [
    ClientManagerComponent
  ],
  declarations: [
    ClientManagerComponent
  ],
  providers: [
    ClientProvider,
    AddressFactory,
  ]
})
export class ClientModule {
}
