import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from '../shared/shared.module';
import {HeaderComponent} from './components/header/header.component';
import {ProductModule} from '../product/product.module';
import {ClientModule} from '../client/client.module';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';
import {CheckoutModule} from '../checkout/checkout.module';
import {OrderService} from '../checkout/services/order.service';
import {PRODUCT_MODULE_GLOBAL_CONFIG} from '../product/consts/product-module-global-config.const';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {HttpClientModule} from '@angular/common/http';
import {DbInMemoryService} from './services/db-in-memory.service';
import {NavigationComponent} from './components/navigation/navigation.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {FooterComponent} from './components/footer/footer.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {ClientProvider} from '../client/provider/client.provider';
import {OrderProvider} from '../checkout/provider/order.provider';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../../environments/environment';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {ThemeService} from './services/theme.service';
import {ThemeFactory} from './factory/theme.factory';
import {faCircle, faUser} from '@fortawesome/free-regular-svg-icons';
import {
  faArrowRight, faArrowLeft, faCartPlus, faAngleLeft, faAngleRight, faAngleDown, faSearch, faTimes, faShoppingCart,
  faBars, faSlidersH, faPalette
} from '@fortawesome/free-solid-svg-icons';
import {ThemeManagerComponent} from './components/theme-manager/theme-manager.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

library.add(
  faUser, faCircle, faArrowLeft, faArrowRight, faAngleLeft, faAngleRight, faAngleDown, faCartPlus, faSearch, faTimes,
  faShoppingCart, faBars, faSlidersH, faPalette
);

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NotFoundPageComponent,
    HeaderComponent,
    NavigationComponent,
    SidebarComponent,
    FooterComponent,
    ThemeManagerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SwiperModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      DbInMemoryService, {
        dataEncapsulation: false,
      }
    ),
    NgbDropdownModule.forRoot(),
    SharedModule,
    ProductModule,
    ClientModule,
    CheckoutModule,
    MatSidenavModule,
    FontAwesomeModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
    AppRoutingModule
  ],
  providers: [
    ThemeService,
    ThemeFactory,
    {
      provide: PRODUCT_MODULE_GLOBAL_CONFIG,
      useValue: {
        checkoutUrl: '/checkout'
      }
    },
    {
      provide: OrderService,
      useClass: OrderService
    },
    {
      provide: OrderProvider,
      useClass: OrderProvider
    },
    {
      provide: ClientProvider,
      useClass: ClientProvider
    },
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
