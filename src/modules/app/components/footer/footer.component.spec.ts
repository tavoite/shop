import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FooterComponent} from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let native: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FooterComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have link to company website', () => {
    expect(native.querySelector('a').getAttribute('href')).toEqual('http://tavoite.pl');
  });

  it('should display app version', () => {
    component.appVersion = '1.0.0';

    fixture.detectChanges();

    expect(native.querySelector('.app-version').textContent).toEqual('1.0.0');
  });
});
