import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ThemeManagerComponent} from './theme-manager.component';
import {SharedModule} from '../../../shared/shared.module';
import {NgbDropdownConfig, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {ThemeService} from '../../services/theme.service';
import {ThemeFactory} from '../../factory/theme.factory';
import {RouterTestingModule} from '@angular/router/testing';

describe('ThemeManagerComponent', () => {
  let component: ThemeManagerComponent;
  let fixture: ComponentFixture<ThemeManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThemeManagerComponent],
      imports: [
        SharedModule,
        NgbDropdownModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        ThemeService,
        ThemeFactory
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
