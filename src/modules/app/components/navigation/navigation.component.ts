import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CategoryRepository} from '../../../product/repositories/category.repository';
import {Category} from '../../../product/models/category';
import {DropdownItem} from '../../../shared/models/dropdown-item';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  @Output()
  public linkClick = new EventEmitter();

  public categoryCollection: Array<Category> = [];

  public constructor(private categoryRepository: CategoryRepository) {
  }

  public ngOnInit(): void {
    this.categoryRepository.getAll().subscribe((categoryCollection: Array<Category>) => {
      this.categoryCollection = categoryCollection;
    });
  }

  public onLinkClick(): void {
    this.linkClick.emit();
  }

  public get dropdownItems(): Array<DropdownItem> {
    const dropdownItems = [];

    for (const category of this.categoryCollection) {
      dropdownItems.push(new DropdownItem(category.name, ['product-list', category.id]));
    }

    return dropdownItems;
  }
}
