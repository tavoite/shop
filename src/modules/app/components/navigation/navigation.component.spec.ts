import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NavigationComponent} from './navigation.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedModule} from '../../../shared/shared.module';
import {ProductModule} from '../../../product/product.module';
import {CategoryRepository} from '../../../product/repositories/category.repository';
import {of} from 'rxjs';
import {Category} from '../../../product/models/category';
import {click} from '../../../shared/testing/helpers/click.helper';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;
  let native: HTMLElement;

  beforeEach(async(() => {
    const categoryRepositorySpy = jasmine.createSpyObj('CategoryRepository', ['getAll']);

    categoryRepositorySpy.getAll.and.returnValue(of([
      new Category(1, 'Test category 1'),
      new Category(2, 'Test category 2'),
    ]));

    TestBed.configureTestingModule({
      declarations: [
        NavigationComponent,
      ],
      imports: [
        SharedModule,
        ProductModule,
        NgbDropdownModule.forRoot(),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: CategoryRepository,
          useValue: categoryRepositorySpy
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have two item-links', () => {
    expect(native.querySelectorAll('.item-link').length).toEqual(2);
  });

  it('should raise linkClick event when item clicked', () => {
    let clicked: boolean = false;

    component.linkClick.subscribe(category => {
      clicked = true;
    });

    click(<HTMLElement>native.querySelectorAll('.item-link')[0]);

    expect(clicked).toBeTruthy();
  });
});
