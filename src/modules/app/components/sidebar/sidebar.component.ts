import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Input()
  public sidenav: MatSidenav;

  @Output()
  public sidenavToggle = new EventEmitter<boolean>();

  @HostListener('window:resize', ['$event'])
  public onResize(): void {
    this.closeSidebar();
  }

  public onButtonClick(): void {
    this.closeSidebar();
  }

  public onLinkClick(): void {
    this.closeSidebar();
  }

  public onThemeSelect(): void {
    this.closeSidebar();
  }

  private closeSidebar(): void {
    this.sidenav.close();
    this.sidenavToggle.emit(false);
  }
}
