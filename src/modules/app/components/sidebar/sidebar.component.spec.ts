import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SidebarComponent} from './sidebar.component';
import {MatSidenavModule} from '@angular/material';
import {NavigationComponent} from '../navigation/navigation.component';
import {SharedModule} from '../../../shared/shared.module';
import {ProductModule} from '../../../product/product.module';
import {NgbDropdownConfig, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ThemeManagerComponent} from '../theme-manager/theme-manager.component';
import {ThemeService} from '../../services/theme.service';
import {ThemeFactory} from '../../factory/theme.factory';

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  let native: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SidebarComponent,
        NavigationComponent,
        ThemeManagerComponent
      ],
      imports: [
        MatSidenavModule,
        SharedModule,
        ProductModule,
        NgbDropdownModule,
        FontAwesomeModule,
        RouterTestingModule,
      ],
      providers: [
        ThemeService,
        ThemeFactory,
        NgbDropdownConfig
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title element', () => {
    expect(native.querySelector('.title').textContent).toEqual('Sidebar');
  });
});
