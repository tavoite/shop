import {Theme} from '../models/theme';
import {Injectable} from '@angular/core';

@Injectable()
export class ThemeFactory {
  private document: HTMLElement;

  public constructor() {
    this.document = document.documentElement;
  }

  public create(key: string, name: string): Theme {
    const theme = new Theme(key, name);

    theme.primaryColor = this.getDefinedColor(key, 'primary');
    theme.primaryColor1 = this.getDefinedColor(key, 'primary-1');
    theme.primaryColor2 = this.getDefinedColor(key, 'primary-2');

    return theme;
  }

  private getDefinedColor(key: string, property: string): string {
    return getComputedStyle(this.document).getPropertyValue(`--c-theme-${key}-${property}`);
  }
}
