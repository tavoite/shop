import {Component, Renderer2} from '@angular/core';
import {TouchDetectorService} from '../shared/services/touch-detector.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public appName: string = 'Shop';
  public appVersion: string = '1.0.5';
  public readonly TOUCH_DISABLED_CLASS = 'touch-disabled';
  public readonly SCROLL_DISABLED_CLASS = 'scroll-disabled';

  public constructor(public touchDetector: TouchDetectorService, private renderer: Renderer2) {
    this.handleTouchDisabledClass();
  }

  public onSidenavToggle(open: boolean): void {
    if (open) {
      this.renderer.addClass(document.body, this.SCROLL_DISABLED_CLASS);
    } else {
      this.renderer.removeClass(document.body, this.SCROLL_DISABLED_CLASS);
    }
  }

  private handleTouchDisabledClass(): void {
    if (this.touchDetector.isTouchDisabled()) {
      this.renderer.addClass(document.body, this.TOUCH_DISABLED_CLASS);
    } else {
      this.renderer.removeClass(document.body, this.TOUCH_DISABLED_CLASS);
    }
  }
}
