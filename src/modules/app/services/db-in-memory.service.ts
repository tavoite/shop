import {InMemoryDbService} from 'angular-in-memory-web-api';
import {ProductResponseInterface} from '../../product/interfaces/product-response.interface';
import {PaymentResponseInterface} from '../../checkout/interfaces/payment-response.interface';
import {TransportResponseInterface} from '../../checkout/interfaces/transport-response.interface';

export class DbInMemoryService implements InMemoryDbService {
  public createDb() {
    /* tslint:disable:max-line-length */
    const products: Array<ProductResponseInterface> = [
      {
        'id': 1,
        'name': 'Samsung Galaxy Note 9 N960F Dual SIM 6/128 Midnight Black',
        'price': 4298,
        'quantity': 26,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-note-9-n960f-dual-sim-6128-midnight-black-440888,2018/8/pr_2018_8_9_8_38_36_650_05.jpg',
          'main': true,
          'id': 1
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-note-9-n960f-dual-sim-6128-midnight-black-440888,2018/8/pr_2018_8_9_8_38_23_195_02.jpg',
          'main': false,
          'id': 2
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-note-9-n960f-dual-sim-6128-midnight-black-440888,2018/8/pr_2018_8_9_8_38_19_86_01.jpg',
          'main': false,
          'id': 3
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-note-9-n960f-dual-sim-6128-midnight-black-440888,2018/8/pr_2018_8_9_8_38_14_570_00.jpg',
          'main': false,
          'id': 4
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-note-9-n960f-dual-sim-6128-midnight-black-440888,2018/8/pr_2018_8_9_8_38_27_477_03.jpg',
          'main': false,
          'id': 5
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-note-9-n960f-dual-sim-6128-midnight-black-440888,2018/8/pr_2018_8_9_8_38_31_555_04.jpg',
          'main': false,
          'id': 6
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Renata',
          'date': '2018-08-31T08:16:00.000Z',
          'content': 'Telefon użytkuje jako służbowy i nie wyobrażam sobie pracy na innym sprzęcie. Jest szybki, przejrzysty, elegancki i niezawodny. Mega plus za pojemną baterię i funkcję szybkiego ladowania, która nie raz uratowała mi życie :):):)',
          'id': 1
        }, {
          'rating': 6,
          'authorName': 'Tytan',
          'date': '2018-09-06T15:29:00.000Z',
          'content': 'To niesamowitej jakości smartfon, aparat, nawigacja, kamera, i komp w jednym. Stylistyka nie powala bo jest podobny po prostu do ósemki, ale funkcjonalnością przewyższa go o głowę. Polecam!!!',
          'id': 2
        }],
        'attributes': [{
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Samsung Exynos 9810 (4 rdzenie, 2.7 GHz + 4 rdzenie, 1.7 GHz)', 'id': 1}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Mali-G72 MP18', 'id': 2}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '6 GB', 'id': 3}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '6,4"', 'id': 6}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2960 x 1440', 'id': 7}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 4000 mAh', 'id': 18}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 8.1 Oreo', 'id': 19}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '12.0 Mpix - tył (podwójny)', 'id': 20}, {'name': '8.0 Mpix - przód', 'id': 21}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/1.7 - przedni obiektyw', 'id': 22}, {
            'name': 'f/1.5 - tylny obiektyw',
            'id': 23
          }, {'name': 'f/2.4 - tylny teleobiektyw', 'id': 24}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K (do 60 kl./s)', 'id': 26}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM Active LTE - Obsługa dwóch kart SIM w technologii aktywny Dual SIM', 'id': 27}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,8 mm', 'id': 28}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '76,3 mm', 'id': 29}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '161,9 mm', 'id': 30}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '204 g', 'id': 31}]
        }, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Czarny - Midnight Black', 'id': 32}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Obudowa wykonana ze szkła i aluminium', 'id': 33}, {
            'name': 'Funkcja ładowania bezprzewodowego',
            'id': 34
          }, {'name': 'Pyłoszczelność i wodoszczelność (IP68)', 'id': 35}, {'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Żyroskop',
            'id': 37
          }, {'name': 'Magnetometr', 'id': 38}, {'name': 'Barometr', 'id': 39}, {'name': 'Pulsometr', 'id': 40}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Czujnik Halla', 'id': 43}, {
            'name': 'Wbudowane głośniki stereo',
            'id': 44
          }, {'name': 'Czytnik linii papilarnych', 'id': 45}, {'name': 'Skaner twarzy', 'id': 46}, {
            'name': 'Skaner tęczówki oka',
            'id': 47
          }, {'name': 'Funkcja szybkiego ładowania Quick Charge', 'id': 48}, {'name': 'USB OTG', 'id': 49}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel USB typ C', 'id': 51}, {
            'name': 'Przejściówka USB-C -&gt; micro USB',
            'id': 52
          }, {'name': 'Przejściówka USB-C -&gt; USB', 'id': 53}, {'name': 'Słuchawki', 'id': 54}, {
            'name': 'Rysik',
            'id': 55
          }, {'name': 'Instrukcja szybkiego uruchomienia telefonu', 'id': 56}]
        }, {'name': 'Gwarancja', 'filterable': false, 'id': 25, 'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]}]
      }, {
        'id': 2,
        'name': 'Huawei P9 Lite mini Dual SIM czarny',
        'price': 499,
        'quantity': 78,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-p9-lite-mini-dual-sim-czarny-379550,2017/8/pr_2017_8_21_15_0_36_118_00.jpg',
          'main': true,
          'id': 7
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-p9-lite-mini-dual-sim-czarny-379550,2017/8/pr_2017_8_21_15_1_59_593_00.jpg',
          'main': false,
          'id': 8
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-p9-lite-mini-dual-sim-czarny-379550,2017/8/pr_2017_8_21_15_2_2_545_01.jpg',
          'main': false,
          'id': 9
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-p9-lite-mini-dual-sim-czarny-379550,2017/8/pr_2017_8_21_15_2_25_761_07.jpg',
          'main': false,
          'id': 10
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-p9-lite-mini-dual-sim-czarny-379550,2017/8/pr_2017_8_21_15_2_21_793_06.jpg',
          'main': false,
          'id': 11
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-p9-lite-mini-dual-sim-czarny-379550,2017/8/pr_2017_8_21_15_2_41_885_12.jpg',
          'main': false,
          'id': 12
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'janusz biznesu',
          'date': '2017-09-30T17:49:00.000Z',
          'content': 'Telefon zakupiony w rabatowej cenie. Jakość wykonania, aparaty, głośniki, czytnik palca, szybkość działania, fabryczne szkło hartowane z przodu i na obiektywie aparatu. Bardzo polecam osobom, które chcą fajny telefon w dobrej cenie.',
          'id': 3
        }, {
          'rating': 3,
          'authorName': 'Anonim',
          'date': '2018-02-07T20:24:00.000Z',
          'content': 'Fajny lecz straszna obudowa',
          'id': 4
        }, {
          'rating': 6,
          'authorName': 'Ulka',
          'date': '2018-02-27T17:11:00.000Z',
          'content': 'Jestem bardzo zadowolona z tego telefonu. Jest nowoczesny, aparat robi ładne zdjęcia. Spisuje się na medal. Polecam wybrać ten model.',
          'id': 5
        }, {
          'rating': 5,
          'authorName': 'Pablo',
          'date': '2018-03-24T22:18:00.000Z',
          'content': 'Realizacja zamówienia perfekcyjna jak zawsze. Co do telefonu... parametry techniczne oczywiste przed zakupem a w praktyce sprawdzają się bardzo dobrze, trudno dopatrywać się wad w takim telefonie jeśli patrzeć przez pryzmat ceny w tym przypadku poniżej 500 zł., płynne działanie, akceptowalne zdjęcia, dobry zasięg, dobrze leży w dłoni, bateria wystarcza na 2 dni nawet przy dosc intensywnym użytkowaniu ale tu dużo zależy od zasięgu sieci, sprawny czytnik palca, wyświetlacz wystarczający, polecam.',
          'id': 6
        }, {
          'rating': 6,
          'authorName': 'Monster Pro',
          'date': '2018-05-02T18:31:00.000Z',
          'content': 'Bardzo dobre polecam',
          'id': 7
        }, {
          'rating': 6,
          'authorName': 'Tooonek',
          'date': '2018-06-14T15:03:00.000Z',
          'content': 'Najlepszy parametrowo smartfon do 500 zł. Nie mam najmniejszych zastrzeżeń, powiem więcej nie spodziewałem się za dużej płynności w takiej kasie a tu szok, świetny telefon! Brawo Huawei',
          'id': 8
        }, {
          'rating': 6,
          'authorName': 'DENDO',
          'date': '2018-06-29T17:20:00.000Z',
          'content': 'Fajny smartfonik, do tego korzystnie wpada cenowo. Działa jak należy.Jest bardzo zgrabny a to sobie cenie.Kompaktowość zawsze na plus, a co najważniejsze nie jest to kosztem jakości bo ma wszystko to co droższe modele np. czytnik linii papilarnych który sprawuje sie niezawodnie.EKSTRA sprzęt!',
          'id': 9
        }, {'rating': 6, 'authorName': 'Eldo', 'date': '2018-07-16T10:53:00.000Z', 'content': 'Super telefon.', 'id': 10}, {
          'rating': 6,
          'authorName': 'Zeta',
          'date': '2018-08-23T11:22:00.000Z',
          'content': 'Telefon mimo, że średniak to działa lepiej niż nie jeden flagowiec. Mam sporo apek z których często korzystam jednocześnie. W kwestii cena/jakość trudno bedzie znaleźć coś lepszego. Huawei to jednak niezawodna marka.&nbsp;',
          'id': 11
        }, {
          'rating': 6,
          'authorName': 'Derbi',
          'date': '2018-09-01T20:13:00.000Z',
          'content': 'Polecam - Spoko tel do 5 stówek :) ma wszystko czego oczekuje się od smartfona. Działa zadowalająco szybko i sprawnie, nie zcina, ma fajne filtry i aparaty. Dla mnie super alternatywa do droższych modeli.&nbsp;',
          'id': 12
        }, {
          'rating': 6,
          'authorName': 'Martyna',
          'date': '2018-09-05T13:59:00.000Z',
          'content': 'Zastanawiałam się między xiaomi a huaweiem, wybór padł na ten model i nie żałuje. Chodzi sprawnie, jest bardzo ładnie wykonany, bateria starcza na 1,5 dnia, powgrywane aplikacje do zdjęć mają super filtryu i zdjęcia wychodzą pięknie!',
          'id': 13
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {'name': 'Micro USB - 1 szt.', 'id': 65}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik zbliżenia',
            'id': 42
          }, {'name': 'Czytnik linii papilarnych', 'id': 45}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 425 (4 rdzenie, 1.40 GHz, Cortex A53)', 'id': 58}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Adreno 308', 'id': 59}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '2 GB', 'id': 60}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '16 GB', 'id': 61}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5"', 'id': 63}]
        }, {'name': 'Rozdzielczość ekranu', 'filterable': true, 'id': 7, 'options': [{'name': '1280 x 720', 'id': 64}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3020 mAh', 'id': 66}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '13.0 Mpix - tył', 'id': 68}, {'name': '5.0 Mpix - przód', 'id': 69}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,1 mm', 'id': 72}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '71 mm', 'id': 73}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '143,5 mm', 'id': 74}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '145 g', 'id': 75}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}]
      }, {
        'id': 3,
        'name': 'Xiaomi Redmi 5 32GB Dual SIM LTE Black',
        'price': 678,
        'quantity': 38,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-32gb-dual-sim-lte-black-408123,2018/2/pr_2018_2_22_15_28_9_233_00.jpg',
          'main': true,
          'id': 13
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-32gb-dual-sim-lte-black-408123,2018/2/pr_2018_2_9_14_46_38_687_03.jpg',
          'main': false,
          'id': 14
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-32gb-dual-sim-lte-black-408123,2018/2/pr_2018_2_22_15_24_21_322_00.jpg',
          'main': false,
          'id': 15
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-32gb-dual-sim-lte-black-408123,2018/2/pr_2018_2_9_14_46_28_45_00.jpg',
          'main': false,
          'id': 16
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-32gb-dual-sim-lte-black-408123,2018/2/pr_2018_2_9_14_46_31_702_01.jpg',
          'main': false,
          'id': 17
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Vitas',
          'date': '2018-07-12T19:17:00.000Z',
          'content': '3 GM RAM pozwalają na multitasking(swobodne korzystanie i przełączanie się pomiędzy kilkoma aplikacjami). Szybko przyzwyczaiłem się do wielkości(jest większy od poprzedniego LG K6). Bateria jest solidna i starcza na około dwa dni korzystania. Telefon ma szybki czytnik linii papilarnych, ułożony w wygodnym miejscu (zaraz pod palcem wskazującym na tyle obudowy).',
          'id': 14
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}, {'name': 'Beidou', 'id': 84}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {'name': 'Micro USB - 1 szt.', 'id': 65}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Czytnik linii papilarnych', 'id': 45}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '5.0 Mpix - przód', 'id': 69}, {'name': '12.0 Mpix - tył', 'id': 87}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 450 (8 rdzeni, 1.80 GHz, A53)', 'id': 78}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Adreno 506', 'id': 79}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '3 GB', 'id': 80}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '32 GB', 'id': 81}]}, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5,7"', 'id': 82}]
        }, {'name': 'Rozdzielczość ekranu', 'filterable': true, 'id': 7, 'options': [{'name': '1440 x 720', 'id': 83}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3300 mAh', 'id': 85}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.1 Nougat', 'id': 86}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - tylny obiektyw', 'id': 88}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '7,7 mm', 'id': 89}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '72,8 mm', 'id': 90}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '151,8 mm', 'id': 91}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '158 g', 'id': 92}]
        }]
      }, {
        'id': 4,
        'name': 'Motorola Moto Z3 Play 4/64GB Dual SIM czarny',
        'price': 1998,
        'quantity': 1,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-z3-play-464gb-dual-sim-czarny-439250,2018/7/pr_2018_7_18_10_50_26_642_05.jpg',
          'main': true,
          'id': 18
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-z3-play-464gb-dual-sim-czarny-439250,2018/7/pr_2018_7_18_10_50_11_94_00.jpg',
          'main': false,
          'id': 19
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-z3-play-464gb-dual-sim-czarny-439250,2018/7/pr_2018_7_18_10_50_14_313_01.jpg',
          'main': false,
          'id': 20
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-z3-play-464gb-dual-sim-czarny-439250,2018/7/pr_2018_7_18_10_50_17_329_02.jpg',
          'main': false,
          'id': 21
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-z3-play-464gb-dual-sim-czarny-439250,2018/7/pr_2018_7_18_10_50_20_220_03.jpg',
          'main': false,
          'id': 22
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-z3-play-464gb-dual-sim-czarny-439250,2018/7/pr_2018_7_18_10_50_23_173_04.jpg',
          'main': false,
          'id': 23
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Shadow',
          'date': '2018-08-09T06:41:00.000Z',
          'content': 'Pierwsze wrażenia - super. Telefon dużo wygodniej leży w dłoni z założoną nakładką baterii(jest w zestawie). W zestawie są przejściówki, słuchawki. Dla mnie super sprzęt w dobrej cenie. PUBG i Mortal Combat chodzą jak należy. Moto Z3 Play robi też niezłe zdjęcia. Zobaczymy, jak będzie chodzić po kilku miesiącach. Na razie polecam.',
          'id': 15
        }, {
          'rating': 6,
          'authorName': 'ada',
          'date': '2018-08-14T12:19:00.000Z',
          'content': 'mi zależało na po pierwsze dobrych zdjęciach - jest, dobrej baterii - jest, i szybki ładowaniu - jest. moto mods to na pewno dodatkowy plus w tym telefonie. polecam',
          'id': 16
        }],
        'attributes': [{
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}, {'name': 'Beidou', 'id': 84}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)', 'id': 16}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 8.1 Oreo', 'id': 19}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '8.0 Mpix - przód', 'id': 21}, {'name': '12.0 + 5.0 Mpix - tył', 'id': 100}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Obudowa wykonana ze szkła i aluminium', 'id': 33}, {'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Żyroskop',
            'id': 37
          }, {'name': 'Magnetometr', 'id': 38}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik zbliżenia',
            'id': 42
          }, {'name': 'Czytnik linii papilarnych', 'id': 45}, {'name': 'Skaner twarzy', 'id': 46}, {
            'name': 'Szkło Corning Gorilla Glass 3',
            'id': 108
          }]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Kabel USB typ C', 'id': 51}, {'name': 'Słuchawki', 'id': 54}, {
            'name': 'Ładowarka TurboPower™ 18 W',
            'id': 109
          }, {'name': 'Przejściówka USB-C -&gt; Jack 3.5 mm', 'id': 110}, {'name': 'Moto Power Pack', 'id': 111}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 636 (4 rdzenie, 1,80 GHz, Kryo + 4 rdzenie, 1.60 GHz, Kryo)', 'id': 93}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Adreno 509', 'id': 94}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '64 GB', 'id': 96}]}, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '6"', 'id': 97}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2160 x 1080', 'id': 98}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3000 mAh', 'id': 99}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.0 - przedni obiektyw', 'id': 101}, {'name': 'f/1.7 - tylny obiektyw', 'id': 102}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K', 'id': 103}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '6,7 mm', 'id': 104}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '76,5 mm', 'id': 105}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '156,5 mm', 'id': 106}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '156 g', 'id': 107}]
        }]
      }, {
        'id': 5,
        'name': 'Xiaomi Redmi 5A 16GB Dual SIM LTE Grey',
        'price': 479,
        'quantity': 85,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5a-16gb-dual-sim-lte-grey-402292,2018/1/pr_2018_1_11_9_8_23_784_00.jpg',
          'main': true,
          'id': 24
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5a-16gb-dual-sim-lte-grey-402292,2018/1/pr_2018_1_22_8_39_22_162_00.jpg',
          'main': false,
          'id': 25
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5a-16gb-dual-sim-lte-grey-402292,2018/1/pr_2018_1_22_8_39_25_553_01.jpg',
          'main': false,
          'id': 26
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5a-16gb-dual-sim-lte-grey-402292,2018/1/pr_2018_1_22_8_39_28_741_02.jpg',
          'main': false,
          'id': 27
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5a-16gb-dual-sim-lte-grey-402292,2018/1/pr_2018_1_22_8_39_49_539_00.jpg',
          'main': false,
          'id': 28
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5a-16gb-dual-sim-lte-grey-402292,2018/1/pr_2018_1_22_8_39_32_22_03.jpg',
          'main': false,
          'id': 29
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Zdzisław',
          'date': '2018-05-23T07:54:00.000Z',
          'content': 'Do zakupu tego telefonu namówiła mnie żona. Nie mogłem się przekonać do smartfonów, ale musze powiedziec ze ten jest bardzo prosty w obsłudze. Myślę, że w kwocie do 500 zł to bardzo dobry sprzęt. Korzysta się z niego lekko, aparat robi ładne zdjęcia a wyświetlacz jest przyjemny i ma ładną kolorystykę. Spokojnie polecam, pozdrawiam',
          'id': 17
        }, {
          'rating': 6,
          'authorName': 'Ząbek',
          'date': '2018-06-09T17:23:00.000Z',
          'content': 'w tej cenie to jest naprawdę bardzo dobry smartfon. największy plus to bateria, która trzyma przy sporej liczbie telefonów - około 2-3 dni. Zdjęcia są ok. Aplikacje facebook, instagram działają dobrze. Polecam też korzystać z messangera w wersji Lite, aby telefon nie zamulał. Telefon trochę przypomina iPhone 6 :)',
          'id': 18
        }, {
          'rating': 6,
          'authorName': 'Agnes',
          'date': '2018-06-20T16:01:00.000Z',
          'content': 'Bardzo dobry Xiaomi w dobrej cenie. Nie ma co przepłacać za mega drogie modele bo ma wszystko co powinien jako smartfon. Działa zadziwająco szybko i ładnie wygląda wizualnie. Wyświetlacz jest dla mnie czyteny, nie mam problemu z obsługą. Bateria ładuje się w mgnieniu oka. Dla mnie super;)',
          'id': 19
        }, {
          'rating': 6,
          'authorName': 'Cinek',
          'date': '2018-07-03T20:54:00.000Z',
          'content': 'Bardzo dobry telefon do codziennego korzystania - odpisywanie na SMS, facebook, Endomondo, Instagram. Zdjęcia robi nie najgorsze. Bateria daje radę wytrzymać 1-2 dni. Jestem zadowolony.',
          'id': 20
        }, {
          'rating': 6,
          'authorName': 'Wojtek',
          'date': '2018-07-25T06:20:00.000Z',
          'content': 'Jestem bardzo zadowolony z zakupu',
          'id': 21
        }, {
          'rating': 1,
          'authorName': 'Michał',
          'date': '2018-08-02T07:35:00.000Z',
          'content': 'Bardzo szybko się rysuje i niszczy , wolny i niewygodny .',
          'id': 22
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2018-08-03T08:49:00.000Z',
          'content': 'Za te pieniądze nie można dużo oczekiwać, ale kupiłem go mojej mamie i jest bardzo zadowolona. Można dzwonić pisać sms i zrobić jakieś zdjęcia, nie jest do demon szybkości, ale działa poprawnie i polecam.',
          'id': 23
        }, {
          'rating': 6,
          'authorName': 'Hoqs',
          'date': '2018-08-10T05:04:00.000Z',
          'content': 'Odpowiednia cena do produktu. Telefon nie zamula to ludzie tylko go zamulają i zaśmiecają pamięć ram poprzez otwarte w tle aplikację.  Rysowanie? bez szkła i etui w dzisiejszych czasach nie oczekujcie wiele .  Zdjęcia ? ok jak robić profesjonalne to telefony nie służą do tego :0 ale wielu o tym zapomina. Polecam bo nie warto przepłacać za monopol wiodących marek.',
          'id': 24
        }, {
          'rating': 6,
          'authorName': 'VULKan',
          'date': '2018-08-11T17:23:00.000Z',
          'content': 'bardzo fajny działa świetnie trochę aplikacji mam ale mam też kartę micro sd . Tak czy siak telefon jest super',
          'id': 25
        }, {
          'rating': 6,
          'authorName': 'ziomal',
          'date': '2018-08-12T16:32:00.000Z',
          'content': 'Bardzo fajny polecam ;)',
          'id': 26
        }, {
          'rating': 6,
          'authorName': 'Praski',
          'date': '2018-09-01T13:19:00.000Z',
          'content': 'Kupiłem telefon z myślą że za tą kasę to szrot, ale nie miałem innej opcji telefon był potrzebny na już. Po pierwszym uruchomieniu zaskoczyłem się że tak budżetowe rozwiązanie może tak fajnie działać. Bateria 3k trzyma lepiej niż się spodziewałem aparat robi fajne foty a system działa nadzwyczaj płynnie. Za tą cenę polecam każdemu \n;D',
          'id': 27
        }, {
          'rating': 6,
          'authorName': 'Hubert',
          'date': '2018-09-01T15:53:00.000Z',
          'content': 'bardzo fajny telefon',
          'id': 28
        }, {
          'rating': 6,
          'authorName': 'KOMBI',
          'date': '2018-09-04T21:14:00.000Z',
          'content': 'Prosty w obsłudze i bardzo funkcjonalny smartfon. Cena w porównaniu do jakości i oferowanych funkcji po prostu miażdży konkurencję. Zewnetrznie też calkiem ok wykonany. Nie mam uwag i spokojnie w tej kasie polecam zakup :)',
          'id': 29
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}, {'name': 'Beidou', 'id': 84}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Magnetometr', 'id': 38}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'USB OTG', 'id': 49}, {
            'name': 'Czujnik podczerwieni',
            'id': 118
          }, {'name': 'Obsługa radia FM', 'id': 119}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {
            'name': 'Instrukcja szybkiego uruchomienia telefonu',
            'id': 56
          }, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 425 (4 rdzenie, 1.40 GHz, Cortex A53)', 'id': 58}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Adreno 308', 'id': 59}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '2 GB', 'id': 60}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '16 GB', 'id': 61}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5"', 'id': 63}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1280 x 720', 'id': 64}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '13.0 Mpix - tył', 'id': 68}, {'name': '5.0 Mpix - przód', 'id': 69}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Czarny', 'id': 76}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.1 Nougat', 'id': 86}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - tylny obiektyw', 'id': 88}, {'name': 'f/2.0 - przedni obiektyw', 'id': 101}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-polimerowa 3000 mAh', 'id': 113}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,4 mm', 'id': 114}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '70,1 mm', 'id': 115}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '140 mm', 'id': 116}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '137 g', 'id': 117}]
        }]
      }, {
        'id': 6,
        'name': 'Xiaomi Redmi 5 Plus 64GB Dual SIM LTE Black',
        'price': 847,
        'quantity': 44,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-plus-64gb-dual-sim-lte-black-408131,2018/2/pr_2018_2_9_14_46_42_218_04.jpg',
          'main': true,
          'id': 30
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-plus-64gb-dual-sim-lte-black-408131,2018/2/pr_2018_2_9_14_46_38_687_03.jpg',
          'main': false,
          'id': 31
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-plus-64gb-dual-sim-lte-black-408131,2018/2/pr_2018_2_9_14_46_35_437_02.jpg',
          'main': false,
          'id': 32
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-plus-64gb-dual-sim-lte-black-408131,2018/2/pr_2018_2_9_14_46_28_45_00.jpg',
          'main': false,
          'id': 33
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-redmi-5-plus-64gb-dual-sim-lte-black-408131,2018/2/pr_2018_2_9_14_46_31_702_01.jpg',
          'main': false,
          'id': 34
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'dreamtheater',
          'date': '2018-03-21T19:11:00.000Z',
          'content': 'Telefon bardzo ok, procesor wydajny na tyle ze przecietny uzytkownik bedzie zadowolony w codziennym uzytkowaniu. Bateria super, przy bardzo dobrej optymalizacji i dzieki energooszczednemu procesorowi trzyma 2-3 dni. Wyswietlacz przy tej rozdzielczosci wiecej niz dobry. Dosc duzy ale jak ktos sie na niego decyduje to musi miec tego swiadomosc. Aparat jak to w xiaomi: duzo swiatla-fotki ok, malo- srednio slabo.  Wykonanie za to jest swietne, wyglada na 2 razy drozszy. Ogolnie polecam bardzo',
          'id': 30
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2018-04-14T17:13:00.000Z',
          'content': 'Kupiłem go 1 miesiąc temu. To jest cudo jak na taką cenę. Zdjęcia robi nawet ładne (w porównaniu do Samsunga s9).procesor jest dobry. Płynny ładny design ( oprócz białego paska na dole, zastąpiliby ekranem do dołu lub przyciskami). bateria trzyma tak na włączonym ekranie około 7-8godz, więc przeciętna w porównaniu do LG X Power. Ogólnie na taką cenę to polecam.',
          'id': 31
        }, {
          'rating': 6,
          'authorName': 'Czarny',
          'date': '2018-05-13T08:59:00.000Z',
          'content': 'Telefon posiadam od tygodnia i jestem mega zadowolony. Przed kupnem sprawdzałem wiele telefonów (do 1000zł) i wybór padł na 2 modele : Mi A1 i Redmi 5 Plus. Ostatecznie wybrałem Redmi 5 Plus\'a ze względu na MIUI. Co do zalet telefonu to posiada sporą baterię 4000 mAh, co przekłada się na 2 dni częstego użytkowania. Zaletą z pewnością jest też wyświetlacz FHD+, który naprawdę daje radę, procek też jest bardzo wydajny. Ogólne wrażenia są bardzo pozytywne, wszystko działa płynnie :) Polecam.',
          'id': 32
        }, {
          'rating': 6,
          'authorName': 'tomek01',
          'date': '2018-05-19T06:47:00.000Z',
          'content': 'telefon super, działa jak należy w atutu pond 75tk punktów mówi samo za siebie. Najsłabsze ogniwo w tym fonie to aparat ale mimo wszystko polecam. Do szczęścia brakuje NFC.',
          'id': 33
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2018-05-19T16:56:00.000Z',
          'content': 'Wcześniej posiadałem Redmi Note 4 wersja snapdragon 3/32. Redmi 5 plus przekonał mnie wielkością wyświetlacza 5.99 oraz  standardem HD+. Bateria jak wiadomo po 3 ładowaniu jeszcze się układa. 5-6 h SOT to jednak nie problem i dwa dni w cyklu mieszanym. Piękną robotę robi obsługa gestami. Znikają klawisze ekranowe i wyświetlacz staje się jeszcze większy. Gesty są intuicyjne. Telefon bardzo ładny. Jestem zadowolony. Polecam.',
          'id': 34
        }, {'rating': 6, 'authorName': 'Michał', 'date': '2018-05-28T07:10:00.000Z', 'content': 'Wszystko super', 'id': 35}, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2018-06-22T20:59:00.000Z',
          'content': 'Tydzień, śmiga że tylko, jak za taką cenę to lepszego nie znajdzie się, Nokia i inne droższe się chowają.',
          'id': 36
        }, {
          'rating': 6,
          'authorName': 'Sławek',
          'date': '2018-07-03T22:38:00.000Z',
          'content': 'Klasa w dobrej cenie.Telefonik super plus.',
          'id': 37
        }, {
          'rating': 6,
          'authorName': 'Mark',
          'date': '2018-07-06T13:11:00.000Z',
          'content': 'Wszysko OK. Przy dobrych ustawieniach i średnim używaniu telefonu bateria trzyma 2 dni. Telefon działa płynnie, nie ma ścinek przy kilku apkach. Zdjęcia, jak na telefon do 900 złotych są świetne - ładne, jasne.',
          'id': 38
        }, {
          'rating': 5,
          'authorName': 'Gander',
          'date': '2018-07-17T09:17:00.000Z',
          'content': 'Telefon bardzo fajny, szybki, wygodny, cienki, ale:\n1) MIUI to nie moja bajka, dla mnie trochę zaciemnia interfejs, zdecydowanie bardziej wolę czysty Android (Android One).\n2) Nie mogłem znaleźć oficjalnej informacji kiedy będzie aktualizacja do 8.x - ponoć gdzieś w IV kwartale tego roku, bądź ręcznie, bety.\n3) Strasznie narzuca utworzenie konta Mi Cloud, nie szanuje mojego "siedź cicho, nie chcę tego". Irytujące sugestje że będzie fajnie. Wystarczy mi już jeden gigant, który zbiera o mnie dane.',
          'id': 39
        }, {
          'rating': 1,
          'authorName': 'Piotr',
          'date': '2018-08-02T18:54:00.000Z',
          'content': 'Towar zakupiony 02 sierpnia.Zainstalowane pelno zbednych programow nie do usuniecia,klawiatura sie wiesza-zwrot',
          'id': 40
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-08-03T07:30:00.000Z',
          'content': 'Piotrze, przykro nam w powodu powstałych niedogodności.Opisane problemy nie powinny występować w nowym smartfonie. Czy próbowałeś zainstalować najnowsze aktualizacje dla telefonu i aplikacji? W razie trudności do dyspozycji pozostaje nasza infolinia serwisowa pod numerem 34 377 00 30 lub mailowo pod adresem: serwis@x-kom.pl.',
          'id': 41
        }, {
          'rating': 6,
          'authorName': 'lukasz4493',
          'date': '2018-08-17T06:59:00.000Z',
          'content': 'Telefon kupiony miesiąc temu, wszystko działa bardzo sprawnie - bateria trzyma 1,5 - 2 dni przy dość częstym używaniu. Żadnych zbędnych aplikacji jak w komentarzu wyżej nie uświadczyłem, co więcej jest sporo bardzo użytecznych. Polecam',
          'id': 42
        }, {
          'rating': 4,
          'authorName': 'Łukasz',
          'date': '2018-08-30T20:22:00.000Z',
          'content': 'Jak za te pieniądze to telefon OK, jednak jest kilka ale. Bateria w porządku, przy moim intensywnym użytkowaniu wytrzymuje prawie cały dzień (widoczny spadek czasu pracy po trzech miesiącach), aparat raczej rozczarowywuje, moduł GPS lepiej pominąć, szybszy miałem w Nokii E61 a to było wieeeki temu. Do dzwonienia polecam ale jak się przesiadasz z jakiegoś flagowca to trzeba się uzbroić w wielkie pokłady cierpliwości.',
          'id': 43
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {'name': 'Micro USB - 1 szt.', 'id': 65}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 4000 mAh', 'id': 18}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Czytnik linii papilarnych', 'id': 45}, {
            'name': 'Czujnik podczerwieni',
            'id': 118
          }]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '5.0 Mpix - przód', 'id': 69}, {'name': '12.0 Mpix - tył', 'id': 87}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Adreno 506', 'id': 79}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.1 Nougat', 'id': 86}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - tylny obiektyw', 'id': 88}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '4 GB', 'id': 95}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '64 GB', 'id': 96}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2160 x 1080', 'id': 98}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 625 (8 rdzeni, 2.0 GHz, Cortex A53)', 'id': 120}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,99"', 'id': 121}]}, {
          'name': 'Grubość',
          'filterable': false,
          'id': 18,
          'options': [{'name': '8,05 mm', 'id': 122}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '74,45 mm', 'id': 123}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '158,5 mm', 'id': 124}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '180 g', 'id': 125}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja sprzedawcy)', 'id': 126}]
        }]
      }, {
        'id': 7,
        'name': 'Xiaomi Mi A2 Lite 4/64GB Black',
        'price': 1049,
        'quantity': 49,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-mi-a2-lite-464gb-black-437482,2018/7/pr_2018_7_25_12_42_17_210_04.jpg',
          'main': true,
          'id': 35
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-mi-a2-lite-464gb-black-437482,2018/7/pr_2018_7_25_12_42_5_70_01.jpg',
          'main': false,
          'id': 36
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-mi-a2-lite-464gb-black-437482,2018/7/pr_2018_7_25_12_42_9_117_02.jpg',
          'main': false,
          'id': 37
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-mi-a2-lite-464gb-black-437482,2018/7/pr_2018_7_25_12_42_13_288_03.jpg',
          'main': false,
          'id': 38
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,xiaomi-mi-a2-lite-464gb-black-437482,2018/7/pr_2018_7_25_12_42_0_898_00.jpg',
          'main': false,
          'id': 39
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2018-08-13T06:18:00.000Z',
          'content': 'bardzo dobry telefon',
          'id': 44
        }, {
          'rating': 6,
          'authorName': 'Rybuś',
          'date': '2018-08-16T08:36:00.000Z',
          'content': 'Wszystko zgodnie z oczekiwaniami - doskonała wydajność w niskiej cenie. Ciężko o lepszy telefon za te pieniądze. Po paru dniach przyzwyczaiłem się nawet do korzystania w nocha. Xiaomi Mi A2 robi dobre zdjęcia, migawka jest szybka, ogólnie nie zacina się, nie muli.',
          'id': 45
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2018-08-19T19:12:00.000Z',
          'content': 'Telefon posiadam od kilku dni sprawuję się dobrze w codziennym używaniu. Zaskoczyła i jednocześnie ucieszyła mnie opcja szybkiego ładowania o której wcześniej nie widziałem. Sama bateria to spokojne 2 dni użytkowania. A co do samego Androida One aktualizacja pojawiła się drugiego dnia użytkowania. Aparat z funkcja rozmycia tła działa przetestowałem i uważam, że dobrze sobie radzi. Polecam serdecznie :)',
          'id': 46
        }, {
          'rating': 6,
          'authorName': 'GGVlad',
          'date': '2018-08-22T17:24:00.000Z',
          'content': 'Jestem szczęśliwym posiadaczem tego telefonu od 24h, próbowałem go zaciąć. Bezskutecznie, jedyny minus to aparat, ale jak dla przeciętnego użytkownika jest w sam raz',
          'id': 47
        }, {
          'rating': 6,
          'authorName': 'Walich',
          'date': '2018-08-30T19:30:00.000Z',
          'content': 'Telefon bardzo dobry,\nDziała szybko. Bateria wytrzymuje 2 dni normalnego użytkowania (internet, sms, GPS),\nZdjęcia w dzień piękne, gorzej z nocymi.\nTrzeba się przyzwyczaić do "czystego androida"',
          'id': 48
        }, {
          'rating': 6,
          'authorName': 'gola2008',
          'date': '2018-08-31T05:55:00.000Z',
          'content': 'Mam od wczoraj . Przesiadłem się uwaga z Iphone 6s ! I jestem mega zadowolony chociaż bałem się jak to będzie po przesiadce z Ios. \nCzysty android śmiga bardzo dobrze . Zdjęcie w dzień robi lepsze niż mój poprzednik . Tryb portretowy super. \n\nZa te kasę nie ma co się zastanawiać tylko brać .\nCzytnik TouchId bardzo szybki.\n\nMiałem dylemat pomiędzy redmi note 5 ale ten lepiej leży w dłoni przynajmniej dla mnie :)',
          'id': 49
        }, {
          'rating': 6,
          'authorName': 'Grizli',
          'date': '2018-08-31T22:19:00.000Z',
          'content': 'Telefon zdecydowanie wyrożnia się dobrą baterią i wydajnym procesorem. W tej cenie ma bardzo dobre parametry i bajery. Wizualnie prezentuje się bardzo okazale. Elegancki sprzęt! Brawo Xiaomi!',
          'id': 50
        }, {
          'rating': 6,
          'authorName': 'Klaudiusz',
          'date': '2018-09-06T12:51:00.000Z',
          'content': 'Xiaomi są po prostu pancerne, głównie za to je cenie, nie zliczę ile razy mi upadł, mam trochę drewniane ręce, i nie ma na nim żadnego pęknięcia. Wygląda nieźle, pracuje płynnie, ciężko się do czegoś przyczepić.',
          'id': 51
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}, {'name': 'Beidou', 'id': 84}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 8.1 Oreo', 'id': 19}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Magnetometr',
            'id': 38
          }, {'name': 'Czujnik światła', 'id': 41}, {'name': 'Czujnik zbliżenia', 'id': 42}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }, {'name': 'USB OTG', 'id': 49}, {'name': 'Czujnik podczerwieni', 'id': 118}, {'name': 'Metalowa obudowa', 'id': 134}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {
            'name': 'Instrukcja szybkiego uruchomienia telefonu',
            'id': 56
          }, {'name': 'Kabel microUSB', 'id': 77}, {'name': 'Silikonowe plecki', 'id': 135}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '5.0 Mpix - przód', 'id': 69}, {'name': '12.0 + 5.0 Mpix - tył', 'id': 100}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Adreno 506', 'id': 79}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - tylny obiektyw', 'id': 88}, {'name': 'f/2.0 - przedni obiektyw', 'id': 101}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '4 GB', 'id': 95}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '64 GB', 'id': 96}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 625 (8 rdzeni, 2.0 GHz, Cortex A53)', 'id': 120}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5,84"', 'id': 127}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2280 x 1080', 'id': 128}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-polimerowa 4000 mAh', 'id': 129}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,75 mm', 'id': 130}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '71,68 mm', 'id': 131}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '149,33 mm', 'id': 132}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '175 g', 'id': 133}]
        }]
      }, {
        'id': 8,
        'name': 'Motorola Moto G5 FHD 3/16GB Dual SIM szary',
        'price': 514,
        'quantity': 58,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5-fhd-316gb-dual-sim-szary-356681,2017/7/pr_2017_7_10_14_51_30_195.jpg',
          'main': true,
          'id': 40
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5-fhd-316gb-dual-sim-szary-356681,pr_2017_3_23_9_32_22_637.jpg',
          'main': false,
          'id': 41
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5-fhd-316gb-dual-sim-szary-356681,pr_2017_3_23_9_30_44_855.jpg',
          'main': false,
          'id': 42
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5-fhd-316gb-dual-sim-szary-356681,pr_2017_3_23_9_30_46_855.jpg',
          'main': false,
          'id': 43
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5-fhd-316gb-dual-sim-szary-356681,pr_2017_3_23_9_31_32_793.jpg',
          'main': false,
          'id': 44
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5-fhd-316gb-dual-sim-szary-356681,pr_2017_3_23_9_31_34_965.jpg',
          'main': false,
          'id': 45
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'ROB',
          'date': '2017-07-05T17:13:00.000Z',
          'content': 'Są różni wielbiciele ja akurat jestem fanem Motoroli, od lat z nich korzystam, teraz właśnie przyszedł czas na kolejny nowy model. Jeśli chodzi o Moto G5 to pierwsze co ma 3 GB pamięci RAM, więc o zacinkach nie mogę tu wspomnieć bo ich po prostu brak, bynajmniej do tej pory. Wyświetlacz IPS o świetnej jakości, wideo fhd, no i masa możliwości Androida 7.0. POLECAM',
          'id': 52
        }, {
          'rating': 6,
          'authorName': 'Luk',
          'date': '2017-08-17T19:45:00.000Z',
          'content': 'Super telefon w bardzo dobrej cenie. Czytnik linii papilarnych działa super bez zarzutu. Wymienna bateria i podwójna karta sim plus karta pamięci rewelacja. Mały wybór dedykowanych akcesoriów. Trzeba uważać przy zakupie aby nie kupić do G5 Plus. Czysty android to fajna sprawa. Jedyny minus jak dla mnie to trochę mało przydatnych widżetów jakie miałem w poprzednim HTC One i trochę dziwne rozwiązania i nazwy w menu wysuwanym z góry albo to może to kwestia przyzwyczajenia do HTC.Ogólnie super zakup',
          'id': 53
        }, {
          'rating': 6,
          'authorName': 'Rafał',
          'date': '2017-08-31T11:28:00.000Z',
          'content': 'Telefon w dobrych pieniądzach. Oprawa solidna, zaokrąglony tył świetnie dopasowuje się do dłoni, także telefon w 100% poręczny i co też dobre, że obudowa nie jest śliska. Co do wnętrza żadnych uwag, telefon bardzo płynnie i szybko działa, obsługa LTE 800MHz więc z internetem tez dobrze sobie radzi. Chyba jako jeden z nielicznych smartfonów ma tak wytrzymałą baterię, z reguły 3-4 dni i to przy dość aktywnym korzystaniu + szybkie ładowanie.',
          'id': 54
        }, {
          'rating': 5,
          'authorName': 'Piotr',
          'date': '2017-09-19T19:30:00.000Z',
          'content': 'Bardzo duży plus dla sklepu X-KOM i obsługi. Telefon dostałem na drugi dzień po zamówieniu.\n\nCo do samego telefonu to wszystko super i ekstra, ale ma jeden mankament, który po przesiadce z Moto G3 trochę mnie podłamał. W Moto G5 nie ma dodatkowego głośnika do odtwarzania muzyki jak w G3, jego zadanie przejął głośniczek od słuchawki. Jak łatwo się domyślić spadła sporo moc głośnika i jakość odtwarzanego dźwięku. Idzie z tym żyć, ale jakiś taki żal pozostał ;) Ale telefon wart swojej ceny.',
          'id': 55
        }, {
          'rating': 6,
          'authorName': 'krzysiek',
          'date': '2017-09-30T09:12:00.000Z',
          'content': 'Super telefon w przystępnej cenie! Piękny design! Mocna bateria, płynne działanie i bardzo dobry aparat fotorgraficzny! Polecam zakup!',
          'id': 56
        }, {
          'rating': 5,
          'authorName': 'Les',
          'date': '2017-10-18T07:45:00.000Z',
          'content': 'Bardzo dobry telefon, działa płynnie, dobra jakość rozmów. Ale procesor 8-mio rdzeniowy i wyświetlacz FHD potrzebują więcej prądu. Gdyby miał akumulator 4 Ah byłoby idealnie.',
          'id': 57
        }, {
          'rating': 6,
          'authorName': 'antek',
          'date': '2017-10-29T14:53:00.000Z',
          'content': 'bardzo podoba mi się pod względem zarówno wizualnym jak i użytkowym, prosty w obsłudze, wygodna jest szybka funkcja ładowania dzięki której kilka minut ładowania dostarcza nam kilku godzin użytkowania. Bardzo potrzebna dla  mnie jest funkcja dual sim więc jestem bardzo zadowolony i polecam zainteresowanym',
          'id': 58
        }, {
          'rating': 6,
          'authorName': 'igor',
          'date': '2017-10-30T21:39:00.000Z',
          'content': 'super, kolejny fajny model od motoroli, w tej półce cenowej wypada rewelacyjnie względem konkurencji',
          'id': 59
        }, {
          'rating': 6,
          'authorName': 'Rosa',
          'date': '2017-11-30T07:21:00.000Z',
          'content': 'Za takie pieniądze super model.  Działa bez zarzutu, jest szybki, net ani filmiki się nie tną, nie zawiesza się. Do tego robi niezłe zdjęcia, Jedyne co to obudowa mogłaby być trochę ładniejsza,  bo nie wygląda jakoś rewelacyjnie.',
          'id': 60
        }, {
          'rating': 6,
          'authorName': 'Krzysiek',
          'date': '2018-01-04T11:57:00.000Z',
          'content': 'Trzymając w ręku telefon sprawia wrażenie dużo większego. Dość cichy głośnik a tak to po za tym wszystko ok.',
          'id': 61
        }, {
          'rating': 5,
          'authorName': 'Bums',
          'date': '2018-04-17T14:20:00.000Z',
          'content': 'Miałem Moto G3 i było lepsze. Zmienili głośnik na jakieś tanie coś, które służy do rozmów i jednocześnie do dźwięku dzwonka, mediów. Nie słyszę czasem jak ktoś dzwoni, nie wspominając o mediach. Obudowa po kilku miesiącach używania traci, jest słaba. To aluminium na klapce z tyłu, jest tylko na jej części. Wolałem gumowany plastik z G3. Ogólnie działa ok, nie ścina, aparat daje radę. Liczę, że wrócą do tradycyjnego głośnika, bo to najgorszy mankament. Dałbym gwiazdkę mniej, ale cena jest OK.',
          'id': 62
        }, {
          'rating': 5,
          'authorName': 'Trejder',
          'date': '2018-05-25T06:49:00.000Z',
          'content': 'Telefon wymiata (3 GB RAM, szybki procesor) i naprawdę niczego mu nie brakuje. Ale ma jedną wielką wadę -- okazał się być mało popularny na rynku, ma kiepskie wsparcie producenta i został na Androidzie 7.0. Tańsze, starsze modele (np. G4, E4), które okazały się bardziej popularne na rynku, mają lepsze wsparcie i już dawno jadą na 7.1.1. Bardzo słabo. Jeśli więc nie planujesz roota lub ręcznej aktualizacji systemu to zastanów się przed zakupem trzy razy, czy pozostanie z Androidem 7.0 jest OK.',
          'id': 63
        }, {
          'rating': 6,
          'authorName': 'Przemek',
          'date': '2018-06-02T12:03:00.000Z',
          'content': 'Super produkt, córka bardzo zadowolona, zwłaszcza z bajerów z ruchami komórką, , nic się nie zacina, bardzo wyraźny ekran, niezłe zdjęcia. Stosunek jakość/cena na 10 gwiazdek.',
          'id': 64
        }, {
          'rating': 6,
          'authorName': 'Bart',
          'date': '2018-08-20T08:26:00.000Z',
          'content': 'Za te pieniądze jestem bardzo zadowolony.\nParametry lepiej niż przyzwoite, telefon działa płynnie bez zacięć.  Oczywiście wygląd szału nie robi obudowa jest mocno plastikowa i bez wodotrysków ale to w końcu 500 zł 3gb ramu 8 rdzeniowy procek, który do normalnego użytku spisuje się świetnie. Należy pamiętać żeby zamontować szybką kartę sd bo telefon jest wyczulony na stare i wolne.',
          'id': 65
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }, {'name': 'Funkcja szybkiego ładowania Quick Charge', 'id': 48}, {'name': 'USB OTG', 'id': 49}, {
            'name': 'Obsługa radia FM',
            'id': 119
          }, {'name': 'Metalowa obudowa', 'id': 134}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {
            'name': 'Instrukcja szybkiego uruchomienia telefonu',
            'id': 56
          }, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '16 GB', 'id': 61}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5"', 'id': 63}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '13.0 Mpix - tył', 'id': 68}, {'name': '5.0 Mpix - przód', 'id': 69}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '3 GB', 'id': 80}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 430 (4 rdzenie, 1.40 GHz, A53 + 4 rdzenie, 1.1 GHz, A53)', 'id': 136}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Adreno 505', 'id': 137}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080', 'id': 138}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 2800 mAh', 'id': 139}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - przedni obiektyw', 'id': 140}, {'name': 'f/2.0 - tylny obiektyw', 'id': 141}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '9,5 mm', 'id': 142}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '73 mm', 'id': 143}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '144,4 mm', 'id': 144}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '153 g', 'id': 145}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Szary', 'id': 146}]}]
      }, {
        'id': 9,
        'name': 'LG G6 platynowy',
        'price': 1498,
        'quantity': 45,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-g6-platynowy-357954,2017/7/pr_2017_7_12_11_1_25_324.jpg',
          'main': true,
          'id': 46
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-g6-platynowy-357954,pr_2017_3_28_9_53_56_163.jpg',
          'main': false,
          'id': 47
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-g6-platynowy-357954,pr_2017_3_28_9_53_17_972.jpg',
          'main': false,
          'id': 48
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-g6-platynowy-357954,pr_2017_3_28_9_54_5_758.jpg',
          'main': false,
          'id': 49
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-g6-platynowy-357954,pr_2017_3_28_9_54_36_417.jpg',
          'main': false,
          'id': 50
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-g6-platynowy-357954,pr_2017_3_28_9_55_7_920.jpg',
          'main': false,
          'id': 51
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'czarny',
          'date': '2017-05-05T08:18:00.000Z',
          'content': 'Posiadam telefon od prawie 2 tygodni i stwierdzam, że jest REWELACYJNY!\nŚwietne materiały wykonania - szkło i metal, powodują uczucie trzymania czegoś naprawdę solidnego. Ekran, podwójny aparat z przodu i z tyłu, wodoodporność (przetestowana przeze mnie), sprawiają że jak dla mnie jest to do tej pory numer jeden.\nPracuje niesamowicie płynnie, robi świetne zdjęcia (na 7 gwiazdek zasługuje szerokokątny aparat). Przy intensywnym użytkowaniu wytrzymuje 1,5 - 2 dni. POLECAM!',
          'id': 66
        }, {
          'rating': 6,
          'authorName': 'Luki',
          'date': '2017-07-11T21:03:00.000Z',
          'content': 'Fani lansu i marketingu z pewnością go nie docenia. Ale do mnie przemówił certyfikat IP68, norma militarna, pewny chwyt w dłoni, solidny wyglad i fenomenalny aparat szerokokątny.',
          'id': 67
        }, {
          'rating': 6,
          'authorName': 'Marcus',
          'date': '2017-08-27T20:23:00.000Z',
          'content': 'Rewelacja. Bardzo ergonomiczny. Świetny wyświetlacz. Odporny bardziej od innych smartfonów. Dla mnie wizualnie bardzo ładny, prezentuje się lepiej niż na zdjęciach. Solidny i ważący jak telefon a nie zabawka. Szeroki kąt na wakacjach niezastąpiony. Bateria trzyma do 2 dni. Bo.dobra jakość dźwięku z Jacka i głośniczka. Jak dla mnie super zaletą jest radio FM.',
          'id': 68
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2017-09-01T05:35:00.000Z',
          'content': 'Telefon oceniam bardzo dobrze. Jest nieduży, dobrze leży w dłoni. Nie traci zasięgu a rozmowy odbywają się w bardzo dobrej jakości.',
          'id': 69
        }, {
          'rating': 6,
          'authorName': 'Slawek',
          'date': '2017-09-10T03:43:00.000Z',
          'content': 'Bardzo dobry telefon. Napewno wart swojej ceny w promocji za 2199zl. Nie lada gradka kto ceni sobie telefon z najwyzszej polki nie dokonca z tej lansiarskiej. Jednym slowem Samsung  S7 w nowoczesnym wydaniu jak S8/S8+. Polecam X-kom i telefonik tez :)',
          'id': 70
        }, {
          'rating': 6,
          'authorName': 'Grzeogrzo',
          'date': '2017-09-11T05:50:00.000Z',
          'content': 'Suprr. Nie tnie. Nie ma problemow z bateria.',
          'id': 71
        }, {
          'rating': 6,
          'authorName': 'GT',
          'date': '2017-09-11T06:51:00.000Z',
          'content': 'Na początku, kiedy telefon wchodził na rynek, nie byłem do niego przekonamy i wątpiłem, czy zastąpi moją wysłużoną G3. Ale cena mnie zachęciła. Poza tym nakładka LG, która dla mnie od lat jest najlepsza i nie mogę się z nią rozstać. Telefon rzeczywiście jest solidny. Nie mam do niego żadnych zastrzeżeń. To pop prostu dobry, solidnej budowy sprzęt. Brakuje tylko bezprzewodowego ładowania.',
          'id': 72
        }, {
          'rating': 6,
          'authorName': 'Nina',
          'date': '2017-09-11T11:05:00.000Z',
          'content': 'Mając LG G5 nie byłam pewna czy zmiana na G6 faktycznie stanie się odczuwalna jednak jak wzykle się nie zawiodłam. Na początku brakowało mi modularności jednak aparat w niczym nie ustępował temu z G5. Dzięki trybowi oszczędnemu podczas gry, mogę cały dzień cieszyć się Mortal Kombat X i nie obawiać się o utratę połowy baterii w ciągu dnia. \nJako topowy model nie ma sobie równych, klasa i moc sama w sobie.\nGorąco polecam wszystkim wymagającym użytkownikom.',
          'id': 73
        }, {
          'rating': 5,
          'authorName': 'Jacek',
          'date': '2017-09-13T07:09:00.000Z',
          'content': 'Telefon jest świetny. Przede wszystkim jest solidnie zbudowany. Norma militarna oraz IP68 dają dużą swobodę użytkowania w różnych warunkach. Dużym atutem jest wygląd - urządzenie jest po prostu ładne bez zbędnych bajerów. Do tego slot na karty pamięci, gniazdo słuchawek, przyzwoity aparat i ekran.',
          'id': 74
        }, {
          'rating': 6,
          'authorName': 'Bartosz',
          'date': '2017-09-13T17:10:00.000Z',
          'content': 'Idealny stosunek ceny do jakości. Telefon działa bardzo dobrze, bateria długo wytrzymuje a szerokokątny aparat robi mega wrażenie. Co tu chcieć więcej?:)',
          'id': 75
        }, {
          'rating': 6,
          'authorName': 'Erver',
          'date': '2017-09-17T16:19:00.000Z',
          'content': 'LG G6. Telefon znakomity pod każdym względem. Prędkość działania nie odstaje od tegorocznych flagowców. Nakładka producenta jest przejrzysta i daje możliwość instalowania motywów spoza sklepu producenta. Jakość aparatu określam na rewelacyjną. Szeroki obiektyw daje duże możliwości, najlepiej sprawdzi się na wyjazdach. Multimedia spisują się na tym modelu super. Plus za wodoodporność i funkcje rozpoznawania twarzy i czytnik linii papilarnych. Polecam wszystkim!',
          'id': 76
        }, {
          'rating': 6,
          'authorName': 'f0rman',
          'date': '2017-09-25T08:24:00.000Z',
          'content': 'Świetny telefon, świetne wykonanie, świetny design. \n\nJednak można znaleźć parę wad:\n- średni głośnik,\n- brak dedykowanych akcesoriów od LG.\n\nMimo to oceniam na 6.',
          'id': 77
        }, {
          'rating': 6,
          'authorName': 'Robert',
          'date': '2017-10-03T07:03:00.000Z',
          'content': 'Świetny telefon. Bardzo dobrze i solidnie wykonany w stosunku do ceny. Bardzo dobry wyświetlacz i bateria. Nie ma sensu przeplacać za flagowce innych producentów. G6 jwst zdecydowanie godny polecenia.',
          'id': 78
        }, {
          'rating': 6,
          'authorName': 'kupiony w Redudzie',
          'date': '2017-10-12T07:46:00.000Z',
          'content': 'telefon jest super tylko ze trafił mi sie egzemplarz kupiony u was z sieci p2p i z otwartym pudełkiem:(',
          'id': 79
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2017-10-12T12:15:00.000Z',
          'content': 'Bardzo nam przykro, jeśli napotkałeś jakiekolwiek problemy związane z Twoją przesyłką. Prosimy o kontakt z nami: x-kom@x-kom.pl lub pod numerem 34 377 00 00. Chcielibyśmy bliżej przyjrzeć się Twojej sytuacji.',
          'id': 80
        }, {
          'rating': 5,
          'authorName': 'Beata',
          'date': '2017-10-16T06:21:00.000Z',
          'content': 'świetny telefon ale średni dźwięk',
          'id': 81
        }, {
          'rating': 6,
          'authorName': 'Cheyenne',
          'date': '2017-11-04T09:05:00.000Z',
          'content': 'Polecam telefon pod każdym względem. Bateria przy normalnym użytkowaniu wytrzymuje 2-3dni. Rewelacyjny ekran, dźwięk jak najbardziej ok.  Wykonanie na wysokim poziomie.',
          'id': 82
        }, {
          'rating': 6,
          'authorName': 'Adam',
          'date': '2017-11-19T22:03:00.000Z',
          'content': 'Mam ten smartfon od ponad 2 miesięcy. Jest po prostu EXTRA',
          'id': 83
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2017-11-22T08:44:00.000Z',
          'content': 'Jak dla mnie świetny. Bateria jak zwykle mogłaby być lepsza ale nie jest źle. Na wyłączonym lte cały dzień w pracy pociągnie. Bateria w widoczny sposób się wyczerpuje jedynie podczas oglądania youtube na lte. W pozostałych przypadkach naprawdę nieźle trzyma. Chyba mogę polecić.',
          'id': 84
        }, {
          'rating': 6,
          'authorName': 'Edi',
          'date': '2017-12-04T09:45:00.000Z',
          'content': 'Urządzenie jest po prostu kapitalne. LG postarało się z modelem G6 i zrobili naprawdę solidnego flagowca, którego się nie powstydzą. Szczerze polecam LG G6, oraz zakupy w x-kom. W momencie mojego zakupu cena była najniższa spośród konkurencji A zamówiony telefon w dwa dni dotarł do salonu i czekał na odbiór.',
          'id': 85
        }, {
          'rating': 6,
          'authorName': 'Frycek',
          'date': '2017-12-09T19:18:00.000Z',
          'content': 'Smartfon w moich rękach od miesiąca. Poprzedni huawei p8.  Wszystko co chciałem mieć w smartfonie to mam.  No może brakuje mi diody powiadomień. Fotki super, filmiki jeszcze lepsze. Odtwarzacz obsługuje kodek flack. Gra wyśmienicie.',
          'id': 86
        }, {
          'rating': 6,
          'authorName': 'bartek',
          'date': '2017-12-29T20:57:00.000Z',
          'content': 'zakupiony 31.10. poprzedni to G4. \npo 3 dniach dopiero się przyzwyczaiłem. Teraz już bym nie oddał i nie zamienił :)',
          'id': 87
        }, {
          'rating': 6,
          'authorName': 'Jarek',
          'date': '2018-01-11T09:31:00.000Z',
          'content': 'Świetny telefon! Po przesiadce z Samsunga Galaxy S5 G900F brakuje mi pulsometru i diody IR, ale poza tym nie mam sie do czego przyczepić. Wykonanie świetne, a ekran po prostu rewelacyjny. Aparat robi doskonałe zdjecia i kręci jeszcze lepsze filmy. Polecam!',
          'id': 88
        }, {
          'rating': 4,
          'authorName': 'ja42',
          'date': '2018-01-12T08:53:00.000Z',
          'content': 'Telefon całkiem ok. Nie słuchajcie opinii, że robi dobre zdjęcia bo ich jakość jest bardzo słaba.',
          'id': 89
        }, {
          'rating': 6,
          'authorName': 'Dominik',
          'date': '2018-01-16T21:36:00.000Z',
          'content': 'Jeśli ten telefon robi Twoim zdaniem kiepskie zdjęcia to chyba nie potrafisz robić zdjęć smartfonem :)',
          'id': 90
        }, {
          'rating': 5,
          'authorName': 'Maciej',
          'date': '2018-01-18T20:37:00.000Z',
          'content': 'Byl a5 2016, w opcji P10 i S6  . Kupiony cenie 1749zl.\n+:\nEkran najlepszy do 2tys.  Płaski , nie klika sie nic przypadkowo (jak w Sams.) \nWielkosc 5.7 jak inne 5.2 .\nAparat bdb, aczkolwiek Samsung robi lepsze zdjecia (mniej szumu) . Przewaga to szer. kąt który jest rewelacyjny ( szkoda że bez stab.).\nBat. W nocy max 1% schodzi. W dzien ok 50-70%  (12h. Używania) \nMinusy: \nDość ciężka nakładka , w H P10. 2x plynniej to działa. \nSelphie słabe, dźwięk średniej jakości. \nNie ma ideału :)',
          'id': 91
        }, {
          'rating': 6,
          'authorName': 'Karolina',
          'date': '2018-02-12T21:53:00.000Z',
          'content': 'Telefon jest bardzo dobrze wykonany ,solidny,wodoodporny\nMa świetny aparat szerokokątny robi znakomite zdjęcia.\nJa ma model w kolorze płatynowym polecam bardzo serdecznie.😊😊😊',
          'id': 92
        }, {
          'rating': 6,
          'authorName': 'mhcezrw',
          'date': '2018-02-16T14:30:00.000Z',
          'content': 'Bardzo dobry telefon.\nDobrze wykonany, ładny, z rewelacyjnym ekranem i niezłym aparatem. Owszem, szumy są (chociaż tylko, jeśli będziemy na siłe powiększać zdjęcia i ich szukać), jakość nieco gorsza niż w Samsunugu, ale G6 nadrabia wszystko szerokim kątem.\nMinusem jest bateria, która trzyma w porywach dwa dni.\nAle za 1739 zł? Brać, nie gadać.',
          'id': 93
        }, {
          'rating': 6,
          'authorName': 'Artur',
          'date': '2018-02-25T12:48:00.000Z',
          'content': 'za tą cenę to idealny telefon',
          'id': 94
        }, {
          'rating': 6,
          'authorName': 'Olcia ',
          'date': '2018-03-07T16:51:00.000Z',
          'content': 'Piękny telefonik z świetnym aparacikiem. Uwielbiamy robić wspólne zdjęcia ze znajomymi na jakichś wypadach i imprezkach. Mam wrażenie, że pomieści więcej osób niż normalny aparat. &lt;3  Szybko się ładuje. Podłączam do ładowania rano i  bardzo dużo korzystam i bateria trzyma mi jeden dzień.',
          'id': 95
        }, {
          'rating': 6,
          'authorName': 'Tomasz',
          'date': '2018-03-14T13:09:00.000Z',
          'content': 'Smartfon działa bardzo, bardzo płynnie. W rok po premierze jest wciąż bardzo atrakcyjny technicznie. Bardzo ładny design, choć to akurat kwestia gustu. Jak dla mnie bije na głowę samsungi z giętym ekranem. Super leży w dłoni, choć ostatecznie wylądował w etui ze strachu o uszkodzenia. Chyba jedyną wadą jak dla mnie jest jeden średnio fajnie umieszczony głośnik. Wada do pominięcia, bo i tak tel. mam sparowany ze słuchawkami BT. Dziś, kiedy jego cena spadła o połowę od dnia premiery - super wybór!',
          'id': 96
        }, {
          'rating': 6,
          'authorName': 'Mandrak',
          'date': '2018-03-26T16:33:00.000Z',
          'content': 'Kapitalny telefon i jeden z lepszych na rynku w tej cenie. Ekran 18:9 HDR z Dolby Vision. Wygląd wersji platinium nieprzeciętny. Telefon po 5 m-cach nie tnie ani nie zacina. System jest super płynny. Nakładka świetna - wybudzanie i zamykanie dwuklikiem etc. Aparat tylni bardzo dobry, ale skrzydła rozwija dopiero w trybie ręcznym, Zdjęcia nie gorsze niż z S8, są bardziej naturalne i nie tak docieplone. Dodatkowo obiektyw z szerokim kątem. Jedyny minus to słaba przednia kamerka do selfie. Polecam!',
          'id': 97
        }, {
          'rating': 6,
          'authorName': 'Krzysiek',
          'date': '2018-03-29T13:13:00.000Z',
          'content': 'Telefon naprawde super, mam go pół roku i dosłownie rewelacja. Jedyny minus zdjecia w nocy.',
          'id': 98
        }, {
          'rating': 6,
          'authorName': 'waliada',
          'date': '2018-04-06T08:24:00.000Z',
          'content': 'bardzo dobry telefon, polecam',
          'id': 99
        }, {
          'rating': 6,
          'authorName': 'mefisssto',
          'date': '2018-04-06T21:24:00.000Z',
          'content': 'Mam go od paru dni, w cenie poniżej 1600 zł jest według mnie bezkonkurencyjnym telefonem w kategorii cena/jakość. Sprawia wrażenie o wiele bardziej solidnego i wytrzymałego niż Samsung Galaxy s8, którego również miałem okazję używać. Bateria trzyma nieźle, wytrzymuje cały dzień całkiem intensywnego użytkowania (chociaż nie ma cudów, myślę że w przypadku intensywnego grania w gry bateria topniałaby szybko). Ogólnie po kilku dniach brak istotnych minusów, zobaczymy co będzie dalej.',
          'id': 100
        }, {
          'rating': 6,
          'authorName': 'Jacko',
          'date': '2018-04-12T12:49:00.000Z',
          'content': 'Naprawde spoko telefon. Miałem juz rózne i ten jest moim faworytem. Bateria wytrzymuje około doby przy intensywnym użytkowaniu. Fajnie wygląda i działa natychmiastowo, nawet przy robieniu kilku rzeczy na raz. Aparat jest OK, zdjęcia dobrej jakości zarówno przednim jak i tylnym.&nbsp;',
          'id': 101
        }, {
          'rating': 6,
          'authorName': 'Dracles',
          'date': '2018-06-01T10:46:00.000Z',
          'content': 'Posiadam od miesiąca i jestem w pełni zadowolony. Piękne zdjęcia, płynność działania i dostał aktualizację do oreo. Na głowę bije wszystkie średniaki do 2k zł.. Gorąco polecam cena/jakość nie do pobicia w tym momencie.',
          'id': 102
        }, {
          'rating': 2,
          'authorName': 'Krzysztof',
          'date': '2018-07-13T08:37:00.000Z',
          'content': 'Tak jak byłem napalony na ten telefon, tak mój zapał zgasł praktycznie kilka godzin po jego kupieniu. Telefon niemiłosiernie się grzeje przy pracujacym Chromie i programie do słuchania muzyki. Zmierzyłem temperaturę, która dochodziła do 48 stopni; telefonu prawie nie dało się trzymać w dłoni. A bateria... po 4 godzinach ze 100% do 0%. Szkoda, wielka szkoda, bo telefon zapowiadał się świetnie. Zwrocony po 3 dniach.',
          'id': 103
        }, {
          'rating': 6,
          'authorName': 'johnnybravo',
          'date': '2018-07-25T17:10:00.000Z',
          'content': 'Najlepszy telefon na swiecie i przy tym rewelacyjna cena',
          'id': 104
        }, {
          'rating': 5,
          'authorName': 'rec',
          'date': '2018-07-30T16:31:00.000Z',
          'content': 'Na plus: dobra prędkość dzialania, wygląd (polecam wersję platinum), ogólna jakość wykonania, szeroki kąt.\n\nNa minus: wolałbym amoleda (ale takiego normalnego, płaskiego jak tutaj), automatyczna jasność ekranu w zacienionych pomieszczeniach trochę za bardzo sciemnia ekran (u mnie do 12%, podczas gdy 30 % jest komfortowo), gniazdo słuchawkowe trochę ciężko chodzi (trzeba wcisnąć na chama jacka), żal trochę braku DAC hifi.\n\nPodsumowanie za 1500 zł warto dla płynności działania i jakości wykonania',
          'id': 105
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2018-08-06T12:29:00.000Z',
          'content': 'Jestem bardzo zadowolony. Telefon co prawda nie najnowszy, ale w pełni rekompensuje to cena. Dobry aparat - radzi sobie nawet w niedoświetlonych warunkach. To zasługa zastosowanego, stosunkowo jasnego obiektywu F/1.8',
          'id': 106
        }, {
          'rating': 6,
          'authorName': 'Wojtek',
          'date': '2018-08-28T09:58:00.000Z',
          'content': 'LG G6 jest bardzo dobrym smartfonem. Snapdragon 821 jest nadal wydajny, aparat podstawowy piękny (dla zdecydowanej większości użytkowników), a szerokokątny... rewelacja! Wodoodporność, pewny chwyt w dłoni, ładny i niezaokrąglony wyświetlacz, zadowalający i czysty głośnik, świetna jakość wykonania. Miałem wątpliwości co do wyglądu, ale pozytywnie się zaskoczyłem – w rzeczywistości smartfon jest bardzo elegancki. Zdecydowanie polecam platynowy kolor. :)',
          'id': 107
        }, {
          'rating': 5,
          'authorName': 'Kampos',
          'date': '2018-09-02T16:47:00.000Z',
          'content': 'Wykonanie telefonu pierwsza klasa. Nie jest tak smukły jak konkurencja, ma szeroką ramkę, ale dla mnie to dobrze. Mamy wrażenie, że trzymamy w ręku coś porządnego, a nie wydmuszkę. Urządzenie dobrze leży w dłoni, nie jest za duże. Aparat robi bardzo dobre zdjęcia - zawsze można się posiłkować trybem ręcznym. Szeroki kąt już nie tak szczegółowy, ale zdjęcia robią wrażenie. Na wycieczki w sam raz. Selfiaki odstają już od tylnego aparatu, ale jak na taką matrycę jest bardzo spoko. Platynowy rządzi!',
          'id': 108
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {'name': 'Gniazdo kart nanoSIM - 1 szt.', 'id': 150}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Pyłoszczelność i wodoszczelność (IP68)', 'id': 35}, {'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Żyroskop',
            'id': 37
          }, {'name': 'Magnetometr', 'id': 38}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik zbliżenia',
            'id': 42
          }, {'name': 'Czytnik linii papilarnych', 'id': 45}, {
            'name': 'Funkcja szybkiego ładowania Quick Charge',
            'id': 48
          }, {'name': 'Szkło Corning Gorilla Glass 3', 'id': 108}, {'name': 'Odporność na wstrząsy i upadki', 'id': 162}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel USB typ C', 'id': 51}, {'name': 'Słuchawki', 'id': 54}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '5.0 Mpix - przód', 'id': 69}, {'name': '13.0 Mpix - tył (podwójny)', 'id': 152}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '32 GB', 'id': 81}]}, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5,7"', 'id': 82}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K', 'id': 103}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 821 (2 rdzenie, 2.35 GHz, Kryo + 2 rdzenie, 1.6 GHz, Kryo)', 'id': 147}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Adreno 530', 'id': 148}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2880 x 1440', 'id': 149}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-polimerowa 3300 mAh', 'id': 151}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - przedni obiektyw szerokokątny', 'id': 153}, {
            'name': 'f/1.8 - tylny obiektyw',
            'id': 154
          }, {'name': 'f/2.4 - tylny obiektyw szerokokątny', 'id': 155}]
        }, {'name': 'Dual SIM', 'filterable': false, 'id': 17, 'options': [{'name': 'Nie', 'id': 156}]}, {
          'name': 'Grubość',
          'filterable': false,
          'id': 18,
          'options': [{'name': '7,9 mm', 'id': 157}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '71,9 mm', 'id': 158}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '149 mm', 'id': 159}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '163 g', 'id': 160}]}, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Srebrny', 'id': 161}]
        }]
      }, {
        'id': 10,
        'name': 'Motorola Moto G5S FHD 3/32GB Dual SIM szary',
        'price': 599,
        'quantity': 57,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5s-fhd-332gb-dual-sim-szary-383389,2017/9/pr_2017_9_20_14_6_18_846_00.jpg',
          'main': true,
          'id': 52
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5s-fhd-332gb-dual-sim-szary-383389,2017/9/pr_2017_9_20_14_5_5_308_05.jpg',
          'main': false,
          'id': 53
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5s-fhd-332gb-dual-sim-szary-383389,2017/9/pr_2017_9_20_14_5_45_296_00.jpg',
          'main': false,
          'id': 54
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5s-fhd-332gb-dual-sim-szary-383389,2017/9/pr_2017_9_20_14_6_3_610_00.jpg',
          'main': false,
          'id': 55
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5s-fhd-332gb-dual-sim-szary-383389,2017/9/pr_2017_9_20_14_4_55_416_03.jpg',
          'main': false,
          'id': 56
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,motorola-moto-g5s-fhd-332gb-dual-sim-szary-383389,2017/9/pr_2017_9_20_14_4_48_275_00.jpg',
          'main': false,
          'id': 57
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'ZALORK',
          'date': '2017-12-16T18:05:00.000Z',
          'content': 'Sam telefon jest Świetny i serdecznie go polecam , ale nie zgodzę się z tym że "wystarczy 15 minut ładowania, by kontynuować pracę ze smartfonem przez kolejne 5 godzin."',
          'id': 109
        }, {
          'rating': 3,
          'authorName': 'rrr',
          'date': '2017-12-21T02:07:00.000Z',
          'content': 'Te telefony to odnawiane ?! Niby oryginalnie zapakowany, wszystko jak w nowym , a na tylnej stronie widoczne odbarwienia po nieobecnych naklejkach i ten bolec do kart też jakby go ktoś używał już :/',
          'id': 110
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2017-12-21T07:27:00.000Z',
          'content': 'Zachęcamy do kontaktu: serwis@x-kom.pl lub pod numerem 34 377 00 30. Chcielibyśmy bliżej przyjrzeć się tej sytuacji.',
          'id': 111
        }, {
          'rating': 6,
          'authorName': 'JANEK',
          'date': '2017-12-29T09:49:00.000Z',
          'content': 'Jestem bardzo zadowolonym użytkownikiem tego modelu. jestem zdziwiony jakością wykonania telefonu, za taką cenę spodziewałem się czegoś dużo bardziej przeciętnego, a tutaj bardzo pozytywne zaskoczenie',
          'id': 112
        }, {
          'rating': 6,
          'authorName': 'Jolka',
          'date': '2017-12-30T15:36:00.000Z',
          'content': 'Używam od miesiąca i uważam, że ten telefon to jest strzał w dziesiątkę. W ogóle się nie zacina, nie ma problemu z odszukaniem czego kolwiek, ma bardzo ciekawy wygląd, robi dobre jakościowo zdjęcia, głośnik działa idealnie. Polecam',
          'id': 113
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-02-19T21:17:00.000Z',
          'content': 'Posiadam go kilka miesięcy. Świetnie wykonany, elegancki. Bateria w porządku, fotografie jak na smartfon również, nie zacina, uważam że gra bardzo dobrze. Pozytywnie mnie zaskoczył głośnik. Na minus odstający sporo aparat oraz skromna regulacja głośności, w nocy w totalnej ciszy przy minimum jest dla mnie za głośny żeby coś pooglądać lub pograć. Polecam.',
          'id': 114
        }, {
          'rating': 5,
          'authorName': 'Dominik',
          'date': '2018-03-15T19:16:00.000Z',
          'content': 'Bardzo dobry telefon. Posiadam od 6 miesięcy. Zdecydowanie do zalet zaliczam czysty adroid który pozwala na płynne działanie bez zaśmiecania systemu. Choć z tymi szybkimi aktualizacjami to trochę za dużo powiedziane. Mamy połowę marca a ja wciąż na androidzie 7.1.1. A i bateria trzyma nie najlepiej, jednak myślę że to wina bardzo ekstremalnego użytkownika przeze mnie. Jak już mówiłem telefon dobry chociaż nie bez wad.',
          'id': 115
        }, {
          'rating': 6,
          'authorName': 'Prede',
          'date': '2018-05-29T06:11:00.000Z',
          'content': 'Jak na razie pierwsze dni z telefonem bardzo pozytywnie. Telefon dla 9 latka. Wszystko hula jak należy. Fajny gadżet z diodą doświetlającą selfie.',
          'id': 116
        }, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2018-06-12T19:36:00.000Z',
          'content': 'Po ponad pół roku użytkowania mogę szczerze polecić. Działa płynnie, robi dobre zdjęcia, szybko się ładuje, ekran trwały. Mi bateria trzyma do dwóch dni (przeglądam internet ok 2h dziennie, słucham muzyki ok godziny dziennie, wymieniam kilka smsów, czasem parę minut dzwonię). Jedynie obudowa śliska, ale do tego mogę polecić plecki Nillkin Frosted Shield',
          'id': 117
        }, {
          'rating': 5,
          'authorName': 'Bleb',
          'date': '2018-07-17T08:11:00.000Z',
          'content': 'Telefon działa płynnie, jest przyzwoicie wykonany. Minus za wystający obiektyw aparatu. Podstawowy cover licuje się z nim na zero a więc nadal trzeba mocno uważać żeby go nie porysować. Szkoda że wszyscy są zachłyśnięci amerykańskim snem i musi być duże. Na rynku generalnie brakuje 4,5" z mocniejszym wnętrzem w tym przedziale cenowym.',
          'id': 118
        }, {
          'rating': 6,
          'authorName': 'OnOpole36',
          'date': '2018-09-03T00:23:00.000Z',
          'content': 'OK sprzęt :-)',
          'id': 119
        }, {
          'rating': 6,
          'authorName': 'Pawełz',
          'date': '2018-09-06T07:10:00.000Z',
          'content': 'Za ta cenę naprawdę dobry telefon może są równie dobre Ale na Pewno nie lepsze, bateria super no i hula aż miło polecam.',
          'id': 120
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }, {'name': 'Szkło Corning Gorilla Glass 3', 'id': 108}, {'name': 'Metalowa obudowa', 'id': 134}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '5.0 Mpix - przód', 'id': 69}, {'name': '16.0 Mpix - tył', 'id': 164}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '3 GB', 'id': 80}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.1 Nougat', 'id': 86}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '158 g', 'id': 92}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3000 mAh', 'id': 99}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.0 - przedni obiektyw', 'id': 101}, {'name': 'f/2.0 - tylny obiektyw', 'id': 141}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 430 (4 rdzenie, 1.40 GHz, A53 + 4 rdzenie, 1.1 GHz, A53)', 'id': 136}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Adreno 505', 'id': 137}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080', 'id': 138}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '9,5 mm', 'id': 142}]}, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Szary', 'id': 146}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,2"', 'id': 163}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '73,6 mm', 'id': 165}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '150 mm', 'id': 166}]}]
      }, {
        'id': 11,
        'name': 'Samsung Galaxy S7 G930F 32GB czarny',
        'price': 1498,
        'quantity': 39,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s7-g930f-32gb-czarny-288297,2017/7/pr_2017_7_13_13_37_47_971.jpg',
          'main': true,
          'id': 58
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s7-g930f-32gb-czarny-288297,pr_2016_2_18_8_4_52_813.png',
          'main': false,
          'id': 59
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s7-g930f-32gb-czarny-288297,pr_2016_2_18_8_5_8_581.png',
          'main': false,
          'id': 60
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s7-g930f-32gb-czarny-288297,pr_2016_2_18_8_5_28_606.png',
          'main': false,
          'id': 61
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s7-g930f-32gb-czarny-288297,pr_2016_2_18_8_4_47_499.png',
          'main': false,
          'id': 62
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s7-g930f-32gb-czarny-288297,pr_2016_2_18_8_8_26_744.jpg',
          'main': false,
          'id': 63
        }],
        'reviews': [{
          'rating': 1,
          'authorName': 'wind',
          'date': '2016-03-11T17:02:00.000Z',
          'content': 'Oczywiście mimo że kosztuje 3 tysie to radia dalej nie ma. Przyoszczędził samsung',
          'id': 121
        }, {
          'rating': 6,
          'authorName': 'kMqStore',
          'date': '2016-03-17T02:12:00.000Z',
          'content': 'Galaxy S7 to takie połączenie IPS68 z s5\'tki i dobrego premium designu z s6\'stki\nBrakuje tu oczywiście nieco większej baterii, bo nie okłamujmy się to ekran 4k a 3000mAh to stanowczo za mało nawet jesli mówimy tu o wersji EDGE! \nMam pytanie do was X-kom, a mianowicie czy można u was zakupić 7\'demeczkę w wersji 64GB wraz z nowym VR od samsunga? ( gdzieś słyszałem że rozdają za darmo wraz z zakupem S7)',
          'id': 122
        }, {
          'rating': 6,
          'authorName': 'NX',
          'date': '2016-05-01T10:47:00.000Z',
          'content': 'Super smartfon. Drogi, ale jest świetny.',
          'id': 123
        }, {'rating': 6, 'authorName': 'dryice', 'date': '2016-05-18T15:48:00.000Z', 'content': 'Boski!', 'id': 124}, {
          'rating': 6,
          'authorName': 'mniszka',
          'date': '2016-05-20T08:06:00.000Z',
          'content': 'Telefon rewelacja. Nie żałuję wydanych pieniędzy. Mieszkam w miejscu  oddalonym od cywilizacji i poprzednie telefony bardzo słabo łapały zasięg. Galaxy robi to świetnie, uwierzcie, nie mam problemu z zasięgiem jestem mega zaskoczona. Dodatkowo jakość obrazu, robionych zdjęć jest cudowna. Zdjęcia są przejrzyste i wyraźne. Ma dużą pamięć więc spokojnie mieszczę moją ulubioną muzyczkę. Telefon ma wszystko co potrzeba. Zachęcam!',
          'id': 125
        }, {
          'rating': 6,
          'authorName': 'Bartek',
          'date': '2016-06-24T16:44:00.000Z',
          'content': 'Miałem s6 ale postanowiłem przesiąśc sie na s7 po kilku tygodniach uzytkowania muszę przyznać, że widać różnicę w działaniu pomiedzy tymi dwoma modelami. Przede wszystkim 7 robi o wiele lepsze zdjecia pod wzgledem jakosciowym, według mnie szybciej też działa, bardzo wydajny, bateria trochę dłużej trzyma podczas intensywnego uzytkowania. Szkoda tylko, że cena dość wysoka no ale za dobry sprzęt trzeba zapłacić.',
          'id': 126
        }, {
          'rating': 6,
          'authorName': 'Maladzio',
          'date': '2016-08-10T13:49:00.000Z',
          'content': 'Niezły bajer, spłukał totalnie, ale w końcu mam się czym pochwalić.',
          'id': 127
        }, {
          'rating': 1,
          'authorName': 'Mateusz',
          'date': '2016-10-31T09:17:00.000Z',
          'content': 'Tydzień się nacieszyłem telefonem, a teraz już tydzień leży w serwisie szybie ładowanie trwało 8h i naładował się do 80% czyli 10% na 1h x-kom niestety nie przyjął bo były już rysy mimo gorilla glass 4',
          'id': 128
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2016-11-10T08:34:00.000Z',
          'content': 'Mateusz, bardzo prosimy o kontakt z naszym serwisem - serwis@x-kom.pl lub pod numerem 34 377 00 30. Postaramy się wyjaśnić wątpliwości dotyczące Twojej reklamacji\n            * * *\nDziękujemy za kontakt telefoniczny. Bardzo nam przykro, że autoryzowany serwis nie był w stanie rozwiązać Twojego problemu. Gdybyś miał wątpliwości, zachęcamy do bezpośredniego kontaktu z nami.',
          'id': 129
        }, {
          'rating': 6,
          'authorName': 'Bomba',
          'date': '2016-11-08T18:54:00.000Z',
          'content': 'Rewelacja! Wygląd, funkcjonalność, aparat, system - 10/6!',
          'id': 130
        }, {
          'rating': 6,
          'authorName': 'Samsungdebest',
          'date': '2016-12-03T18:48:00.000Z',
          'content': 'S7 wyszła im niesamowicie dobrze. Jestem z niej zadowolony',
          'id': 131
        }, {
          'rating': 2,
          'authorName': 'al',
          'date': '2016-12-28T19:07:00.000Z',
          'content': 'W  cenie do 2000 zł to głośnik słabiutki, powyżej 2000 zł beznadziejny.',
          'id': 132
        }, {
          'rating': 3,
          'authorName': 'Ramzes',
          'date': '2016-12-29T13:50:00.000Z',
          'content': 'Dlaczego w tak drogim urządzeniu głośnik za kilkanaście złotych, brzmi jak w telefonie za 300 zł.',
          'id': 133
        }, {
          'rating': 6,
          'authorName': 'Makoś',
          'date': '2017-01-01T13:38:00.000Z',
          'content': 'Polecam ten telefon warty swojej ceny można kupić już na x-kom za 2549 zł mam parę dni a świat staje się lepszy w końcu rywalizacja z iPhone 7 ma naprawdę duże szanse',
          'id': 134
        }, {
          'rating': 4,
          'authorName': 'zywy',
          'date': '2017-01-17T14:24:00.000Z',
          'content': 'Jest dobrze, ale bez rewelacji. Po przesiadce z iphone\'a 6 mam poczucie małej płynności, np. w sytuacji przesuwania ekranu startowego do upday. Wizualnie nie ma tragedii, metalowe brzegi na plus, jednak plastikowe plecki w telefonie tej klasy słabo. Mimo niewielkiej różnicy w rozmiarze w moim odczuciu zdecydowanie mniej poręczny niż iPhone 6. Bateria - nie ma tragedii - trzyma mi ok 1.5-2 dni. Ładuje się 1.5h. Jeżeli ktoś nie lubi ios to polecam, w przeciwnym wypadku doradzałbym jednak iphona.',
          'id': 135
        }, {
          'rating': 6,
          'authorName': 'koag',
          'date': '2017-01-18T19:13:00.000Z',
          'content': 'Super telefon, robi mega zdjęcia, wydajność na topowym poziomie, zero zacięć, wszystko śmiga.Polecam.',
          'id': 136
        }, {
          'rating': 6,
          'authorName': 'michal',
          'date': '2017-01-20T10:18:00.000Z',
          'content': '@zywy gdzie Ty widzisz z tyłu plastik? u mnie jest szkło :) telefon świetny, żadnych przycięć, aparat robi wyśmienite zdjęcia, ogólnie polecam :)',
          'id': 137
        }, {
          'rating': 6,
          'authorName': 'B@kus ',
          'date': '2017-02-14T15:42:00.000Z',
          'content': 'Nie zamieniłbym na nic innego, jedyny mankament tego telefonu to mega cienka i śliska obudowa, więc pierwsze co ETUI, potem już wszystko idealnie. Nie zawiesza się, bynajmniej mi ani razu się nie zaciął, a korzystam już 3 miesiące. Menu przejrzyste plus Android, zatem obsługa pierwsza klasa. No i to co najlepsze w Samsung S7 to wyświetlacz, piękne kolory, wysoka rozdzielczość, obraz jak żywy, zdjęcia mistrzostwo. POLECAM',
          'id': 138
        }, {
          'rating': 6,
          'authorName': 'oktober flagowiec',
          'date': '2017-02-27T23:47:00.000Z',
          'content': 'generalnie telefon bardzo dobry, ergonomia,wykonanie,aparat, super.Procesor,płynny szybki,elegancjaJednak gdy, przyszłomi porownac z lg g5, jestem bylem pomiedzyy mlotem a kowadlem. wiec mam obydwa.',
          'id': 139
        }, {
          'rating': 6,
          'authorName': 'farfo',
          'date': '2017-03-11T13:00:00.000Z',
          'content': 'Polecam. Wart swojej ceny',
          'id': 140
        }, {
          'rating': 5,
          'authorName': 'Piotrek',
          'date': '2017-05-14T20:20:00.000Z',
          'content': 'Ogólnie telefon bardzo fajny i według mnie ostatni naprawdę uniwersalny. S8 to już wodotryski które w większości się nie przydają. Jednak rozczarowany jestem jedynie aparatem który ponoć własnie miał być największym atutem. Owszem, jest super jasny, ale ten szerokokątny obiektyw bardzo zniekształca obraz.Można to dopiero dostrzec jak samemu robi się zdjęcia. Widać wtedy że zdjęcia postaci są nienaturalnie zniekształcone. Mój stary S5 robił mniej ostre ale lepsze zdjęcia w dzień bo bez dystorsji.',
          'id': 141
        }, {
          'rating': 6,
          'authorName': 'slaw',
          'date': '2017-05-23T12:49:00.000Z',
          'content': 'Placza ze nie ma radia ... jak by bylo to by plakali ze trzeba placic abonament do panstwa za ow radio. I wez tu dogodz polaczkowi. Mozna sluchac radia cyfrowego ktore jest w pelni za darmo!!',
          'id': 142
        }, {
          'rating': 6,
          'authorName': 'Cris2000',
          'date': '2017-06-11T18:22:00.000Z',
          'content': 'Po posiadaniu Galaxy s4 przyszedł czas na s7. Przeskok między modelami jest ogromny. S7 działa płynnie, szybko a aplikacje uruchamiają się w mniej niż sekundę. Telefon super leży w dłoni a umiejscowienie czytnika lini papilarnych z przodu to dla mnie najlepsze co mogło być. Aparat robi genialne zdjęcia. Wiele ciekawych nowych funkcji które fanom zdjęć urozmaicą zabawę. Bateria wytrzymuje minimum 24 godziny. Wynik bardzo dobry.Podsumowując,urządzenie warte swojej ceny i godne polecenia.',
          'id': 143
        }, {
          'rating': 6,
          'authorName': 'BB',
          'date': '2017-07-07T15:20:00.000Z',
          'content': 'Świetny smartfon. Aparat robi zdjęcia bardzo dobrej jakości, podobnie z filmami. Rewelacyjna jakość dźwięku na dołączonych słuchawkach - przy dobrym utworze ciarki chodzą po plecach. Jedyne zastrzeżenia mam co do samego oprogramowania - Samsung mógłby dać możliwość wyłączenia większej ilości swoich aplikacji. Nie interesuje mnie ich aplikacja do SMS-ów, ani ich market aplikacji. Niepotrzebnie zajmują miejsce na pamięci. To jest już taki mankament, który ostatecznie na ocenę nie ma znaczenia.',
          'id': 144
        }, {
          'rating': 5,
          'authorName': 'moteq',
          'date': '2017-07-12T20:46:00.000Z',
          'content': 'Aparat ma kapitalny fokus, ale moim zdaniem ustepuje Nexusowi 6p.\nEkran jest swietny - jasny, genialnie skalibrowny.\nBateria to duze rozczarowanie, bo zjada ja nawet nic nie robiac. \nTouchWizz jest ok',
          'id': 145
        }, {
          'rating': 6,
          'authorName': 'adams',
          'date': '2017-07-13T05:53:00.000Z',
          'content': 'Po przesiadce z samsunga j5(2016) jest wyraźna różnica na korzyść Samsunga s7 .Prędkość działania,wykonanie ,komfort i jakość wykonywanych zdjęć rewelacja ,dodatkowo ekran  super kolory .Do minusów zaliczyłbym "śliskość obudowy" etui mile widziane',
          'id': 146
        }, {
          'rating': 6,
          'authorName': 'mario ',
          'date': '2017-08-19T15:06:00.000Z',
          'content': 'Telefon - komputer, wszystko na nim zrobię, jest szybki, nie zacina się, a jakość obrazu powiedziałbym, że lepsza niż na moim laptopie. Polecam i jeszcze dodam, że przyda się etui bo obudowa śliska.',
          'id': 147
        }, {
          'rating': 6,
          'authorName': 'Dami',
          'date': '2017-09-10T22:59:00.000Z',
          'content': 'Bateria trzyma bez problemu cały dzień (po roku posiadania!), a do tego wszystkie aplikacje działają bez problemu. Przy wyborze między S7 a S8, myślę ze nie warto przepłacać. Naprawdę solidny telefon, polecam.',
          'id': 148
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2017-09-11T06:48:00.000Z',
          'content': 'Świetny telefon! Szybki, elegancki, nie za duży. Jak na smartfona robi fantastyczne zdjęcia i kręci filmy bardzo dobrej jakości w 4K. Polecam!',
          'id': 149
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2017-09-11T06:51:00.000Z',
          'content': 'Po ponad roku czasu mogę tylko polecić, zwłaszcza jeżeli tak jak mnie nie przekonują was zaokrąglone boki. Najlepszy jest tutaj aparat, szybki i konkretny. Polecam!',
          'id': 150
        }, {
          'rating': 6,
          'authorName': 'Robert',
          'date': '2017-09-11T07:01:00.000Z',
          'content': 'Użytkuje telefon około pół roku-jest wart swojej ceny. Genialny aparat, bardzo mocny, z wytrzymałą baterią.',
          'id': 151
        }, {
          'rating': 6,
          'authorName': 'Damian',
          'date': '2017-09-11T07:09:00.000Z',
          'content': 'Super telefon od Samsunga, świetna prezencja i dobre parametry',
          'id': 152
        }, {
          'rating': 3,
          'authorName': 'Sebastian',
          'date': '2017-09-11T07:37:00.000Z',
          'content': 'Mam od momentu wyjscia na rynek i szczerze nie polecam, sa znavznie lepsze teelfony w tej cenie. Aparat przereklamowany niestety.',
          'id': 153
        }, {
          'rating': 5,
          'authorName': 'Marcin',
          'date': '2017-09-11T07:45:00.000Z',
          'content': 'Prawie ideał. Działa doskonale.  Rozmowy bardzo wyraźne.  Ekran super.  Nie muli. Mały komputer i notatnik w jednym. \nBrak radia. W czasach klęsk żywiołowych warto mieć,  a także tam gdzie zasięg Internetu słabiutki. Za to gwiazdka mniej.',
          'id': 154
        }, {
          'rating': 6,
          'authorName': 'Tomasz',
          'date': '2017-09-11T08:10:00.000Z',
          'content': 'Telefon używam już z 3 msc jak narazie jest ok. Robi bardzo dobre zdjęcia dzięki super szybkiemu autofokusowi. Jak na razie mogę polecić. Dziala sprawnie szybko bez zacięć.',
          'id': 155
        }, {
          'rating': 6,
          'authorName': 'Jarosław',
          'date': '2017-09-11T08:40:00.000Z',
          'content': 'Super telefon seria galaxy jak zwykle na najlepszym poziomie',
          'id': 156
        }, {
          'rating': 6,
          'authorName': 'Krystian',
          'date': '2017-09-11T09:42:00.000Z',
          'content': 'Świetny telefon, korzystam z niego już od roku i wszystko nadal działa tak jakby było nowe z pudełka. Ogólnie telefon posiada wszystko czego można chcieć od telefonu, według mnie najbardziej na uwagę zasługuje jednak kamera - radzi sobie w każdych warunkach i przede wszystkim jest szybka i niezawodna',
          'id': 157
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-11T13:50:00.000Z',
          'content': 'Telefon działa bez zarzutu. Praktycznie każda aplikacja działa płynnie. Zaletą jest możliwość skorzystania z kart SD co przy flagowych smartfonach nie jest oczywiste. Bateria trzyma długo nawet przy korzystaniu z aplikacji.',
          'id': 158
        }, {
          'rating': 6,
          'authorName': 'Dariusz',
          'date': '2017-09-11T16:01:00.000Z',
          'content': 'Do tej pory używałem telefonów LG. Jednak po premierze G5 stwierdziłem, że to nie dla mnie. Wybór padł na Samsunga S7. Nie żałuję. Doceniam w nim przede wszystkim szybkość działania aplikacji. Ekran, bateria, aparat to też elementy na plus. Żeby tylko dostał jeszcze Androida O i działał na nim nadal tak sprawnie.',
          'id': 159
        }, {
          'rating': 6,
          'authorName': 'Przemysław',
          'date': '2017-09-11T20:06:00.000Z',
          'content': 'Aparat broni wszystkie niedoskonałości telefonu hmm bardziej Androida.',
          'id': 160
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2017-09-11T20:10:00.000Z',
          'content': 'Korzystam od lutego. Telefon dobrze trzyma się w dłoni, przy moim dość znacznym jego użytkowaniu nie zamula, nie grzeje się aż nad to. Aparat robi dobre zdjęcia, duży plus za RAW, które można wywołać. Generalnie jak ktoś nie chce telefonu z zakrzywionym ekranem a ma być szybki to S7 mogę polecić.',
          'id': 161
        }, {
          'rating': 6,
          'authorName': 'tommek',
          'date': '2017-09-12T16:20:00.000Z',
          'content': 'Telefon jest na prawdę świetny. Mam go już ponad pół roku i do niczego nie można się przyczepić. Za jego największy atut uważam woodoodporność i mocny procesor oraz niesamowity aparat. Szczerze polecam ten sprzęt!',
          'id': 162
        }, {
          'rating': 6,
          'authorName': 'Zorman ',
          'date': '2017-09-13T08:04:00.000Z',
          'content': 'Tani nie jest, ale warto. Szybki, piękne zdjęcia, nie najgorsza bateria, przede wszystkim ta funkcjonalność. Mam i nie zamienię na żadne S8 itd. Polecam',
          'id': 163
        }, {
          'rating': 5,
          'authorName': 'Rafał',
          'date': '2017-09-13T08:13:00.000Z',
          'content': 'Telefon bardzo dobry. W moim odczuciu wszystko chodzi bardzo sprawnie. Używam telefonu już rok, android jak android, ma swoje wady i zalety. Największa wada to aktualizacje niestety. Ogólnie polecam.',
          'id': 164
        }, {
          'rating': 4,
          'authorName': 'Mariusz',
          'date': '2017-09-13T19:46:00.000Z',
          'content': 'Drugi raz bym go nie wybrał, ale całkiem spoko telefon. Najgorszy jest aparat w słabych warunkach oświetleniowych, a tak chwalili, że to jego największy atut..',
          'id': 165
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-14T05:52:00.000Z',
          'content': 'Używam telefonu od kwietnia 2017 i jak narazie pełna satysfakcja z zakupu. Wydajny, robi super zdjęcia i przede wszystkim IP 68',
          'id': 166
        }, {
          'rating': 5,
          'authorName': 'TheFallen',
          'date': '2017-09-14T08:07:00.000Z',
          'content': 'Smartfon wydajny, ładny, dobrze skasowany, aparat robi ładne zdjęcia. Niestety czasem nakładka Touch wiz powoduje zawieszenie aplikacji wiadomości co nie przystoi urządzeniu z najwyższej półki. Za dużo aplikacji producenta. Ogromnym plusem jest ładowanie indukcyjne. Od samego początku ani razu nie korzystałem bezpośrednio z kabla. Polecam do tego etui Spigen Neo Hybrid gunmetal, które świetnie chroni telefon i dobrze wygląda.',
          'id': 167
        }, {
          'rating': 5,
          'authorName': 'Arend',
          'date': '2017-09-15T12:29:00.000Z',
          'content': 'Troche slaba bateria, za szybko ubywa podczas korzystania z LTE i przegladania zawartosci Internetu, troche sliska obudowa, warto uzywac etui ale cala reszta warta swojej ceny.',
          'id': 168
        }, {
          'rating': 6,
          'authorName': 'PyramidHead',
          'date': '2017-09-15T19:37:00.000Z',
          'content': 'Bardzo dobry telefon nigdy mnie nie zawiódł',
          'id': 169
        }, {
          'rating': 6,
          'authorName': 'Patrycja',
          'date': '2017-09-16T05:59:00.000Z',
          'content': 'Telefon świetny, dużo lepiej dopracowany niż S6 - ja nieprędko zmienię ten model na nowszy. Trochę słaba bateria, ale na moje "użytkowanie" jest ok.',
          'id': 170
        }, {
          'rating': 5,
          'authorName': 'Damian',
          'date': '2017-09-16T12:36:00.000Z',
          'content': 'Super smartfon, ma wszystko',
          'id': 171
        }, {
          'rating': 5,
          'authorName': 'Konrad',
          'date': '2017-09-17T08:08:00.000Z',
          'content': 'Bardzo dobry aparat, niesamowity ekran oraz topowa wydajność. Minusem jest krótki czas pracy na baterii.',
          'id': 172
        }, {
          'rating': 5,
          'authorName': 'Filip',
          'date': '2017-09-17T13:38:00.000Z',
          'content': 'Posiadam ten telefon od roku. Nie ma z nim żadnych problemów, dalej jest szybki, sprawny i w pełni sprawdza się w codziennym życiu. Jedyny minus to używanie szybkiego ładowania bardzo mocno obniżyło wydajność baterii.',
          'id': 173
        }, {
          'rating': 6,
          'authorName': 'Radosław',
          'date': '2017-09-17T20:55:00.000Z',
          'content': 'Telefon pierwsza klasa. Największy plus s7 to aparat - robi zdjęcie szybko i w genialnej jakości! Jak na dzisiejsze standardy kompaktowy rozmiar. Bateria solidna, choć bez rewelacji. Świetny ekran. Myślę, że to najlepszy zeszłoroczny smartfon,  który nie ma się czego wstydzić przy tegorocznych flagowcach. Polecam!',
          'id': 174
        }, {
          'rating': 6,
          'authorName': 'Tom',
          'date': '2017-09-27T11:52:00.000Z',
          'content': 'Jestem zadowolony z zakupu, szczególnie po przesiadce z S4.',
          'id': 175
        }, {
          'rating': 5,
          'authorName': 'Marcin',
          'date': '2017-10-08T09:11:00.000Z',
          'content': 'Sprzęt ok. Obsługa x kom jeszcze bardziej ok. Polecam sklep. Poziom światowy obsługi, a nie jak w tych firmach spod krzaka.',
          'id': 176
        }, {
          'rating': 6,
          'authorName': 'Pan Piotr',
          'date': '2017-12-02T23:44:00.000Z',
          'content': 'Bardzo dobry smartfon. Ma wszystko co powinien teraz posiadać taki sprzęt. Jedynym mankamentem jest bateria, która rzadko kiedy wytrzymuje cały dzień. No ale w dzisiejszych czasach to norma, że trzeba ładować codziennie ( jeśli nie częściej). Jakość wykonania świetna. Mogliby coś zrobić z przyciskiem Home. Bardzo szybko się rysuje i zapewne wpływa to na odczyt linii papilarnych. Swoją drogą szybkość odczytu linii jest niesamowicie szybka, wręcz natychmiastowa. Godny polecenia smartfonik ;)',
          'id': 177
        }, {
          'rating': 6,
          'authorName': 'Tomi',
          'date': '2018-01-12T21:20:00.000Z',
          'content': 'Telefon mam już od jakiegoś miesiąca kupiony w świątecznej promocji więc jakieś tam zdanie sobie na jego temat wyrobiłem. :).\nPo pierwsze bateria. Nie wiem na co ludzie tak narzekaja, ja może nie jestem nastolatkiem który nie ma życia poza fejsem i innymi tego typu instytucjami ;-) ale telefony używam umiarkowanie i bateria wystarcza na tydzień (nie, to nie żadna pomyłka) i powiedziałbym, że jeszcze zostaje 20% więc nie wiem co ci ludzie robią z tym telefonem.\nAparat fotograficzny jest bdb ...',
          'id': 178
        }, {
          'rating': 6,
          'authorName': 'Małgorzata',
          'date': '2018-01-18T08:34:00.000Z',
          'content': 'Pół roku temu kupiłam ten model starszemu synowi, jest z niego zadowolony i działa bezproblemowo, dlatego  gdy młodszy syn potrzebował nowego telefonu również zdecydował się na s7.',
          'id': 179
        }, {
          'rating': 6,
          'authorName': 'maro_mp',
          'date': '2018-02-12T16:39:00.000Z',
          'content': 'Sprzęt ok. Obsługa x kom jeszcze bardziej ok. Polecam sklep.',
          'id': 180
        }, {
          'rating': 6,
          'authorName': 'Woj 63',
          'date': '2018-02-17T11:42:00.000Z',
          'content': 'Niezły  fon jak za 1698.00 zł polecam.\nPo przesiadce z Galaxy s4 widać różnice.',
          'id': 181
        }, {
          'rating': 6,
          'authorName': 'K',
          'date': '2018-03-04T17:30:00.000Z',
          'content': 'Moje plusy i minusy: \n( + ) \nwydajność, bo to telefon kilku letni \ntelefon działa tak jak nowsze modele innych producentów w tej cenie - może rywalizować i wygrywać z Huawei P10 albo LG G6\npłynnie działa\nniezawodna bateria \nodporny na wodę \nrobi świetne zdjęcia \nładnie wykonany \n( - ) \nzbyt mało wbudowanej pamięci \ntrzeba go zabezpieczyć folią i etui ( niestety mam kilka oryginalnych rysek, ale to na własne życzenie )',
          'id': 182
        }, {
          'rating': 1,
          'authorName': 'Maciej Lasecki',
          'date': '2018-03-24T16:08:00.000Z',
          'content': 'Nie kupujcie tej tandety. Totalna porażka. Serwis po roku użytkowania telefonu w sposób normalny (kilka telefonów dziennie, trochę internetu itd. użytek prywatny), stwierdził wypalenie ekranu i nie podlega to naprawie gwarancyjnej, mało tego została unieważniona dalszą gwarancja na telefon. To jest super praktyka pozbyć się klienta na zawszę. Nie polecam.',
          'id': 183
        }, {
          'rating': 6,
          'authorName': 'Monia:):)',
          'date': '2018-04-03T10:09:00.000Z',
          'content': 'Zakupiłam niedawno, na razie nie mam zastrzeżeń. Działa szybko i reaguje na polecenia natychmiastowo, wyświetlacz ok, aparaty bomba! Zobaczymy jak z baterią na dluższą metę - poki co trzyma ładnie:)&nbsp;',
          'id': 184
        }, {
          'rating': 4,
          'authorName': 'Aro',
          'date': '2018-04-28T11:31:00.000Z',
          'content': 'Za cenę 1600 pln jest to jedna z lepszych propozycji mimo że od premiery upłynęło już dużo czasu. Jednak więcej bym nie dał. Pamiętać należy że jest to telefon mocny głównie na papierze (niech nikogo nie zwiedzie antutu i takie tam). A generalnie ma slaby dźwięk, przeciętny aparat z przodu - bez lampy błyskowej!, Brak radia, bateria którą szybciej się zużywa niz u konkurencji itp. Oczywiście aparat z tyłu rewelacja .',
          'id': 185
        }, {
          'rating': 6,
          'authorName': 'Szymon',
          'date': '2018-05-09T14:19:00.000Z',
          'content': 'Daję max za obsługę x-kom. Jeżeli chodzi o telefon to jest to już mój drugi galaxy s7 w ciągu miesiąca (1 z x-kom), i po kilku dniach pojawia się w nim błąd kamery. Nie wiem czy to wina aktualizacji, czy po prostu wadliwe modele. Także proszę uważać przy wyborze tego urządzenia.',
          'id': 186
        }, {
          'rating': 1,
          'authorName': 'SYSOP',
          'date': '2018-05-29T18:03:00.000Z',
          'content': 'Telefon super ... tylko ekran się wypala po 2 miesiącach jeżdżenia z włączona nawigacją ,  jak chcesz mieć wypalone logo np "YANOSIK" na środku ekranu to ten telefon jest idealny dla Ciebie :)',
          'id': 187
        }, {
          'rating': 4,
          'authorName': 'Marecky',
          'date': '2018-06-11T20:16:00.000Z',
          'content': '+ szybki, dobry telefon do kupienia za ~1400pln z wodoodpornością, dobrym aparatem i od niedawna systemem Android Oreo   - głośnik tragiczny w porównaniu do tego z S5 a do innych jak stereo HTC nie ma co nawet próbować. Ekran trafiłem różowy a byłem przekonany że tylko S8 mają tą wadę. Samsung zszedł na psy. Mimo to S7 w tej cenie warto rozważyć bo każda firma wydaje teraz modele na ilość a nie jakość',
          'id': 188
        }, {
          'rating': 6,
          'authorName': 'Ozyrek',
          'date': '2018-06-19T16:32:00.000Z',
          'content': 'W tej cenie to świetna alternatywa dla wszystkich średniaków. Smartfon ma dwa lata od wejścia na rynek, a nadal swoją wydajnością powala konkurencję. Aparat - nawet w ciężkich warunkach robi świetne zdjęcia, jest szybki, bezprzewodowe ładowanie, bateria do dwóch dni, polecam!',
          'id': 189
        }, {
          'rating': 6,
          'authorName': 'miki',
          'date': '2018-06-19T18:33:00.000Z',
          'content': 'potwierdzam pozytywne  opinie, mam od tygodnia i to naprawdę bardzo profesjonalny telefon w dobrej cenie. s7 udał się samsungowi.',
          'id': 190
        }, {
          'rating': 6,
          'authorName': 'użytkownik',
          'date': '2018-06-21T06:13:00.000Z',
          'content': 'to starszy model, ale nadal świetny i w dobrej cenie. w sam raz na moje potrzeby do codziennego używania - facebook, internet, youtube, odczytwanie maila. Super sprzęt.',
          'id': 191
        }, {
          'rating': 6,
          'authorName': 'jahoo',
          'date': '2018-06-22T08:01:00.000Z',
          'content': 'smartfon jest bardzo wydajny i szybki jak dla mnie. wygląda całkiem spoko, bardzo nowocześnie. ma wszystko to co niezbędne. wysoka klasa... nie mam problemu z bateria czy zasiegiem.. wybór jest trafiony w dychę :)',
          'id': 192
        }, {
          'rating': 6,
          'authorName': 'Łobi',
          'date': '2018-06-22T13:47:00.000Z',
          'content': 'KLASYKA! Wygodnie leży w dłoni, czytnik linii papilarnych pod ekranem, dobry aparat robiący ładne zdjęcia i filmy. Na tym zależało mi najbardziej. Przyjemnie się go używa. Potwierdziły się opinie, że ten telefon z 2016 jest niezawodny. godny polecenia',
          'id': 193
        }, {
          'rating': 6,
          'authorName': 'Fasolka',
          'date': '2018-06-22T20:41:00.000Z',
          'content': 'tylko zalety: praca, szybkość, moc, upadł też parę razy nic się nie stało, do tego miłe doradztwo, bardzo dziękuje',
          'id': 194
        }, {
          'rating': 6,
          'authorName': 'AdrianZZZ',
          'date': '2018-06-23T05:56:00.000Z',
          'content': 'Dla zwolenników samsunga pozycja obowiązkowa, jeżeli miałbym określić z jakiego samsunga korzystało mi się najlepiej ( a miałem ich sporo) to właśnie s 7- dobry, niezawodny telefon.',
          'id': 195
        }, {
          'rating': 6,
          'authorName': 'Sitek',
          'date': '2018-06-23T16:46:00.000Z',
          'content': 'jeżeli ktoś szuka jak ja dobrego modelu do pracy i muzyki polecam s7, nic porządniejszego za tę cenę się nie znajdzie, używam od 4 miesięcy, nie mam uwag.',
          'id': 196
        }, {
          'rating': 6,
          'authorName': 'Silvex',
          'date': '2018-06-24T18:48:00.000Z',
          'content': 'Oczywiście smartfon rewelacyjny i dobrze współpracuje z Gear S2!dla mnie to jest super.',
          'id': 197
        }, {
          'rating': 6,
          'authorName': 'Medix ',
          'date': '2018-06-25T13:52:00.000Z',
          'content': 'wygodny w użytkowaniu, łatwa obsługa, rewelacyjny aparat, szybko blokada na odcisk palca, wygląd, wyświetlacz! :)',
          'id': 198
        }, {
          'rating': 6,
          'authorName': 'Onko',
          'date': '2018-06-25T23:17:00.000Z',
          'content': 'Bardzo dobry tel. Polecam. Jestem zadowolony. Telefon kupiłem w maju i spełnia moje oczekiwania. Jest szybki, dobry do zdjęć i ma wiele funkcji bardzo mi pasujących - always on display, generlanie jest bardzo szybki, nawet szybszy od mojego poprzedniego iPhone 6s, którego miałem. Bateria też trzyma dłużej.',
          'id': 199
        }, {
          'rating': 6,
          'authorName': 'Classiccco',
          'date': '2018-06-26T08:29:00.000Z',
          'content': 'To już jak wszyscy dobrze wiedzą klasyk:D Bardzo dobry klasyk, dlatego właśnie na niego padł mój wybór. Polecam każdemu kto potrzebuje mocy, szybkości i prostego użytkowania.',
          'id': 200
        }, {
          'rating': 6,
          'authorName': 'Fiso',
          'date': '2018-06-26T17:01:00.000Z',
          'content': 'Kolejny Samsung. Mam zaufanie do marki. Nie zawiodłem się i polecam. To jest telefon dla wymagających użytkowników. Nadaje się zarówno do celów biznesowych jak i rozrywkowych. Wyświetlacz, praca systemu, ergonomiczny kształt to tylko kilka pozytywów. Dla mnie KOZAK :P',
          'id': 201
        }, {
          'rating': 6,
          'authorName': 'Marecki',
          'date': '2018-06-27T17:02:00.000Z',
          'content': 'Po prostu bombastyczny. Nie mam żadnych zastrzeżeń. Cenowo też korzystnie na chwilę obecną wypada. Kompaktowy smarfon wygodny w obsłudze w moim odczuciu. Polecam i pozdrawiam x-kom!',
          'id': 202
        }, {
          'rating': 6,
          'authorName': 'DarSO',
          'date': '2018-06-28T10:19:00.000Z',
          'content': 'Mega telefon! Cena na X-KOM bardzo korzystna, dlatego się zdecydowałem. Przesyłka błyskawiczna i cała i kompletna. Jak zwykle brak problemów i to sobie cenię. Super!',
          'id': 203
        }, {
          'rating': 6,
          'authorName': 'Przemo',
          'date': '2018-06-28T21:09:00.000Z',
          'content': 'Telefon mam już ponad dwa miesiące. W tym czasie wszystko działa bardzo szybko i wydajnie. Quick Charge to ogromne ułatwienie, podobnie jak bezprzewodowe ładowanie. Zdjęcia są super jakości, oczywiście nie ma co przyrównywać ich do lustrzanki, ale w klasie smartfonów mogę konkurować z iPhone i nowszymi Samsungami',
          'id': 204
        }, {
          'rating': 6,
          'authorName': 'Męski Telefon',
          'date': '2018-06-29T06:15:00.000Z',
          'content': 'W tej chwili to bardzo budżetowy telefon. Fajna alternatywa, jak kogoś nie stać na nowego S8 albo S9. Samsung S7 jest szybki i robi świetne zdjęcia. Ciężko o konkurencyjny w tej cenie. Tym bardziej, że Samsung ma ekran AMOLED.',
          'id': 205
        }, {
          'rating': 6,
          'authorName': 'Ted',
          'date': '2018-06-29T10:23:00.000Z',
          'content': 'PERFEKCYJNY w każdym calu. To mój najlepszy dotychczas samrtfon który rzeczywiście jest smart a nie tylko z opisu. Nie wiesza się i aktualizuje sprawnie. MEGA! Polecam serdecznie, jak ktoś ceni sobie czas i spokój&nbsp;',
          'id': 206
        }, {
          'rating': 6,
          'authorName': 'Papas',
          'date': '2018-07-08T13:04:00.000Z',
          'content': 'Dostawa na czas i jak zwykle miła obsługa na infolinii. Dlatego zawsze polecam Was jako najlepszy sklep z elektroniką. S7 zna chyba każdy, używam od miesiąca i potwierdzam walory. Rewelacyjny telefon za rozsądne pieniądze.',
          'id': 207
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2018-07-20T21:01:00.000Z',
          'content': 'telefon nadal świetnie sobie radzi w 2018 roku, aparat w tej cenie nie ma sobie równych',
          'id': 208
        }, {
          'rating': 5,
          'authorName': 'Krzysiek',
          'date': '2018-08-12T08:18:00.000Z',
          'content': 'W cenie 1399 zł (kupiony w promocji X-kom), oferował najlepszy stosunek ceny do jakości. Znakomity aparat, wyświetlacz, oraz bardzo dobra wydajność telefonu. Jedyne minus w stosunku do nowszych telefonów tej klasy, to nie aktywny na stałe czytnik linii papilarnych.',
          'id': 209
        }, {
          'rating': 6,
          'authorName': 'Jurek',
          'date': '2018-08-21T12:07:00.000Z',
          'content': 'Bajka. A w cenie promocyjnej jaką udało mi się u Was dorwać, ten telefon to już rewelacja! Jestem zachwycony tym jak pracuje i jak bardzo jest funkcjonalny, jeżeli ktoś szuka dobrego smartfonu do wymagających zadań szczerze polecam',
          'id': 210
        }, {
          'rating': 6,
          'authorName': 'Daniel',
          'date': '2018-09-03T10:43:00.000Z',
          'content': 'Pierwszy samsung który mnie nie rozczarował, byłem raczej zwolennikiem Appla ale przy następnej wymianie smartfonu na pewno wezmę pod uwagę Samsunga,  dopracowany w każdym detalu. Polecam po roku użytkowania.',
          'id': 211
        }, {
          'rating': 6,
          'authorName': 'Seba',
          'date': '2018-09-06T19:54:00.000Z',
          'content': 'S7 świetnie układa się w dłoni, lubię jego minimalistyczny wygląd, a jednocześnie bogate wnętrze. Fotki 1 klasa, szybkość działania, czytnik linii papilarnych, bateria - wszystko jest dopracowane na ostatni guzik i działa jak do tej pory niezawodnie. Warto rozważyć zakup.',
          'id': 212
        }],
        'attributes': [{
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 1 szt.', 'id': 150}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/1.7 - przedni obiektyw', 'id': 22}, {'name': 'f/1.7 - tylny obiektyw', 'id': 102}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Funkcja ładowania bezprzewodowego', 'id': 34}, {
            'name': 'Pyłoszczelność i wodoszczelność (IP68)',
            'id': 35
          }, {'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {'name': 'Magnetometr', 'id': 38}, {
            'name': 'Barometr',
            'id': 39
          }, {'name': 'Pulsometr', 'id': 40}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik zbliżenia',
            'id': 42
          }, {'name': 'Czujnik Halla', 'id': 43}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }, {'name': 'Funkcja szybkiego ładowania Quick Charge', 'id': 48}, {
            'name': 'Szkło Corning Gorilla Glass 4',
            'id': 174
          }, {'name': 'Funkcja latarki', 'id': 175}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Słuchawki', 'id': 54}, {'name': 'Kabel USB', 'id': 176}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '5.0 Mpix - przód', 'id': 69}, {'name': '12.0 Mpix - tył', 'id': 87}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '143,5 mm', 'id': 74}]}, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Czarny', 'id': 76}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '32 GB', 'id': 81}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3000 mAh', 'id': 99}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K', 'id': 103}]
        }, {'name': 'Dual SIM', 'filterable': false, 'id': 17, 'options': [{'name': 'Nie', 'id': 156}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Samsung Exynos 8890 (4 rdzenie, 2.3 GHz, ARMv8 + 4 rdzenie, 1.6 GHz, A53)', 'id': 167}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Mali-T880 MP12', 'id': 168}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5,1"', 'id': 169}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2560 x 1440', 'id': 170}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 6.0 Marshmallow', 'id': 171}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '6,9 mm', 'id': 172}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '70,8 mm', 'id': 173}]
        }]
      }, {
        'id': 12,
        'name': 'LG X Power 2 tytanowy',
        'price': 519,
        'quantity': 45,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-x-power-2-tytanowy-374865,2017/7/pr_2017_7_14_15_20_49_333.jpg',
          'main': true,
          'id': 64
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-x-power-2-tytanowy-374865,2017/7/pr_2017_7_14_15_17_6_943.jpg',
          'main': false,
          'id': 65
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-x-power-2-tytanowy-374865,2017/7/pr_2017_7_14_15_16_56_551.jpg',
          'main': false,
          'id': 66
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-x-power-2-tytanowy-374865,2017/7/pr_2017_7_14_15_19_24_30.jpg',
          'main': false,
          'id': 67
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-x-power-2-tytanowy-374865,2017/7/pr_2017_7_14_15_16_7_845.jpg',
          'main': false,
          'id': 68
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lg-x-power-2-tytanowy-374865,2017/7/pr_2017_7_14_15_17_35_835.jpg',
          'main': false,
          'id': 69
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Mieczysław',
          'date': '2017-11-16T17:47:00.000Z',
          'content': 'bardzo dobry produkt, transakcja również przebiegła błyskawicznie \npolecam',
          'id': 213
        }, {
          'rating': 6,
          'authorName': 'Kacper',
          'date': '2018-05-24T13:11:00.000Z',
          'content': 'Jestem zadowolony z produktu bateria jest godna polecenia',
          'id': 214
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 1 szt.', 'id': 150}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Magnetometr',
            'id': 38
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Funkcja szybkiego ładowania Quick Charge', 'id': 48}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Słuchawki', 'id': 54}, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '2 GB', 'id': 60}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1280 x 720', 'id': 64}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '13.0 Mpix - tył', 'id': 68}, {'name': '5.0 Mpix - przód', 'id': 69}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - tylny obiektyw', 'id': 88}, {'name': 'f/2.2 - przedni obiektyw szerokokątny', 'id': 153}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,4 mm', 'id': 114}]}, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Szary', 'id': 146}]
        }, {'name': 'Dual SIM', 'filterable': false, 'id': 17, 'options': [{'name': 'Nie', 'id': 156}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'MediaTek MT6750 (4 rdzenie, 1.5 GHz, Cortex A53+4 rdzenie, 1.0 GHz, Cortex A53)', 'id': 177}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Mali-T860', 'id': 178}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,5"', 'id': 179}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-polimerowa 4500 mAh', 'id': 180}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '78,4 mm', 'id': 181}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '155 mm', 'id': 182}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '165 g', 'id': 183}]}]
      }, {
        'id': 13,
        'name': 'Samsung Galaxy Xcover 4 G390F Dark Silver',
        'price': 789,
        'quantity': 10,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-xcover-4-g390f-dark-silver-356424,2017/7/pr_2017_7_13_13_57_13_362.jpg',
          'main': true,
          'id': 70
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-xcover-4-g390f-dark-silver-356424,pr_2017_3_31_15_29_14_964.jpg',
          'main': false,
          'id': 71
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-xcover-4-g390f-dark-silver-356424,pr_2017_3_21_13_44_31_432.jpg',
          'main': false,
          'id': 72
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-xcover-4-g390f-dark-silver-356424,pr_2017_3_31_15_29_0_807.jpg',
          'main': false,
          'id': 73
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-xcover-4-g390f-dark-silver-356424,pr_2017_3_31_15_29_4_26.jpg',
          'main': false,
          'id': 74
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Mata Leao',
          'date': '2017-05-24T12:49:00.000Z',
          'content': 'mega solidny funkcjonalny smartfon polecam kazdemu!',
          'id': 215
        }, {
          'rating': 6,
          'authorName': 'ixat32',
          'date': '2017-05-27T20:29:00.000Z',
          'content': 'To nie jest Galaxy 8+ ale lepszego nikomu nie potrzeba i w pełni zgadzam się z Mata Leao.',
          'id': 216
        }, {
          'rating': 5,
          'authorName': 'zadowolony',
          'date': '2017-05-31T16:22:00.000Z',
          'content': 'Mam telefon od 1 miesiąca. Pracuję w ciężkim przemyśle dlatego szukałem odpornego telefonu. Telefon naprawdę nie do zdarcia. Szczelny , twardy wzmocniony plastik. Kawał  twardego solidnego telefonu. W związku z tym trochę ciężkawy ale coś za coś .\nPomimo instalowania różnych programów chodzi płynnie bez zawieszek, żadnych problemów z oprogramowaniem. Dobra , głośna  słyszalność rozmów. Na początku był problem z załapaniem namiarów na satelity ( GPS) ale po próbach w końcu poszło.\nPOLECAM',
          'id': 217
        }, {
          'rating': 6,
          'authorName': 'Chłodniarz',
          'date': '2017-06-05T16:46:00.000Z',
          'content': 'Naprawdę dobry smartfon,a na dodatek ciężki do zdarcia :)',
          'id': 218
        }, {
          'rating': 6,
          'authorName': 'Spyra',
          'date': '2017-06-11T21:03:00.000Z',
          'content': 'Telefon mam od niedawna, ale jak do tej pory wszystko jest jak należy, po założeniu szkła na wyświetlacz nie ma problemu z ekranem dotykowym, ładowanie baterii szybkie, trzyma w zależności od użytkowania -/+ 2dni. Miałem do tej pory xcover 2 spora zmiana na plus. Polecam !',
          'id': 219
        }, {
          'rating': 1,
          'authorName': 'MatiCo - nie zadowolony',
          'date': '2017-06-16T09:30:00.000Z',
          'content': 'Zakupiłem XCover4 licząc iż będzie to sprzęt, którego oczekuję. A tu z przedniego aparatu zdjęcia fatalne, brak stabilizacji jest dramatem.\n\nCo pewien czas nie wiem z jakiego powodu traci połączenie z internetem, niby stan POŁĄCZONY, a gdy chcę otworzyć to OffLine',
          'id': 220
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2017-06-19T07:27:00.000Z',
          'content': 'Dziękujemy za Twoją opinię. Bardzo prosimy o kontakt z naszym działem serwisowym, który postara się rozwiązać problemy z Twoim telefonem. Jesteśmy do Twojej dyspozycji: serwis@x-kom.pl lub pod numerem 34 377 00 30.',
          'id': 221
        }, {
          'rating': 5,
          'authorName': 'Jarek',
          'date': '2017-07-04T09:58:00.000Z',
          'content': 'Dobry stosunek ceny do możliwości. \nWzmocniony ale nie monstrualny i gruby,dobrze leży w dłoni. Wydajność dobra, ekran OK choć nie Full HD a tylko HD. U mnie bateria daje radę do 2 dni. Plus - mechaniczne przyciski pod ekranem, nie są podświetlone ale nie ma problemu z trafieniem w odpowiedni przycisk. Bateria wymienna ale klapka odchodzi dosyć ciężko jest wiele zaczepów i nie wiem jakby zniosła częste otwieranie i zamykanie. Aparat całkiem niezły. Dla mnie to dobry tel. do pracy.',
          'id': 222
        }, {
          'rating': 3,
          'authorName': 'rolmigo',
          'date': '2017-07-10T06:14:00.000Z',
          'content': 'telefon fajny i odporny, ale otwarty port usb to jakaś porażka w tego typu telefonie, co jakiś czas trzeba wyciągać zabrudzenia z wnęki USB a po jednym zawilgoceniu nie mogę go naładować, chyba zaśniedział styki! tel ma 2 miesiące!',
          'id': 223
        }, {
          'rating': 5,
          'authorName': 'Opiniodawca',
          'date': '2017-07-12T14:01:00.000Z',
          'content': 'Mam ten telefon od przeszło 2 miesięcy i mogę śmiało polecić. Z pewnością nie jest to najszybszy smartfon na świecie, ale do standardowych zadań (tj. dzwonienie, obsługa mejli i wiadomości, robienie zdjęć i brykanie po sieci) w zupełności wystarcza i wykonuje je bez zająknięcia. Jeżeli dodatkowo ktoś szuka cech jakie powinno posiadać urządzenie będące nieco bardziej odporne fizycznie niż większość "biżuteryjnych" dziś telefonów i ma do wydania ~1000zł - spełni swoją rolę.',
          'id': 224
        }, {
          'rating': 6,
          'authorName': 'Sebastian',
          'date': '2017-07-22T09:10:00.000Z',
          'content': 'Jak narazie super',
          'id': 225
        }, {
          'rating': 6,
          'authorName': 'Maciej ',
          'date': '2017-09-11T17:24:00.000Z',
          'content': 'Telefon już mam pewien czas i tak: żadnych rys i zadrapań na obudowie, długi czas pracy na baterii i w dalszym ciągu tak jest, nie jak w przypadku niektórych modeli, pierwszy miesiąc 2 dni, potem już dzień itd, dobra czułość wyświetlacza nawet w mokrych rękach, płynność i przede wszystkim jakość rozmów, głośno wyraźnie zarówno po mojej jak i po drugiej stronie. Cena adekwatna.',
          'id': 226
        }, {
          'rating': 3,
          'authorName': 'krzysztorf',
          'date': '2017-10-14T19:57:00.000Z',
          'content': 'To nie jest solidny smartfon . Po upadku z około 70 cm pękła szybka .Naprawa to kosmos .Nie polecam',
          'id': 227
        }, {
          'rating': 5,
          'authorName': 'Bartek',
          'date': '2017-11-06T12:49:00.000Z',
          'content': 'telefon działa sprawnie, znawet z dużą ilością zainstalowanych aplikacji, bateria trzyma do 2 dni - przy rzadkim używaniu. Przy intensywnym oczywiście, krócej. \nTelefon używam od 2 tygodn. Jest solidnie wykonany. Zaletą jest wodoodporność i odporność na upadki, w zupełności wystarczy do obsługi poczty, przeglądarki i większości aplikacji. Android 7 jest kompatrybilny z większością aplikacji które działają bezproblemowo, polecam',
          'id': 228
        }, {
          'rating': 6,
          'authorName': 'radek',
          'date': '2018-01-01T12:56:00.000Z',
          'content': 'mam taki  jes tsuper',
          'id': 229
        }, {
          'rating': 5,
          'authorName': 'Mat Krk',
          'date': '2018-03-16T13:00:00.000Z',
          'content': 'Telefon fajny dobrze się trzyma go w ręce, parametry wystarczające. Jedyny poważny minus brak DUAL SIM.',
          'id': 230
        }, {'rating': 6, 'authorName': 'Rafał', 'date': '2018-03-25T13:55:00.000Z', 'content': 'Bardzo dobrze się sprawuje', 'id': 231}],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart microSIM - 1 szt.', 'id': 187}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Pyłoszczelność i wodoszczelność (IP68)', 'id': 35}, {
            'name': 'Akcelerometr',
            'id': 36
          }, {'name': 'Czujnik światła', 'id': 41}, {'name': 'Czujnik zbliżenia', 'id': 42}, {
            'name': 'Odporność na wstrząsy i upadki',
            'id': 162
          }]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel microUSB', 'id': 77}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '2 GB', 'id': 60}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '5"', 'id': 63}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1280 x 720', 'id': 64}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '13.0 Mpix - tył', 'id': 68}, {'name': '5.0 Mpix - przód', 'id': 69}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 2800 mAh', 'id': 139}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.2 - przedni obiektyw', 'id': 140}, {'name': 'f/1.9 - tylny obiektyw', 'id': 188}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Szary', 'id': 146}]}, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Nie', 'id': 156}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Samsung Exynos 7570 (4 rdzenie, 1.40 GHz, A53)', 'id': 184}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Mali-T720', 'id': 185}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, TFT LCD', 'id': 186}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '9,7 mm', 'id': 189}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '73,3 mm', 'id': 190}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '146,3 mm', 'id': 191}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '172 g', 'id': 192}]
        }]
      }, {
        'id': 14,
        'name': 'Nokia 6.1 Dual SIM granatowo-złoty',
        'price': 1199,
        'quantity': 63,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,nokia-61-dual-sim-granatowo-zloty-424512,2018/4/pr_2018_4_18_9_49_16_426_00.jpg',
          'main': true,
          'id': 75
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,nokia-61-dual-sim-granatowo-zloty-424512,2018/4/pr_2018_4_16_14_56_52_762_00.jpg',
          'main': false,
          'id': 76
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,nokia-61-dual-sim-granatowo-zloty-424512,2018/4/pr_2018_4_16_14_56_55_684_01.jpg',
          'main': false,
          'id': 77
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Krzysztof',
          'date': '2018-05-30T18:56:00.000Z',
          'content': 'Jest super. Bardzo wyraźna grafika szczególnie w grach.Wyraźne teledyski na YouTube. Bateria mocna. Płynnie działa,filmy super w 4K, zdjęcia super. Do słuchania muzyki z YouTube polecam subwoofer bo głośnik nie ma basów. Reszta jest na prawdę super. Za głośnik odejmuje jedną gwiazdkę.',
          'id': 232
        }, {
          'rating': 5,
          'authorName': 'Arronax1984',
          'date': '2018-07-23T16:36:00.000Z',
          'content': 'Czysty android śmiga aż miło. System po dwóch miesiącach używania działa płynnie i bez zacięć. Niezła bateria, trzyma cały dzień przy korzystaniu z aplikacji typu netflix, youtube i podobne.',
          'id': 233
        }, {
          'rating': 6,
          'authorName': 'Pidi',
          'date': '2018-07-27T13:06:00.000Z',
          'content': 'Genialny telefon - elegancka, świetnie spasowana metalowa obudowa  , szybki system AndroidOne, NFC dzięki czemu można płacić telefonem, szybki czytnik linii papilarnych. Duża ilość pamięci RAM i miejsca na dane. Niezła bateria i wystarczający procesor. Czego chcieć więcej ?\n\nBrawo Nokia 👌\nLegendarna jakość Nokia powraca - mam nadzieję.',
          'id': 234
        }, {
          'rating': 6,
          'authorName': 'Mikołaj',
          'date': '2018-08-23T16:36:00.000Z',
          'content': 'Bardzo zacny telefon, nie ma się do czego przyczepić, wszystko działa bez zarzutu. Serdecznie polecam zwłaszcza granatową wersję - wygląda bardzo ładnie i elegancko.',
          'id': 235
        }, {
          'rating': 5,
          'authorName': 'JohhnyCage',
          'date': '2018-08-24T08:00:00.000Z',
          'content': 'Dużo pamięci wbudowanej i RAM, czysty Android i gwarantowane aktualizacje w przyszłości, bardzo szybkie działanie. Bateria trzyma mi średnio 2 dni, ale nie wiszę cały czas na telefonie. \nJedną gwiazdkę odejmuje ponieważ moim zdaniem jakość zdjęć z aparatu mogłaby być lepsza, w końcu telefon swoje kosztuje. Generalnie jestem bardzo zadowolony i kupiłbym ponownie.',
          'id': 236
        }, {
          'rating': 6,
          'authorName': 'Sebsonn',
          'date': '2018-08-26T15:54:00.000Z',
          'content': 'Jak najbardziej mogę polecic Nokie 6.1.\nUżytkuje od miesiaca i jestem mega zadowolony. Przesiadłem się z redmi 3s, za tą cenę wyświetlacz fullhd robi robotę. Jestem zadowolony ze wszystkiego. Jedynie do czego mógłbym się doczepić to może trochę słaby głośnik rozmów. Moim zdaniem mógłby mieć trochę lepsza jakość. Poza tym wyróżnia się z tłumu i nie jest stylizowany na iPhone, jak większość teraźniejszych Chińczyków..',
          'id': 237
        }, {'rating': 6, 'authorName': 'Agmis', 'date': '2018-09-03T09:14:00.000Z', 'content': 'Wszystko ok.', 'id': 238}],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 8.1 Oreo', 'id': 19}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '8.0 Mpix - przód', 'id': 21}, {'name': '16.0 Mpix - tył', 'id': 164}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Obudowa wykonana ze szkła i aluminium', 'id': 33}, {'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Żyroskop',
            'id': 37
          }, {'name': 'Magnetometr', 'id': 38}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik zbliżenia',
            'id': 42
          }, {'name': 'Czujnik Halla', 'id': 43}, {'name': 'Czytnik linii papilarnych', 'id': 45}, {
            'name': 'USB OTG',
            'id': 49
          }, {'name': 'Szkło Corning Gorilla Glass 3', 'id': 108}, {'name': 'Obsługa radia FM', 'id': 119}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel USB typ C', 'id': 51}, {
            'name': 'Słuchawki',
            'id': 54
          }, {'name': 'Instrukcja szybkiego uruchomienia telefonu', 'id': 56}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {'name': 'Typ ekranu', 'filterable': false, 'id': 5, 'options': [{'name': 'Dotykowy, IPS', 'id': 62}]}, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Pamięć RAM', 'filterable': true, 'id': 3, 'options': [{'name': '4 GB', 'id': 95}]}, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '64 GB', 'id': 96}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3000 mAh', 'id': 99}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/2.0 - przedni obiektyw', 'id': 101}, {'name': 'f/2.0 - tylny obiektyw', 'id': 141}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K', 'id': 103}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '175 g', 'id': 133}]}, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080', 'id': 138}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,5"', 'id': 179}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Qualcomm Snapdragon 630 (8 rdzeni, 2.20 GHz, Cortex A53)', 'id': 193}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Adreno 508', 'id': 194}]}, {
          'name': 'Grubość',
          'filterable': false,
          'id': 18,
          'options': [{'name': '8,6 mm', 'id': 195}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '75,8 mm', 'id': 196}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '148,8 mm', 'id': 197}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Niebieski', 'id': 198}]}]
      }, {
        'id': 15,
        'name': 'Samsung Galaxy J7 2017 J730F Dual SIM LTE czarny',
        'price': 848,
        'quantity': 28,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-j7-2017-j730f-dual-sim-lte-czarny-376940,2017/7/pr_2017_7_27_9_24_35_427.jpg',
          'main': true,
          'id': 78
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-j7-2017-j730f-dual-sim-lte-czarny-376940,2017/7/pr_2017_7_27_8_57_58_984.jpg',
          'main': false,
          'id': 79
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-j7-2017-j730f-dual-sim-lte-czarny-376940,2017/7/pr_2017_7_27_8_58_2_500.jpg',
          'main': false,
          'id': 80
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-j7-2017-j730f-dual-sim-lte-czarny-376940,2017/7/pr_2017_7_27_8_58_22_736.jpg',
          'main': false,
          'id': 81
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-j7-2017-j730f-dual-sim-lte-czarny-376940,2017/7/pr_2017_7_27_8_58_28_268.jpg',
          'main': false,
          'id': 82
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-j7-2017-j730f-dual-sim-lte-czarny-376940,2017/7/pr_2017_7_27_8_58_42_379.jpg',
          'main': false,
          'id': 83
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Jacek',
          'date': '2017-08-17T13:11:00.000Z',
          'content': 'Solidny telefon.16gb pam.to tr mało ale tel chodzi b.dobrze po zainstalowaniu.kilkunastu aplikacji bateria trzyma 1.5 dnie przy umiarkowanym używaniu.ogolnie za tą cenę polecam.',
          'id': 239
        }, {
          'rating': 6,
          'authorName': 'JACEK',
          'date': '2017-09-13T04:55:00.000Z',
          'content': 'Bardzo fajny telefon, jednak dla mnie nieco za duży. \nWymieniłem na J5 2017.',
          'id': 240
        }, {
          'rating': 5,
          'authorName': 'Sebastian',
          'date': '2017-09-13T19:39:00.000Z',
          'content': 'Telefon jest wart swojej ceny. Nic się nie zacina, ekran ma idealne odwzorowanie kolorów, jakość wykonania stoi na wysokim poziomie, a dźwięk brzmi świetnie. Małe minusy to aparat, który potrafi robić co jakiś czas rozmyte zdjęcia (wada zauważona również przez innych użytkowników tego smartfona, możliwe, że jest to wina wczesnej wersji oprogramowania) i wydajność procesora, która jest przeciętna dla tej półki cenowej. Osobiście polecam ten produkt marki Samsung.',
          'id': 241
        }, {
          'rating': 6,
          'authorName': 'Orzeszeq',
          'date': '2017-12-28T09:12:00.000Z',
          'content': 'Smartfon nie sprawia żadnych problemów, wszystko działa jak należy.\nJedynie obawiałem się po innych komentarzach o jakość połączeń, lecz nic takiego nie uswiadczyłem. W tej cenie bije wszystko inne z tymi parametrami.  Wyświetlacz Amoled, bateria 3600mAh, 3GB Ramu. 16GB pamięci to nie problem,  karta 64GB już jest. Świetny telefon do wszystkiego oprócz graczy.',
          'id': 242
        }, {
          'rating': 6,
          'authorName': 'ward',
          'date': '2018-01-05T21:53:00.000Z',
          'content': 'Świetny telefon, szczerze polecam ....',
          'id': 243
        }, {
          'rating': 6,
          'authorName': 'Wera',
          'date': '2018-01-13T19:20:00.000Z',
          'content': 'Świetny telefon. Polecam. Użytkuje od dwóch miesięcy i jeszcze mi się nie zaciął.',
          'id': 244
        }, {
          'rating': 6,
          'authorName': 'Włodek',
          'date': '2018-02-05T18:58:00.000Z',
          'content': 'W tej cenie wyświetlacz superamoled to miłe zaskoczenie. Niewielkie ramki to kolejny plus urządzenia. Jest poza tym wydajny i płynnie działa. Samsung przyoszczędził na pamięci wbudowanej, ale można sobie z tym poradzić kupując kartę SD. Ogromnym plusem urządzenia jest zasięg. Używam telefonu jako nawigacji ( yanosik ) działa bezbłędnie. Telefon automatycznie dostosowuje jasność ekranu.',
          'id': 245
        }, {
          'rating': 6,
          'authorName': 'jakub0187',
          'date': '2018-04-03T10:54:00.000Z',
          'content': 'Telefon naprawde wart swojej ceny jest on bardzo dobry wszysto działa bez zażutu Polecam!',
          'id': 246
        }, {
          'rating': 3,
          'authorName': 'Mirek Kr.',
          'date': '2018-04-25T10:18:00.000Z',
          'content': 'Wykonanie telefonu na bardzo dobrym poziomie. Sam telefon pracuje w miarę plynnie. Podczas codziennego użytkowania jest bardzo dobrze. Bateria to rewelacja! W połączeniu z typem wyświetlacza, dwa dni można osiągnąć bez problemu. Na wieeelki munus - przy bardzo małej pamięci wewnętrznej, ilość zaimplementowanych NIEPOTRZEBNIE aplikacji, przeraża! To dyskwalifikuje ten telefon do normalnego użytkowania!',
          'id': 247
        }, {
          'rating': 6,
          'authorName': 'Lesio',
          'date': '2018-07-12T09:04:00.000Z',
          'content': 'Bardzo dobry telefon do tysiąca złotych. Spełnia oczekiwania tj: ma dobry aparat, robi całkiem przyzwoite zdjęcia, jest intuicyjny i dość szybki. Dzięki za to że przyszedł w całości już kiedyś z innego sklepu dostałem strzaskany więc się obawiałem.',
          'id': 248
        }, {
          'rating': 5,
          'authorName': 'Jarvis Babbit',
          'date': '2018-07-16T18:10:00.000Z',
          'content': 'Świetny telefon, posiadam go już ponad pół roku. Zero problemów ze scinaniem. 3GB Ramu daje radę.\n\nNa plus rewelacyjny wyświetlacz, całkiem niezła bateria, bardzo ładna aluminiowa obudowa, niezła wydajność w grach.\n\nNa minus troszkę mało pamięci wewnętrznej, słabo działający autofocus.\n\nJeden mankament - prawdopodobnie tylko moja sztuka ale czasami muszę wcisnąć "i" kilka razy bo ekran w tym miejscu jest jakiś leniwy. Nie jestem pewnien czy to wina zabezpieczenia ekranu czy telefonu.',
          'id': 249
        }, {
          'rating': 1,
          'authorName': 'Kamil',
          'date': '2018-08-02T21:00:00.000Z',
          'content': 'Dno i jeszcze raz dno. Poza dosc dobrej jakosci aparatem, tel. jest tragiczny. Przeszedlem z LG XPower i wam powiem, ze jest tragedia, pojemnosc tel. to jakis zart, juz po 3 mies. uzytkowania nie ma miejsca na aktualizacje, a mam moze z 5 aplikacji, poza tymi oryginalnie wgranymi. Bateria ledwo co wytrzymuje jeden dzien, malo dzwoniac, jezeli dzwonisz duzo to max pol dnia. Jego szybkosc, to tez nic nadzwyczajneo, pod kazdym wzgledem poprzedni 2 letni tel. miazdzy go na glowe.',
          'id': 250
        }, {
          'rating': 6,
          'authorName': 'Dariusz',
          'date': '2018-08-19T10:28:00.000Z',
          'content': 'Bateria działa 1.5 dnia, czasem nawet dwa, to super wynik jak na pełne obciążenie smarta, chodzi płynnie, ma wystarczającą ilość pamięci na biblioteki zdjęć małżowinki, z aparatu też jest zadowolona, polecamy',
          'id': 251
        }],
        'attributes': [{
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}, {'name': 'Beidou', 'id': 84}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {
            'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.',
            'id': 17
          }, {'name': 'Micro USB - 1 szt.', 'id': 65}, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Magnetometr',
            'id': 38
          }, {'name': 'Czujnik światła', 'id': 41}, {'name': 'Czujnik zbliżenia', 'id': 42}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Słuchawki', 'id': 54}, {'name': 'Kabel microUSB', 'id': 77}, {'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '13.0 Mpix - tył', 'id': 68}, {'name': '13.0 Mpix - przód', 'id': 202}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '3 GB', 'id': 80}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/1.7 - tylny obiektyw', 'id': 102}, {'name': 'f/1.9 - przedni obiektyw', 'id': 203}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080', 'id': 138}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,5"', 'id': 179}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Samsung Exynos 7870 (8 rdzeni, 1.60 GHz, A53)', 'id': 199}]
        }, {'name': 'Układ graficzny', 'filterable': false, 'id': 2, 'options': [{'name': 'Mali-T830 MP2', 'id': 200}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3600 mAh', 'id': 201}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,0 mm', 'id': 204}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '74,8 mm', 'id': 205}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '152,5 mm', 'id': 206}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '181 g', 'id': 207}]
        }]
      }, {
        'id': 16,
        'name': 'Samsung Galaxy S8 G950F Midnight Black',
        'price': 2548,
        'quantity': 18,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s8-g950f-midnight-black-356430,2017/7/pr_2017_7_13_13_46_47_59.jpg',
          'main': true,
          'id': 84
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s8-g950f-midnight-black-356430,pr_2017_3_28_7_54_55_302.jpg',
          'main': false,
          'id': 85
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s8-g950f-midnight-black-356430,pr_2017_3_28_7_54_53_52.jpg',
          'main': false,
          'id': 86
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s8-g950f-midnight-black-356430,pr_2017_3_28_7_55_0_911.jpg',
          'main': false,
          'id': 87
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,samsung-galaxy-s8-g950f-midnight-black-356430,pr_2017_3_28_7_54_58_864.jpg',
          'main': false,
          'id': 88
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Maniak ',
          'date': '2017-04-21T16:06:00.000Z',
          'content': 'Do ceny można mieć zastrzeżenia bo nie powiem jest drogi, ale do wykonania, działania, jakości, absolutnie nie, wszystko na tip top. POLECAM',
          'id': 252
        }, {
          'rating': 6,
          'authorName': 'słowik',
          'date': '2017-04-25T15:19:00.000Z',
          'content': 'Póki co super, szybki, płynny, duży, ciężkawy, ale dla mnie to dobrze bo czuje że trzymam go w ręce i jest bardziej stabilny. Choćbym chciał nie doszukam się wady, zresztą nawet nie chce skoro wydałem juz tyle pieniążków xD Do blokady użyłem tęczówki, świetna rzecz. Kolejna sprawa, dobrze że jest wodoodporny na tą naszą polską deszczową "wiosnę" się przyda i powiem, że w deszczu dotyk działa bezbłędnie xD',
          'id': 253
        }, {
          'rating': 6,
          'authorName': 'Yeter',
          'date': '2017-04-30T09:10:00.000Z',
          'content': 'Zdecydowanie wersja 5.8", mialem w rękach 6.2" ale to już naprawdę za dużo, ale wedle potrzeb. Pod względem specyfikacji i wyglądu jest bardzo dobrze, nie dopatrzyłem się żadnej najmniejszej wady. Model jak dla mnie mistrzowski.',
          'id': 254
        }, {
          'rating': 5,
          'authorName': 'Tj',
          'date': '2017-05-01T10:14:00.000Z',
          'content': 'Telefon tak jak w większości recenzji, które oglądałem jest bardzo dobrze wykonany, spasowany, dobrze leży w dłoni. Wizualnie przyciąga uwagę nawet tych, którzy nowościami  IT zwykle nie są zainteresowani. Co do konstruktywnej krytyki, to zwróciłbym uwagę na dość ograniczone możliwości asystenta Bixby dla polskich użytkowników, a także brak dedykowanego programu do tworzenia krótkich filmów na wzór SmovieC. Generalnie jestem bardzo zadowolony!',
          'id': 255
        }, {
          'rating': 6,
          'authorName': 'Starling',
          'date': '2017-05-02T08:22:00.000Z',
          'content': 'Zamówiony w przedsprzedaży, mam 2000 klatek na pulpicie, dobry dostęp do czytnika linii papilarnych, rewelacyjny design, aparat ze znakomitym autofokusem a wydajność lepsza od komputera za 2-3k.',
          'id': 256
        }, {
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2017-05-03T10:20:00.000Z',
          'content': 'Jest ładny, solidny, ale na co należny zwrócić uwagę to na jakość obrazu, nie sądzę by jakikolwiek inny smartfon miał taki obraz, realistyczny, prawię jak 3D bynajmniej takie wrażenie sprawia, kolory, żywe, na zdjęciu idealnie odwzorowane od rzeczywistości, bardzo szybki autofocus, zatem fajnie da się uchwycić niektóre momenty. Osobiści jestem bardzo zadowolony z zakupu, pomimo ceny, sądzę, że telefon jest tego wart.',
          'id': 257
        }, {
          'rating': 6,
          'authorName': 'artur',
          'date': '2017-05-03T18:32:00.000Z',
          'content': 'Bardzo dobry model, nawet najlepszy. Podzielam zdanie przedmówcy telefon wart ceny. Rewelacja!',
          'id': 258
        }, {
          'rating': 6,
          'authorName': 'brtk',
          'date': '2017-06-09T11:23:00.000Z',
          'content': 'Świetny smartfon. Do jakości wykonania nie można mieć absolutnie żadnych zastrzeżeń. Super rozwiązanie z przyciskiem pod ekranem. Aparat robi na pewno jedne z najlepszych zdjęć jakie można zrobić telefonem. Zero zacięć, wodoodporność, świetne słuchawki w zestawie. Nie wiem jak mogłem żyć bez szybkiego ładowania. Zarzuty można mieć do asystenta, którego najlepiej jakby w ogóle nie było.',
          'id': 259
        }, {
          'rating': 5,
          'authorName': 'Przemek',
          'date': '2017-06-15T09:28:00.000Z',
          'content': 'Telefon zrobił na mnie bardzo dobre wrażenie i jestem z niego zadowolony chociaż osobiście uważam że nad baterią powinni inżynierowie samsunga jeszcze popracować... :)',
          'id': 260
        }, {
          'rating': 6,
          'authorName': 'bartuq',
          'date': '2017-06-21T11:42:00.000Z',
          'content': 'Smartfon przetrwał potężny koncert. Udało mi się wykonać kilka nagrań FHD (można nagrywać również w UHD). Proszę wpisać na youtube "poland bartuq". \nCzytnik linii papilarnych działa idealnie, poradził sobie nawet przy spoconych dłoniach. \nBateria wytrzymuje długo przy normalnym użytkowaniu ale grając nieco dłużej w gry trzeba ładować raz na dzień.\nWszystko działa bez zarzutów, nie wiesza się, jest poręczny. Gry chodzą płynnie. Ładne kolory, wygląd, dobre wykonanie i dźwięk.\n\nPolecam.',
          'id': 261
        }, {
          'rating': 6,
          'authorName': 'krzysztof_r',
          'date': '2017-06-26T15:06:00.000Z',
          'content': 'S8 zastąpił mi Note4. Wydajność znakomita - wiele aplikacji odpalonych na raz nie robi na S8 wrażenia. Ekran w moim egzemplarzu perfekcyjny i beż żadnych przebarwień o jakich mowa w wielu testach. Bateria pozwala na 1-1,5 dnia intensywnego użytkowania. Czytnik linii papilarnych wymaga przyzwyczajenia ale działa bardzo sprawnie. Aparat to potęga - szybkość działania, jakość zdjęć, kolory i ostrość są idealne. Interface dopracowany z masą ciekawych rozwiązań. Jeden minus - moja żona też go chce :)',
          'id': 262
        }, {
          'rating': 6,
          'authorName': 'kamil227',
          'date': '2017-06-28T06:31:00.000Z',
          'content': 'Telefon używam około miesiąca i muszę przyznać że to najlepszy model od Samsunga i jeden z najlepszych smartfonów na rynku.\nPopieram to swoim doświadczeniem, ponieważ korzystałem łącznie z około 40 różnych smartfonów, również tych z najwyższej półki.\nEkran robi ogromne wrażenie a aparat to jest mistrzostwo jeśli chodzi o jakoś zdjęć niezależnie od warunków oświetlenia\nMiałem wątpliwości przed zakupem, jednak jest on wart takiej kwoty :) Polecam',
          'id': 263
        }, {
          'rating': 6,
          'authorName': 'neres909',
          'date': '2017-07-06T10:43:00.000Z',
          'content': 'Aktualnie chyba nie ładniejszego telefonu z androidem niż Galaxy S8. W połączeniu ze swoją specyfikacją, sprawia, że nada się dla nawet najbardziej wymagających. \nSamsung postarał się też od strony software\'u. O dziwo odkąd mam telefon ani raz nie przyszło mi do głowy, aby zmienić np. launcher.\nOsobom które są zainteresowane kupnem tego telefonu polecam dokupienie Starter Kitu, który ma w zestawue ładowarkę bezprzedodową, folię oraz etui w rozsądnej cenie. Telefon bez etui jest strasznie śliski.',
          'id': 264
        }, {
          'rating': 5,
          'authorName': 'Geek',
          'date': '2017-07-18T18:17:00.000Z',
          'content': 'Co tu dożo mówić; najlepszy smartfon na androidzie i chyba najlepszy smartfon w ogóle. Świetna jakość wykonania, super szybkość i wydajność, innowacyjne technologie (skaner tenczówki, samsung connect, opcjonalne akcesoria). Jedyne do czego można się przyczepić do jakość głośnika multimedialnego który jest przeciętny. Nie potrafię też wyłączyć opcji szybkiego ładowania po kablu z ładowarki sieciowej, ale chyba jeszcze się do tego nie dokopałem w ustawieniach systemu. Ogólnie polecam.',
          'id': 265
        }, {'rating': 6, 'authorName': 'dominik', 'date': '2017-08-21T09:02:00.000Z', 'content': 'sztos!', 'id': 266}, {
          'rating': 6,
          'authorName': 'Dawid ',
          'date': '2017-08-29T17:54:00.000Z',
          'content': 'Najlepszy! ekran bez krawędzi, niesamowite jakościowo zdjęcia, żadnych zacięć, w zasadzie, każdy zna ten telefon i komentarza on nie wymaga. Polecam wszystkim fanom androida.',
          'id': 267
        }, {
          'rating': 5,
          'authorName': 'Nr1',
          'date': '2017-09-10T23:28:00.000Z',
          'content': 'Poza przebijającym różem na wyświetlaczu spełnia oczekiwania.',
          'id': 268
        }, {
          'rating': 6,
          'authorName': 'Adam',
          'date': '2017-09-10T23:45:00.000Z',
          'content': 'Fantastyczny smartfon, po 5 miesiącach użytkowania śmiga dokładnie tak szybko jak zaraz po zakupie. Serdecznie polecam',
          'id': 269
        }, {
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2017-09-11T15:14:00.000Z',
          'content': 'Bardzo dobry telefon za dość wysoką cenę. Wcześniej miałem S7 edge który posiadał ten sam aparat a zdjęcia były dość podobne. Warto jednak skusić się na ten telefon z powodu ekstra ekranu i płynnego działania telefonu .',
          'id': 270
        }, {
          'rating': 5,
          'authorName': 'Patryk',
          'date': '2017-09-15T18:12:00.000Z',
          'content': 'Jak dla mnie to najlepszy telefon jaki można kupić na rynku.',
          'id': 271
        }, {
          'rating': 6,
          'authorName': 'Maciek',
          'date': '2017-12-03T12:02:00.000Z',
          'content': 'Bardzo dobry jak na telefon bardzo dobry rozmiar niema na śfieće lepszyh',
          'id': 272
        }, {
          'rating': 6,
          'authorName': 'Ogryzek',
          'date': '2018-02-28T17:13:00.000Z',
          'content': 'Bardzo dobry telefon. Pasuje do dłoni i czytnik linii wcale mi nie przeszkadza w tam gdzie jest. Wybrałem ten, bo często działam w dwóch apkach jednocześnie, to jest mega funkcja. Aparat też super. Niezawodne zdjęcia w nocy, a w dzień to w ogóle sztosik. A i słuchawki AKG w zestawie.',
          'id': 273
        }, {
          'rating': 6,
          'authorName': 'Radzio',
          'date': '2018-04-30T16:35:00.000Z',
          'content': 'Najlepszy smartfon do wszystkiego. Po pierwsze - wooodporność, szybkość, profesjonalny aparat, szybkie i bezprzewodowe ładowanie. Świetnie na nim się gra, słucha muzyki a nawet ogląda filmy. Polecam.',
          'id': 274
        }, {
          'rating': 6,
          'authorName': 'Seleni',
          'date': '2018-05-11T08:15:00.000Z',
          'content': 'Moja opinia nie może być inna jak tylko POZYTYWNA:) Mogę śmiało polecić ten model. Spełnia najważniejsze dla mnie funkcje (dzwonienie&amp;sms,szybkość działania,płynna obsługa aplikacji itd.) ponadto pięknie wygląda, a aparat i wyświetlacz są po prostu rewelacyjne!!!Selfie wychodzą boskie,wszyscy się zachwycają:D PIĘKNY!!!!',
          'id': 275
        }, {
          'rating': 6,
          'authorName': 'Kwicek',
          'date': '2018-05-30T10:37:00.000Z',
          'content': 'Super. Dobry i uniwersalny smartfon. Koniecznie trzeba kupić etui i szkło na ekran, aby nie martwić się o jakieś przypadkowe uszkodzenia. Dużę plusy za: szybkość, ekran, bezprzewodowe ładowanie, wodoodporność. Z mojej strony bez uwag. Polecam.',
          'id': 276
        }, {
          'rating': 6,
          'authorName': 'Natka',
          'date': '2018-06-02T15:43:00.000Z',
          'content': 'Do plusów S8 mogę zaliczyć szybkość i płynność działania, reaguje natychmiastowo nawet podczas wykonywania kilku poleceń i działania aplikacji w tle, nie mam pod tym kątem zastrzeżeń, generalnie sam aparat wykonany jest fenomenalnie - design, wyświetlacz, aparaty i dźwięk - wszystko zaskakuje jakością na mega wysokim poziomie. Po przesiadce z S6 Edge jest BOMBOWO ;)',
          'id': 277
        }, {
          'rating': 6,
          'authorName': 'Prince',
          'date': '2018-06-19T14:03:00.000Z',
          'content': 'na pewno prestiżowy flagowiec, troche bawie się w montaż filmików i nawet z tym sobie radzi, szósteczka!',
          'id': 278
        }, {
          'rating': 6,
          'authorName': 'Usatysfakcjonowany',
          'date': '2018-08-16T15:39:00.000Z',
          'content': 'Gdyby nie ta wodoodporność miałbym bo telefonie, synek wrzucił do wanny, dobrze, że te zabezpieczenia samsung robi na wysokim poziomie. Polecam',
          'id': 279
        }, {
          'rating': 6,
          'authorName': 'Łuki',
          'date': '2018-08-31T20:10:00.000Z',
          'content': 'Fajny sprzęt. Zależalo mi na dużym wyświetlaczu i płynnym działaniu i tu sprawdza się w 100% Czas pracy baterii jest nawet lepszy niż oczekiwałem więc bardzo pro. Design i aparaty po prostu znakomite. Bez problemu mogę polecić S8 ;)',
          'id': 280
        }, {
          'rating': 6,
          'authorName': 'yhyhydryx',
          'date': '2018-09-03T21:51:00.000Z',
          'content': 'Często aktualizacje od Samsunga. Użytkuję pół roku i wszystko w porządku. Fajne jest te krawędziowe menu :)',
          'id': 281
        }, {
          'rating': 6,
          'authorName': 'Kozioł',
          'date': '2018-09-07T05:36:00.000Z',
          'content': 'Błyskawiczna przesyłka, świetnie obąblowana. Smartfon musi mieć dla mnie wydajność niezłego laptopa-sporo na nim pracuje, po opiniach i recenzjach wiedziałem, że się nie zawiodę, zdecydowanie jeden z najlepszy smartfonów.',
          'id': 282
        }],
        'attributes': [{
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2960 x 1440', 'id': 7}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {'name': 'Gniazdo kart nanoSIM - 1 szt.', 'id': 150}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '8.0 Mpix - przód', 'id': 21}, {'name': '12.0 Mpix - tył', 'id': 87}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/1.7 - przedni obiektyw', 'id': 22}, {'name': 'f/1.7 - tylny obiektyw', 'id': 102}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Funkcja ładowania bezprzewodowego', 'id': 34}, {
            'name': 'Pyłoszczelność i wodoszczelność (IP68)',
            'id': 35
          }, {'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {'name': 'Barometr', 'id': 39}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Czujnik Halla', 'id': 43}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }, {'name': 'Skaner tęczówki oka', 'id': 47}, {'name': 'Szkło Corning Gorilla Glass 5', 'id': 214}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel USB typ C', 'id': 51}, {'name': 'Słuchawki', 'id': 54}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}]
        }, {'name': 'Pamięć wbudowana', 'filterable': true, 'id': 4, 'options': [{'name': '64 GB', 'id': 96}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 3000 mAh', 'id': 99}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K', 'id': 103}]
        }, {'name': 'Dual SIM', 'filterable': false, 'id': 17, 'options': [{'name': 'Nie', 'id': 156}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '149 mm', 'id': 159}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '8,0 mm', 'id': 204}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Samsung Exynos 8895 (4 rdzenie, 2.3 GHz + 4 rdzenie, 1.7 GHz)', 'id': 209}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Mali-G71 MP20', 'id': 210}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,8"', 'id': 211}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '68,2 mm', 'id': 212}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '152 g', 'id': 213}]}]
      }, {
        'id': 17,
        'name': 'Honor 7X LTE Dual SIM 64GB czarny',
        'price': 899,
        'quantity': 96,
        'category': {'name': 'Smartphones', 'id': 1},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,honor-7x-lte-dual-sim-64gb-czarny-383485,2017/11/pr_2017_11_16_15_35_39_756_07.jpg',
          'main': true,
          'id': 89
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,honor-7x-lte-dual-sim-64gb-czarny-383485,2017/11/pr_2017_11_16_15_34_30_235_03.jpg',
          'main': false,
          'id': 90
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,honor-7x-lte-dual-sim-64gb-czarny-383485,2017/11/pr_2017_11_16_15_34_19_703_00.jpg',
          'main': false,
          'id': 91
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,honor-7x-lte-dual-sim-64gb-czarny-383485,2017/11/pr_2017_11_16_15_35_17_20_00.jpg',
          'main': false,
          'id': 92
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,honor-7x-lte-dual-sim-64gb-czarny-383485,2017/11/pr_2017_11_16_15_34_26_938_02.jpg',
          'main': false,
          'id': 93
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,honor-7x-lte-dual-sim-64gb-czarny-383485,2017/11/pr_2017_11_16_15_34_23_594_01.jpg',
          'main': false,
          'id': 94
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'daczek84',
          'date': '2017-11-27T12:01:00.000Z',
          'content': 'To taki odpowiednik Mate 10 Lite w lepszej cenie,gdyby mial NFC bylby kompletny.',
          'id': 283
        }, {
          'rating': 6,
          'authorName': 'Wilk',
          'date': '2017-12-12T10:50:00.000Z',
          'content': 'Korzystam z telefonu od kilku dni. Na prawdę fajny sprzęt. Płynność działania super, czytnik linii papilarnych działa na mrugnięcie oka, nawet pokurzę się o stwierdzenie, że szybciej niż w s7. Aparat robi zdjęcia znośne. Ogólnie bardzo pozytywnie oceniam produkt.',
          'id': 284
        }, {
          'rating': 6,
          'authorName': 'Krispl',
          'date': '2017-12-20T16:35:00.000Z',
          'content': 'Świetny telefon za świetne pieniądze! Korzystając z kodu honor-50 jest jeszcze "świetniej" ;) Czytnik linii papilarnych rewelacyjny, ekran ostry, wyraźny z dobrze odwzorowanymi kolorami. Jeśli się wahacie to śmiało zachęcam do zakupu - warto na 100%',
          'id': 285
        }, {
          'rating': 5,
          'authorName': 'Tom',
          'date': '2017-12-25T13:58:00.000Z',
          'content': 'Cena jak na możliwości ok. Jedyny minus to prawdopodobnie tworzywo zamiast szkła na ekranie.',
          'id': 286
        }, {
          'rating': 6,
          'authorName': 'luk2',
          'date': '2017-12-28T15:43:00.000Z',
          'content': 'Dla Mnie rewelacja, dzisiaj właśnie do mnie dotarł i spełnia jak na razie moje oczekiwania w 100 %. Zobaczymy jak się spisze w dalszym użytkowaniu.',
          'id': 287
        }, {
          'rating': 6,
          'authorName': 'syos',
          'date': '2018-01-02T07:37:00.000Z',
          'content': 'Świetny sprzęt, dopasowany do ręki. Szkło przyjemne w dotyku, wszystko płynnie działa bez zacinki. Przyjemny interfejs z ciekawymi funkcjami. Polecam.',
          'id': 288
        }, {
          'rating': 6,
          'authorName': 'Igor',
          'date': '2018-04-28T08:54:00.000Z',
          'content': 'Telefon jest super. Nic się nie psuje. Jedynym minusem jest odstający aparat ale producent daje etui w którym jest wybrzuszenie na ten aparat. Aparat robi świetne zdjęcia. Telefon godny polecenia:-)',
          'id': 289
        }, {'rating': 6, 'authorName': 'Rudy', 'date': '2018-05-06T14:07:00.000Z', 'content': 'Świetny,', 'id': 290}, {
          'rating': 6,
          'authorName': 'Sergio',
          'date': '2018-05-30T07:58:00.000Z',
          'content': 'Dwa dni na baterii przy zwyklym uzytkowaniu. Szybki, duzo pamieci, otrzymal aktualizacje do Oreo. Ekran przecietnej jakosci, kolory sa zniebieszczone ale jest mozliwosc kalibracji kolorow wedlug wlasnych preferencji. Ogolem polecam za ta cene',
          'id': 291
        }, {
          'rating': 6,
          'authorName': 'Wariacina',
          'date': '2018-07-01T13:51:00.000Z',
          'content': 'Robi co ma robić. Polecam!',
          'id': 292
        }, {
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2018-08-09T20:59:00.000Z',
          'content': 'Polecam! Za taką cenę ma wszystko nawet żyroskop* :)',
          'id': 293
        }, {
          'rating': 6,
          'authorName': 'Norbert',
          'date': '2018-08-10T05:55:00.000Z',
          'content': 'Kupiłem go wczoraj i zdąrzyłem go wytestować na swój sposób:kilka aplikacji naraz, transfer danych, przeglądanie internetu,próbne zdjęcia w słońcu i muszę przyznać że telefon jest super  natychmiast reaguje nie zamula mimo wielu czynności naraz...zobaczymy czy za rok czy dwa będzie tak samo dobrze pracował.Duży plus również dla obsługi salonu xkom w Białej Podlaskiej mili uprzejmi ludzie doradzili w paru kwestiach i za to należą im się słowa uznania za dobrze wykonywaną pracę.',
          'id': 294
        }],
        'attributes': [{
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'A-GPS, GLONASS', 'id': 13}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {'name': 'Micro USB - 1 szt.', 'id': 65}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '8.0 Mpix - przód', 'id': 21}, {'name': '16.0 + 2.0 Mpix - tył', 'id': 218}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Magnetometr',
            'id': 38
          }, {'name': 'Czujnik światła', 'id': 41}, {'name': 'Czujnik zbliżenia', 'id': 42}, {
            'name': 'Czytnik linii papilarnych',
            'id': 45
          }, {'name': 'Metalowa obudowa', 'id': 134}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Instrukcja szybkiego uruchomienia telefonu', 'id': 56}, {
            'name': 'Kabel microUSB',
            'id': 77
          }, {'name': 'Zasilacz', 'id': 208}, {'name': 'Etui', 'id': 221}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, IPS', 'id': 62}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 7.0 Nougat', 'id': 67}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': 'FullHD 1080p', 'id': 70}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}]
        }, {'name': 'Kolor', 'filterable': false, 'id': 22, 'options': [{'name': 'Czarny', 'id': 76}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '64 GB', 'id': 96}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2160 x 1080', 'id': 98}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '156,5 mm', 'id': 106}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '165 g', 'id': 183}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Mali-T830 MP2', 'id': 200}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'HiSilicon Kirin 659 (4 rdzenie, 2.36 GHz, A53 + 4 rdzenie, 1.70 GHz, A53)', 'id': 215}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '5,93"', 'id': 216}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-polimerowa 3340 mAh', 'id': 217}]
        }, {'name': 'Grubość', 'filterable': false, 'id': 18, 'options': [{'name': '7,6 mm', 'id': 219}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '75,3 mm', 'id': 220}]
        }]
      }, {
        'id': 18,
        'name': 'Dell Vostro 7580 i7-8750H/16GB/240+1000/10Pro GTX1050Ti',
        'price': 5689,
        'quantity': 15,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-7580-i7-8750h16gb240100010pro-gtx1050ti-435912,2018/6/pr_2018_6_20_21_58_4_869_10.jpg',
          'main': true,
          'id': 95
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-7580-i7-8750h16gb240100010pro-gtx1050ti-435912,2018/6/pr_2018_6_20_21_57_59_290_08.jpg',
          'main': false,
          'id': 96
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-7580-i7-8750h16gb240100010pro-gtx1050ti-435912,2018/6/pr_2018_6_20_21_57_45_165_03.jpg',
          'main': false,
          'id': 97
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-7580-i7-8750h16gb240100010pro-gtx1050ti-435912,2018/6/pr_2018_6_20_21_57_39_103_01.jpg',
          'main': false,
          'id': 98
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-7580-i7-8750h16gb240100010pro-gtx1050ti-435912,2018/6/pr_2018_6_20_21_57_35_571_00.jpg',
          'main': false,
          'id': 99
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-7580-i7-8750h16gb240100010pro-gtx1050ti-435912,2018/6/pr_2018_6_20_21_57_56_369_07.jpg',
          'main': false,
          'id': 100
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Adam',
          'date': '2018-07-14T12:02:00.000Z',
          'content': 'Laptop godny polecenia. Jak na laptop Dell płacimy za jakość, aby sprzęt działał przez wiele lat niezawodnie. Do tego, mamy 3 lata (seria VOSTRO) gwarancji. Laptop ma dwa dyski, szybki na system i programy, drugi na dane. Matryca jest IPS i świetnie odwzorowuje kolory, także bez wahania mogę pokazać projekty podczas spotkań klientom. Moje oczekiwania są spełnione. Praca jest szybka i płynna. W czasie renderowania albo grania wentylatory wchodzą na wyższe obroty i laptop nie grzeje się.',
          'id': 295
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Podświetlana klawiatura', 'id': 255}, {
            'name': 'Białe podświetlenie klawiatury',
            'id': 256
          }, {'name': 'Wydzielona klawiatura numeryczna', 'id': 257}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Możliwość zabezpieczenia linką (port Noble Wedge)', 'id': 259}, {
            'name': 'Wbudowany czytnik linii papilarnych',
            'id': 260
          }, {'name': 'Szyfrowanie TPM', 'id': 261}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i7-8750H (6 rdzeni, od 2.20 GHz do 4.10 GHz, 9 MB cache)', 'id': 222}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '16 GB (SO-DIMM DDR4, 2666MHz)', 'id': 223}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce GTX 1050Ti', 'id': 231}, {'name': '+ Intel UHD Graphics 630', 'id': 232}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '4096 MB GDDR5 (pamięć własna)', 'id': 233}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'USB 3.1 Gen. 1 (USB 3.0) - 3 szt.', 'id': 240}, {
            'name': 'HDMI - 1 szt.',
            'id': 241
          }, {'name': 'Thunderbolt 3 - 1 szt.', 'id': 242}, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 3500 mAh, Li-Ion', 'id': 247}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Pro PL (wersja 64-bitowa)', 'id': 248}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '25 mm', 'id': 250}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '389 mm', 'id': 251}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '270 mm', 'id': 252}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '2,75 kg (z baterią)', 'id': 253}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja x-kom sp. z o.o.)', 'id': 263}]
        }]
      }, {
        'id': 19,
        'name': 'Apple MacBook Air i5/8GB/128GB/HD 6000/Mac OS',
        'price': 3799,
        'quantity': 1,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_39_43_998.jpg',
          'main': true,
          'id': 101
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_39_54_140.jpg',
          'main': false,
          'id': 102
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_6_825.jpg',
          'main': false,
          'id': 103
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_11_991.jpg',
          'main': false,
          'id': 104
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_16_538.jpg',
          'main': false,
          'id': 105
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_20_882.jpg',
          'main': false,
          'id': 106
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Patryk',
          'date': '2017-09-05T19:28:00.000Z',
          'content': 'Mimo, iż od wielu lat niezmienny (poza procesorem) jest to dalej jeden z najlepszych wyborów dla studentów.',
          'id': 296
        }, {
          'rating': 5,
          'authorName': 'Przemysław',
          'date': '2017-09-06T16:55:00.000Z',
          'content': 'Apple od zawsze dba o szczegóły. Tu także nie jest inaczej. Fajnie spasowane elementy, wydajny system. Jedyny minus - słaba kompatybilność z produktami spod znaku okna. Nie mniej jednak od wielu lat posiadam sprzęt spod znaku ugryzionego jabłka i mocno sobie go chwalę.',
          'id': 297
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-07T06:17:00.000Z',
          'content': 'To mój pierwszy MacBook, ale już wiek że nie ostatni. Nigdy nie sądziłem, że korzystanie z komputera może być tak przyjemne. Polecam Wszystkim którzy zastanawiają się nad zakupem - Nie ma nad czym się zastanawiać! Trzeba brać :)',
          'id': 298
        }, {'rating': 6, 'authorName': 'Piotr', 'date': '2017-09-07T07:24:00.000Z', 'content': 'Fajny', 'id': 299}, {
          'rating': 5,
          'authorName': 'Adrian',
          'date': '2017-09-07T10:35:00.000Z',
          'content': 'Wszystko pięknie , tylko wielki minus za wyświetlacz TN co oddaje fatalne odwzorowanie kolorów',
          'id': 300
        }, {
          'rating': 5,
          'authorName': 'Michał',
          'date': '2017-09-07T10:56:00.000Z',
          'content': 'Świetny dla studenta',
          'id': 301
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-07T15:51:00.000Z',
          'content': 'Idealny na początek przygody z macOS!',
          'id': 302
        }, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2017-09-07T19:21:00.000Z',
          'content': 'Bardzo dobry laptop, polecam. Bateria trzyma spokojnie 10 godzin, śmiga aż miło. Oczywiście nie pograsz na nim w Minecrafta na ultra ustawieniach w 4k, ale na uczelnie jest moim zdaniem najlepszym wyborem.',
          'id': 303
        }, {
          'rating': 5,
          'authorName': 'Jakub',
          'date': '2017-09-08T06:16:00.000Z',
          'content': 'Komputer w 100% dopracowany, wydajny i mobilny idealny dla osób dużo podróżujących, jedynym minusem jest ekran ( błyszczący i kiepskie odwzorowanie kolorów)',
          'id': 304
        }, {
          'rating': 3,
          'authorName': 'Oskar',
          'date': '2017-09-08T08:14:00.000Z',
          'content': 'Kupiłem prawie rok temu i nie jestem do końca zadowolony. Owszem bateria trzyma super, klawiatura jest Ok, ale niestety po przesiadce z komputera PC z dużym ekranem praca na tym maku jest dla mnie bardzo uciążliwa i wpatrywanie się w ekran o małej rozdzielczości jest bardzo uciążliwe dla moich oczu. Poza tym jest w miarę Ok',
          'id': 305
        }, {
          'rating': 5,
          'authorName': 'Wojciech',
          'date': '2017-09-08T08:24:00.000Z',
          'content': 'Trochę za mały ekran jak dla mnie',
          'id': 306
        }, {
          'rating': 6,
          'authorName': 'Julia',
          'date': '2017-09-09T08:39:00.000Z',
          'content': 'Super cienki, jak ktoś ceni mobilność i dlugi czas na baterii to laptop będzie idealny. :)',
          'id': 307
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-10T07:04:00.000Z',
          'content': 'Jedyną wadą sprzętu jest rozdzielczość, tutaj niestety musimy pójść na kompromis - dłuższa praca na baterii kosztem rozdzielczości.',
          'id': 308
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2017-09-10T08:29:00.000Z',
          'content': 'Sprzęt zakupiony by działać mobilnie i do tego nadaje się świetnie. Bateria trzyma 10h i prawie wcale się nie przegrzewa. Na minus mała ilość pamięci wewnętrznej, Ale zakup dobrej karty pamięci i adaptera wychodzi taniej i estetycznie wygląda. System stabilny i responsywny. Wada to matryca tn a nie ips. Ogólnie polecam .',
          'id': 309
        }, {
          'rating': 6,
          'authorName': 'Arkadiusz',
          'date': '2017-09-10T14:48:00.000Z',
          'content': 'Właśnie zakupiłem MacBook-a i jestem bardzo zadowolony',
          'id': 310
        }, {
          'rating': 6,
          'authorName': 'Marek',
          'date': '2017-09-20T20:26:00.000Z',
          'content': 'Macbook Air 13 jest jednym z najlepszych dostępnych ultrabooków do kwoty 4 000PLN. Płynność systemu MacOS jest wyśmienita. Bateria bez problemu wytrzymuje 10h, nawet 12h.',
          'id': 311
        }, {
          'rating': 6,
          'authorName': 'UżytkownikMacBooka',
          'date': '2017-10-14T19:25:00.000Z',
          'content': 'W tej cenie nie znajdziemy lepszego laptopa. To świetny wybór na studia, do domu, pracy i tak dalej. Oczywiście w gry nie pogramy, ale w zamian dostajemy macOS-a, czyli szybki, stabilny i dopracowany system. Tak, warto!',
          'id': 312
        }, {
          'rating': 6,
          'authorName': 'jmsmeja',
          'date': '2017-11-09T21:03:00.000Z',
          'content': 'Laptop a dokładnie ultrabook przyzwoity jak na swoją cenę. Szczególnie trafi w gusta osób korzystających z laptopa do szkoły/pracy. Niestety na tym laptopie nie pogramy w gry, nawet jeśli będą nam płynnie chodziły to laptop przy grach jest na tyle gorący że ciężko na spokojnie korzystać z rozrywki. Do oglądania filmów jak najbardziej. Co do ekranu różnice możemy ujrzeć w momencie kiedy obok naszego laptopa stoi laptop od apple z wyższej półki z ekranem retina.',
          'id': 313
        }, {
          'rating': 6,
          'authorName': 'Student',
          'date': '2018-01-15T15:30:00.000Z',
          'content': 'Wraz z lapkiem kupiłem kartę pamięci 128 GB i wyszło taniej niż wersja 256 GB. Noszę go na uczelnię i bateria to istny sztos. Spokojnie 12 h. To nie ściema. Trochę na minus matryca, ale Apple podobno od lat jej nie zmienia. W domu i tak podpinam go pod dodatkowy monitor i lapek działa stabilnie.',
          'id': 314
        }, {
          'rating': 6,
          'authorName': 'Adam Stolarski',
          'date': '2018-02-28T23:27:00.000Z',
          'content': 'Od dwóch lat mamy go pod dachem. Dzielnie sobie radzi. Zakładam ze wydajność będzie w akceptacji jeszcze przez ok 2 lata.',
          'id': 315
        }, {
          'rating': 6,
          'authorName': 'hello24',
          'date': '2018-03-03T10:41:00.000Z',
          'content': 'Mam laptopa od 2 miesięcy. To jest pierwszy laptop z firmy Apple. Długo zastanawiałem się nad zakupem tego sprzętu. Zawsze używałem Windows-a. Myślałem, że system maOS siera jest trudniejszy w obsłudze. Szybko się nauczyłem obsługi. Laptop rewelacyjny. Jak dla mnie to Windows przeszłość. Gorąco polecam zakup tego laptopa',
          'id': 316
        }, {
          'rating': 2,
          'authorName': 'Linux guru',
          'date': '2018-03-08T04:00:00.000Z',
          'content': 'Sprzet slaby. Za te same pieniadze mozna miec znacznie lepszy procesor np i7-8550U gdzie 8MB cache w porownaniu z 3MB tutaj jest powalajace, a 4 rdzenie po 4GHz jest znacznie lepsze niz 2 rdzenie po 3GHz tutaj. Mozna rowniez miec znacznie lepsza matryce full hd ips oraz dysk ssd 256 GB.  Dluga praca na baterii to juz norma dla nowych procesorow. Kompatybilnosc oprogramowania jest oczywiscie kolejnym mankamentem. Sprzet dla ludzi ktorzy jego wartosc okreslaja na podstawie logo na obudowie.',
          'id': 317
        }, {
          'rating': 6,
          'authorName': 'AppleBoy',
          'date': '2018-05-31T13:53:00.000Z',
          'content': 'Macbook Air posiadam i polecam szybki i sprawny. To mój pierwszy komputer z systemem mac os. Polecam dla nowych użytkowników i starszych. Jedną wadą jest wyświetlacz LCD nie Retina.',
          'id': 318
        }, {
          'rating': 6,
          'authorName': '123',
          'date': '2018-06-18T13:01:00.000Z',
          'content': 'Klasyk. Działa szybko i niezawodnie. Na najwyższych obrotach. Design typowy dla jabłuszka sprawdza się podczas służbowych spotkań. Nie buczy, nie grzeje się, świetnie reaguje. Dla mnie sprawdzona marka i najlepszy sprzęt! Polecam&nbsp;',
          'id': 319
        }, {
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2018-08-20T11:25:00.000Z',
          'content': 'Świetny laptop działa bardzo szybko, do tego ma rewelacyjne aplikacje które mogą się przydać każdemu. Na baterii starcza mi na dwa dni pracy. Na plus jest aluminiowa obudowa nie brudzi sie szybko. System jest prosty w obsłudze idealny dla każdego.',
          'id': 320
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Moduł Bluetooth', 'id': 239}, {'name': 'Wi-Fi 802.11 b/g/n/ac', 'id': 273}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'Thunderbolt - 1 szt.', 'id': 275}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5 (2 rdzenie, od 1.8 GHz do 2.9 GHz, 3 MB cache)', 'id': 264}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR3, 1600 MHz)', 'id': 265}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED', 'id': 267}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1440 x 900 (WXGA+)', 'id': 269}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 6000', 'id': 270}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': 'FaceTime HD', 'id': 272}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'macOS Sierra', 'id': 276}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': 'Od 3 do 17 mm', 'id': 277}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '325 mm', 'id': 278}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '227 mm', 'id': 279}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,35 kg (z baterią)', 'id': 280}]
        }, {'name': 'Gwarancja', 'filterable': false, 'id': 25, 'options': [{'name': '12 miesięcy (gwarancja producenta)', 'id': 282}]}]
      }, {
        'id': 20,
        'name': 'Lenovo Ideapad 320s-13 i3-7100U/4GB/128/Win10 Szary',
        'price': 2099,
        'quantity': 81,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320s-13-i3-7100u4gb128win10-szary-407270,2017/10/pr_2017_10_17_12_53_3_71_00.jpg',
          'main': true,
          'id': 107
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320s-13-i3-7100u4gb128win10-szary-407270,2017/10/pr_2017_10_17_12_53_9_977_02.jpg',
          'main': false,
          'id': 108
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320s-13-i3-7100u4gb128win10-szary-407270,2017/10/pr_2017_10_17_12_53_6_555_01.jpg',
          'main': false,
          'id': 109
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320s-13-i3-7100u4gb128win10-szary-407270,2017/10/pr_2017_10_17_12_53_13_103_03.jpg',
          'main': false,
          'id': 110
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320s-13-i3-7100u4gb128win10-szary-407270,2017/10/pr_2017_10_17_12_53_30_995_09.jpg',
          'main': false,
          'id': 111
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320s-13-i3-7100u4gb128win10-szary-407270,2017/10/pr_2017_10_17_12_53_15_931_04.jpg',
          'main': false,
          'id': 112
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Edka',
          'date': '2018-07-21T12:37:00.000Z',
          'content': 'Bardzo dobry laptop za rozsądną cenę. Ma wszystko czego potrzebowałam do efektywnej pracy. Jest dynamiczny i elegancki, wygodnie się na nim pracuje. Ma też matową matrycę, to ułatwia pracę w nasłonecznionych pomieszczeniach.',
          'id': 321
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}, {'name': '4 GB (SO-DIMM DDR4, 2133MHz)', 'id': 284}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'Czytnik kart pamięci microSD - 1 szt.', 'id': 287}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-7100U (2 rdzenie, 2.40 GHz, 3 MB cache)', 'id': 283}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 620', 'id': 285}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '16,9 mm', 'id': 290}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '308 mm', 'id': 291}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '211 mm', 'id': 292}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,20 kg (z baterią)', 'id': 293}]
        }]
      }, {
        'id': 21,
        'name': 'HP Envy 13 i5-8250U/8GB/256PCIe/Win10 FHD',
        'price': 3499,
        'quantity': 79,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-434940,2018/2/pr_2018_2_28_9_30_16_306_03.jpg',
          'main': true,
          'id': 113
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-434940,2018/2/pr_2018_2_28_9_30_23_166_05.jpg',
          'main': false,
          'id': 114
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-434940,2018/2/pr_2018_2_28_9_30_12_899_02.jpg',
          'main': false,
          'id': 115
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-434940,2018/2/pr_2018_2_28_9_30_26_588_06.jpg',
          'main': false,
          'id': 116
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-434940,2018/2/pr_2018_2_28_9_30_9_587_01.jpg',
          'main': false,
          'id': 117
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-434940,2018/2/pr_2018_2_28_9_30_6_305_00.jpg',
          'main': false,
          'id': 118
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Jola',
          'date': '2018-07-08T14:41:00.000Z',
          'content': 'Sporo mnie kosztował ale było warto się zdecydować. Laptop HP ENVY 13,3 jest najlepszym laptopem jaki kiedykolwiek miałam. Bałam się, że będzie trochę za mały po zmianie z poprzedniego laptopa 15,6 ale pracuje się na nim bardzo wygodnie. Poza tym  tego jest lekki i poręczny, nie ma problemu z zabraniem go w dowolne miejsce. Długo pracuje na baterii, i ma świetne wbudowane nagłośnienie. Serdecznie polecam :)',
          'id': 322
        }, {
          'rating': 6,
          'authorName': 'Dona',
          'date': '2018-08-17T19:16:00.000Z',
          'content': 'Dużo podróżuję i laptop jest doskonały do tego celu. Szybko uruchamia się, ekran ma świetne kolory i jest czytelny nawet w słońcu. Błyszczący ekran nie utrudnia korzystania a jest bardzo pomocny, bo jak ktoś patrzy z boku(w pociągu, albo kawiarni) to nie może czytać tego co piszę. Polecam. Jestem bardzo zadowolona. Sprzęt na 7/6!!',
          'id': 323
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {'name': 'Kabel zasilający', 'id': 308}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane cztery głośniki', 'id': 300}, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'Czytnik kart pamięci microSD - 1 szt.', 'id': 287}, {'name': 'USB Typu-C - 2 szt.', 'id': 302}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Aluminiowe wnętrze laptopa', 'id': 254}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR3, 1866 MHz)', 'id': 295}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '8 GB', 'id': 296}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS', 'id': 298}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 6793 mAh, Li-Ion', 'id': 303}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '13,9 mm', 'id': 304}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '305,4 mm', 'id': 305}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '215,6 mm', 'id': 306}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,32 kg (z baterią)', 'id': 307}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }]
      }, {
        'id': 22,
        'name': 'HP 250 G6 i3-6006U/8GB/256/W10 FHD',
        'price': 2129,
        'quantity': 27,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-250-g6-i3-6006u8gb256w10-fhd-392036,2017/11/pr_2017_11_7_18_43_9_210_00.jpg',
          'main': true,
          'id': 119
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-250-g6-i3-6006u8gb256w10-fhd-392036,2017/11/pr_2017_11_7_18_39_55_606_03.jpg',
          'main': false,
          'id': 120
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-250-g6-i3-6006u8gb256w10-fhd-392036,2017/11/pr_2017_11_7_18_39_45_808_00.jpg',
          'main': false,
          'id': 121
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-250-g6-i3-6006u8gb256w10-fhd-392036,2017/11/pr_2017_11_7_18_39_52_465_02.jpg',
          'main': false,
          'id': 122
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-250-g6-i3-6006u8gb256w10-fhd-392036,2017/11/pr_2017_11_7_18_39_49_355_01.jpg',
          'main': false,
          'id': 123
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-250-g6-i3-6006u8gb256w10-fhd-392036,2017/11/pr_2017_11_7_18_39_58_606_04.jpg',
          'main': false,
          'id': 124
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'af2001',
          'date': '2018-02-17T16:37:00.000Z',
          'content': 'Sprzęt spełnia pokładane w nim nadzieje. Jest wykorzystywany do tzw. codziennej pracy, Jeśli chodzi o wygląd i wykonanie jest bardzo dobre, obudowa chyba plastikowa, ale wrażenie jest takie jak by to był metal. Nic nie trzeszczy, nie skrzypi, dobrze spasowane elementy. Czas pokarze czy konstrukcja jest wytrzymała.  Dysk SSD robi swoje, start systemu około 7 sekund, jest cicho i szybko. Bateria po naładowaniu od 100% do 10% około 6h. Deklarowane przez  producenta 7h pewnie jest osiągalne. :)',
          'id': 324
        }, {
          'rating': 6,
          'authorName': 'Raiga',
          'date': '2018-07-27T11:43:00.000Z',
          'content': 'Zamówiłem już 2 sztukę dla znajomych i bardzo polecam. Za te pieniądze idealny sprzęt do pracy i nauki. Jedyny mały minusik za spasowanie sprzętu po modyfikacji przez X-Kom, pare zatrzasków nie było do końca dociśniętych i są jakieś drobne ślady otwierania laptopa ale to już niestety jest chyba bardziej spowodowane tym jak buduje się teraz wszystkie nowe nie "biznesowe" laptopy ;)',
          'id': 325
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {'name': 'Bateria (podstawowa)', 'id': 323}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '8 GB', 'id': 296}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-6006U (2 rdzenie, 2.00 GHz, 3 MB cache)', 'id': 310}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/0', 'id': 312}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW', 'id': 313}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 520', 'id': 315}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '0.3 Mpix', 'id': 316}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 2800 mAh, Li-Ion', 'id': 317}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '24 mm', 'id': 318}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '380 mm', 'id': 319}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '254 mm', 'id': 320}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '2,10 kg (z baterią)', 'id': 321}]
        }]
      }, {
        'id': 23,
        'name': 'Huawei MateBook D 15.6" i5-8250U/8GB/128+1000/Win10 MX150',
        'price': 2999,
        'quantity': 57,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb1281000win10-mx150-426852,2018/4/pr_2018_4_26_8_43_35_776_00.jpg',
          'main': true,
          'id': 125
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb1281000win10-mx150-426852,2018/4/pr_2018_4_24_15_49_28_518_01.jpg',
          'main': false,
          'id': 126
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb1281000win10-mx150-426852,2018/4/pr_2018_4_24_15_49_31_440_02.jpg',
          'main': false,
          'id': 127
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb1281000win10-mx150-426852,2018/4/pr_2018_4_24_15_49_25_643_00.jpg',
          'main': false,
          'id': 128
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb1281000win10-mx150-426852,2018/4/pr_2018_4_24_15_49_34_284_03.jpg',
          'main': false,
          'id': 129
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb1281000win10-mx150-426852,2018/4/pr_2018_4_24_15_49_37_144_04.jpg',
          'main': false,
          'id': 130
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'M4rcin',
          'date': '2018-07-16T19:08:00.000Z',
          'content': 'Wspaniały, lekki i elegancki sprzęt. ma złącze HDMI, więc wygodnie podłączyć monitor do pracy, albo projektor na prezentacji. Dobrze prezentuje się przy spotkaniach z Klientami na mieście. Bateria starcza na 4-6 godzin w moim przypadku wydajnej pracy(tworzenie storn internetowych). Największą zaletą dla mnie jest matryca - matowa IPS ze świetnymi kątami widzenia. Sprzęt zarówno wydajny dla binzesowo, jak i do pisania stron internetowych.',
          'id': 326
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2018-07-29T11:13:00.000Z',
          'content': 'Świetny sprzęt do kwoty około 3300-3400 zł. Bardzo dobra jakość wykonania, solidne głośniki, dobry ekran. Karta graficzna MX150 w mocniejszej wersji o taktowaniu rdzenia 1468 MHz  i TDP 25W.',
          'id': 327
        }, {
          'rating': 6,
          'authorName': 'Piotr',
          'date': '2018-08-18T05:37:00.000Z',
          'content': 'Komputer kupiłem 17.08 w Częstochowie, w pełni spełnia moje oczekiwania, metalowa obudowa, duży dysk, matryca matowa IPS, i5 ósmej generacji, do użytku domowego i nie tylko, w pełni polecam.',
          'id': 328
        }, {
          'rating': 6,
          'authorName': 'Piotr',
          'date': '2018-08-19T09:25:00.000Z',
          'content': 'Polecam, główne zalety, niska waga, ergonomia, matryca IPS matowa, duży dysk, dedykowana grafika. komputer w pełni spełnił moje oczekiwania.',
          'id': 329
        }, {
          'rating': 6,
          'authorName': 'Domino',
          'date': '2018-08-23T09:15:00.000Z',
          'content': 'Inni producenci mogą brać przykład',
          'id': 330
        }, {
          'rating': 6,
          'authorName': 'Johny',
          'date': '2018-08-23T16:17:00.000Z',
          'content': 'Jestem bardzo zadowolony z zakupu. Laptop jest smukły, wygląda delikatnie ale jest solidny, sprawdza się w pracy przy cięższych zadaniach jak i to paru lepszych tytułów w gamingu.',
          'id': 331
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}, {'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik Halla',
            'id': 43
          }, {'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}, {'name': 'Dolby Atmos Sound System', 'id': 329}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '16,9 mm', 'id': 290}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce MX150', 'id': 326}, {'name': '+ Intel UHD Graphics 620', 'id': 327}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3800 mAh, Li-Ion', 'id': 330}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '358 mm', 'id': 331}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '239 mm', 'id': 332}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '1,86 kg (z baterią)', 'id': 333}]}]
      }, {
        'id': 24,
        'name': 'HP OMEN 15 i5-7300HQ/8GB/128+1000/Win10 GTX1050',
        'price': 3599,
        'quantity': 20,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-omen-15-i5-7300hq8gb1281000win10-gtx1050-401556,2017/7/pr_2017_7_20_9_51_30_493.jpg',
          'main': true,
          'id': 131
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-omen-15-i5-7300hq8gb1281000win10-gtx1050-401556,2017/7/pr_2017_7_19_11_20_38_686.jpg',
          'main': false,
          'id': 132
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-omen-15-i5-7300hq8gb1281000win10-gtx1050-401556,2017/7/pr_2017_7_19_11_23_5_201.jpg',
          'main': false,
          'id': 133
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-omen-15-i5-7300hq8gb1281000win10-gtx1050-401556,2017/7/pr_2017_7_20_9_51_15_337.jpg',
          'main': false,
          'id': 134
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-omen-15-i5-7300hq8gb1281000win10-gtx1050-401556,2017/7/pr_2017_7_19_11_21_7_63.jpg',
          'main': false,
          'id': 135
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-omen-15-i5-7300hq8gb1281000win10-gtx1050-401556,2017/7/pr_2017_7_19_11_20_50_156.jpg',
          'main': false,
          'id': 136
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Maciek',
          'date': '2018-07-16T17:24:00.000Z',
          'content': 'Wygląd ok, wykonanie solidne, uruchamia się szybko, ma dysk SSD. 8 GB RAM i szybki procek. Trochę grzeje się w stresie i do grania używam podkładki chłodzącej. Ogólnie polecam, bo dobry laptop. Kolega pomagał mi wybrać i w tej cenie był najtańszy z taką wydajnością.',
          'id': 332
        }, {
          'rating': 6,
          'authorName': 'Remas',
          'date': '2018-08-13T21:37:00.000Z',
          'content': 'Posiadam tego lapka nie ma nic lepszego do grania typowy lapek do grania wszystko co nowe działą',
          'id': 333
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}, {'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Podświetlana klawiatura', 'id': 255}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)',
            'id': 322
          }, {'name': 'Czerwone podświetlenie klawiatury', 'id': 345}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '4096 MB GDDR5 (pamięć własna)', 'id': 233}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {'name': 'Moduł Bluetooth', 'id': 239}, {
            'name': 'Wi-Fi 802.11 b/g/n/ac',
            'id': 273
          }]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'USB 3.1 Gen. 1 (USB 3.0) - 3 szt.', 'id': 240}, {
            'name': 'HDMI - 1 szt.',
            'id': 241
          }, {'name': 'RJ-45 (LAN) - 1 szt.', 'id': 244}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'Mini Display Port - 1 szt.', 'id': 339}, {'name': 'Wejście mikrofonowe - 1 szt.', 'id': 340}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '389 mm', 'id': 251}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-7300HQ (4 rdzenie, od 2.5 GHz do 3.5 GHz, 6MB cache)', 'id': 334}]
        }, {'name': 'Chipset', 'filterable': true, 'id': 41, 'options': [{'name': 'Intel HM175', 'id': 335}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2666MHz)', 'id': 336}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce GTX 1050', 'id': 337}, {'name': '+ Intel HD Graphics 630', 'id': 338}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 4550 mAh, Li-Ion', 'id': 341}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '24,8 mm', 'id': 342}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '278 mm', 'id': 343}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '2,62 kg (z baterią)', 'id': 344}]}]
      }, {
        'id': 25,
        'name': 'ASUS VivoBook R520UA i3-8130U/8GB/240SSD+1TB/Win10',
        'price': 2449,
        'quantity': 52,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431293,2018/5/pr_2018_5_24_13_44_13_433_07.jpg',
          'main': true,
          'id': 137
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431293,2018/5/pr_2018_5_24_13_44_10_433_06.jpg',
          'main': false,
          'id': 138
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431293,2018/5/pr_2018_5_24_13_44_0_323_02.jpg',
          'main': false,
          'id': 139
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431293,2018/5/pr_2018_5_24_13_43_54_886_00.jpg',
          'main': false,
          'id': 140
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431293,2018/5/pr_2018_5_24_13_44_7_902_05.jpg',
          'main': false,
          'id': 141
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431293,2018/5/pr_2018_5_24_13_44_51_527_00.jpg',
          'main': false,
          'id': 142
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2018-07-26T12:27:00.000Z',
          'content': 'Za tą cenę super laptop . Lekki i szybki, 2 dyski to wielka zaleta.',
          'id': 334
        }, {
          'rating': 6,
          'authorName': 'Piotr',
          'date': '2018-08-21T15:53:00.000Z',
          'content': 'Laptop bardzo lekki, cichy, chłodny. Idealny do codziennego użytku. Niektóre gry, również działają z wysokim poziomem klatek/s. Sim City, CS GO (60-125fps) , ETS2 i podobne to nie problem. Nawet mimo dłuższej rozgrywki , laptop się nie nagrzewa, a wentylator jest ledwo słyszalny. Ekran dzięki full HD jest bardzo ładny, obraz ostry, czytanie mniej męczy wzrok. Ogólnie polecam w tej cenie :-)',
          'id': 335
        }, {
          'rating': 6,
          'authorName': 'Witold',
          'date': '2018-08-23T08:33:00.000Z',
          'content': 'Bardzo fajny laptop. Lekki, nie nagrzewa się, bardzo szybko działa (polecam dysk SSD). Dobry wyświetlacz i naprawdę niezłe głośniki.',
          'id': 336
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Wbudowany czytnik linii papilarnych', 'id': 260}, {'name': 'Szyfrowanie TPM', 'id': 261}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-8130U (2 rdzenie, od 2.2 GHz do 3.4 GHz, 4MB cache)', 'id': 346}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3727 mAh, Li-Ion', 'id': 348}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,5 mm', 'id': 349}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '361 mm', 'id': 350}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '243 mm', 'id': 351}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,62 kg (z baterią)', 'id': 352}]
        }]
      }, {
        'id': 26,
        'name': 'ASUS VivoBook R520UA i3-8130U/8GB/256SSD/Win10',
        'price': 2349,
        'quantity': 10,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb256ssdwin10-431325,2018/5/pr_2018_5_24_13_44_13_433_07.jpg',
          'main': true,
          'id': 143
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb256ssdwin10-431325,2018/5/pr_2018_5_24_13_44_10_433_06.jpg',
          'main': false,
          'id': 144
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb256ssdwin10-431325,2018/5/pr_2018_5_24_13_44_0_323_02.jpg',
          'main': false,
          'id': 145
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb256ssdwin10-431325,2018/5/pr_2018_5_24_13_43_54_886_00.jpg',
          'main': false,
          'id': 146
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb256ssdwin10-431325,2018/5/pr_2018_5_24_13_44_7_902_05.jpg',
          'main': false,
          'id': 147
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb256ssdwin10-431325,2018/5/pr_2018_5_24_13_44_51_527_00.jpg',
          'main': false,
          'id': 148
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Lorel',
          'date': '2018-08-02T06:42:00.000Z',
          'content': 'Laptop jest bardzo dobry. Nie gram w gry i chciałem uniwersalny - wydajny, aby nie był ciężki. Obudowa wygląda dobrze(co prawda jest to materiał platikopodobny, ale bardzo trwały i dobrze wykończony). Matryca ma świetne kolory i jest bardzo czytelna(Full HD). Sprzęt na miarę moich oczekiwań. Bateria wytrzymuje koło 4 godizn.',
          'id': 337
        }, {
          'rating': 6,
          'authorName': 'Kasia',
          'date': '2018-08-03T15:13:00.000Z',
          'content': 'Lekki i wydajny. Jestem zadowolona, że w tej cenie udało mi się kupić laptopa na studia. Polecam',
          'id': 338
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Wbudowany czytnik linii papilarnych', 'id': 260}, {'name': 'Szyfrowanie TPM', 'id': 261}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Dysk SSD M.2 PCIe',
          'filterable': false,
          'id': 39,
          'options': [{'name': '256 GB', 'id': 297}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-8130U (2 rdzenie, od 2.2 GHz do 3.4 GHz, 4MB cache)', 'id': 346}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3727 mAh, Li-Ion', 'id': 348}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,5 mm', 'id': 349}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '361 mm', 'id': 350}]
        }, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '243 mm', 'id': 351}]
        }, {
          'name': 'Miejsce na dodatkowy wewnętrzny dysk SATA',
          'filterable': false,
          'id': 43,
          'options': [{'name': 'Możliwość montażu dysku SATA (elementy montażowe w zestawie - sanki)', 'id': 353}]
        }, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,55 kg (z baterią)', 'id': 354}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 355}]
        }]
      }, {
        'id': 27,
        'name': 'Apple MacBook Air i5/8GB/128GB/HD 6000/Mac OS',
        'price': 3799,
        'quantity': 15,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_39_43_998.jpg',
          'main': true,
          'id': 101
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_39_54_140.jpg',
          'main': false,
          'id': 102
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_6_825.jpg',
          'main': false,
          'id': 103
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_11_991.jpg',
          'main': false,
          'id': 104
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_16_538.jpg',
          'main': false,
          'id': 105
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-air-i58gb128gbhd-6000mac-os-368639,pr_2016_4_28_12_38_20_882.jpg',
          'main': false,
          'id': 106
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Patryk',
          'date': '2017-09-05T19:28:00.000Z',
          'content': 'Mimo, iż od wielu lat niezmienny (poza procesorem) jest to dalej jeden z najlepszych wyborów dla studentów.',
          'id': 339
        }, {
          'rating': 5,
          'authorName': 'Przemysław',
          'date': '2017-09-06T16:55:00.000Z',
          'content': 'Apple od zawsze dba o szczegóły. Tu także nie jest inaczej. Fajnie spasowane elementy, wydajny system. Jedyny minus - słaba kompatybilność z produktami spod znaku okna. Nie mniej jednak od wielu lat posiadam sprzęt spod znaku ugryzionego jabłka i mocno sobie go chwalę.',
          'id': 340
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-07T06:17:00.000Z',
          'content': 'To mój pierwszy MacBook, ale już wiek że nie ostatni. Nigdy nie sądziłem, że korzystanie z komputera może być tak przyjemne. Polecam Wszystkim którzy zastanawiają się nad zakupem - Nie ma nad czym się zastanawiać! Trzeba brać :)',
          'id': 341
        }, {'rating': 6, 'authorName': 'Piotr', 'date': '2017-09-07T07:24:00.000Z', 'content': 'Fajny', 'id': 342}, {
          'rating': 5,
          'authorName': 'Adrian',
          'date': '2017-09-07T10:35:00.000Z',
          'content': 'Wszystko pięknie , tylko wielki minus za wyświetlacz TN co oddaje fatalne odwzorowanie kolorów',
          'id': 343
        }, {
          'rating': 5,
          'authorName': 'Michał',
          'date': '2017-09-07T10:56:00.000Z',
          'content': 'Świetny dla studenta',
          'id': 344
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-07T15:51:00.000Z',
          'content': 'Idealny na początek przygody z macOS!',
          'id': 345
        }, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2017-09-07T19:21:00.000Z',
          'content': 'Bardzo dobry laptop, polecam. Bateria trzyma spokojnie 10 godzin, śmiga aż miło. Oczywiście nie pograsz na nim w Minecrafta na ultra ustawieniach w 4k, ale na uczelnie jest moim zdaniem najlepszym wyborem.',
          'id': 346
        }, {
          'rating': 5,
          'authorName': 'Jakub',
          'date': '2017-09-08T06:16:00.000Z',
          'content': 'Komputer w 100% dopracowany, wydajny i mobilny idealny dla osób dużo podróżujących, jedynym minusem jest ekran ( błyszczący i kiepskie odwzorowanie kolorów)',
          'id': 347
        }, {
          'rating': 3,
          'authorName': 'Oskar',
          'date': '2017-09-08T08:14:00.000Z',
          'content': 'Kupiłem prawie rok temu i nie jestem do końca zadowolony. Owszem bateria trzyma super, klawiatura jest Ok, ale niestety po przesiadce z komputera PC z dużym ekranem praca na tym maku jest dla mnie bardzo uciążliwa i wpatrywanie się w ekran o małej rozdzielczości jest bardzo uciążliwe dla moich oczu. Poza tym jest w miarę Ok',
          'id': 348
        }, {
          'rating': 5,
          'authorName': 'Wojciech',
          'date': '2017-09-08T08:24:00.000Z',
          'content': 'Trochę za mały ekran jak dla mnie',
          'id': 349
        }, {
          'rating': 6,
          'authorName': 'Julia',
          'date': '2017-09-09T08:39:00.000Z',
          'content': 'Super cienki, jak ktoś ceni mobilność i dlugi czas na baterii to laptop będzie idealny. :)',
          'id': 350
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-09-10T07:04:00.000Z',
          'content': 'Jedyną wadą sprzętu jest rozdzielczość, tutaj niestety musimy pójść na kompromis - dłuższa praca na baterii kosztem rozdzielczości.',
          'id': 351
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2017-09-10T08:29:00.000Z',
          'content': 'Sprzęt zakupiony by działać mobilnie i do tego nadaje się świetnie. Bateria trzyma 10h i prawie wcale się nie przegrzewa. Na minus mała ilość pamięci wewnętrznej, Ale zakup dobrej karty pamięci i adaptera wychodzi taniej i estetycznie wygląda. System stabilny i responsywny. Wada to matryca tn a nie ips. Ogólnie polecam .',
          'id': 352
        }, {
          'rating': 6,
          'authorName': 'Arkadiusz',
          'date': '2017-09-10T14:48:00.000Z',
          'content': 'Właśnie zakupiłem MacBook-a i jestem bardzo zadowolony',
          'id': 353
        }, {
          'rating': 6,
          'authorName': 'Marek',
          'date': '2017-09-20T20:26:00.000Z',
          'content': 'Macbook Air 13 jest jednym z najlepszych dostępnych ultrabooków do kwoty 4 000PLN. Płynność systemu MacOS jest wyśmienita. Bateria bez problemu wytrzymuje 10h, nawet 12h.',
          'id': 354
        }, {
          'rating': 6,
          'authorName': 'UżytkownikMacBooka',
          'date': '2017-10-14T19:25:00.000Z',
          'content': 'W tej cenie nie znajdziemy lepszego laptopa. To świetny wybór na studia, do domu, pracy i tak dalej. Oczywiście w gry nie pogramy, ale w zamian dostajemy macOS-a, czyli szybki, stabilny i dopracowany system. Tak, warto!',
          'id': 355
        }, {
          'rating': 6,
          'authorName': 'jmsmeja',
          'date': '2017-11-09T21:03:00.000Z',
          'content': 'Laptop a dokładnie ultrabook przyzwoity jak na swoją cenę. Szczególnie trafi w gusta osób korzystających z laptopa do szkoły/pracy. Niestety na tym laptopie nie pogramy w gry, nawet jeśli będą nam płynnie chodziły to laptop przy grach jest na tyle gorący że ciężko na spokojnie korzystać z rozrywki. Do oglądania filmów jak najbardziej. Co do ekranu różnice możemy ujrzeć w momencie kiedy obok naszego laptopa stoi laptop od apple z wyższej półki z ekranem retina.',
          'id': 356
        }, {
          'rating': 6,
          'authorName': 'Student',
          'date': '2018-01-15T15:30:00.000Z',
          'content': 'Wraz z lapkiem kupiłem kartę pamięci 128 GB i wyszło taniej niż wersja 256 GB. Noszę go na uczelnię i bateria to istny sztos. Spokojnie 12 h. To nie ściema. Trochę na minus matryca, ale Apple podobno od lat jej nie zmienia. W domu i tak podpinam go pod dodatkowy monitor i lapek działa stabilnie.',
          'id': 357
        }, {
          'rating': 6,
          'authorName': 'Adam Stolarski',
          'date': '2018-02-28T23:27:00.000Z',
          'content': 'Od dwóch lat mamy go pod dachem. Dzielnie sobie radzi. Zakładam ze wydajność będzie w akceptacji jeszcze przez ok 2 lata.',
          'id': 358
        }, {
          'rating': 6,
          'authorName': 'hello24',
          'date': '2018-03-03T10:41:00.000Z',
          'content': 'Mam laptopa od 2 miesięcy. To jest pierwszy laptop z firmy Apple. Długo zastanawiałem się nad zakupem tego sprzętu. Zawsze używałem Windows-a. Myślałem, że system maOS siera jest trudniejszy w obsłudze. Szybko się nauczyłem obsługi. Laptop rewelacyjny. Jak dla mnie to Windows przeszłość. Gorąco polecam zakup tego laptopa',
          'id': 359
        }, {
          'rating': 2,
          'authorName': 'Linux guru',
          'date': '2018-03-08T04:00:00.000Z',
          'content': 'Sprzet slaby. Za te same pieniadze mozna miec znacznie lepszy procesor np i7-8550U gdzie 8MB cache w porownaniu z 3MB tutaj jest powalajace, a 4 rdzenie po 4GHz jest znacznie lepsze niz 2 rdzenie po 3GHz tutaj. Mozna rowniez miec znacznie lepsza matryce full hd ips oraz dysk ssd 256 GB.  Dluga praca na baterii to juz norma dla nowych procesorow. Kompatybilnosc oprogramowania jest oczywiscie kolejnym mankamentem. Sprzet dla ludzi ktorzy jego wartosc okreslaja na podstawie logo na obudowie.',
          'id': 360
        }, {
          'rating': 6,
          'authorName': 'AppleBoy',
          'date': '2018-05-31T13:53:00.000Z',
          'content': 'Macbook Air posiadam i polecam szybki i sprawny. To mój pierwszy komputer z systemem mac os. Polecam dla nowych użytkowników i starszych. Jedną wadą jest wyświetlacz LCD nie Retina.',
          'id': 361
        }, {
          'rating': 6,
          'authorName': '123',
          'date': '2018-06-18T13:01:00.000Z',
          'content': 'Klasyk. Działa szybko i niezawodnie. Na najwyższych obrotach. Design typowy dla jabłuszka sprawdza się podczas służbowych spotkań. Nie buczy, nie grzeje się, świetnie reaguje. Dla mnie sprawdzona marka i najlepszy sprzęt! Polecam&nbsp;',
          'id': 362
        }, {
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2018-08-20T11:25:00.000Z',
          'content': 'Świetny laptop działa bardzo szybko, do tego ma rewelacyjne aplikacje które mogą się przydać każdemu. Na baterii starcza mi na dwa dni pracy. Na plus jest aluminiowa obudowa nie brudzi sie szybko. System jest prosty w obsłudze idealny dla każdego.',
          'id': 363
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Moduł Bluetooth', 'id': 239}, {'name': 'Wi-Fi 802.11 b/g/n/ac', 'id': 273}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'Thunderbolt - 1 szt.', 'id': 275}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5 (2 rdzenie, od 1.8 GHz do 2.9 GHz, 3 MB cache)', 'id': 264}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR3, 1600 MHz)', 'id': 265}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED', 'id': 267}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1440 x 900 (WXGA+)', 'id': 269}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 6000', 'id': 270}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': 'FaceTime HD', 'id': 272}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'macOS Sierra', 'id': 276}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': 'Od 3 do 17 mm', 'id': 277}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '325 mm', 'id': 278}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '227 mm', 'id': 279}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,35 kg (z baterią)', 'id': 280}]
        }, {'name': 'Gwarancja', 'filterable': false, 'id': 25, 'options': [{'name': '12 miesięcy (gwarancja producenta)', 'id': 282}]}]
      }, {
        'id': 28,
        'name': 'Acer Nitro 5 i7-7700HQ/16GB/240+1000/Win10 GTX1050Ti',
        'price': 4399,
        'quantity': 70,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-nitro-5-i7-7700hq16gb2401000win10-gtx1050ti-434854,2017/7/pr_2017_7_17_8_56_5_580.jpg',
          'main': true,
          'id': 149
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-nitro-5-i7-7700hq16gb2401000win10-gtx1050ti-434854,2017/7/pr_2017_7_17_8_55_48_156.jpg',
          'main': false,
          'id': 150
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-nitro-5-i7-7700hq16gb2401000win10-gtx1050ti-434854,2017/7/pr_2017_7_17_8_54_15_898.jpg',
          'main': false,
          'id': 151
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-nitro-5-i7-7700hq16gb2401000win10-gtx1050ti-434854,2017/7/pr_2017_7_17_8_54_22_821.jpg',
          'main': false,
          'id': 152
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-nitro-5-i7-7700hq16gb2401000win10-gtx1050ti-434854,2017/7/pr_2017_7_17_8_55_37_187.jpg',
          'main': false,
          'id': 153
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-nitro-5-i7-7700hq16gb2401000win10-gtx1050ti-434854,2017/7/pr_2017_7_17_8_55_18_138.jpg',
          'main': false,
          'id': 154
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'mikołaj',
          'date': '2018-08-09T22:47:00.000Z',
          'content': 'bardzo dobry laptop. Polecam !',
          'id': 364
        }, {'rating': 6, 'authorName': 'Michał', 'date': '2018-08-17T05:09:00.000Z', 'content': 'Polecam !', 'id': 365}, {
          'rating': 6,
          'authorName': 'Daniel',
          'date': '2018-08-28T11:44:00.000Z',
          'content': 'Miło mnie zaskoczył. Jestem zadowolony z zakupu i gorąco polecam!',
          'id': 366
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Wydzielona klawiatura numeryczna', 'id': 257}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}, {
            'name': 'Czerwone podświetlenie klawiatury',
            'id': 345
          }]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce GTX 1050Ti', 'id': 231}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '4096 MB GDDR5 (pamięć własna)', 'id': 233}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.', 'id': 286}, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i7-7700HQ (4 rdzenie, od 2.80 GHz do 3.80 GHz, 6 MB cache)', 'id': 356}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '16 GB (SO-DIMM DDR4, 2133MHz)', 'id': 357}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 3220 mAh, Li-Ion', 'id': 358}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '26,6 mm', 'id': 359}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '390 mm', 'id': 360}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '266 mm', 'id': 361}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '2,70 kg (z baterią)', 'id': 362}]
        }]
      }, {
        'id': 29,
        'name': 'ASUS VivoBook S14 S410UA i5-8250U/8GB/256SSD/Win10',
        'price': 2949,
        'quantity': 1,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410ua-i5-8250u8gb256ssdwin10-403868,2018/1/pr_2018_1_22_7_57_7_910_00.jpg',
          'main': true,
          'id': 155
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410ua-i5-8250u8gb256ssdwin10-403868,2018/1/pr_2018_1_22_7_56_13_921_01.jpg',
          'main': false,
          'id': 156
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410ua-i5-8250u8gb256ssdwin10-403868,2018/1/pr_2018_1_22_7_56_21_422_03.jpg',
          'main': false,
          'id': 157
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410ua-i5-8250u8gb256ssdwin10-403868,2018/1/pr_2018_1_22_7_56_17_828_02.jpg',
          'main': false,
          'id': 158
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410ua-i5-8250u8gb256ssdwin10-403868,2018/1/pr_2018_1_22_7_39_55_259_01.jpg',
          'main': false,
          'id': 159
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410ua-i5-8250u8gb256ssdwin10-403868,2018/1/pr_2018_1_22_7_56_10_124_00.jpg',
          'main': false,
          'id': 160
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'obudowa-pytanie',
          'date': '2018-02-16T12:57:00.000Z',
          'content': 'Dzień dobry,\nCzy obudowa jest w całości z aluminium czy tylko ta część za wyświetlaczem?',
          'id': 367
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-03-03T09:53:00.000Z',
          'content': 'Ramka wokół matrycy została wykonana z tworzywa sztucznego. Pozostałe elementy obudowy są wykonane z aluminium (spód i górna pokrywa). Zachęcamy do kontaktu z naszymi doradcami pod numerem 34 377 00 00. Chętnie pomożemy w przygotowaniu zamówienia.',
          'id': 368
        }, {
          'rating': 6,
          'authorName': 'Tajpaner',
          'date': '2018-02-21T07:00:00.000Z',
          'content': 'Super klawiatura, ciche działanie urządzenia, matryca bardzo dobre odwzorowanie kolorów (wieczorem nie ma efektu zmęczenia oczu) - metalowa obudowa bardzo fajnie się sprawdza, nie ma poodbijanych "paluchów" - super alternatywa dla Zenbooka. Polecam szczerze.',
          'id': 369
        }, {
          'rating': 2,
          'authorName': 'Maciek',
          'date': '2018-03-16T20:57:00.000Z',
          'content': 'Zwróciłem tego laptopa po 3 dniach użytkowania, jego jakość wykonania jest śmieszna. Metalowa jest tylko wierzchnia część obudowy, która ugina się pod delikatnym naciskiem, reszta jest plastikowa, diody podświetlające klawiaturę rażą w oczy, a spasowanie touchpada pozostawia wiele do życzenia, wydaje się jakby nie był do końca przytwierdzony do reszty sprzętu.\n\nDo dobrych rzeczy zaliczam świetny czytnik linii papilarnych oraz małe ramki dookoła ekranu.',
          'id': 370
        }, {
          'rating': 6,
          'authorName': 'Aleksandra',
          'date': '2018-04-23T18:23:00.000Z',
          'content': 'Super. Jestem bardzo zadowolona. Laptop ma niewielkie ramki, ale to widać nawet na zdjęciach. Nie gram w gry, a korzystam z komputera do nauki na studiach, oglądania filmów i serfowania po internecie. Asus Vivobook jest bardzo lekki i mogę wszędzie zabrać go ze sobą. Jest bardzo szybki. Dziękuję Panu z Bonarki za pomoc w wyborze. Polecam. X-kom to bardzo dobry i profesjonalny sklep.',
          'id': 371
        }, {
          'rating': 6,
          'authorName': 'Mik',
          'date': '2018-05-11T12:15:00.000Z',
          'content': 'Świetny lapek. Mam go jakiś drugi miesiąc, działa poprawnie i nie ma z nim problemów. POLECAM ZAKUP. Lapek jest smukły i zajmuje niewiele miejsca. Pełna profeska :)',
          'id': 372
        }, {
          'rating': 5,
          'authorName': 'Grzegorz',
          'date': '2018-06-16T12:12:00.000Z',
          'content': 'Laptop bardzo fajny, ładny wizualnie, szybki (system jest gotowy do działania w kilka sekund po włączeniu) poza tym śmiga aż miło. Jedyny minus to touchpad, który lekko się ugina, chyba jest to drobna wada tych modeli bo to samo występuje w innym bardzo podobnym modelu o kilkaset złoty droższym. Poza tym jeśli chodzi o ten konkretnie model to sprawdzałem działanie touchpada w dwóch różnych sklepach i było to samo. Poza tym ogólnie polecam. Jak dla mnie jakość wykonania jest całkiem niezła.',
          'id': 373
        }, {
          'rating': 6,
          'authorName': 'Majka',
          'date': '2018-06-27T17:58:00.000Z',
          'content': 'Laptop Asus działa u mnie bez zastrzeżeń od około 4 miesięcy. Jest bardzo smukły i elegancki. Pracuje płynnie i reaguje bardzo szybko. Korzystam z niego do pracy i internetu i na tym poziomie nie mam uwag. Zabieram go też na wyjazdy służbowe i tu dużym udogodnieniem jest dość pojemna bateria. Moim zdaniem dobra inwestycja i polecam...',
          'id': 374
        }, {
          'rating': 6,
          'authorName': 'Justin',
          'date': '2018-07-17T21:12:00.000Z',
          'content': 'Bardzo lekki, szybki, ma szerokie kąty widzenia matrycy i dobre kolory w tak smukłej obudowie ma  dużo portów- 3 usb, hdmi, usb C. Na moje potrzeby świetny laptop do lightroom i internetu.',
          'id': 375
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-08-08T03:38:00.000Z',
          'content': 'świetny sprzęt. mam go od kilku miesięcy i jestem w 100% zadowolony',
          'id': 376
        }, {
          'rating': 6,
          'authorName': 'Andrzej',
          'date': '2018-08-27T15:21:00.000Z',
          'content': 'Córka bardzo zadowolona, w tym roku zaczyna studia, chciała mieć coś poręcznego, małego i dobrej jakości. Laptop sprawdzam od tygodnia, pracuje się na nim szybko, widać, że  Asus użył dobrych materiałów. Dziękuje za uprzejmą pomoc na infolinii.',
          'id': 377
        }, {
          'rating': 6,
          'authorName': 'Janusz',
          'date': '2018-09-04T11:59:00.000Z',
          'content': 'Do prowadzenia sklepu internetowego nadaje się świetnie, Dysk SSD 256 GB jak najbardziej  nadaje rozpędu. Ma matową matrycę, jest lekki i dość solidnie wykonany, Myślę, że to niezły wybór do 3 tysięcy złotych.',
          'id': 378
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Wbudowany czytnik linii papilarnych', 'id': 260}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Dysk SSD M.2 PCIe',
          'filterable': false,
          'id': 39,
          'options': [{'name': '256 GB', 'id': 297}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/0', 'id': 312}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Miejsce na dodatkowy wewnętrzny dysk SATA',
          'filterable': false,
          'id': 43,
          'options': [{'name': 'Możliwość montażu dysku SATA (elementy montażowe w zestawie - sanki)', 'id': 353}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 355}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '20 GB', 'id': 363}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, EWV', 'id': 364}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '14,0"', 'id': 365}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3653 mAh, Li-Ion', 'id': 366}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19 mm', 'id': 367}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '326 mm', 'id': 368}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '226 mm', 'id': 369}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,43 kg (z baterią)', 'id': 370}]
        }]
      }, {
        'id': 30,
        'name': 'HP Envy 13 i5-8250U/8GB/256PCIe/Win10 FHD',
        'price': 3849,
        'quantity': 42,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-412023,2018/2/pr_2018_2_28_9_30_16_306_03.jpg',
          'main': true,
          'id': 161
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-412023,2018/2/pr_2018_2_28_9_30_23_166_05.jpg',
          'main': false,
          'id': 162
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-412023,2018/2/pr_2018_2_28_9_30_12_899_02.jpg',
          'main': false,
          'id': 163
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-412023,2018/2/pr_2018_2_28_9_30_26_588_06.jpg',
          'main': false,
          'id': 164
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-412023,2018/2/pr_2018_2_28_9_30_9_587_01.jpg',
          'main': false,
          'id': 165
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-envy-13-i5-8250u8gb256pciewin10-fhd-412023,2018/2/pr_2018_2_28_9_30_6_305_00.jpg',
          'main': false,
          'id': 166
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Mariusz',
          'date': '2018-03-25T18:07:00.000Z',
          'content': 'Trudno kupić jasny laptop, cienki, mały, lekki, z CPU 8-mej generacji, z SSD, z minimum 8 GB pamieci RAM aby być w 100% zadowolonym ( w tym laptopie najbardziej denerwuje mnie użyta pamięci RAM DDR3, czemu nie DDR4?. miłą niespodzianką jest fakt że WiFi działa na 5GHz, dźwięk jest niezły jak na laptop, w dodatku na taki malutki, tu duży plus. W takiej cenie nic lepszego się nie znajdzie, dziwne to bo przecież koszt tego laptopa to 4000 zł., taniej  już można kupić niezły laptop gamingowy.',
          'id': 379
        }, {
          'rating': 6,
          'authorName': 'Luiza',
          'date': '2018-06-29T19:30:00.000Z',
          'content': 'Działa i wygląda bosko. Nie mam zastrzeżeń do sprzetu. Hp nigdy mnie jeszcze nie zawiodło. Komputer jest szybki, nie przegrzewa się i dziala bardzo cicho nawet pod dużym obciążeniem. Nie wstyd sie z nim pokazać - wizualnie bomba. Zdecydowanie polecam bo jest wart tych pieniedzy!',
          'id': 380
        }, {
          'rating': 6,
          'authorName': 'igor',
          'date': '2018-08-08T15:11:00.000Z',
          'content': 'Zgrabny, ładny i wygodny laptop. Na plus lekkość, lekki i nowoczesny design, kompaktowe rozmiary i stosunkowo mocne podzespoły. Ogólne wrażenie świetne. Jeśli szukać minusów, to brak możliwości zmiany natężenia podświetlania klawiatury i spora ramka dookoła ekranu (do tego jednak szybko się przyzwyczaiłem).',
          'id': 381
        }, {
          'rating': 6,
          'authorName': 'ania',
          'date': '2018-08-09T14:06:00.000Z',
          'content': 'Jestem posiadaczką tego modelu od niecałego miesiąca, ale z czystym sumieniem mogę go polecić. Podczas zakupu, jak to kobieta kierowałam się głównie..wyglądem ;). Laptop jest przede wszystkim bardzo smukły, wygląda naprawdę designersko i "drogo". Wcześniej korzystałam z jabuszka, ale muszę przyznać, że obecny sprzęt plasuje się równie dobrze w rankingu jak nie lepiej. Z racji wykonywanego zawodu, bardzo ważnym kryterium była dla mnie również waga sprzętu (często podróżuje). polecam',
          'id': 382
        }, {
          'rating': 6,
          'authorName': 'hermes',
          'date': '2018-08-27T15:21:00.000Z',
          'content': 'Zla mnie głównym atutem jest wyświetlacz i dobre odzworowanie kolorów. Szczególnie w materiałach wideo lepszej jakości np w 4k. Drugi wiodący plus to bateria - potwierdzam, że spokojnie 9-10 godz mozna pracowac bez ladowania :)!',
          'id': 383
        }, {
          'rating': 6,
          'authorName': 'Janek10',
          'date': '2018-08-28T13:41:00.000Z',
          'content': 'Envy przekonał jakością dźwięku i pięknym obrazem, przy wyborze zależało mi głównie na tym. Jeśli miałbym powiedzieć coś więcej to przede wszystkim jest szybki i bateria jest potwornie mocna, wytrzyma ponad 10h bez ładowania.',
          'id': 384
        }, {
          'rating': 6,
          'authorName': 'fan_HP',
          'date': '2018-08-28T14:38:00.000Z',
          'content': 'Szukałem komputera, który przyda się w pracy jak i domu. Sprawdza się zarówno tu i tu, w pracy ważna jest wydajność i żywotność baterii, w domu mogę skupic sie na obrazie przy filmach albo zdjęciach, komputer jest warty zakupu',
          'id': 385
        }, {
          'rating': 6,
          'authorName': 'Kamil',
          'date': '2018-08-28T20:03:00.000Z',
          'content': 'Dużo nie trzeba pisać, jest to naprawdę fajny laptop. Świetnie sie na nim ogląda filmy, do tego audio od bang&amp;olufsen. Bogate wnętrze, warto go kupić! Od trzech miesięcy uzywam i na razie (odpukac) zero problemów',
          'id': 386
        }, {
          'rating': 6,
          'authorName': 'kmag',
          'date': '2018-08-29T12:27:00.000Z',
          'content': 'Jak najbardziej polecam, laptop nie dość że świetnie działa to robi również spore wrażenie wizualne... jest piękny. Wszystko działa jak powinno, przyjemnie ogląda się filmy, jest szybki, lekki i do niczego nie można się przyczepić.',
          'id': 387
        }, {
          'rating': 5,
          'authorName': 'Radek',
          'date': '2018-08-29T13:24:00.000Z',
          'content': 'Szczerz polecam ten komputer, korzystam z niego od trzech miesięcy i jeszcze mnie nie zawidódł, jego głowne cechy to solidność, trwałość i wydajność',
          'id': 388
        }, {
          'rating': 6,
          'authorName': 'roztropny',
          'date': '2018-08-29T14:37:00.000Z',
          'content': 'Po tym jak pozytywnie zaskoczyło mnie envy nie sądze, żebym zaufał innej marce. Szybkość i wydajność intel i5, ram ddr3, świetna jakośc obrazu full hd 4k, ekran ips, świetna karta sieciowa, to wszystko mówi za siebie',
          'id': 389
        }, {
          'rating': 6,
          'authorName': 'Leon',
          'date': '2018-08-30T06:27:00.000Z',
          'content': 'Envy zachwyca swymi parametrami, może zawstydzić nie jeden komputer z wyższej półki, polecam każdemu kto szuka komputera, który odnajdzie się w każdej sytuacji.',
          'id': 390
        }, {
          'rating': 6,
          'authorName': 'zachwycony',
          'date': '2018-08-30T21:35:00.000Z',
          'content': 'Do envy podszedłem dość sceptycznie, jednak czytając opinie i testy powoli doszedłem do momentu, w którym uznałem, że musze go mieć. Zakupu nie żałuję, jest to świetny produkt, imponuje szybkościa, żywotnością o wykonaniu nie wspominając.',
          'id': 391
        }, {
          'rating': 6,
          'authorName': 'cleo',
          'date': '2018-08-31T05:55:00.000Z',
          'content': 'Kupiłam głównie ze względu na jego wygląd, jest po prostu niesamowicie piękny. Ważne są też jego rozmiary i niezwykła wydajność, zajmuje tyle miejsca co teczka, zmieści się nawet do damskiej torebki :)',
          'id': 392
        }, {
          'rating': 6,
          'authorName': 'Tymek',
          'date': '2018-08-31T11:23:00.000Z',
          'content': 'Zakup envy był spontanicznym wyborem, bardzo spodobał mi sie gdy byłem w sklepie, mój komputer był juz stary, dlatego zdecydowałem sie na zmianę. Komputer spisuje się znakomicie już od kilku miesięcy, zadziwiają jego wymiary (w porównaniu ze starym), już o szybkości nie wspomnę, jest genialny!',
          'id': 393
        }, {
          'rating': 5,
          'authorName': 'Remik',
          'date': '2018-08-31T16:46:00.000Z',
          'content': 'Z czystym sumieniem mogę polecić envy każdemu, kto jest zainteresowany zmianą sprzętu. Envy to ogromna wydajność, jego specyfikacji nie można niczego zarzucić, to co jest wpakowane do środka wykonuje świetną robotę, na początku delikatnie denerwował mnie touchpad, przeszło po trzech dniach :)',
          'id': 394
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {'name': 'Kabel zasilający', 'id': 308}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane cztery głośniki', 'id': 300}, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'Czytnik kart pamięci microSD - 1 szt.', 'id': 287}, {'name': 'USB Typu-C - 2 szt.', 'id': 302}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Aluminiowe wnętrze laptopa', 'id': 254}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR3, 1866 MHz)', 'id': 295}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '8 GB', 'id': 296}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS', 'id': 298}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 6793 mAh, Li-Ion', 'id': 303}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '13,9 mm', 'id': 304}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '305,4 mm', 'id': 305}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '215,6 mm', 'id': 306}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,32 kg (z baterią)', 'id': 307}]
        }]
      }, {
        'id': 31,
        'name': 'Acer Aspire 5 i5-8250U/8G/240+1000/Win10 MX150 FHD IPS',
        'price': 3149,
        'quantity': 3,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-aspire-5-i5-8250u8g2401000win10-mx150-fhd-ips-434843,2017/6/pr_2017_6_28_13_29_35_37.jpg',
          'main': true,
          'id': 167
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-aspire-5-i5-8250u8g2401000win10-mx150-fhd-ips-434843,2017/6/pr_2017_6_28_13_28_49_662.jpg',
          'main': false,
          'id': 168
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-aspire-5-i5-8250u8g2401000win10-mx150-fhd-ips-434843,2017/6/pr_2017_6_28_13_28_41_927.jpg',
          'main': false,
          'id': 169
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-aspire-5-i5-8250u8g2401000win10-mx150-fhd-ips-434843,2017/6/pr_2017_6_28_13_28_45_959.jpg',
          'main': false,
          'id': 170
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-aspire-5-i5-8250u8g2401000win10-mx150-fhd-ips-434843,2017/6/pr_2017_6_28_13_28_52_927.jpg',
          'main': false,
          'id': 171
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,acer-aspire-5-i5-8250u8g2401000win10-mx150-fhd-ips-434843,2017/6/pr_2017_6_28_13_28_56_21.jpg',
          'main': false,
          'id': 172
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Buki90',
          'date': '2018-07-18T05:16:00.000Z',
          'content': 'Laptop bardzo szybki, ma dobre zawiasy(trzyma się sztywno). Ma wszystko, czego oczekiwałem, nie grzeje się. Bateria wytrzymuje około 4-5 godzin przeglądania internetu przy średnim podświetleniu. Polecam.',
          'id': 395
        }, {
          'rating': 6,
          'authorName': 'Feli',
          'date': '2018-08-03T14:21:00.000Z',
          'content': 'To już mój trzeci Acer i nie mogę powiedzieć złego słowa. Fajny model, przystępny jeżeli bierzemy pod uwagę stosunek cena-podzespoły. Szybko się uruchamia, nie wiesza, nie przegrzewa, sklep wysłał go szybko tak jak prosiłem. Do pracy i gier świetny. Śmiało polecam.',
          'id': 396
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Podświetlana klawiatura', 'id': 255}, {
            'name': 'Białe podświetlenie klawiatury',
            'id': 256
          }, {'name': 'Wydzielona klawiatura numeryczna', 'id': 257}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {'name': 'Moduł Bluetooth', 'id': 239}, {
            'name': 'Wi-Fi 802.11 b/g/n/ac',
            'id': 273
          }]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.', 'id': 286}, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/0', 'id': 312}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce MX150', 'id': 326}, {'name': '+ Intel UHD Graphics 620', 'id': 327}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 3220 mAh, Li-Ion', 'id': 358}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '12 GB', 'id': 371}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '22 mm', 'id': 372}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '382 mm', 'id': 373}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '263 mm', 'id': 374}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '2,20 kg (z baterią)', 'id': 375}]
        }]
      }, {
        'id': 32,
        'name': 'HP Pavilion x360 i5-8250U/8GB/240SSD/Win10 FHD Touch',
        'price': 3049,
        'quantity': 35,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i5-8250u8gb240ssdwin10-fhd-touch-412674,2017/7/pr_2017_7_19_10_47_9_75.jpg',
          'main': true,
          'id': 173
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i5-8250u8gb240ssdwin10-fhd-touch-412674,2017/7/pr_2017_7_19_10_47_52_266.jpg',
          'main': false,
          'id': 174
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i5-8250u8gb240ssdwin10-fhd-touch-412674,2017/7/pr_2017_7_19_10_45_23_442.jpg',
          'main': false,
          'id': 175
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i5-8250u8gb240ssdwin10-fhd-touch-412674,2017/7/pr_2017_7_19_10_45_26_536.jpg',
          'main': false,
          'id': 176
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i5-8250u8gb240ssdwin10-fhd-touch-412674,2017/7/pr_2017_7_19_10_45_30_474.jpg',
          'main': false,
          'id': 177
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i5-8250u8gb240ssdwin10-fhd-touch-412674,2017/7/pr_2017_7_19_10_45_33_661.jpg',
          'main': false,
          'id': 178
        }],
        'reviews': [{
          'rating': 4,
          'authorName': 'Mg',
          'date': '2018-05-31T20:02:00.000Z',
          'content': 'Zalety jakie są każdy widzi\nIstotne wady to Bardzo głośny wiatrak, brak przycisku do wyłączania ekranu i czerwona lampka na klawiaturze, przy mute. Ale przede wszystkim wiatrak.',
          'id': 397
        }, {
          'rating': 6,
          'authorName': 'Piero',
          'date': '2018-07-03T06:55:00.000Z',
          'content': 'U mnie wiatrak jest praktycznie bezgłośny przy zwykłej pracy (office, przeglądarka czy filmy) dopiero przy włączeniu gierki robi się słyszalny. Przycisk do wyłączenia ekranu chyba nie występuje w większości laptopów, a co do czerwonej lampki to mi akurat nie przeszkadza.\nNawet jeżeli uwzględnić te wady to myślę zalet jest znacznie więcej. Wszystko działa bez zarzutu, żadnej "zwiechy" od nowości. W tej cenie lepszego 360 ze świecą szukać.',
          'id': 398
        }, {
          'rating': 5,
          'authorName': 'qbranchmaster',
          'date': '2018-08-11T18:59:00.000Z',
          'content': 'Mam ten laptop od około dwóch tygodni. Ogólnie jestem bardzo zadowolony. Dotyk na ekranie działa bardzo dobrze, nie ma z nim żadnego problemu.  Jest szybki, sprawny i stabilny. Podczas normalnej pracy w stylu internet, pisanie nie grzeje się. Czasem wiatrak potrafi się bardziej pod obciążeniem rozkręcić, ale nie jest to uciążliwe wg mnie, nie jest to częste. Laptop uruchamia się błyskawicznie, w kilka sekund.  Zawiasy są sztywne, dobrze trzymają ekran.',
          'id': 399
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '227 mm', 'id': 279}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '14,0"', 'id': 365}]
        }, {
          'name': 'Miejsce na dodatkowy wewnętrzny dysk SATA',
          'filterable': false,
          'id': 43,
          'options': [{'name': 'Możliwość montażu dysku SATA (brak elementów montażowych)', 'id': 376}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS, dotykowy', 'id': 377}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3615 mAh, Li-Ion', 'id': 378}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,9 mm', 'id': 379}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '335 mm', 'id': 380}]
        }, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,64 kg (z baterią)', 'id': 381}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk rozszerzony na profesjonalnej linii montażowej x-kom', 'id': 382}]
        }]
      }, {
        'id': 33,
        'name': 'Dell Vostro 3568 i3-6006U/8GB/256+1000/10Pro FHD FPR',
        'price': 2449,
        'quantity': 3,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-3568-i3-6006u8gb256100010pro-fhd-fpr-383813,2017/10/pr_2017_10_30_13_0_48_687_05.jpg',
          'main': true,
          'id': 179
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-3568-i3-6006u8gb256100010pro-fhd-fpr-383813,2017/10/pr_2017_10_30_13_0_30_250_00.jpg',
          'main': false,
          'id': 180
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-3568-i3-6006u8gb256100010pro-fhd-fpr-383813,2017/10/pr_2017_10_30_13_0_33_563_01.jpg',
          'main': false,
          'id': 181
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-3568-i3-6006u8gb256100010pro-fhd-fpr-383813,2017/10/pr_2017_10_30_13_0_38_78_02.jpg',
          'main': false,
          'id': 182
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-3568-i3-6006u8gb256100010pro-fhd-fpr-383813,2017/10/pr_2017_10_30_13_0_45_234_04.jpg',
          'main': false,
          'id': 183
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-3568-i3-6006u8gb256100010pro-fhd-fpr-383813,2017/10/pr_2017_10_30_13_0_41_390_03.jpg',
          'main': false,
          'id': 184
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'solczyk',
          'date': '2018-03-27T16:57:00.000Z',
          'content': 'Laptop zdecydowanie dobry. Wszystko chodzi płynnie nic się nie zacina. System odpala się szybko. Nie nagrzewa się. Od czasu do czasu włącza się wiatrak ale da się do tego przyzwyczaić. Jakość wykonania, wiadomo jak na tą cenę może nie powala. Plastik nie ugina się pod naciskiem, klawiatura ma krótki skok.  Przydaje się również dysk zewnętrzny tym bardziej, że ssd nie ma zbyt dużej pojemności. Ogólnie jestem zadowolony z tego zakupu. Szybka wysyłka zgodnazopisem\nLaptop w sam raz do pracy w domu.',
          'id': 400
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Wbudowany czytnik linii papilarnych',
            'id': 260
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Pro PL (wersja 64-bitowa)', 'id': 248}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja x-kom sp. z o.o.)', 'id': 263}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-6006U (2 rdzenie, 2.00 GHz, 3 MB cache)', 'id': 310}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 520', 'id': 315}]
        }, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '380 mm', 'id': 319}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW DualLayer', 'id': 383}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 2750 mAh, Li-Ion', 'id': 384}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '23,5 mm', 'id': 385}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '260 mm', 'id': 386}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '2,30 kg (z baterią)', 'id': 387}]}]
      }, {
        'id': 34,
        'name': 'Huawei MateBook D 15.6"  i5-8250U/8GB/256GBSSD/Win10',
        'price': 2699,
        'quantity': 1,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb256gbssdwin10-426850,2018/4/pr_2018_4_26_8_43_9_916_00.jpg',
          'main': true,
          'id': 185
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb256gbssdwin10-426850,2018/4/pr_2018_4_24_15_49_28_518_01.jpg',
          'main': false,
          'id': 186
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb256gbssdwin10-426850,2018/4/pr_2018_4_24_15_49_31_440_02.jpg',
          'main': false,
          'id': 187
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb256gbssdwin10-426850,2018/4/pr_2018_4_24_15_49_25_643_00.jpg',
          'main': false,
          'id': 188
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb256gbssdwin10-426850,2018/4/pr_2018_4_24_15_49_34_284_03.jpg',
          'main': false,
          'id': 189
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,huawei-matebook-d-156-i5-8250u8gb256gbssdwin10-426850,2018/4/pr_2018_4_24_15_49_37_144_04.jpg',
          'main': false,
          'id': 190
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Kris',
          'date': '2018-08-11T11:38:00.000Z',
          'content': 'Udany laptop , jest szybki , porządnie wykonany     , łatwy w przenoszeniu , porządna matryca IPS.',
          'id': 401
        }],
        'attributes': [{
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Czujnik światła', 'id': 41}, {
            'name': 'Czujnik Halla',
            'id': 43
          }, {'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}, {'name': 'Dolby Atmos Sound System', 'id': 329}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '16,9 mm', 'id': 290}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Dysk SSD M.2 PCIe',
          'filterable': false,
          'id': 39,
          'options': [{'name': '256 GB', 'id': 297}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '358 mm', 'id': 331}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '239 mm', 'id': 332}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3800 mAh, Li-Polymer', 'id': 388}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '1,74 kg (z baterią)', 'id': 389}]}]
      }, {
        'id': 35,
        'name': 'HP Pavilion i5-8250U/8GB/1TB+240SSD/Win10 GF 940MX',
        'price': 3199,
        'quantity': 76,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-i5-8250u8gb1tb240ssdwin10-gf-940mx-413366,2018/2/pr_2018_2_27_6_50_28_21_00.jpg',
          'main': true,
          'id': 191
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-i5-8250u8gb1tb240ssdwin10-gf-940mx-413366,2018/2/pr_2018_2_27_6_50_38_177_02.jpg',
          'main': false,
          'id': 192
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-i5-8250u8gb1tb240ssdwin10-gf-940mx-413366,2018/2/pr_2018_2_27_6_51_19_100_01.jpg',
          'main': false,
          'id': 193
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-i5-8250u8gb1tb240ssdwin10-gf-940mx-413366,2018/2/pr_2018_2_27_6_50_41_584_03.jpg',
          'main': false,
          'id': 194
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-i5-8250u8gb1tb240ssdwin10-gf-940mx-413366,2018/2/pr_2018_2_27_6_50_35_68_01.jpg',
          'main': false,
          'id': 195
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-i5-8250u8gb1tb240ssdwin10-gf-940mx-413366,2018/2/pr_2018_2_27_6_51_15_944_00.jpg',
          'main': false,
          'id': 196
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Mika',
          'date': '2018-05-31T17:21:00.000Z',
          'content': 'Korzystam z tego HP w pracy i do pracy biurowej, prezentacji i internetu sprawdza się fantastycznie. Co ważne korzystam z niego po kilka dobrych godzin dziennie i nie grzeje się jakoś zbytnio ani nie hałasuje-wydajnościowo w moim odczuciu jest naprawde godny polecenia... Wizualnie bez zastrzezeń. Działanie płynne jest nawet przy dużej eksploatacji. :)',
          'id': 402
        }, {
          'rating': 6,
          'authorName': 'Krzysiek',
          'date': '2018-08-15T18:28:00.000Z',
          'content': 'Jestem zadowolony z lapka. Troche się grzeje przy grach ale poza tym wszystko fajnie. Myślę ze warto kupić podkładkę chłodzącą wraz z laptopem. Dziękuję.',
          'id': 403
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Wbudowane głośniki stereo',
            'id': 44
          }, {'name': 'Aluminiowe wnętrze laptopa', 'id': 254}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {'name': 'Szyfrowanie TPM', 'id': 261}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS', 'id': 298}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': '+ Intel UHD Graphics 620', 'id': 327}, {'name': 'NVIDIA GeForce 940MX', 'id': 390}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '19,9 mm', 'id': 379}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk rozszerzony na profesjonalnej linii montażowej x-kom', 'id': 382}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '359 mm', 'id': 391}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '247 mm', 'id': 392}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '1,90 kg (z baterią)', 'id': 393}]}]
      }, {
        'id': 36,
        'name': 'Dell Inspiron 3576 i5-8250U/8G/256/Win10 R520 FHD',
        'price': 2899,
        'quantity': 52,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3576-i5-8250u8g256win10-r520-fhd-406776,2018/2/pr_2018_2_16_15_22_20_532_04.jpg',
          'main': true,
          'id': 197
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3576-i5-8250u8g256win10-r520-fhd-406776,2018/2/pr_2018_2_16_15_22_10_0_01.jpg',
          'main': false,
          'id': 198
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3576-i5-8250u8g256win10-r520-fhd-406776,2018/2/pr_2018_2_16_15_22_6_484_00.jpg',
          'main': false,
          'id': 199
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3576-i5-8250u8g256win10-r520-fhd-406776,2018/2/pr_2018_2_16_15_22_55_255_01.jpg',
          'main': false,
          'id': 200
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3576-i5-8250u8g256win10-r520-fhd-406776,2018/2/pr_2018_2_16_15_22_51_942_00.jpg',
          'main': false,
          'id': 201
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3576-i5-8250u8g256win10-r520-fhd-406776,2018/2/pr_2018_2_16_15_22_13_766_02.jpg',
          'main': false,
          'id': 202
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'U...K',
          'date': '2018-06-28T14:49:00.000Z',
          'content': 'Dobry laptop. Mam go od ponad miesiąca. Korzystam z laptopa na studiach informatycznych - najważniejszy Visual Studio śmiga bez problemu. W porównaniu z poprzednim laptopem 6 letnim, robi pogrom.',
          'id': 404
        }, {
          'rating': 5,
          'authorName': 'Piotr',
          'date': '2018-08-11T07:58:00.000Z',
          'content': 'Fajny, szybki wszystko ok',
          'id': 405
        }, {
          'rating': 6,
          'authorName': 'Crash',
          'date': '2018-08-22T20:36:00.000Z',
          'content': 'Kupiłem ten laptop Dell do szkoły i jest najlepszy laptop w tej cenie. Porównywałem wiele modeli, ale jakość i wykonaanie tego rządzi! Bateria to spokojne 4-6 godzin (zależy od jasności ekranu i wykonywanych zadań), na dodatek mega szybki dzięki 8 GB RAM i dyskowi SSD 256 GB.',
          'id': 406
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}, {
            'name': 'LAN 10/100 Mbps',
            'id': 395
          }]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '24 mm', 'id': 318}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '380 mm', 'id': 319}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': '+ Intel UHD Graphics 620', 'id': 327}, {'name': 'AMD Radeon 520', 'id': 394}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW DualLayer', 'id': 383}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '260 mm', 'id': 386}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 2700 mAh, Li-Ion', 'id': 396}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '2,22 kg (z baterią)', 'id': 397}]}]
      }, {
        'id': 37,
        'name': 'Lenovo Ideapad 320-15 i5-8250U/8GB/256/Win10 Srebrny',
        'price': 2599,
        'quantity': 84,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320-15-i5-8250u8gb256win10-srebrny-430463,2017/8/pr_2017_8_4_10_26_0_879_11.jpg',
          'main': true,
          'id': 203
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320-15-i5-8250u8gb256win10-srebrny-430463,2017/8/pr_2017_8_4_10_25_23_108_02.jpg',
          'main': false,
          'id': 204
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320-15-i5-8250u8gb256win10-srebrny-430463,2017/8/pr_2017_8_4_10_25_30_891_04.jpg',
          'main': false,
          'id': 205
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320-15-i5-8250u8gb256win10-srebrny-430463,2017/8/pr_2017_8_4_10_25_19_717_01.jpg',
          'main': false,
          'id': 206
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320-15-i5-8250u8gb256win10-srebrny-430463,2017/8/pr_2017_8_4_10_25_16_639_00.jpg',
          'main': false,
          'id': 207
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,lenovo-ideapad-320-15-i5-8250u8gb256win10-srebrny-430463,2017/8/pr_2017_8_4_10_25_35_579_05.jpg',
          'main': false,
          'id': 208
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'RobertBrave',
          'date': '2018-08-31T13:11:00.000Z',
          'content': 'Sprzęt prezentuje się naprawdę świetnie. Klawiatura w ciemniejszym odcieniu szarości idealnie komponuje się ze srebrną obudową.\nKomputer z przeznaczeniem biurowym, więc nie zależało na karcie graficznej, ale za to bardzo sprawnie działa na dysku SSD i 8GB RAM.',
          'id': 407
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)',
            'id': 322
          }]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Dysk SSD M.2 PCIe',
          'filterable': false,
          'id': 39,
          'options': [{'name': '256 GB', 'id': 297}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/0', 'id': 312}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '12 GB', 'id': 371}]
        }, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '2,20 kg (z baterią)', 'id': 375}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW DualLayer', 'id': 383}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '260 mm', 'id': 386}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '2-komorowa, 4000 mAh, Li-Ion', 'id': 398}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '22,9 mm', 'id': 399}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '378 mm', 'id': 400}]
        }]
      }, {
        'id': 38,
        'name': 'Dell Vostro 5568 i5-7200U/8GB/256/10Pro FHD',
        'price': 3399,
        'quantity': 66,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-5568-i5-7200u8gb25610pro-fhd-331300,2017/10/pr_2017_10_19_16_4_44_621_10.jpg',
          'main': true,
          'id': 209
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-5568-i5-7200u8gb25610pro-fhd-331300,2017/10/pr_2017_10_19_16_5_4_183_00.jpg',
          'main': false,
          'id': 210
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-5568-i5-7200u8gb25610pro-fhd-331300,2017/10/pr_2017_10_19_16_4_16_261_02.jpg',
          'main': false,
          'id': 211
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-5568-i5-7200u8gb25610pro-fhd-331300,2017/10/pr_2017_10_19_16_4_23_949_04.jpg',
          'main': false,
          'id': 212
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-5568-i5-7200u8gb25610pro-fhd-331300,2017/10/pr_2017_10_19_16_4_27_183_05.jpg',
          'main': false,
          'id': 213
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-vostro-5568-i5-7200u8gb25610pro-fhd-331300,2017/10/pr_2017_10_19_16_4_30_636_06.jpg',
          'main': false,
          'id': 214
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Michał',
          'date': '2017-01-28T21:35:00.000Z',
          'content': 'fajny i mocny laptop, cichy - przy pracy biurowej wiatrak się praktycznie nie uruchamia,  bardzo dobre wykonanie, super bateria 8-9 godzin normalnej pracy (szkoda że nie można zdemontować), dodatkowo podświetlana klawiatura, lekka konstrukcja i gwarancja 3 lata. Używam prawie od 2 miesięcy i polecam :)',
          'id': 408
        }, {
          'rating': 6,
          'authorName': 'Marek',
          'date': '2017-02-13T19:38:00.000Z',
          'content': 'Super laptop, solidne wykonanie gorąco polecam!!!',
          'id': 409
        }, {
          'rating': 6,
          'authorName': 'Wojtek',
          'date': '2017-03-31T08:59:00.000Z',
          'content': 'Pracuję na nim w biurze już 3 miesiące. Bateria trzyma do 8-9 h, wykonanie świetne, lekka konstrukcja, wszystko na tak! Rewelacyjny sprzęcik :)',
          'id': 410
        }, {'rating': 6, 'authorName': 'Marek', 'date': '2017-09-05T07:57:00.000Z', 'content': 'Dobry laptop', 'id': 411}, {
          'rating': 6,
          'authorName': 'Kamil',
          'date': '2017-09-05T18:40:00.000Z',
          'content': 'Bardzo dobry wybór! Super laptop, biznesowy i elegancki, a dodatkowo podświetlana klawiatura',
          'id': 412
        }, {
          'rating': 6,
          'authorName': 'Kamil',
          'date': '2017-09-05T18:47:00.000Z',
          'content': 'Bardzo dobry komputer do pracy w ciężkich warunkach biurowych i domowych :D',
          'id': 413
        }, {
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2017-09-05T21:15:00.000Z',
          'content': 'Świetny laptop. Jak zawsze Dell nie zawodzi. Dobry stosunek ceny do jakości. Pozytywnie również oceniam sklep xkom. Szybka przesyłka, profesjonalna obsługa.',
          'id': 414
        }, {'rating': 6, 'authorName': 'Aleg', 'date': '2017-09-05T21:35:00.000Z', 'content': 'Niezłe urządzenie', 'id': 415}, {
          'rating': 6,
          'authorName': 'Andrzej',
          'date': '2017-09-06T09:11:00.000Z',
          'content': 'Szybki, wydajny i lekki. Bardzo dobry sprzęt do pracy biurowej.',
          'id': 416
        }, {
          'rating': 2,
          'authorName': 'Tomasz',
          'date': '2017-09-06T09:15:00.000Z',
          'content': 'Jakość materiałów pozostawia wiele do życzenia. Zawiasy, śrubki,  touchpad czy spasowanie górnej klapy to jakieś nieporozumienie.  \'Reszta\' ok,  bateria długo działa,  wydajność na wysokim poziomie.',
          'id': 417
        }, {
          'rating': 6,
          'authorName': 'Pawel',
          'date': '2017-09-07T11:54:00.000Z',
          'content': 'Laptop godny polecenia',
          'id': 418
        }, {
          'rating': 5,
          'authorName': 'Jakub',
          'date': '2017-09-07T22:21:00.000Z',
          'content': 'Polecam, szczególnie do codziennego użytku.',
          'id': 419
        }, {
          'rating': 4,
          'authorName': 'Michał',
          'date': '2017-09-08T09:46:00.000Z',
          'content': 'Polecam.  Używam od roku i zero problemów',
          'id': 420
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2017-09-08T22:17:00.000Z',
          'content': 'Sprzęt dla ludzi którzy cenią sobie estetykę i niezawodność',
          'id': 421
        }, {
          'rating': 6,
          'authorName': 'Maciej',
          'date': '2017-09-09T09:23:00.000Z',
          'content': 'Bardzo dobry biznesowy laptop. Dysk SSD daje mu odpowiednio szybkie ładowanie systemu i aplikacji. Polecam.',
          'id': 422
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2017-09-10T07:39:00.000Z',
          'content': 'Rewelacyjny sprzęt, posiadam go od 5 miesięcy. Bateria trzyma ok 7h. Serdecznie polecam !',
          'id': 423
        }, {
          'rating': 5,
          'authorName': '.abc',
          'date': '2017-11-23T15:28:00.000Z',
          'content': 'Laptop bardzo dobry. Jakość materiałów i ogólne wykonanie ok. Jedyne mankamenty to karta dźwiękowa która czasami nie wytrzyma i zatrzeszczy oraz matryca która ma dość małe kąty widzenia. Ale mimo to polecam :)',
          'id': 424
        }, {
          'rating': 4,
          'authorName': 'Waldemar',
          'date': '2018-01-20T09:01:00.000Z',
          'content': 'Wykonanie bardzo dobre, klawisze działają przyjemnie, podświetlenie jest ok. Stosunkowo lekki, cienki, poręczny do transportu nawet w aktówce. Uruchamia się bardzo szybko i rewelacyjna sprawa wystarczy odcisk palca do logowania. Możliwość zainstalowania dodatkowego dysku HDD, niestety nie widzi mojego starego dysku, a serwis nie ma wsparcia na sprzęt nie Della !   Bateria wystarcza na 5 h przy internecie i filmach. Wyświetlacz HD dobry tylko na wprost, druga siedząca z boku już nie widzi dobrze.',
          'id': 425
        }, {
          'rating': 5,
          'authorName': 'Robert',
          'date': '2018-02-03T12:21:00.000Z',
          'content': 'Generalnie bez uwag, parametry sprzętowe bardzo dobre, niezły procesor, sporo pamięci ram + szybki dysk daje komfort pracy. Zgadzam się z częścią opinii dotyczących matrycy, mogłaby  być lepsza.',
          'id': 426
        }, {
          'rating': 3,
          'authorName': 'kos',
          'date': '2018-03-20T14:25:00.000Z',
          'content': 'Zawiodłem się jakością matrycy, obraz na 100% jasności jest bardzo ciemny, kolory blade a biel szara. Miałem starszy model dell gdzie kolory były o niebo lepsze a kolor biały był biały.',
          'id': 427
        }, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2018-04-02T12:17:00.000Z',
          'content': 'Bardzo dobrej jakości sprzęt. Całkowicie spełnia moje oczekiwania.',
          'id': 428
        }, {
          'rating': 1,
          'authorName': 'Użytkownik',
          'date': '2018-04-13T05:15:00.000Z',
          'content': 'W naszej firmie kilka osób posiada ten model laptopa z tą specyfikacją. Jeden z najgorszych modeli jakie miałem przyjemność użytkować. Mój model wyłącza się przynajmniej dwa razy dziennie. Podobne rzeczy dzieją się współpracownikom, aczkolwiek jest kilka osób którym ten laptop działa prawidłowo. Nie wiem, może trafiłem na wadliwy model. W każdym razie w tej cenie nie polecam.',
          'id': 429
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-04-13T12:45:00.000Z',
          'content': 'Przykro nam z powodu powstałych niedogodności. Bardzo prosimy o kontakt z naszym działem serwisowym, postaramy się znaleźć rozwiązanie tej sytuacji. Jesteśmy do Twojej dyspozycji: serwis@x-kom.pl lub pod numerem 34 377 00 30.',
          'id': 430
        }, {
          'rating': 6,
          'authorName': 'Caroline ',
          'date': '2018-04-16T19:01:00.000Z',
          'content': 'Sprzęt działa bardzo dobrze i ładnie się prezentuje, urzekło mnie podświetlenie. Dell to sprawdzona marka. To juz mój drugi laptop tej firmy i z pewnością nie ostatni. Jest lekki i nie przegrzewa mi się. Kolory i komfort użytkowania na najwyższym poziomie. Wielki plus za klawiaturę numeryczną, sprawdza się w pracy:) dziękuję za szybką wysyłkę.',
          'id': 431
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Wydzielona klawiatura numeryczna', 'id': 257}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Wbudowany czytnik linii papilarnych', 'id': 260}, {
            'name': 'Aluminiowa obudowa',
            'id': 281
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '32 GB', 'id': 81}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {'name': 'Moduł Bluetooth', 'id': 239}, {
            'name': 'Wi-Fi 802.11 b/g/n/ac',
            'id': 273
          }]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'USB 3.1 Gen. 1 (USB 3.0) - 3 szt.', 'id': 240}, {
            'name': 'HDMI - 1 szt.',
            'id': 241
          }, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Pro PL (wersja 64-bitowa)', 'id': 248}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 620', 'id': 285}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {'name': 'Typ ekranu', 'filterable': false, 'id': 5, 'options': [{'name': 'Matowy, LED', 'id': 314}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '380 mm', 'id': 319}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Miejsce na dodatkowy wewnętrzny dysk SATA',
          'filterable': false,
          'id': 43,
          'options': [{'name': 'Możliwość montażu dysku SATA (brak elementów montażowych)', 'id': 376}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-7200U (2 rdzenie, od 2.5 GHz do 3.1 GHz, 3MB cache)', 'id': 401}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3500 mAh, Li-Ion', 'id': 402}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,2 mm', 'id': 403}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '253 mm', 'id': 404}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '1,98 kg (z baterią)', 'id': 405}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja producenta)', 'id': 406}]
        }]
      }, {
        'id': 39,
        'name': 'Microsoft Surface Pro i5-7300U/8GB/128SSD/Win10P+klawiatura',
        'price': 3799,
        'quantity': 82,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,microsoft-surface-pro-i5-7300u8gb128ssdwin10pklawiatura-444494,2017/5/pr_2017_5_26_8_9_33_740.jpg',
          'main': true,
          'id': 215
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,microsoft-surface-pro-i5-7300u8gb128ssdwin10pklawiatura-444494,2017/5/pr_2017_5_26_8_9_54_803.jpg',
          'main': false,
          'id': 216
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,microsoft-surface-pro-i5-7300u8gb128ssdwin10pklawiatura-444494,2018/8/pr_2018_8_20_15_10_18_691_00.jpg',
          'main': false,
          'id': 217
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,microsoft-surface-pro-i5-7300u8gb128ssdwin10pklawiatura-444494,2017/5/pr_2017_5_26_8_8_55_425.jpg',
          'main': false,
          'id': 218
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,microsoft-surface-pro-i5-7300u8gb128ssdwin10pklawiatura-444494,2018/8/pr_2018_8_20_15_7_11_350_00.jpg',
          'main': false,
          'id': 219
        }],
        'reviews': [],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Akcelerometr', 'id': 36}, {'name': 'Żyroskop', 'id': 37}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Magnezowa pokrywa matrycy', 'id': 416}, {
            'name': 'Corning Gorilla Glass 4',
            'id': 417
          }]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {'name': 'Klawiatura', 'id': 418}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'Czytnik kart pamięci microSD - 1 szt.', 'id': 287}, {
            'name': 'Mini Display Port - 1 szt.',
            'id': 339
          }, {'name': 'Złącze stacji dokującej - 1 szt.', 'id': 411}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Pro PL (wersja 64-bitowa)', 'id': 248}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 620', 'id': 285}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '8 GB', 'id': 296}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS, dotykowy', 'id': 377}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-7300U (2 rdzenie, od 2.6 GHz do 3.5 GHz, 3MB cache)', 'id': 407}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '12,3"', 'id': 408}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2736 x 1824', 'id': 409}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '5.0 Mpix', 'id': 410}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '8,5 mm', 'id': 412}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '292 mm', 'id': 413}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '201 mm', 'id': 414}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '0,77 kg (z baterią)', 'id': 415}]}]
      }, {
        'id': 40,
        'name': 'Dell Inspiron 5370 i3-7130U/8GB/128/Win10 FHD',
        'price': 2479,
        'quantity': 11,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i3-7130u8gb128win10-fhd-393449,2017/12/pr_2017_12_14_9_40_43_405_10.jpg',
          'main': true,
          'id': 220
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i3-7130u8gb128win10-fhd-393449,2017/12/pr_2017_12_14_9_40_39_186_09.jpg',
          'main': false,
          'id': 221
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i3-7130u8gb128win10-fhd-393449,2017/12/pr_2017_12_14_9_40_4_668_01.jpg',
          'main': false,
          'id': 222
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i3-7130u8gb128win10-fhd-393449,2017/12/pr_2017_12_14_9_40_10_388_02.jpg',
          'main': false,
          'id': 223
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i3-7130u8gb128win10-fhd-393449,2017/12/pr_2017_12_14_9_40_15_622_03.jpg',
          'main': false,
          'id': 224
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i3-7130u8gb128win10-fhd-393449,2017/12/pr_2017_12_14_9_40_19_435_04.jpg',
          'main': false,
          'id': 225
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Kritik',
          'date': '2018-01-22T11:43:00.000Z',
          'content': 'Szybki, lekki, i bardzo cicho pracuje. Bez duzego obciazenia pracuje w ogóle bez zadnych dzwiekow, na chlodzeniu pasywnym. Polecam.',
          'id': 432
        }, {
          'rating': 6,
          'authorName': 'Janek',
          'date': '2018-01-23T20:03:00.000Z',
          'content': 'Zamówienie perfekcyjnie zrealizowane',
          'id': 433
        }, {
          'rating': 6,
          'authorName': 'Adam',
          'date': '2018-01-30T20:52:00.000Z',
          'content': 'Estetycznie i solidnie wykonany, lekki ,dobra matryca- idealna do pracy biurowej, dźwięk ok, ssd szybszy od moich myśli :) bardzo dobry stosunek cena/jakość. Wady: tylko dwa porty usb 3.1',
          'id': 434
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-04-18T17:11:00.000Z',
          'content': 'Laptop jest w całości wykonany z aluminium, co daje uczucie trwałości. Ekran nie chybocze się, co jest zdecydowanie na plus (szczególnie z moim poprzednikiem, Samsungiem 530U3B). Klawiatura jest bardzo wygodna, nie jest specjalnie głośna. Jeśli chodzi o touchpad, to też nie mogę narzekać - jest precyzyjny, a jego czułość da się wygodnie regulować. \nJedyny mankament, ale to nie laptopa, tylko samego windowsa to skalowanie - sprawia to problem, szczególnie w starszych programach.',
          'id': 435
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}, {'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Możliwość zabezpieczenia linką (port Noble Wedge)', 'id': 259}, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, IPS', 'id': 228}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'Czytnik kart pamięci microSD - 1 szt.', 'id': 287}, {'name': 'Noble Lock - 1 szt.', 'id': 420}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 620', 'id': 285}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 355}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-7130U (2 rdzenie, 2.70 GHz, 3 MB cache)', 'id': 419}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3166 mAh, Li-Ion', 'id': 421}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '15,9 mm', 'id': 422}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '324 mm', 'id': 423}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '220 mm', 'id': 424}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,42 kg (z baterią)', 'id': 425}]
        }]
      }, {
        'id': 41,
        'name': 'ASUS R541UA-DM1404T-8 i3-7100U/8GB/256SSD/DVD/Win10 FHD',
        'price': 2349,
        'quantity': 20,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r541ua-dm1404t-8-i3-7100u8gb256ssddvdwin10-fhd-358634,2017/12/pr_2017_12_11_11_5_39_771_06.jpg',
          'main': true,
          'id': 226
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r541ua-dm1404t-8-i3-7100u8gb256ssddvdwin10-fhd-358634,2017/12/pr_2017_12_11_11_7_59_421_00.jpg',
          'main': false,
          'id': 227
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r541ua-dm1404t-8-i3-7100u8gb256ssddvdwin10-fhd-358634,2017/12/pr_2017_12_11_11_5_35_880_05.jpg',
          'main': false,
          'id': 228
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r541ua-dm1404t-8-i3-7100u8gb256ssddvdwin10-fhd-358634,2017/12/pr_2017_12_11_11_5_17_816_00.jpg',
          'main': false,
          'id': 229
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r541ua-dm1404t-8-i3-7100u8gb256ssddvdwin10-fhd-358634,2017/12/pr_2017_12_11_11_5_21_551_01.jpg',
          'main': false,
          'id': 230
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r541ua-dm1404t-8-i3-7100u8gb256ssddvdwin10-fhd-358634,2017/12/pr_2017_12_11_11_5_25_51_02.jpg',
          'main': false,
          'id': 231
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Piotr',
          'date': '2017-08-22T12:28:00.000Z',
          'content': 'Bardzo wygodny do pracy biurowej. Szybki dysk SSD, wysoka rozdzielczość i wygodna klawiatura zapewniają komfort pracy. Nie uruchamiałem jeszcze gier, ale nie do tego ma mi służyć (dlatego nie przeszkadzała mi zintegrowana karta graf.).  Wentylator trochę głośny ale za to świetnie chłodzi i nawet jak na dworze było 30 stopni to komputer był wręcz zimny. Osobiście spośród komputerów na biura z dyskiem SSD w tej cenie lepszego nie znalazłem',
          'id': 436
        }, {
          'rating': 6,
          'authorName': 'krzysiek ',
          'date': '2017-08-26T10:38:00.000Z',
          'content': 'Typowy, dobry model do pracy, smukły, nie za ciężki, z portem Kensington, co dla mnie było priorytetem. Cenie sobie cisze podczas pracy i laptop to zapewnia, nie słychać żadnych szumów ze środka, napęd z nagrywarką DVD co też było na liście moich wymagań, matowa matryca w fullHD daję bardzo ładny, naturalny obraz, kolory są nasycone. Praca na nim jest bezstresowa, ani razu mi nie zamulił, nie utracił danych. Bardzo dobra cena w stosunku do jakości. Polecam',
          'id': 437
        }, {
          'rating': 6,
          'authorName': 'boryss ',
          'date': '2017-08-28T17:50:00.000Z',
          'content': 'Bezawaryjny, solidnie wykonany, do codziennych, standardowych zadań, wart uwagi.',
          'id': 438
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2017-09-05T20:23:00.000Z',
          'content': 'Pomimo tej ceny dysk SSD sprawuje sie znakomicie. Laptop dobrze wykonany. Ogólnie polecam.',
          'id': 439
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2017-09-06T08:45:00.000Z',
          'content': 'Warto kupić z dyskiem SSD. Laptop o wiele szybciej pracuje niż na zwyklym dysku.',
          'id': 440
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2017-09-06T15:06:00.000Z',
          'content': 'Super laptop, przystępna cena',
          'id': 441
        }, {'rating': 5, 'authorName': 'Michal', 'date': '2017-09-07T11:28:00.000Z', 'content': 'Super', 'id': 442}, {
          'rating': 6,
          'authorName': 'Szymon',
          'date': '2017-09-07T12:36:00.000Z',
          'content': 'Bardzo fajny laptop. Szybki, wydajny. Znakomity do pracy',
          'id': 443
        }, {
          'rating': 5,
          'authorName': 'Mateusz',
          'date': '2017-09-07T12:57:00.000Z',
          'content': 'Super laptopie lekki i szybki',
          'id': 444
        }, {
          'rating': 5,
          'authorName': 'Jarosław',
          'date': '2017-09-08T08:13:00.000Z',
          'content': 'Świetna jakość względem ceny, polecam!',
          'id': 445
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2017-09-08T09:07:00.000Z',
          'content': 'Jak najbardziej polecam ;)',
          'id': 446
        }, {
          'rating': 1,
          'authorName': 'Dm',
          'date': '2017-09-08T09:20:00.000Z',
          'content': 'Nie polecam, zbyt głośno działa. Również mą dysk który trzeszczy cały czas.',
          'id': 447
        }, {
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2017-09-08T09:27:00.000Z',
          'content': 'Super produkt. Produkt godny polecenia. Jestem zadowolony. Dobre wykonanie laptopa. Matryca ok. Zachęcam do zakupu.',
          'id': 448
        }, {
          'rating': 6,
          'authorName': 'Przemysław',
          'date': '2017-09-09T17:30:00.000Z',
          'content': 'Polecam, laptop sprawuje się znakomicie. Używam do przede wszystkim do prac biurowych i przeglądania internetu.',
          'id': 449
        }, {
          'rating': 6,
          'authorName': 'Ja',
          'date': '2017-09-10T08:07:00.000Z',
          'content': 'Polecam szybki i niedrogi',
          'id': 450
        }, {
          'rating': 6,
          'authorName': 'Kamil',
          'date': '2017-09-10T19:57:00.000Z',
          'content': 'Świetny produkt. Lekki oraz szybki.',
          'id': 451
        }, {
          'rating': 6,
          'authorName': 'Kamil',
          'date': '2017-09-10T21:04:00.000Z',
          'content': 'Jak najbardziej godny polecenia',
          'id': 452
        }, {
          'rating': 1,
          'authorName': 'Paula',
          'date': '2017-09-16T08:06:00.000Z',
          'content': 'Gdyby była ocena minusowa na pewno taką bym wystawiła. \nPo zakupie tego laptopa okazało się, że bateria nie działa a laptop działa tylko na kablu zasilającym. Laptop został wymieniony na inny (ten sam model), po 3 godzinach użytkowania wyłączył się 2krotnie generując awarie windowsa. Do sklepu w którym dokonałam zakupu przysłano mi trzeci laptop na wymianę i już w sklepie okazało się, że znowu problem z bateria.. Prosiłam o wymianę na inny laptop z nie gorszymi parametrami - odmówiono mi. :)',
          'id': 453
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2017-09-18T12:40:00.000Z',
          'content': 'Bardzo prosimy o kontakt z naszym działem serwisowym: serwis@x-kom.pl lub pod numerem 34 377 00 30. Chcielibyśmy bliżej przyjrzeć się Twojej sprawie i znaleźć rozwiązanie tej sytuacji.',
          'id': 454
        }, {
          'rating': 5,
          'authorName': 'Tomasz',
          'date': '2017-10-20T13:07:00.000Z',
          'content': 'Laptop kupiony za 2349 zł. Wygodna matowa matryca, dobrze umieszczone głośniki. SSD i 8gb ramu to myślę, że wystarczy do podstawowej pracy w domu i biurze (opinia parę dni po zakupie). \nMinusy: \n-niezrozumiały pomysł Asusa położenia przycisku zasilania zaraz przy klawiaturze numerycznej - palec może się zahaczyć przy wciskaniu minusa.\n-głośny. Tzn głośniejszy niż przyzwyczajony jestem do 2 asusów których używam. Wiatrak wentylatora ma wysoki dźwięk zwłaszcza gdy obciążymy procesor.',
          'id': 455
        }, {
          'rating': 1,
          'authorName': 'Paweł',
          'date': '2017-10-23T12:08:00.000Z',
          'content': 'tandetne wykończenie, głośny wentylator, plastikowy dźwięk klawiszy, brak podświetlenia capslock i klawisza włącz/wyłącz :-(',
          'id': 456
        }, {
          'rating': 3,
          'authorName': 'jan',
          'date': '2018-02-27T15:58:00.000Z',
          'content': 'Witam.\nNiestety produkt wadliwy zwrócony do sklepu.',
          'id': 457
        }, {
          'rating': 5,
          'authorName': 'darek_sc',
          'date': '2018-03-22T18:51:00.000Z',
          'content': 'Zakupiony laptop charakteryzuję się wątpliwą kulturą pracy, wentylator generuje dziwny terkot, nie jest to szum jak w większości innych laptopów. Dysk SSD sprawia że system uruchamia się błyskawicznie, komputer dział sprawnie, gładzik jest precyzyjny i wygodny. Preinstalowany program antywirusowy powodował konflikt systemowy i ciągłe błędy.\nZainstalowane głośniki grają naprawdę głośno, dźwięk jest dobrej jakości. Piętą achillesową jest kultura pracy wentylatora która jest fatalna.',
          'id': 458
        }, {
          'rating': 6,
          'authorName': 'Judi',
          'date': '2018-04-10T16:03:00.000Z',
          'content': 'Laptop został przeze mnie zakupiony głównie do pracy biurowej, przeglądania internetu i tworzenia nieskomplikowanych grafik. Radzi sobie świetnie :) nie zacina się, działa płynnie nawet podczas wykonywania kilku zadań na raz, poza tym jest lekki i elegancki a to ważne, bo często zabieram go do pracy. Dobrze radzi sobie też z filmami, matowa i duża matryca spełnia swoją rolę. Sprawdza się w 100%',
          'id': 459
        }, {
          'rating': 1,
          'authorName': 'Princteon',
          'date': '2018-06-10T09:17:00.000Z',
          'content': 'Jedna gwiazdka to i tak za wiele, laptop najprawdopodobniej został mi sprzedany wadliwy, ciągle się wyłącza, laptop działa tylko na włączonej baterii totalny badziew. Już pomijam inne minusy których jest cała masa.. "sprzęt" nie posiada żadnych plusów. A te formułkę, że zapraszacie na kontakt z działem serwisowym schowajcie sobie do kieszeni.. Wasz dział serwisowy to totalna pomyłka. Nigdy więcej nic od was nie kupie i nikomu nie polece. Mój 10 letni komputer przy tym "waszym czymś" to sztos.',
          'id': 460
        }, {
          'rating': 6,
          'authorName': 'Mariusz ',
          'date': '2018-06-26T18:44:00.000Z',
          'content': 'ASUS to u mnie sprawdzona marka. To mój 3 laptop od tego producenta i jestem zadowolony. Nie zauważyłem zeby pracował za głośno albo się nadmiernie grzał. Dla moich potrzeb jest zupełnie wystarczający. Używam go w domu, syn czasem w coś na nim zagra i nie mówił zeby coś było nie tak. Ja polecam!!!',
          'id': 461
        }, {
          'rating': 6,
          'authorName': 'PIOTER',
          'date': '2018-08-18T17:08:00.000Z',
          'content': 'Sprzęt wygląda na zywo dokładnie jak na zdjeciach. Preznetuje się i wykonany jest bardzo dobrze. Zdaje egzamin do codziennych zastosowan w domu. Nic więcej mi nie trzeba. Dobry laptop w dobrej cenie - polecam ASUSa bo jeszcze się na nim nie zawiodłem.&nbsp;',
          'id': 462
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)',
            'id': 322
          }, {'name': 'Dostępne sterowniki tylko dla systemu Windows 10', 'id': 432}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Moduł Bluetooth', 'id': 239}, {'name': 'LAN 10/100 Mbps', 'id': 395}, {
            'name': 'Wi-Fi 802.11 b/g/n',
            'id': 426
          }]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.', 'id': 286}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-7100U (2 rdzenie, 2.40 GHz, 3 MB cache)', 'id': 283}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 620', 'id': 285}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '8 GB', 'id': 296}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/0', 'id': 312}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 355}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW DualLayer', 'id': 383}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3350 mAh, Li-Ion', 'id': 427}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '27,7 mm', 'id': 428}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '381 mm', 'id': 429}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '252 mm', 'id': 430}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '2,00 kg (z baterią)', 'id': 431}]
        }]
      }, {
        'id': 42,
        'name': 'Dell Inspiron 3567 i3-6006U/8GB/256+1000/Win10 FHD',
        'price': 2249,
        'quantity': 77,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3567-i3-6006u8gb2561000win10-fhd-425280,2017/11/pr_2017_11_2_15_4_58_581_01.jpg',
          'main': true,
          'id': 232
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3567-i3-6006u8gb2561000win10-fhd-425280,2017/11/pr_2017_11_2_15_4_54_690_00.jpg',
          'main': false,
          'id': 233
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3567-i3-6006u8gb2561000win10-fhd-425280,2017/11/pr_2017_11_2_15_4_20_6_00.jpg',
          'main': false,
          'id': 234
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3567-i3-6006u8gb2561000win10-fhd-425280,2017/11/pr_2017_11_2_15_4_24_209_01.jpg',
          'main': false,
          'id': 235
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3567-i3-6006u8gb2561000win10-fhd-425280,2017/11/pr_2017_11_2_15_4_28_537_02.jpg',
          'main': false,
          'id': 236
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-3567-i3-6006u8gb2561000win10-fhd-425280,2017/11/pr_2017_11_2_15_4_32_753_03.jpg',
          'main': false,
          'id': 237
        }],
        'reviews': [],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {'name': 'Bateria (podstawowa)', 'id': 323}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}, {
            'name': 'LAN 10/100 Mbps',
            'id': 395
          }]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-6006U (2 rdzenie, 2.00 GHz, 3 MB cache)', 'id': 310}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 520', 'id': 315}]
        }, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '380 mm', 'id': 319}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW DualLayer', 'id': 383}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '4-komorowa, 2750 mAh, Li-Ion', 'id': 384}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '260 mm', 'id': 386}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '24,1 mm', 'id': 433}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '2,26 kg (z baterią)', 'id': 434}]}]
      }, {
        'id': 43,
        'name': 'ASUS ZenBook UX410UA i5-8250U/8GB/256SSD/Win10',
        'price': 3799,
        'quantity': 16,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux410ua-i5-8250u8gb256ssdwin10-421763,2017/10/pr_2017_10_2_9_27_35_398_00.jpg',
          'main': true,
          'id': 238
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux410ua-i5-8250u8gb256ssdwin10-421763,2017/10/pr_2017_10_2_9_27_41_821_02.jpg',
          'main': false,
          'id': 239
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux410ua-i5-8250u8gb256ssdwin10-421763,2017/10/pr_2017_10_2_9_27_53_619_06.jpg',
          'main': false,
          'id': 240
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux410ua-i5-8250u8gb256ssdwin10-421763,2017/10/pr_2017_10_2_9_27_38_586_01.jpg',
          'main': false,
          'id': 241
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux410ua-i5-8250u8gb256ssdwin10-421763,2017/10/pr_2017_10_2_9_27_59_463_08.jpg',
          'main': false,
          'id': 242
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux410ua-i5-8250u8gb256ssdwin10-421763,2017/10/pr_2017_10_2_9_27_56_603_07.jpg',
          'main': false,
          'id': 243
        }],
        'reviews': [],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Podświetlana klawiatura', 'id': 255}, {
            'name': 'Białe podświetlenie klawiatury',
            'id': 256
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {'name': 'Firmowe etui na laptopa', 'id': 439}, {
            'name': 'Firmowa mysz',
            'id': 440
          }, {'name': 'Przejściówka HDMI -&gt; VGA', 'id': 441}, {'name': 'Przejściówka USB -&gt; RJ-45', 'id': 442}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,35 kg (z baterią)', 'id': 280}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Dysk SSD M.2 PCIe',
          'filterable': false,
          'id': 39,
          'options': [{'name': '256 GB', 'id': 297}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, EWV', 'id': 364}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '14,0"', 'id': 365}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '19 mm', 'id': 367}]
        }, {
          'name': 'Miejsce na dodatkowy wewnętrzny dysk SATA',
          'filterable': false,
          'id': 43,
          'options': [{'name': 'Możliwość montażu dysku SATA (brak elementów montażowych)', 'id': 376}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/1', 'id': 435}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 4211 mAh, Li-Ion', 'id': 436}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '323 mm', 'id': 437}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '224 mm', 'id': 438}]
        }]
      }, {
        'id': 44,
        'name': 'ASUS VivoBook S14 S410 i5-8250U/8GB/256SSD/Win10 MX150',
        'price': 3199,
        'quantity': 93,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410-i5-8250u8gb256ssdwin10-mx150-403879,2018/1/pr_2018_1_22_7_57_7_910_00.jpg',
          'main': true,
          'id': 244
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410-i5-8250u8gb256ssdwin10-mx150-403879,2018/1/pr_2018_1_22_7_56_13_921_01.jpg',
          'main': false,
          'id': 245
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410-i5-8250u8gb256ssdwin10-mx150-403879,2018/1/pr_2018_1_22_7_56_21_422_03.jpg',
          'main': false,
          'id': 246
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410-i5-8250u8gb256ssdwin10-mx150-403879,2018/1/pr_2018_1_22_7_56_17_828_02.jpg',
          'main': false,
          'id': 247
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410-i5-8250u8gb256ssdwin10-mx150-403879,2018/1/pr_2018_1_22_7_39_55_259_01.jpg',
          'main': false,
          'id': 248
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-s14-s410-i5-8250u8gb256ssdwin10-mx150-403879,2018/1/pr_2018_1_22_7_56_10_124_00.jpg',
          'main': false,
          'id': 249
        }],
        'reviews': [{
          'rating': 3,
          'authorName': 'Kuba',
          'date': '2018-03-23T10:46:00.000Z',
          'content': 'Jestem zdziwiony, że w ciągu 2 tygodni cena zmieniła się o 100 zł, bo ja kupowałem za 3599 zł, co BARDZO mi się nie podoba... Odnośnie samego sprzętu to problemem jest klawiatura, na której odbijają się odciski palców oraz przeciętnej jakości matryca. Elementy nie są dobrze spasowane, pod naciskiem w okolicach touchpada słychać trzaski. Wielkim problemem jest działanie karty sieciowej, sterowniki od producenta tylko pogarszają sprawę. Na plus bateria, rozmiar, waga, podświetlenie klawiatury.',
          'id': 463
        }, {
          'rating': 6,
          'authorName': 'Maciej',
          'date': '2018-04-12T21:26:00.000Z',
          'content': 'Studiuję informatykę i korzystam dużo z VisualStudio i PhotoShop. Ten Asus S14 to fajna alternatywa dla Macbooka Air. Windows przy dostosowaniu ustawień działa naprawdę dobrze i nie trzeba mieć oryginalnych programów. Fajnie też, że w tak małym i smukłym sprzęcie jest złącze HDMI, bo mogę podpiąć dodatkowy monitor w domu, bez skomplikowanych adapterów. Laptop jest lekki i mieście się w plecaku. Do gier nie używałem, bo nie przepadam. Polecam sprzęt ;-)',
          'id': 464
        }, {
          'rating': 5,
          'authorName': 'Hyrdus',
          'date': '2018-04-19T14:55:00.000Z',
          'content': 'Jestem zniesmaczony faktem że zamówiłem go za 3599zł, w dniu odbioru w salonie cena spadła o 100zł lecz sprzedawca nie chciał jej obniżyć. No trudno. Sam laptop bardzo dobry. Kapitalna matryca IPS, świetny zawsze chłodny palmrest, lekka i bardzo mocna konstrukcja. Naprawdę używa mi go znacznie wygodniej niż firmowego Macbooka Air :) Na plus wygląd, bateria, potężna specyfikacja. Na minus obsługa w dniu zakupu a i głośne chłodzenie.',
          'id': 465
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-04-19T15:17:00.000Z',
          'content': 'Przykro nam z powodu powstałych niedogodności. Prosimy o kontakt telefonicznie pod numerem 34 377 00 00 lub mailowo pod adresem x-kom@x-kom.pl. Wyjaśnimy tą sprawę dla Ciebie.',
          'id': 466
        }, {
          'rating': 4,
          'authorName': 'Łukasz',
          'date': '2018-04-20T09:10:00.000Z',
          'content': 'Generalnie laptop bardzo w porządku.  Plastikowy korpus absolutnie nie przeszkadza (w aspekcie temperatury obudowy jest wręcz na plus). Klawiatura bardzo przyjemna, gładzik niestety wypada już średnio - poślizg jest prawidłowy i co do precyzji również nie mam większych zastrzeżeń ale wydaje z siebie nieprzyjemny odgłos "puknięcia" jakby był luźno zamocowany. Problemem jest niestety ekran i to nawet nie w kwestii barw a prześwietleń co widać na zdjęciu. Na dole w rogach i mniej więcej na środku.',
          'id': 467
        }, {
          'rating': 4,
          'authorName': 'Jarecki',
          'date': '2018-06-30T06:15:00.000Z',
          'content': 'Laptop generalnie ok, ale ... \nTouchpad często żyje własnym życiem , w porównaniu z MacBook - przepaść , poza tym hałas , laptop bez obciążenia jest wyraźnie słyszalny , moim zdaniem za bardzo.',
          'id': 468
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-07-02T11:23:00.000Z',
          'content': 'Jeśli z Twoim produktem występują jakiekolwiek problemy, prosimy o kontakt: serwis@x-kom.pl lub pod numerem 34 377 00 30. Postaramy się pomóc.',
          'id': 469
        }, {
          'rating': 4,
          'authorName': 'Grzesiek',
          'date': '2018-07-05T22:44:00.000Z',
          'content': 'ZALETY: 1. Konfiguracja sprzętowa w rozsądnej cenie, 2. Cienka ramka wokół ekranu, małe rozmiary i niska waga, 3. W miarę dokładne i dobre wykonanie, chociaż nie wiem czy byłby solidny bo jednak plastik cienki i trochę się uginający. 4. Ładny. WADY: 1. Ekran o trochę niskiej jasności, 2. Przycisk uruchamiania wśród klawiszy klawiatury, 3. Karta sieciowa mimo poprawnych sterowników co chwilę się wyłączała i nie dało się go używać. Na szczęście sprzedawca po drugim zgłoszeniu uznał reklamację.',
          'id': 470
        }, {
          'rating': 6,
          'authorName': 'Szymon',
          'date': '2018-09-05T05:49:00.000Z',
          'content': 'Mały, lekki, poręczny i wydajny.\nUruchamia się w kilka sekund więc ssd na m2 świetna sprawa.\nGTA 5 działa, klatek co prawda nie mierzyłem ale na oko płynnie wszystko.\nChciałbym jeszcze dodać iż ten komputer mieści się swobodnie w etui 13,2 cala mimo ekranu 14 calowego ... dziwne :)',
          'id': 471
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Białe podświetlenie klawiatury', 'id': 256}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Wbudowany czytnik linii papilarnych', 'id': 260}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '1/0', 'id': 312}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce MX150', 'id': 326}, {'name': '+ Intel UHD Graphics 620', 'id': 327}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Miejsce na dodatkowy wewnętrzny dysk SATA',
          'filterable': false,
          'id': 43,
          'options': [{'name': 'Możliwość montażu dysku SATA (elementy montażowe w zestawie - sanki)', 'id': 353}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 355}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '20 GB', 'id': 363}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, EWV', 'id': 364}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '14,0"', 'id': 365}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3653 mAh, Li-Ion', 'id': 366}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19 mm', 'id': 367}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '326 mm', 'id': 368}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '226 mm', 'id': 369}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,43 kg (z baterią)', 'id': 370}]
        }]
      }, {
        'id': 45,
        'name': 'HP Pavilion x360  i3-7100U/4GB/128SSD/W10 FHD Touch',
        'price': 2199,
        'quantity': 56,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i3-7100u4gb128ssdw10-fhd-touch-412775,2017/7/pr_2017_7_19_10_47_9_75.jpg',
          'main': true,
          'id': 250
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i3-7100u4gb128ssdw10-fhd-touch-412775,2017/7/pr_2017_7_19_10_47_52_266.jpg',
          'main': false,
          'id': 251
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i3-7100u4gb128ssdw10-fhd-touch-412775,2017/7/pr_2017_7_19_10_45_23_442.jpg',
          'main': false,
          'id': 252
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i3-7100u4gb128ssdw10-fhd-touch-412775,2017/7/pr_2017_7_19_10_45_26_536.jpg',
          'main': false,
          'id': 253
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i3-7100u4gb128ssdw10-fhd-touch-412775,2017/7/pr_2017_7_19_10_45_30_474.jpg',
          'main': false,
          'id': 254
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,hp-pavilion-x360-i3-7100u4gb128ssdw10-fhd-touch-412775,2017/7/pr_2017_7_19_10_45_33_661.jpg',
          'main': false,
          'id': 255
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Piotrowski Michał ',
          'date': '2018-04-18T19:47:00.000Z',
          'content': 'Laptopa używa córka. Czasami żona pożycza, aby przeglądać internet, albo oglądać jakieś filmy. Laptop działa płynnie. Jest lekki i ma dotykowy ekran. Zawiasy solidne, ale to trzeba oceniac po dwóch latach, a nie miesiącu. Najważniejsze było, aby córka mogła zabierać sprzęt do szkoły, gdy mają zadane prezentacje. Wszystkie oczekiwania zostały spełnione. Polecamy laptop HP do nauki i codziennego używania.',
          'id': 472
        }, {
          'rating': 6,
          'authorName': 'jumper',
          'date': '2018-05-19T23:37:00.000Z',
          'content': 'lapka kupiłem dla córki(pełnoletniej). Dopiero się z nim zapoznaje ale z tego , co wiem, jest zachwycona(miała przedtem inne laptopy)\nPolecam!',
          'id': 473
        }, {
          'rating': 6,
          'authorName': 'Magda',
          'date': '2018-07-26T16:46:00.000Z',
          'content': 'Spełnia wszystkie moje oczekiwania, bardzo fajnie się na nim pracuje jest wygodny i całkiem lekki, nie mam problemu z zabieraniem go do pracy, czy na weekend. Zarówno do pracy czy do chwil relaksu świetnie się sprawdził:)',
          'id': 474
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '4 GB', 'id': 95}, {'name': '4 GB (SO-DIMM DDR4, 2133MHz)', 'id': 284}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '227 mm', 'id': 279}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-7100U (2 rdzenie, 2.40 GHz, 3 MB cache)', 'id': 283}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel HD Graphics 620', 'id': 285}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '14,0"', 'id': 365}]}, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS, dotykowy', 'id': 377}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,9 mm', 'id': 379}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '335 mm', 'id': 380}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '1,64 kg (z baterią)', 'id': 381}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3470 mAh, Li-Ion', 'id': 443}]
        }]
      }, {
        'id': 46,
        'name': 'ASUS VivoBook R520UF i5-8250/8GB/256SSD+1TB/Win10',
        'price': 2999,
        'quantity': 60,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520uf-i5-82508gb256ssd1tbwin10-443794,2018/5/pr_2018_5_24_13_44_13_433_07.jpg',
          'main': true,
          'id': 256
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520uf-i5-82508gb256ssd1tbwin10-443794,2018/5/pr_2018_5_24_13_44_10_433_06.jpg',
          'main': false,
          'id': 257
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520uf-i5-82508gb256ssd1tbwin10-443794,2018/5/pr_2018_5_24_13_44_0_323_02.jpg',
          'main': false,
          'id': 258
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520uf-i5-82508gb256ssd1tbwin10-443794,2018/5/pr_2018_5_24_13_43_54_886_00.jpg',
          'main': false,
          'id': 259
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520uf-i5-82508gb256ssd1tbwin10-443794,2018/5/pr_2018_5_24_13_44_7_902_05.jpg',
          'main': false,
          'id': 260
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520uf-i5-82508gb256ssd1tbwin10-443794,2018/5/pr_2018_5_24_13_44_51_527_00.jpg',
          'main': false,
          'id': 261
        }],
        'reviews': [],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': '+ Intel UHD Graphics 620', 'id': 327}, {'name': 'NVIDIA GeForce MX130', 'id': 444}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3727 mAh, Li-Ion', 'id': 348}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,5 mm', 'id': 349}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '361 mm', 'id': 350}]
        }, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '243 mm', 'id': 351}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk rozszerzony na profesjonalnej linii montażowej x-kom', 'id': 382}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '1,56 kg (z baterią)', 'id': 445}]}]
      }, {
        'id': 47,
        'name': 'ASUS VivoBook R520UA i3-8130U/8GB/240SSD+1TB/Win10',
        'price': 2549,
        'quantity': 48,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431606,2018/5/pr_2018_5_24_18_19_12_321_06.jpg',
          'main': true,
          'id': 262
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431606,2018/5/pr_2018_5_24_18_18_56_821_00.jpg',
          'main': false,
          'id': 263
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431606,2018/5/pr_2018_5_24_18_21_19_635_00.jpg',
          'main': false,
          'id': 264
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431606,2018/5/pr_2018_5_24_18_18_59_711_01.jpg',
          'main': false,
          'id': 265
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431606,2018/5/pr_2018_5_24_18_19_9_883_05.jpg',
          'main': false,
          'id': 266
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-vivobook-r520ua-i3-8130u8gb240ssd1tbwin10-431606,2018/5/pr_2018_5_24_18_19_7_368_04.jpg',
          'main': false,
          'id': 267
        }],
        'reviews': [],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Szyfrowanie TPM', 'id': 261}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 2 szt.', 'id': 347}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 262}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel UHD Graphics 620', 'id': 299}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i3-8130U (2 rdzenie, od 2.2 GHz do 3.4 GHz, 4MB cache)', 'id': 346}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3727 mAh, Li-Ion', 'id': 348}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '19,5 mm', 'id': 349}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '361 mm', 'id': 350}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '243 mm', 'id': 351}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,67 kg (z baterią)', 'id': 446}]
        }]
      }, {
        'id': 48,
        'name': 'ASUS R542UF-DM157T i5-8250U/8GB/240+1TB/Win10 MX130',
        'price': 3199,
        'quantity': 48,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r542uf-dm157t-i5-8250u8gb2401tbwin10-mx130-427453,2017/9/pr_2017_9_14_10_29_53_823_02.jpg',
          'main': true,
          'id': 268
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r542uf-dm157t-i5-8250u8gb2401tbwin10-mx130-427453,2017/9/pr_2017_9_14_10_29_46_760_00.jpg',
          'main': false,
          'id': 269
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r542uf-dm157t-i5-8250u8gb2401tbwin10-mx130-427453,2017/9/pr_2017_9_14_10_29_49_916_01.jpg',
          'main': false,
          'id': 270
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r542uf-dm157t-i5-8250u8gb2401tbwin10-mx130-427453,2017/9/pr_2017_9_14_10_29_57_464_03.jpg',
          'main': false,
          'id': 271
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r542uf-dm157t-i5-8250u8gb2401tbwin10-mx130-427453,2017/7/pr_2017_7_26_13_18_48_817.jpg',
          'main': false,
          'id': 272
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-r542uf-dm157t-i5-8250u8gb2401tbwin10-mx130-427453,2017/7/pr_2017_7_26_13_18_51_911.jpg',
          'main': false,
          'id': 273
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Tomasz',
          'date': '2018-08-17T08:27:00.000Z',
          'content': 'Laptop sprawuje się bardzo dobrze. Lekki, szybki i wygodny. Jedyną wadą jest kamerka której obraz jest przeciętnej jakości. Niemniej gorąco polecam.',
          'id': 475
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wydzielona klawiatura numeryczna',
            'id': 257
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Szyfrowanie TPM',
            'id': 261
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '15,6"', 'id': 229}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {
            'name': 'Wi-Fi 802.11 a/b/g/n/ac',
            'id': 238
          }, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}, {'name': 'USB 2.0 - 1 szt.', 'id': 288}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '0.3 Mpix', 'id': 316}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '380 mm', 'id': 319}]}, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2400MHz)', 'id': 324}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/1', 'id': 325}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': '+ Intel UHD Graphics 620', 'id': 327}, {'name': 'NVIDIA GeForce MX130', 'id': 444}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Dysk rozszerzony na profesjonalnej linii montażowej x-kom', 'id': 382}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Nagrywarka DVD+/-RW DualLayer', 'id': 383}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '2-komorowa, 4940 mAh, Li-Polymer', 'id': 447}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '23,2 mm', 'id': 448}]}, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '251 mm', 'id': 449}]
        }, {'name': 'Waga', 'filterable': false, 'id': 21, 'options': [{'name': '2,07 kg (z baterią)', 'id': 450}]}]
      }, {
        'id': 49,
        'name': 'ASUS ZenBook UX430UN i7-8550U/16GB/512SSD/Win10 MX150',
        'price': 4999,
        'quantity': 60,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux430un-i7-8550u16gb512ssdwin10-mx150-396722,2017/7/pr_2017_7_25_9_44_59_88.jpg',
          'main': true,
          'id': 274
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux430un-i7-8550u16gb512ssdwin10-mx150-396722,2017/7/pr_2017_7_25_9_45_15_136.jpg',
          'main': false,
          'id': 275
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux430un-i7-8550u16gb512ssdwin10-mx150-396722,2017/7/pr_2017_7_25_9_47_9_82.jpg',
          'main': false,
          'id': 276
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux430un-i7-8550u16gb512ssdwin10-mx150-396722,2017/7/pr_2017_7_25_9_45_4_698.jpg',
          'main': false,
          'id': 277
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux430un-i7-8550u16gb512ssdwin10-mx150-396722,2017/7/pr_2017_7_25_9_45_18_418.jpg',
          'main': false,
          'id': 278
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,asus-zenbook-ux430un-i7-8550u16gb512ssdwin10-mx150-396722,2017/7/pr_2017_7_25_9_45_22_230.jpg',
          'main': false,
          'id': 279
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'SDW',
          'date': '2018-02-04T21:35:00.000Z',
          'content': 'Tak - mocny komputer, tak - bardzo wysoka jakość materiałów, tak - satysfakcja z użytkowania. W pudełku myszka przewodowa w gratisie i jeszcze druga w sklepie. Słychać wentylator, gdy procesor obciążony na max. jednak w planie "Zrównoważonym" jest cichy. Wygodna klawiatura, długi czas pracy na akumulatorze. Polecam, uważam, że wart jest swojej ceny.',
          'id': 476
        }, {
          'rating': 6,
          'authorName': 'Przemek',
          'date': '2018-03-21T11:11:00.000Z',
          'content': 'PLUSY:\n - ładny, szybki i wydajny,\n - bardzo miła w dotyku i pracy klawiatura\n - gładzik wielodotykowy, wystarczająco śliski i precyzyjny. \n - wyświetlacz. raz, że matowy, a dwa, ze dobrze odwzorowuje kolory. \n - lekki, poręczny, mała ładowarka. \n - głośniki są naprawdę \nMINUSY:\n - coil whine, czyli dość nieprzyjemny, chociaż cichy, dźwięk pracy cewki. \n - czytnik linii papilarnych - skuteczność ok. 80%.\n - preinstalowany system jest zawalony bloatwarem',
          'id': 477
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-06-11T17:07:00.000Z',
          'content': 'To już mój 3 zenbook i za każdym razem jestem coraz bardziej zaskoczony jakością i estetyką! W mojej pracy prezencja ma bardzo duże znaczenie a z takim laptopem od razu widać że "gość ma styl". Dodatkowo świetne parametry dla wymagających!',
          'id': 478
        }, {
          'rating': 6,
          'authorName': 'Via',
          'date': '2018-07-02T14:36:00.000Z',
          'content': 'świetna 14-tka z dobrą matrycą, jasne podświetlenie, naturalne kolory, minimalistyczne ramki. Obudowa również solidna i sprzęt śmiga szybko. Jest lekki. Przy długim renderowaniu bez podstawki chłodzącej czuć, że robi się ciepły, ale przy normalnej pracy, obróbce fotek - chłodny. W moim odczuciu - super sprzęt. Ponadprzeciętny użytkownik będzie zadowolony.',
          'id': 479
        }, {
          'rating': 5,
          'authorName': 'Piotr',
          'date': '2018-07-30T18:07:00.000Z',
          'content': 'Elegancki i szybki laptop. Bardzo jestem zadowolony z niego. Prezentuje się idealnie i bateria starcza na długo. Jest mega cichy, ale też trochę się grzeje. Na szczęście nie ma throttlingu i da się z tym żyć. Cóż. Taka cena bardzo smukłej obudowy.',
          'id': 480
        }, {
          'rating': 2,
          'authorName': 'Mateusz',
          'date': '2018-08-04T20:02:00.000Z',
          'content': 'Wykonanie super, moc mega... \nale co z tego kiedy \npisk cewek wierci mi dziurę w mózgu... powaga, wiem, ze cewki CZASEM popiskują czy to na karcie graficznej czy w laptopie, ale ten ultrabook sieje piskiem głośniej od wentylatorów, słyszę go nawet przy włączonej klimatyzacji...  mam nadzieje, ze jest to pechowy egzemplarz i zaraz po weekendzie wymienię go na niewadliwy. Chociaż sadząc po postach w sieci boję się, że nie ma „niewadliwych”, podobno piszczą DELLe, Asusy, Samsungi serii 9.',
          'id': 481
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-08-06T06:38:00.000Z',
          'content': 'Jeżeli z Twoim produktem występują jakiekolwiek problemy, prosimy o kontakt z naszym Serwisem pod adresem serwis@x-kom.pl lub telefonicznie pod numerem 34 377 00 30. Postaramy się pomóc.',
          'id': 482
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {'name': 'Czytnik kart pamięci - 1 szt.', 'id': 15}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Podświetlana klawiatura', 'id': 255}, {
            'name': 'Białe podświetlenie klawiatury',
            'id': 256
          }, {'name': 'Wielodotykowy, intuicyjny touchpad', 'id': 258}, {
            'name': 'Wbudowany czytnik linii papilarnych',
            'id': 260
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {'name': 'Ochrona dostępu do dysków twardych za pomocą hasła', 'id': 458}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}, {
            'name': 'Firmowe etui na laptopa',
            'id': 439
          }, {'name': 'Przejściówka USB -&gt; RJ-45', 'id': 442}, {'name': 'Przejściówka micro HDMI -&gt; HDMI', 'id': 459}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.',
            'id': 286
          }, {'name': 'USB 2.0 - 1 szt.', 'id': 288}, {'name': 'Micro HDMI - 1 szt.', 'id': 454}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce MX150', 'id': 326}, {'name': '+ Intel UHD Graphics 620', 'id': 327}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED, EWV', 'id': 364}]
        }, {'name': 'Przekątna ekranu', 'filterable': false, 'id': 6, 'options': [{'name': '14,0"', 'id': 365}]}, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '15,9 mm', 'id': 422}]
        }, {'name': 'Szerokość', 'filterable': false, 'id': 19, 'options': [{'name': '324 mm', 'id': 423}]}, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i7-8550U (4 rdzenie, od 1.80 GHz do 4.00 GHz, 8 MB cache)', 'id': 451}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '16 GB (SO-DIMM DDR3, 1866 MHz)', 'id': 452}]
        }, {'name': 'Dysk SSD M.2', 'filterable': false, 'id': 28, 'options': [{'name': '512 GB', 'id': 453}]}, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 4335 mAh, Li-Polymer', 'id': 455}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '225 mm', 'id': 456}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,30 kg (z baterią)', 'id': 457}]
        }]
      }, {
        'id': 50,
        'name': 'Apple MacBook Pro i5 2,3GHz/8GB/128/Iris 640 Space Gray',
        'price': 5699,
        'quantity': 14,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-pro-i5-23ghz8gb128iris-640-space-gray-368643,2017/6/pr_2017_6_6_15_52_32_403.jpg',
          'main': true,
          'id': 280
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-pro-i5-23ghz8gb128iris-640-space-gray-368643,2017/6/pr_2017_6_6_15_52_36_716.jpg',
          'main': false,
          'id': 281
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-pro-i5-23ghz8gb128iris-640-space-gray-368643,2017/6/pr_2017_6_6_15_52_28_886.jpg',
          'main': false,
          'id': 282
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,apple-macbook-pro-i5-23ghz8gb128iris-640-space-gray-368643,2017/6/pr_2017_6_6_15_53_10_533.jpg',
          'main': false,
          'id': 283
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'martom',
          'date': '2018-07-11T07:10:00.000Z',
          'content': 'Kupiłem rok temu do pracy. Fajny sprzęt, szybki, płynny, dobry dźwięk, przejrzysty obraz. Nie mogę się do niczego doczepić.',
          'id': 483
        }, {
          'rating': 5,
          'authorName': 'Sumek',
          'date': '2018-09-03T11:31:00.000Z',
          'content': 'Apple to Apple, chociaż za duża rozdzielczość czasami jak na 13 cali.',
          'id': 484
        }, {
          'rating': 6,
          'authorName': 'foger',
          'date': '2018-09-04T00:20:00.000Z',
          'content': 'Dziś go odebrałem, wielkie dzięki dla x-kom za świetny rabat z którego miałem okazje skorzystać raz jak zawsze za fachową obsługę, co do Maca... to mój pierwszy sprzęt spod  nagryzionego jabłka, wykonanie, ekran, głośniki, klawiatura,bateria, wszystko na najwyższym na wypasie... system całkiem przyjazny, myślę że za kilka dni będę już zżyty, aktualnie się poznajemy ale już wiem że będzie to" prawdziwa przyjaźń " Nie bójcie się odstawić windowsa. Sprzęt swoje kosztował ale uważam że warto,Polecam',
          'id': 485
        }, {
          'rating': 6,
          'authorName': 'Yakkow91',
          'date': '2018-09-05T20:48:00.000Z',
          'content': 'Bardzo fajny sprzęcior! Polecam!',
          'id': 486
        }],
        'attributes': [{
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Podświetlana klawiatura',
            'id': 255
          }, {'name': 'Aluminiowa obudowa', 'id': 281}, {
            'name': 'Wielodotykowy gładzik Force Touch',
            'id': 470
          }, {'name': 'Klawiatura w układzie UK', 'id': 471}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio',
            'id': 235
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'USB Typu-C (z Thunderbolt) - 2 szt.', 'id': 465}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '0/0 (pamięć wlutowana)', 'id': 266}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': 'Pamięć współdzielona', 'id': 271}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': 'FaceTime HD', 'id': 272}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'macOS Sierra', 'id': 276}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '12 miesięcy (gwarancja producenta)', 'id': 282}]
        }, {
          'name': 'Maksymalna obsługiwana ilość pamięci RAM',
          'filterable': false,
          'id': 26,
          'options': [{'name': '8 GB', 'id': 296}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5 (2 rdzenie, od 2.3 GHz do 3.6 GHz, 4 MB cache)', 'id': 460}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR3, 2133 MHz)', 'id': 461}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Błyszczący, LED, IPS, Retina', 'id': 462}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2560 x 1600 (WQXGA)', 'id': 463}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'Intel Iris Graphics 640', 'id': 464}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '14,9 mm', 'id': 466}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '305 mm', 'id': 467}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '212 mm', 'id': 468}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,38 kg (z baterią)', 'id': 469}]
        }]
      }, {
        'id': 51,
        'name': 'Dell Inspiron 5370 i5-8250U/8GB/256/Win10 R530 FHD',
        'price': 3179,
        'quantity': 7,
        'category': {'name': 'Notebooks', 'id': 2},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i5-8250u8gb256win10-r530-fhd-393456,2017/12/pr_2017_12_14_9_40_43_405_10.jpg',
          'main': true,
          'id': 284
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i5-8250u8gb256win10-r530-fhd-393456,2017/12/pr_2017_12_14_9_40_39_186_09.jpg',
          'main': false,
          'id': 285
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i5-8250u8gb256win10-r530-fhd-393456,2017/12/pr_2017_12_14_9_40_4_668_01.jpg',
          'main': false,
          'id': 286
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i5-8250u8gb256win10-r530-fhd-393456,2017/12/pr_2017_12_14_9_40_10_388_02.jpg',
          'main': false,
          'id': 287
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i5-8250u8gb256win10-r530-fhd-393456,2017/12/pr_2017_12_14_9_40_15_622_03.jpg',
          'main': false,
          'id': 288
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,dell-inspiron-5370-i5-8250u8gb256win10-r530-fhd-393456,2017/12/pr_2017_12_14_9_40_19_435_04.jpg',
          'main': false,
          'id': 289
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Mariusz',
          'date': '2018-01-27T16:24:00.000Z',
          'content': 'Mały zgrabny laptop o dobrych parametrach, solidnie wykonany, bardzo dobra matryca, sztywne solidne zawiasy, brak podświetlenia klawiszy, długi czas pracy na baterii nawet 7 godzin. Ogólnie polecam .',
          'id': 487
        }, {
          'rating': 6,
          'authorName': 'pwrlax',
          'date': '2018-03-01T16:49:00.000Z',
          'content': 'Naprawdę super sprzęt, solidnie wykonany, dobre parametry, świetny wygląd, super wydajność na potrzeby domowe. Uruchamia się kilka sekund, podobnie zamyka. Jak za te pieniądze naprawdę ekstra- miałem wcześniej Asusa VivoBook S14 ale odstawał wyraźnie wykonaniem.\nJedyne co mu brakuje do perfekcji to podświetlana klawiatura.',
          'id': 488
        }, {
          'rating': 6,
          'authorName': 'Anna W.',
          'date': '2018-04-14T18:36:00.000Z',
          'content': 'Lekki i elegancki sprzęt. Pracuje na nim i nie mam zastrzeżeń. Na polecenia reaguje błyskawicznie. Matryca duża, jasna, kolorystyka bardzo dobra. Bateria z tego co zdążyłam przetestować trzyma kilka dobrych godzin i szybko się ładuje.&nbsp;',
          'id': 489
        }, {
          'rating': 5,
          'authorName': 'Kasia',
          'date': '2018-05-27T08:45:00.000Z',
          'content': 'W tej cenie nie ma nic lepszego, laptop sprawuje się świetnie do kodzenia i przelgądania internetu, bateria trzyma 8-9h duży minus za grafikę',
          'id': 490
        }, {
          'rating': 5,
          'authorName': 'Leszek',
          'date': '2018-07-06T20:57:00.000Z',
          'content': 'Fajny, dobrze jakościowo wykonany laptop. Jedyny minus to dość głośny chwilami wentylator ale idzie się przyzwyczaić.',
          'id': 491
        }, {
          'rating': 6,
          'authorName': 'MIchał',
          'date': '2018-08-25T16:43:00.000Z',
          'content': 'Bardzo dobry laptop, szybki, ładny i jakościowo wykonany na poziomie bardzo dobrym. Jeśli ktoś natomiast kupuje laptopa i nie potrzebuje wydajności graficznej to polecam wyłączyć w menedżerze kartę graficzną Radeona. Powinno poprawić to czas pracy na baterii, a na pewno niweluje losowe buczenie tej karty w różnych momentach. Oprócz tego co napisałem wcześniej kultura pracy na najwyższym poziomie, laptopa nie słychać w ogóle.',
          'id': 492
        }],
        'attributes': [{
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wbudowane głośniki stereo', 'id': 44}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Możliwość zabezpieczenia linką (port Noble Wedge)', 'id': 259}, {'name': 'Aluminiowa obudowa', 'id': 281}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '16 GB', 'id': 61}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Zasilacz', 'id': 208}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '1920 x 1080 (FullHD)', 'id': 230}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}]
        }, {'name': 'Kamera internetowa', 'filterable': false, 'id': 34, 'options': [{'name': '1.0 Mpix', 'id': 236}]}, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {'name': 'Moduł Bluetooth', 'id': 239}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'HDMI - 1 szt.', 'id': 241}, {
            'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.',
            'id': 245
          }, {'name': 'DC-in (wejście zasilania) - 1 szt.', 'id': 246}, {
            'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.',
            'id': 274
          }, {'name': 'Czytnik kart pamięci microSD - 1 szt.', 'id': 287}, {'name': 'Noble Lock - 1 szt.', 'id': 420}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '13,3"', 'id': 268}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)', 'id': 294}]
        }, {'name': 'Dysk SSD M.2 PCIe', 'filterable': false, 'id': 39, 'options': [{'name': '256 GB', 'id': 297}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Matowy, LED', 'id': 314}]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': '+ Intel UHD Graphics 620', 'id': 327}, {'name': 'AMD Radeon 530', 'id': 472}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom', 'id': 355}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': '3-komorowa, 3166 mAh, Li-Ion', 'id': 421}]
        }, {'name': 'Wysokość', 'filterable': false, 'id': 20, 'options': [{'name': '15,9 mm', 'id': 422}]}, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '324 mm', 'id': 423}]
        }, {'name': 'Głębokość', 'filterable': false, 'id': 37, 'options': [{'name': '220 mm', 'id': 424}]}, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '1,42 kg (z baterią)', 'id': 425}]
        }]
      }, {
        'id': 52,
        'name': 'Intel i5-8400 2.80GHz 9MB BOX',
        'price': 899,
        'quantity': 77,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-8400-280ghz-9mb-box-383500,2017/10/pr_2017_10_5_14_35_20_906_00.jpg',
          'main': true,
          'id': 290
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Kacpelux',
          'date': '2017-10-18T17:24:00.000Z',
          'content': 'Polecam gorąco, w pełni spełnia moje oczekiwania, bardzo dobra wydajność.\nPod obciążeniem taktowanie wynosi 3,8GHz dla 6 rdzeni!',
          'id': 493
        }, {
          'rating': 6,
          'authorName': 'Łazarz',
          'date': '2017-10-23T10:43:00.000Z',
          'content': 'prawuje się idealnie jak na swoja cene',
          'id': 494
        }, {
          'rating': 6,
          'authorName': 'Janek',
          'date': '2018-01-21T11:01:00.000Z',
          'content': 'Ja również polecam jak tylko będzie znów dostępny :)\nProcesor bardzo dobry spełnia oczekiwania. W grach żadnych problemów. 6 rdzeni jeszcze przez jakiś czas nie będzie w pełni wykorzystywane w grach ale jak juz do tego dojdzie to odrazu będzie plus posiadania tego procesora a zawsze można na i7 wymienić tez ósmej generacji\nNie ma z nim żadnych problemów. Z dodatkowym chłodzeniem nie boxowym za jedyne 70zł temperatury są bardzo dobre. W spoczynku zaledwie 30 stopni a w stresie też nie są wysokie',
          'id': 495
        }, {
          'rating': 6,
          'authorName': 'Darek',
          'date': '2018-03-18T07:38:00.000Z',
          'content': 'Czy jest dodawane chłodzenie ? Bo nie macie w opisie nic.',
          'id': 496
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-03-19T13:31:00.000Z',
          'content': 'Procesor Intel i5-8400 posiada w zestawie chłodzenie. Natomiast wersja i5-8600K sprzedawana jest bez wentylatora. Zachęcamy do zakupu. A w razie dodatkowych pytań nasi Doradcy są do dyspozycji pod adresem x-kom@x-kom.pl lub telefonicznie pod numerem 34 377 00 00.',
          'id': 497
        }, {
          'rating': 6,
          'authorName': 'Emin3X',
          'date': '2018-04-20T14:45:00.000Z',
          'content': 'Chyba najlepszy możliwy wybór w takiej cenie. 6 rdzeni robi robotę!',
          'id': 498
        }, {
          'rating': 6,
          'authorName': 'plej',
          'date': '2018-04-22T06:44:00.000Z',
          'content': '6 rdzeni stabilnie - przy dodatkowym chłodzeniu, dobrej paście i przewiewnej obudowie 24-26 stopni, przy normalnym graniu 60 stopni w Core Temp jeszcze nie widziałem. Miła odmiana po I5-4570. Polecam.',
          'id': 499
        }, {
          'rating': 6,
          'authorName': 'Valik',
          'date': '2018-05-03T12:47:00.000Z',
          'content': 'Świetny procesor, cena jak u i3-8350 ale znacznie lepsze! Polecam! Najtańszy z całej serii i5-8xxx, i moim zdaniem najbardziej korzystny stosunek cena / jakość.',
          'id': 500
        }, {
          'rating': 6,
          'authorName': 'UNITRA',
          'date': '2018-05-08T15:30:00.000Z',
          'content': 'Procesor jest mocarzem do gier za nie duże pieniądze.Po przesiadce z i5 4690k znaczny wzrost wydajności,Procesor współpracuje z MSI Z 370 Tomahawk i pamięciami Patriot Viper 4 o pojemosci 16 giga.',
          'id': 501
        }, {
          'rating': 6,
          'authorName': 'Andrzej',
          'date': '2018-05-15T07:40:00.000Z',
          'content': 'W pełni spełnia moje oczekiwania.',
          'id': 502
        }, {
          'rating': 6,
          'authorName': 'Adam',
          'date': '2018-05-29T13:23:00.000Z',
          'content': 'Chyba najtańszy procesor jaki jest najlepszy w chwili obecenej do gier. Niestety konkurencja serwuje wciąż slabsze procesory jezeli chodzi do gier.',
          'id': 503
        }, {
          'rating': 6,
          'authorName': 'Trybik',
          'date': '2018-06-11T14:33:00.000Z',
          'content': 'Polecam naprawdę jest wart swojej ceny.',
          'id': 504
        }, {
          'rating': 6,
          'authorName': 'Rudelke',
          'date': '2018-06-13T16:51:00.000Z',
          'content': 'Dobry i budżetowy procek. Z dobrym chłodzeniem (kod x-kom: 291318) nigdy nie rozkręciłem go powyżej 65C (nawet na stress testach).',
          'id': 505
        }, {
          'rating': 6,
          'authorName': 'Rami',
          'date': '2018-06-14T10:23:00.000Z',
          'content': 'Rewelacyjna wydajnosc do ceny. 6 rdzeni daje rade z kilkoma maszynami wirtualnymi czy do wymagających gier. \nNa minus można zaliczyc max 3.8GHz na wszystkich rdzeniach - 4 GHz jest dla jednego rdzenia',
          'id': 506
        }, {
          'rating': 6,
          'authorName': 'ridex',
          'date': '2018-06-17T23:44:00.000Z',
          'content': 'Polecam gorąco! Bardzo dobry procesor w swojej cenie.',
          'id': 507
        }, {
          'rating': 6,
          'authorName': 'Szymon',
          'date': '2018-06-28T13:55:00.000Z',
          'content': 'Jak na cenę jest genialny, bardzo polecam :)',
          'id': 508
        }, {
          'rating': 6,
          'authorName': 'BarT00',
          'date': '2018-07-13T21:22:00.000Z',
          'content': 'Działa jak natura chciała :). Polecam bardzo dobry proc.',
          'id': 509
        }, {
          'rating': 6,
          'authorName': 'matrioshq',
          'date': '2018-07-20T14:16:00.000Z',
          'content': 'Świetna wydajność, dobra cena.',
          'id': 510
        }, {
          'rating': 6,
          'authorName': 'Damian',
          'date': '2018-07-25T10:40:00.000Z',
          'content': 'Mega polecam, super, wydajnosc skoczyla mi mega. POLECAM',
          'id': 511
        }, {
          'rating': 6,
          'authorName': 'Black',
          'date': '2018-08-23T22:36:00.000Z',
          'content': 'Dobry potworek ! Oczywiście polecam dokupić osobno chłodzenie.',
          'id': 512
        }, {
          'rating': 6,
          'authorName': 'D&amp;V',
          'date': '2018-08-24T10:34:00.000Z',
          'content': 'Procesor jest kozacki Polecam do gier i nie tylko :D',
          'id': 513
        }, {
          'rating': 6,
          'authorName': 'Daniel',
          'date': '2018-08-25T14:57:00.000Z',
          'content': 'Świetny procesor, wysoka wydajność, \nna box\'owym chłodzeniu ma niewysokie temperatury wart zakupu.\n(Same box\'owe chłodzenie jest dość głośnie podczas wysokiego obciążenia)',
          'id': 514
        }],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-8400', 'id': 474}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '2.8 GHz', 'id': 476}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '6 wątków', 'id': 478}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '9 MB', 'id': 479}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {'name': 'Gwarancja', 'filterable': false, 'id': 25, 'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]}]
      }, {
        'id': 53,
        'name': 'Intel i3-8100 3.60GHz 6MB BOX',
        'price': 549,
        'quantity': 78,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i3-8100-360ghz-6mb-box-383498,2017/10/pr_2017_10_5_14_38_55_482_00.jpg',
          'main': true,
          'id': 291
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2018-01-21T13:24:00.000Z',
          'content': 'Polecam procesor, świetna relacja ceny do jakości.',
          'id': 515
        }, {
          'rating': 6,
          'authorName': 'Radek',
          'date': '2018-02-04T23:37:00.000Z',
          'content': 'Procesor bardzo wydajny, 4 rdzenie 4 watki, uzywam od grudnia i jestem mega zadowolony. Np przy gta V przy braku karty graficznej nawet nie ma mowy o jakimkolwiek zacieciu. Lepiej dolozyc do lepszej plyty glownej i kupic procesor polowe tanszy od tak samo wydajnej i5 i eyjdzoemy na tym jeszcze do przodu :-P  Z dyskiem ssd, dodatkowym chlodzeniem i 8gb ramu na plycie msi gaming plus z370 smiga az milo ;-) a jakby do tego dolozyc karte graficzna to nie ma do czego sie przyczepic ;-)',
          'id': 516
        }, {
          'rating': 6,
          'authorName': 'x-mark',
          'date': '2018-03-14T17:43:00.000Z',
          'content': 'W trej cenie nie ma lepszego, nawet i5 7generacji wymiękają. Na z370 Tomahawk śmiga aż miło, a temperatury na boxowym chłodzeniu z pastą thermal gryzzli na pełnym obciążeniu max 60 stopni :)',
          'id': 517
        }, {
          'rating': 6,
          'authorName': 'LiZiR',
          'date': '2018-04-09T18:31:00.000Z',
          'content': 'Gorąco polecam. Stosunek jakość/cena. W grach radzi sobie bardzo dobrze, bez GPU wiedźmin na 24 klatkach :)',
          'id': 518
        }, {
          'rating': 6,
          'authorName': 'Jakszas',
          'date': '2018-04-12T20:42:00.000Z',
          'content': 'Bardzo fajny procesor jak za tą cenę. Idealny jeśli się kupuje budżetowy sprzęt lub jeśli się chce później kupić coś lepszego. na boksowym chłodzeniu nie grzeje się i wiatrak nie jest aż tak głośny ale jeśli jest się fanem cichego sprzętu to polecam jednak zaopatrzyć się w jakieś chłodzenie lepsze od boxowego.',
          'id': 519
        }, {
          'rating': 6,
          'authorName': 'salim019',
          'date': '2018-04-16T17:28:00.000Z',
          'content': 'z tej połki cenowej to zdecydowanie strzał w dyche... pracuje cicho i sprawnie nawet w ciagu kilkugodzinnego grania... spoko sprawa, nie ma co przepłacać.&nbsp;',
          'id': 520
        }, {
          'rating': 6,
          'authorName': 'michał',
          'date': '2018-04-24T21:29:00.000Z',
          'content': 'Kupiłem i działa bez zarzutu. W tej cenie nie widzę obecnie nic lepszego na rynku. Opinię pisze po kilku dniach użytkowania. Polecam.',
          'id': 521
        }, {
          'rating': 6,
          'authorName': 'szefol',
          'date': '2018-06-06T12:58:00.000Z',
          'content': 'Bardzo fajny procesor w fajnej cenie.  Bardzo dobra relacja ceny do wydajności.\n\ni3 z 4 rdzeniami to dobry zakup do zastosowań biurowych. \n\nTym bardziej, że w sprzedaży są już dostępne tanie płyty główne pod Coffee Lake.\n\nBOXowe chłodzenie spokojnie wystarcza. Hałas i temperatury w normie.',
          'id': 522
        }, {
          'rating': 5,
          'authorName': 'Tomek',
          'date': '2018-06-27T08:05:00.000Z',
          'content': 'Procesor rewelacja - 4 rdzenie/4wątki. Nie ma co płakać, że 8 generacja wymaga nowych płyt - takie życie, że czasem trzeba zainwestować a nie wymienić tylko procek na starej płycie. Nowe i3 wspiera intel optane, bardzo dobra wydajność w grach. Jedyna wada do BOXowe chłodzenie, której jest nieporozumieniem (sprawdza się ale generuje za duży szum)',
          'id': 523
        }, {
          'rating': 6,
          'authorName': 'MichałB',
          'date': '2018-07-11T15:38:00.000Z',
          'content': 'i3 to zdecydowanie dobry procesor. Do pracy biurowej całkowicie wydajny. W obecnej cenie nie ma sobie równych.Trzyma niskie temperatury, do budżetowej skrzynki wystarczy aż nadto',
          'id': 524
        }, {
          'rating': 6,
          'authorName': 'Kajetan',
          'date': '2018-07-19T11:49:00.000Z',
          'content': 'W tej cenie chyba lepiej nie będzie.',
          'id': 525
        }, {
          'rating': 6,
          'authorName': 'Ęri',
          'date': '2018-08-02T16:59:00.000Z',
          'content': 'Bardzo dobry procesor, polecam, bardzo wydajny,  nie ma sobie równych w tej cenie.',
          'id': 526
        }, {
          'rating': 6,
          'authorName': 'Sharky',
          'date': '2018-08-06T20:34:00.000Z',
          'content': 'Procek mega i do tego atrakcyjna cena. dodatkowo mam chłodzenie SilentiumPC Spartan 3 LT HE1012 ya 59zł i cisza i temperatury niskie polecam. kupiłem 2szt. i nie mają lekko.',
          'id': 527
        }, {
          'rating': 6,
          'authorName': 'jenot',
          'date': '2018-08-10T14:55:00.000Z',
          'content': 'myślę, że w swojej cenie to nie do pobicia procesor, szczególnie jeśli chodzi o jakość w grach. wydajnościowy mistrz. tani, ale wymiata zdecydowanie. dobra alterantywa do droższych modeli. polecam w 100%',
          'id': 528
        }, {
          'rating': 6,
          'authorName': 'Maciek',
          'date': '2018-08-18T10:53:00.000Z',
          'content': 'Świetny czterordzeniowy procesor , na ten moment cena / wydajność jest super .',
          'id': 529
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-08-22T23:06:00.000Z',
          'content': 'Nie sprawia problemów, jest wydajny, w miarę ciche chłodzenie (czasami coś słychać jak jest obciążenie), ogolnie to polecam.',
          'id': 530
        }, {
          'rating': 6,
          'authorName': 'Arni',
          'date': '2018-08-29T19:16:00.000Z',
          'content': 'Bardzo dobry procesor.',
          'id': 531
        }, {
          'rating': 6,
          'authorName': 'Kawix',
          'date': '2018-09-06T13:41:00.000Z',
          'content': 'Cztery wydajne rdzenie,cena która nie dobija.Model: i3-8100 to rozsądny wybór.',
          'id': 532
        }],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i3', 'id': 486}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i3-8100', 'id': 487}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.6 GHz', 'id': 488}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '6 MB', 'id': 491}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }]
      }, {
        'id': 54,
        'name': 'Intel i5-8600K 3.60GHz 9MB',
        'price': 1099,
        'quantity': 26,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-8600k-360ghz-9mb-383504,2017/10/pr_2017_10_5_14_48_0_403_00.jpg',
          'main': true,
          'id': 292
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2018-01-15T14:30:00.000Z',
          'content': 'Procesor sprawuje się b.dobrze. Zintegrowana grafika bezproblemowo odtwarza filmy w 4K.',
          'id': 533
        }, {
          'rating': 6,
          'authorName': 'Dominik',
          'date': '2018-02-07T12:35:00.000Z',
          'content': 'Procesor jest znakomity!  Potężna wydajność w grach, nawet bez OC. Super temperatury - przy Thermal Grizzly Kryonaut w spoczynku mam 20-22 stopnie a w stresie nie osiąga nawet 60! Świetny produkt, doskonały stosunek jakości do ceny. Co do X-Kom - znakomita obsługa, wysyłka w czasie, wszystko w nowiutkim nienaruszonym stanie. Dla mnie rewelacja :)',
          'id': 534
        }, {
          'rating': 6,
          'authorName': 'Bartłomiej',
          'date': '2018-03-13T19:07:00.000Z',
          'content': 'Super wydajność i temperatury po OC. 5 GHz max 70 stopni na rdzeniach na chlodzenio AIO, myślę że po skalpie można by pójść jeszcze w górę :D',
          'id': 535
        }, {
          'rating': 6,
          'authorName': 'Inaczej',
          'date': '2018-03-17T12:51:00.000Z',
          'content': 'Procesor bardzo dobry. Jeszcze go nie podkręcałem, ale w grach sprawuje się idealnie. Bardzo dobry wybór dla gracza, bo starczy na kilka lat, a i do innych zastosowań jak montaż video też się nada. Pozdrawiam doskonałą obsługę ze sklepu X-kom w Atrium Reduta w Warszawie 😊',
          'id': 536
        }, {
          'rating': 6,
          'authorName': 'Raf',
          'date': '2018-04-06T14:14:00.000Z',
          'content': 'Z początku wybór padł na i7 8700K.Skończyło się na kupnie 8600K i tak sobie myślę jak coś,to wymienię na i7.Zostałem przy 8600K - procesor jest na tyle mocny,że nie potrzebuje wątki w i7 a  za różnicę kupiłem lepsze chłodzenie do procesora.',
          'id': 537
        }, {
          'rating': 6,
          'authorName': 'Bogdan',
          'date': '2018-04-15T07:47:00.000Z',
          'content': 'Przesiadłem się z Ryzena i daje radę.Jest bardzo wydajny i nie grzeje sie specjalnie. Zobaczymy jak będzie w przyszłości, ale obecnie nie mam żadnych zarzutów i polecam.&nbsp;',
          'id': 538
        }, {
          'rating': 6,
          'authorName': 'timon29',
          'date': '2018-05-16T06:14:00.000Z',
          'content': 'Znakomity procesor, temperatury bardzo niskie. 32* bez OC na boxowym chłodzeniu. OC do 4,5Ghz powaliło na kolana i7 przy grach.\nStosunek cena - jakość jest nie tyle dobra co znakomita... Po zmianie chłodzenia na Noctua NH-D15 i podkręceniu do 4,8Ghz temperatura nie przekracza 85*C w stresie',
          'id': 539
        }, {
          'rating': 4,
          'authorName': 'Bober',
          'date': '2018-05-29T08:32:00.000Z',
          'content': 'Dobry procesor. Wydajny i energooszczędny. Niestety pasta zastosowana przez Intela pod head-spreaderem sprawia, że przy obciążeniu temperatury rdzeni szaleją (różnice między najcieplejszym a najchłodniejszym rdzeniem przy obciążeniu potrafią przekroczyć 10 stopni). W OC nie bawiłem się jeszcze gdyż gwarancja i te sprawy, ale kiedy już się za to zabiorę, odblokowany mnożnik bardzo w tym pomoże. 4/6',
          'id': 540
        }, {
          'rating': 6,
          'authorName': 'Radek',
          'date': '2018-06-17T08:51:00.000Z',
          'content': 'Fajny wydajny w dośc dobrej cenie procesor :) Spokojnie sobie radzi z grami, po automatycznym OC - 4.5 ale mozna zawsze wiecej wycisnac :)',
          'id': 541
        }, {
          'rating': 6,
          'authorName': 'kuczaj',
          'date': '2018-06-27T09:15:00.000Z',
          'content': 'Czego chcieć więcej od procesora - niesamowita wydajność nawet bez OC, cena/jakość, energooszczędność, zintergowana grafa która jest zaskakująco wydajna.\nIdealna dla kogoś, kto jednak chciałby więcej mocy niż 8400 ( bo po OC ten potworek spokojnie wystarczy na najbliższe parę lat), a nie chce przepłacać za 8700/8700k ( nie oszukujmy się, tylko ułamek posiadaczy wykorzystuje jego potencjał).',
          'id': 542
        }, {
          'rating': 6,
          'authorName': 'Mike',
          'date': '2018-07-17T08:04:00.000Z',
          'content': 'Polecam ten procesor. Kupiony do nowego zestawu z GTX 1050 Ti. Dobrze się sprawdza grę,można podkręca,z dobrym chodzeniem nie nagrzewa się tak bardzo,wysoka wydajność w grach!',
          'id': 543
        }, {
          'rating': 3,
          'authorName': 'Marcin',
          'date': '2018-07-22T21:01:00.000Z',
          'content': 'Strasznie się grzeje. Już po 11 sekundach testu OCCT w stresie przy małym zestawie danych osiąga 87°C i test zostaje przerwany ze względu na  zbyt wysoką temperaturę. W spoczynku w tej chwili mam 43°C. W czasie samego testu pod obciążeniem komputer zaczyna strasznie zamulać. Procesor zakupiłem 1,5 miesiąca temu. Może trafiłem na ,,gorącą sztukę\'\' albo to wina chłodzenia Deepcool Gammaxx GT ,nie wiem. Sprawdzę na innym chłodzeniu a gdy nie będzie widocznej poprawy będę reklamował.',
          'id': 544
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-07-23T07:26:00.000Z',
          'content': 'Bardzo prosimy o kontakt z naszym działem serwisowym, postaramy się znaleźć rozwiązanie tej sytuacji. Jesteśmy do Twojej dyspozycji: serwis@x-kom.pl lub pod numerem 34 377 00 30.',
          'id': 545
        }, {
          'rating': 1,
          'authorName': 'Diwad',
          'date': '2018-07-27T16:07:00.000Z',
          'content': 'Nie polecam procesora. Strasznie bottleneckuje mojego hd5450 natomiast 14 godzin renderingu w 3ds to kpina. osobiście polecam AMD',
          'id': 546
        }, {'rating': 6, 'authorName': 'Kurła', 'date': '2018-09-05T17:22:00.000Z', 'content': 'Polecam', 'id': 547}, {
          'rating': 6,
          'authorName': 'Piotr K',
          'date': '2018-09-06T09:44:00.000Z',
          'content': 'działa z GOODRAM 8GB 3200MHz IRIDIUM Black CL16 ale mam 2X 8 GB aby mieć 16GB :D',
          'id': 548
        }, {
          'rating': 6,
          'authorName': 'Karol',
          'date': '2018-09-07T07:32:00.000Z',
          'content': 'Idealnie się sprawuje, temperatury przy grach umiarkowane. Polecam!',
          'id': 549
        }],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '6 wątków', 'id': 478}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '9 MB', 'id': 479}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-8600K', 'id': 494}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.6 GHz (4.3 GHz w trybie turbo)', 'id': 495}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '95 W', 'id': 496}]}, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Odblokowany mnożnik', 'id': 497}, {'name': 'Wersja BOX (brak wentylatora w zestawie)', 'id': 498}]
        }]
      }, {
        'id': 55,
        'name': 'Intel i7-8700 3.20GHz 12MB BOX',
        'price': 1379,
        'quantity': 1,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-8700-320ghz-12mb-box-383506,2017/10/pr_2017_10_5_14_26_29_735_00.jpg',
          'main': true,
          'id': 293
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Tankowiec',
          'date': '2017-10-11T15:25:00.000Z',
          'content': 'W trybie turbo wszystkie rdzenie  chodzą na  4,3 GHz !!! Jedynie co  można zmienić to dokupić jakieś chłodzenie  za max 100zł i  święty spokój na parę lat.',
          'id': 550
        }, {
          'rating': 6,
          'authorName': 'Mirek',
          'date': '2017-12-14T08:56:00.000Z',
          'content': 'Procesor mam osadzony na płycie MSI z370 SLI PLUS (po aktualizacji BIOSu do v. A.10). Przy napięciu ustawionym ręcznie na 1.15 v trzyma 4.3 GHz przy obciążeniu wszystkich rdzeni. Z chłodzeniem Scythe Grand Kama Cross rev. B dochodzi do 70 stopni (przed zmniejszeniem napięcia prawie 90). Dlatego polecam undervolting. Przesiadłem się z i5 3470 3.20 GHz. Skok wydajności - przepaść. Świetny procesor.',
          'id': 551
        }, {
          'rating': 6,
          'authorName': 'PawełM',
          'date': '2017-12-29T19:35:00.000Z',
          'content': 'Procesor z najwyższej półki!\nSzybka  i sprawna dostawa!',
          'id': 552
        }, {
          'rating': 6,
          'authorName': 'Radlan',
          'date': '2018-02-12T12:29:00.000Z',
          'content': 'Wydajny sześciordzeniowiec, o małej ilości wydzielanego ciepła - co dla mnie ma szczególne znaczenie. Sprawdza się w grach jak i w wymagającej pracy jak grafika, montaż nieliniowy. Świetnie współgra z Gigabyte Z370 AORUS Gaming K3 oraz pamięciami Crucial 2400MHz Ballistix Sport LT. W komplecie wiatraczek - na początek starczy. Wudowany Intell UHD też ok do pracy zanim podepniemy videomocarza. Mam go zbyt krótko, by powiedzieć więcej. Na teraz uważam, że zakup 10 na 10. Polecam...',
          'id': 553
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2018-03-21T10:51:00.000Z',
          'content': 'Po przesiadce z i5 4690 leciwy już gtx 970 dostał skrzydeł. Temperatur nie sprawdzałem ale z chłodzeniem za 100zł i w obudowie Phanteks Eclipse niesłyszalny. Gorąco polecam :)',
          'id': 554
        }, {
          'rating': 6,
          'authorName': 'Tomasz',
          'date': '2018-04-25T05:35:00.000Z',
          'content': 'Świetny szybki procesor, doskonała wydajność zarówno w grach jak i w projektowaniu AutoCad.\nPolecam;',
          'id': 555
        }, {
          'rating': 6,
          'authorName': 'm.',
          'date': '2018-05-02T16:34:00.000Z',
          'content': 'Świetny, szybki procesor. Polecam, jestem bardzo zadowolony z zakupu.',
          'id': 556
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-05-09T09:30:00.000Z',
          'content': 'Procesor świetny. Dobrze sprawuje się z płytą msi 370   sli plus oraz chłodzeniem fortis 3. W spoczynku temperatura około 35 stopni. W stresie nie przekracza 75. Procesor wart zakupu',
          'id': 557
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2018-05-20T15:03:00.000Z',
          'content': 'Świetny procesor. Przeskoczyłem z 4 generacji na 8 i wydajność jest ogromna. 6/12 jak najbardziej daje radę. Nie ma do czego się przyczepić. Zaraz po zainstalowaniu, polecam pobawić się z VCORE :)',
          'id': 558
        }, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2018-06-03T15:26:00.000Z',
          'content': 'bardzo wydajny procesor dla osób nie chcących bawić się w oc, mi się trafiła chłodna szuka, przy chodzeniu dark rock 3 temperatura w spoczynku to 38 stopni,  i to w dość cieplej obudowie pure base 600 window,',
          'id': 559
        }, {
          'rating': 6,
          'authorName': 'Dawid B',
          'date': '2018-07-15T09:57:00.000Z',
          'content': 'Bardzo dobry i wydajny procesor. Warto jednak pomyśleć o lepszym chłodzeniu a przynajmniej lepszej paście termo. Wydajność nie wiem jak opisać bo przechodziłem z i3 4 generacji... nie da się tego porównać',
          'id': 560
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2018-07-23T02:33:00.000Z',
          'content': 'Bardzo wydajny procesor! przerzucilem się z Athlon 880k x4 i porównując modele to przepaść, i7 8700 to dosłownie torpeda, jednocześnie koduje film i gram bez spadku wydajności a i w takiej sytuacji obciążenie wynosi średnio 75%, przy samym kodowaniu 29%, przy gierkach nawet nie przekracza 50%, zakupiłem chłodzenie za symboliczne 100zl silentium PC fera 3 i np. przy pełnym obciążeniu w Occt średnio 68-72 stopnie, bez stresu 32 stopnie, podczas grania i dekodowania średnio 55-60. POLECAM!',
          'id': 561
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2018-07-23T11:54:00.000Z',
          'content': 'Procesor genialny, na ASRock z 370 pro 4 i SPC Fera 3 v2 w spoczynku 38°, niestety jeszcze nie miałem okazji go "zestresować" ale czuję, że to najlepszy wybór dla osób, które nie bawi OC ani GLUT na i7 8700K ;)',
          'id': 562
        }, {
          'rating': 5,
          'authorName': 'Artur',
          'date': '2018-08-05T10:46:00.000Z',
          'content': 'Procesor ok na płycie Asus TUF Z370-PLUS GAMING - po wł profilu XMP dla pamięci 3000 MHz mnożnik procesora podbija się automatycznie  do 102.5 dzięki czemu mam stałe 4.4 GHz na wszystkich rdzeniach. Turbo 4.6 jak w specyfikacji to atrapa i działa tylko gdy procesor nie jest obciążany . Minus 1 gwiazdka za glut pod IHS-em i wysokie temperatury w stresie. Ale polecam ten procesor idealny dla graczy i nie tylko.',
          'id': 563
        }, {
          'rating': 5,
          'authorName': 'mrd',
          'date': '2018-08-12T11:57:00.000Z',
          'content': 'Przesiadka z i5-4690k, zdecydowanie zauważalny skok wydajności. Siedzi w MSI Z370 SLI PLUS z 2x GOODRAM 8GB 3000MHz IRDM X Red CL16 @3200MHz. Chłodzi go Fortis 3 HE1425 v2, stres (OCCT mały zestaw danych) do 77 stopni, gry max 65 stopni,  spoczynek (film/Internet) 35 stopni. Obudowa SPC AQUARIUS AQ-X70W z 5x Sigma PRO 120. Świetny towarzysz dla Inno3D GeForce GTX 1080 iChill X4 8GB GDDR5X także póki co bardzo komfortowa rozgrywka w WQHD. Z minusów boost mógł by być do 4.5GHz',
          'id': 564
        }, {
          'rating': 6,
          'authorName': 'Staszek',
          'date': '2018-08-29T05:44:00.000Z',
          'content': 'Bardzo dobry procesor dla tych którzy nie bawią się w podkręcanie wystarczy ustawić w Biosie GAME BOOST  i wszystkie rdzenie chodzą na 4,3 GHz na płycie z 370 Tomahawk',
          'id': 565
        }, {
          'rating': 6,
          'authorName': 'lotharek',
          'date': '2018-08-30T11:01:00.000Z',
          'content': 'na plycie msi z370 gaming plus na boxowym chlodzeniu z gtx1080 tez msi sea hawk - urywa łeb. cichy, wydajny, chlodny. plyta mi sie ustawila na 0,965V, w biosie zmienilem zeby wiatrak sie rozkrecal pow 60stopni- jeszzce sie nie rozkrecila na powaznie.. robi swoja robote :-)',
          'id': 566
        }, {
          'rating': 6,
          'authorName': 'Michal',
          'date': '2018-09-02T05:28:00.000Z',
          'content': 'Procesor bardzo wydajny i szybki po przejściu z i3 4170 znaczny skok wydajności, mam małe problemy z tym procesorze na płycie msi b360 mortar ponieważ nie mozna zmienić napięcia procesora przez co grzeje sie procesor po ustawieniu co u ratio na sztywno x40 problem zniknął ale jestem w kontakcie z msi być mozna cos zmienia.',
          'id': 567
        }],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i7', 'id': 499}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i7-8700', 'id': 500}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.2 GHz (4.6 GHz w trybie turbo)', 'id': 501}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {'name': 'Pamięć podręczna', 'filterable': false, 'id': 51, 'options': [{'name': '12 MB', 'id': 503}]}]
      }, {
        'id': 56,
        'name': 'Intel i7-8700K 3.70GHz 12MB',
        'price': 1579,
        'quantity': 86,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-8700k-370ghz-12mb-383508,2017/10/pr_2017_10_5_14_52_57_819_00.jpg',
          'main': true,
          'id': 294
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'FozzyPL',
          'date': '2017-10-09T19:41:00.000Z',
          'content': 'Procesor jest świetny! Nic dodać nic ująć!',
          'id': 568
        }, {
          'rating': 5,
          'authorName': 'Michał',
          'date': '2017-10-14T11:33:00.000Z',
          'content': 'Witam! Całą platformę wraz z tym procesorem kupiłem z myślą o grach i dlatego, ze korzystam z monitora 144hz. Jest przyrost mocy tym bardziej, że przesiadka była z 2500k, ale szału nie robi, temperatury pod obciążeniem dosyć wysokie nawet bez OC. Myślę, że procesor dobry dla osób które teraz właśnie kupują komputer lub planują zmianę z ivy/sandy.  Względem kabylake wydajność znikoma. Polecam szczególnie dla tych którzy korzystają z monitara 144hz.',
          'id': 569
        }, {
          'rating': 6,
          'authorName': 'Dabu',
          'date': '2017-11-08T16:52:00.000Z',
          'content': 'Jest moc, zdecydowanie lepiej utrzymuje płynne 144 fps w Overwatchu/PUBG, niż mój stary 3770k. Jest też różnica w sofcie Adobe (AE, Photoshop), ale nie tak duża jak w grach. Niestety temperatury są spore, OC 4.8 GHz, 1.28 v z Noctua D15S daje ~80 c w stress teście, ~60-70 c w grach/renderowaniu.\n\nGeneralnie do grania w 144 Hz idealne, do innych zastosowań wybrałbym raczej Ryzena.',
          'id': 570
        }, {
          'rating': 6,
          'authorName': 'Rafał',
          'date': '2017-11-25T22:04:00.000Z',
          'content': 'Procek jest fajny jak ktoś potrzebuje sporo mocy. Do obróbki zdjęć jak i gier nadaje się idealnie. 8 generacja najmocniejszego procka dla konsumenta :)',
          'id': 571
        }, {
          'rating': 6,
          'authorName': 'Hubert',
          'date': '2017-12-12T08:12:00.000Z',
          'content': 'Znakomity procesor. w tej cenie. \nMoc - wydajność - cena \n   -- szkoda ze prąd ciągnie:-)\nDo wszystkiego:-)\n\nJak ktoś składa komputer, i nie chce kupować od razu karty graficznej, to spokojnie ten procek dla dzieciaka wystarczy, na gry typu:\nPlants vs. Zombies™ Garden Warfare \nCS-GO czy inne w  4K HDR !\nitp\n\nPamiętać należy o dobrym chłodzeniu dla tego procesora.\n\nPolecam',
          'id': 572
        }, {
          'rating': 6,
          'authorName': 'Woyciech13',
          'date': '2017-12-28T14:24:00.000Z',
          'content': 'Procesorek działa ładniutko z MSI Z370 Tomahawk, 2x8GB Corsair LPX 3000 MHz oraz SSD Samsung 960 EVO. W BIOS temperatura procesora 38 stopni C. Chłodzenie Noctua NHU14S.  Zdecydowanie polecam.',
          'id': 573
        }, {
          'rating': 6,
          'authorName': 'Woyciech13',
          'date': '2017-12-28T23:01:00.000Z',
          'content': 'Zdecydowanie polecam. Śmiga aż miło na MSI Z370 Tomahawk + 2x8GB Corsair 3000MHz. Temperatura w BIOS w spoczynku to 38 stopni C. Chłodzenie Noctua NHU14S.',
          'id': 574
        }, {
          'rating': 6,
          'authorName': 'SzwagierPL',
          'date': '2018-01-01T20:11:00.000Z',
          'content': 'Bardzo Dobry Procesor ! Serdecznie Polecam !\nPozdrawiam !',
          'id': 575
        }, {
          'rating': 6,
          'authorName': 'Sebastian M.',
          'date': '2018-01-02T21:38:00.000Z',
          'content': 'Przesiadka co prawda z nie tak starego i7-6700K no ale różnica jest. Ile taka mała rzecz potrafi dać radości.. POLECAM! :)',
          'id': 576
        }, {
          'rating': 6,
          'authorName': 'Bartek',
          'date': '2018-01-22T13:03:00.000Z',
          'content': 'Bardzo dobry highendowy procesor w tandemie z grandisem2 przy delikatnym OC na 4.8ghz temperatura nie przekracza 70 stopni',
          'id': 577
        }, {
          'rating': 6,
          'authorName': 'szymon78',
          'date': '2018-02-02T15:37:00.000Z',
          'content': 'polecam mozna podkrecic nawet 5200',
          'id': 578
        }, {
          'rating': 6,
          'authorName': 'Driver1221',
          'date': '2018-04-03T20:48:00.000Z',
          'content': 'Dobry procek i tyle - co tu dużo pisać:) Ogólnie mówiąc po tych sześciu miesiącach użytkowania jestem mega zadowolony. Gołym okiem widać różnicę w wydajności w porównaniu do procesora czwartej generacji. Gorąco polecam:)',
          'id': 579
        }, {
          'rating': 3,
          'authorName': 'Kamilo',
          'date': '2018-04-09T23:05:00.000Z',
          'content': 'Procesor bardzo dobry do gier bardzo wydajny ale temperatury są ogromne i mówienie o tematyce w bios to kpina jak czytam te komentarze odpal Aida i stres test pogadamy o temperaturze bo bez skalpu nie podchodz',
          'id': 580
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2018-04-10T13:21:00.000Z',
          'content': 'Jeżeli masz kłopot z temperaturami procesora, prosimy o kontakt z naszym Serwisem pod numerem telefonu 34 377 00 30 lub mailowo, pod adresem serwis@x-kom.pl. Postaramy się znaleźć najkorzystniejsze rozwiązanie.',
          'id': 581
        }, {
          'rating': 6,
          'authorName': 'Piotr',
          'date': '2018-04-18T17:08:00.000Z',
          'content': 'świetny sprzęt !! spokojnie kręci sie na 5.0 GHz',
          'id': 582
        }, {
          'rating': 6,
          'authorName': 'Piotrek',
          'date': '2018-04-21T08:04:00.000Z',
          'content': 'Rewelacyjny procesor, moze po podreceniu na 5.0 temperatury nie sa zbyt niskie w obciązeniu, ale to pewno tez wina mojego chlodzenia SilentiumPC Fortis3..',
          'id': 583
        }, {
          'rating': 6,
          'authorName': 'Szynszyl',
          'date': '2018-06-12T10:41:00.000Z',
          'content': 'Chyba już wszystko zostało powiedziane, świetny procek w rozsądnych pieniądzach!!potwierdzam jakiść  i7 i Polecam',
          'id': 584
        }, {
          'rating': 6,
          'authorName': 'Deds',
          'date': '2018-06-29T13:02:00.000Z',
          'content': 'MOCARZ MOCARZ MOCARZ. NAJLEPSZY NAJEFEKTOWNIEJSZY PROCEK Z JAKIM MIAŁEM DO CZYNIENIA&gt;&gt;&gt;&gt;RADZI SOBIE DOSŁOWNIE ZE WSZYSTKIM I MEGA UŁATWIA PRACĘ&lt;&lt;&lt; ŻAŁUJĘ TYLKO ŻE TYLE CZASU GO NIE MIAŁEM',
          'id': 585
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2018-07-04T18:57:00.000Z',
          'content': 'Świetny procek, nie spodziewałem się takiego wzrosty wydajności i komfortu pracy z przesiadki z i5 6500. Chłodzony przez Arctic Alpine 11 GT jak na takie chłodzenie temperatury pozytywne. Chciałem widzieć płytę wiec chłodzenie lepsze schowane puki co. W 100% zadowolenie',
          'id': 586
        }, {
          'rating': 6,
          'authorName': 'Dreknar',
          'date': '2018-08-16T07:46:00.000Z',
          'content': 'Bardzo wydajny procesor już po wyjęciu z pudełka. Do pracy biurowej + gry + półprofesjonalne zastosowanie (sporadyczna edycja filmów/obrazów, konfiguracja kilku maszyn wirtualnych) jest to wystarczająca moc. Przy dobrej płycie głównej można dodatkowo uzyskać kilka Hz przy pomocy kilku kliknięć w Biosie.',
          'id': 587
        }, {
          'rating': 6,
          'authorName': 'Klon',
          'date': '2018-08-27T01:26:00.000Z',
          'content': 'świetny procek Intel i7-8700K, 6 rdzeni, 12 wątków, przeogromna moc w streamowaniu, niesamowita wydajność, dla mnie procesor idealny',
          'id': 588
        }, {
          'rating': 6,
          'authorName': 'Lew',
          'date': '2018-09-03T13:02:00.000Z',
          'content': 'Co tu dużo opowiadać po prostu najlepszy, sklep też niczego sobie ;) polecam',
          'id': 589
        }],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '95 W', 'id': 496}]}, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Odblokowany mnożnik', 'id': 497}, {'name': 'Wersja BOX (brak wentylatora w zestawie)', 'id': 498}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i7', 'id': 499}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '12 MB', 'id': 503}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i7-8700K', 'id': 504}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.7 GHz (4.7 GHz w trybie turbo)', 'id': 505}]
        }]
      }, {
        'id': 57,
        'name': 'Intel i5-8500 3.00GHz BOX',
        'price': 899,
        'quantity': 54,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-8500-300ghz-box-421225,2018/4/pr_2018_4_4_11_54_2_482_00.jpg',
          'main': true,
          'id': 295
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Dan',
          'date': '2018-06-17T08:12:00.000Z',
          'content': 'Procesor w końcu zakupiony i złożony z płytą Asrock B360 m Pro4. Działa stabilnie i bardzo szybko. 6 rdzeni 4 GB Mhz powinno wystarczyć na kilka latek.',
          'id': 590
        }, {
          'rating': 6,
          'authorName': 'Nikola',
          'date': '2018-06-29T22:53:00.000Z',
          'content': 'jest dobrze. z pasta od be quiet dc1 i plyta od asrock z370 nie przekracza 50° przy wysokich nakładach pracy. cóż duzo mówić, starczy na pare ladnych lat.',
          'id': 591
        }, {'rating': 6, 'authorName': 'asti', 'date': '2018-08-03T20:01:00.000Z', 'content': 'dobry procesor !', 'id': 592}],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '6 wątków', 'id': 478}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '9 MB', 'id': 479}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-8500', 'id': 506}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.0 GHz (4.1 GHz w trybie turbo)', 'id': 507}]
        }]
      }, {
        'id': 58,
        'name': 'Intel Pentium Gold G5400 3.70GHz 4MB BOX',
        'price': 279,
        'quantity': 42,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-pentium-gold-g5400-370ghz-4mb-box-423208,2018/5/pr_2018_5_5_11_4_21_861_00.jpg',
          'main': true,
          'id': 296
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2018-08-07T07:43:00.000Z',
          'content': 'Wszystko ideolo. Bardzo dobra pomoc techniczna. Procesor jest wydajny i zimny 30 stopni na przeglądarce i 50 podczas grania. Box intela jest cichszy od mojego dysku twardego. Polecam',
          'id': 593
        }, {
          'rating': 6,
          'authorName': 'Tahar',
          'date': '2018-08-21T11:35:00.000Z',
          'content': 'W dobrej konfiguracji można z tego procesora wycisnąć siódme poty. W połączeniu z GeForce 1050Ti komputer wyciągnie zaawansowane gry jak Wiedźmin 3 czy Gta V w średnich wymaganiach.',
          'id': 594
        }],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Pentium', 'id': 508}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'G5400', 'id': 509}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.7 GHz', 'id': 510}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '2 rdzenie', 'id': 511}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '4 MB', 'id': 512}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 610', 'id': 513}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '54 W', 'id': 514}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Hyper-Threading', 'id': 515}]
        }]
      }, {
        'id': 59,
        'name': 'Intel i7-8086K 4.00GHz 12Mb 40th Anniversary Edition',
        'price': 1689,
        'quantity': 77,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-8086k-400ghz-12mb-40th-anniversary-edition-432745,2018/6/pr_2018_6_8_11_42_49_995_00.jpg',
          'main': true,
          'id': 297
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-8086k-400ghz-12mb-40th-anniversary-edition-432745,2018/6/pr_2018_6_1_14_24_0_864_00.jpg',
          'main': false,
          'id': 298
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Karol',
          'date': '2018-06-09T22:05:00.000Z',
          'content': 'Podkrecony do 5.3 GHz :D',
          'id': 595
        }, {
          'rating': 6,
          'authorName': 'Shadow',
          'date': '2018-06-16T23:35:00.000Z',
          'content': 'xd mam ten procek na customie z dwoma chłodnicami 360 i poszedł na 5,4',
          'id': 596
        }, {
          'rating': 6,
          'authorName': 'DanielXVII',
          'date': '2018-06-25T18:41:00.000Z',
          'content': 'Polecam bez podkręcania sam ustawił się na 4,4GHZ z temperatura 60C przy obciążeniu na chłodzeniu Fortis 3',
          'id': 597
        }, {
          'rating': 6,
          'authorName': 'Wojtek ',
          'date': '2018-07-09T21:54:00.000Z',
          'content': 'Jest moc, dużo możliwości. Procesor póki co też na Fortisie 3 w obudowie Fractal Design Meshify C. Temperatury na pograniczu 57-63 stopni w największym stresie. Procesor obowiązkowy dla każdego, kto śmiga na monitorze 4K. Jest lipiec 2018 i w tej chwili nie ma lepszego procka na takie potrzeby.',
          'id': 598
        }, {
          'rating': 6,
          'authorName': 'Gin',
          'date': '2018-07-26T18:18:00.000Z',
          'content': 'Jest moc, jest potencjał, musiało być podkręcenie na własną rękę, dopiero teraz czuje, że nie miałem lepszego procka.',
          'id': 599
        }, {
          'rating': 6,
          'authorName': 'Damian',
          'date': '2018-08-09T17:14:00.000Z',
          'content': 'Procesor jak najbardziej udany dla intela, chodziły plotki ze tylko na jednym rdzeniu potrafi sam wkręcic sie na 5.0ghz. Ja na 100% obciążeniu zaobserwowałem u procesora na wszystkich rdzeniach 5.0ghz (bez podkręcania, procek robił to sam). Jeśli chcecie wydajny procesor bez zabawy w podkręcanie to procesor dla was.',
          'id': 600
        }, {
          'rating': 6,
          'authorName': 'Oskar',
          'date': '2018-08-20T06:07:00.000Z',
          'content': 'Procesor wydajny mile zaskoczony jestem temperaturami wbrew opinii krążącymi w internecie bez OC na 4 GHz ok 60 stopni na obciążeniu na prostym chłodzeniu wodnym.',
          'id': 601
        }, {
          'rating': 6,
          'authorName': 'Arch Foto Gracz',
          'date': '2018-08-31T23:15:00.000Z',
          'content': 'Do\n-architektury \n-fotografii\n-gier\n\nGENIALNY!!!!!',
          'id': 602
        }],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}, {'name': 'Intel Hyper-Threading', 'id': 515}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '95 W', 'id': 496}]}, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Odblokowany mnożnik', 'id': 497}, {'name': 'Wersja BOX (brak wentylatora w zestawie)', 'id': 498}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i7', 'id': 499}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '12 MB', 'id': 503}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i7-8086K', 'id': 516}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '4.0 GHz (5.0 GHz w trybie turbo)', 'id': 517}]
        }]
      }, {
        'id': 60,
        'name': 'AMD Ryzen 5 2600',
        'price': 825,
        'quantity': 70,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-2600-421255,2018/4/pr_2018_4_10_12_28_47_511_00.jpg',
          'main': true,
          'id': 299
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-2600-421255,2018/4/pr_2018_4_10_12_28_50_667_01.jpg',
          'main': false,
          'id': 300
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Stanisław',
          'date': '2018-04-20T07:32:00.000Z',
          'content': 'Świetny. Nie grzeje się, łatwy do oc.',
          'id': 603
        }, {
          'rating': 3,
          'authorName': 'Seba',
          'date': '2018-04-22T04:43:00.000Z',
          'content': 'Wcale nie jest swietny. To to samo co 1600 tylko 3 fps wiecej. Tak czy siak wolniejszy od intela i nawet zen2 bedzie wolniejszy od intela obecnego wiec ta sama podstawka i tak nie ma sensu jesli patrzec na wydajnosc intela obecnego :D',
          'id': 604
        }, {
          'rating': 6,
          'authorName': 'Kulku',
          'date': '2018-05-09T11:50:00.000Z',
          'content': 'POLECAM TEN PROCESOR. W grach tez sobie daje rade',
          'id': 605
        }, {
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2018-05-26T17:24:00.000Z',
          'content': 'Polecam ten procesor. Temperatury są w porządku a milyn zaskoczeniem jest to że pasuje na niego chłodzenie Hyper TX3.',
          'id': 606
        }, {
          'rating': 6,
          'authorName': 'Professor',
          'date': '2018-06-10T09:22:00.000Z',
          'content': 'świetny procesor w dobrej cenie, podkręcony do stabilnych 4 Ghz',
          'id': 607
        }, {
          'rating': 6,
          'authorName': 'Securone',
          'date': '2018-07-15T16:57:00.000Z',
          'content': 'Po przesiadce z Fx 6300 wielka różnica na + dla Ryzena.',
          'id': 608
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2018-08-02T06:53:00.000Z',
          'content': 'Miałem dylemat między i5 8400 a właśnie R6 2600, wybrałem Ryzena, nie żałuję ani trochę bo procesor żadko kiedy wchodzi mi na 100% wykorzystania, a przy graniu w np Wiedźmina 3 na GTX 1070ti + streamowanie w 1080p/60fps nie zauważyłem spadków FPS poniżej 80. Polecam :)',
          'id': 609
        }, {
          'rating': 6,
          'authorName': 'Wiedźmin',
          'date': '2018-08-26T19:05:00.000Z',
          'content': 'Następna legendarnego R5 1600 :)',
          'id': 610
        }, {
          'rating': 6,
          'authorName': 'Chester',
          'date': '2018-08-28T22:19:00.000Z',
          'content': 'Zdecydowanie multizadaniowy procesor w dobrych pieniądzach. Moc i stabilność mogę powiedzieć ze nawet ponad oczekiwania.... warto dodać, że dość cicho pracuje. KOZACKI!',
          'id': 611
        }, {
          'rating': 6,
          'authorName': 'Wiesiek',
          'date': '2018-08-31T13:27:00.000Z',
          'content': 'Perfekcyjne chłodzenie, po prostu szapo ba! Szybki, stabilnie pracuje, 6 rdzeni, 12 wątków, pyka bez zająknięcia. Stere i nowe tutuły  śmigają więc chyba ciężko żeby kogoś rozczarował, dzięki X za szybką wysyłkę tak jak prosiłem',
          'id': 612
        }, {
          'rating': 6,
          'authorName': 'Mikser',
          'date': '2018-09-02T02:28:00.000Z',
          'content': 'Ostra konkurencja dla intela, z którym wygrywa przede wszystkim cenowo. Zdecydowałem się przez wzgląd na kasę, za te pieniądze przewyższa oczekiwania. Nie widać spadków wydajności, BARDZO SPOKO !!!',
          'id': 613
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {'name': 'Liczba rdzeni fizycznych', 'filterable': true, 'id': 49, 'options': [{'name': '6 rdzeni', 'id': 477}]}, {
          'name': 'TDP',
          'filterable': false,
          'id': 55,
          'options': [{'name': '65 W', 'id': 483}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 5 2600', 'id': 519}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.4 GHz (3.9 GHz w trybie turbo)', 'id': 521}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '19 MB', 'id': 522}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2933', 'id': 523}]
        }, {'name': 'Technologia produkcji procesora', 'filterable': false, 'id': 54, 'options': [{'name': '12 nm', 'id': 524}]}]
      }, {
        'id': 61,
        'name': 'AMD Ryzen 3 2200G 3,5GHz',
        'price': 435,
        'quantity': 84,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-3-2200g-35ghz-407884,2018/4/pr_2018_4_3_15_44_31_303_00.jpg',
          'main': true,
          'id': 301
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'EzioPL',
          'date': '2018-02-20T18:55:00.000Z',
          'content': 'Układ vega bardzo wydajny na to liczyłem procesor kupiłem 15 minut po jego premierze, szkoda tylko że vega obsługuje tylko wyjścia hdmi oraz displayport.',
          'id': 614
        }, {
          'rating': 6,
          'authorName': 'Luk',
          'date': '2018-03-17T22:59:00.000Z',
          'content': 'Kupiłem ten procesor razem z płytą ASUS Prime B350M-A. Zestaw pracuje cicho, nawet na chłodzeniu box. U mnie Vega działa z DVI. Jak zwykle super obsługa w X-KOM.',
          'id': 615
        }, {
          'rating': 6,
          'authorName': 'AKacek',
          'date': '2018-03-21T09:21:00.000Z',
          'content': 'Procesor (w zasadzie APU) jest świetny, za tą cene otrzymujemy procesor na poziomie i5 3550K z wydajną grafiką. Wiedźmin 3 - 1080p detale średnie ok 30kl/s. także rewelacja:) Trzeba pamiętać tylko aby DDR4 chodziły w dual chanel, z jedną kością wydajność GPU (na CPU to nie wpływa) spada prawie o połowę.',
          'id': 616
        }, {
          'rating': 6,
          'authorName': 'Lukasz',
          'date': '2018-04-14T20:52:00.000Z',
          'content': 'Nie potrzebuję mocnej karty graficznej, bo gram tylko niedzielnie, ale jednak to co oferuje Intel to za mało. iGPU mam ustawione na 1360 MHz. CPU nie ruszałem. Gram w nowego Dooma na ustawieniach średnich w full hd przy około 40 fps. Maszyny wirtualne śmigają. \nDo kompletu mam 8 GB RAM 3200MHz od Gskilla w dualu. Wszystko pracuje stabilnie na płycie MSI B350M Bazooka. Dołączony cooler daje radę i jest cichy.\nJestem pod wrażeniem tego APU.  Stosunek ceny do jakości jest bardzo dobry.',
          'id': 617
        }, {
          'rating': 6,
          'authorName': 'Kacper',
          'date': '2018-04-17T10:14:00.000Z',
          'content': 'Procesor jest bardzo wydajny i jakościowo dobry jak na tą cenę. Dobrze zintegrowana grafika. Nie grzeje się nad wyraz i jest w miare cichy nawet przy wielogodzinnym graniu. Tani i dobry to najlepsza rekomendacja.&nbsp;',
          'id': 618
        }, {
          'rating': 6,
          'authorName': 'Tomass',
          'date': '2018-06-09T19:21:00.000Z',
          'content': 'Dla mnie do codziennycch zastosowań zdaje egzamin.Zachęciła mnie cena, bo naprawde jest atrakcyjna w porównaniu do reszty.Ryzen to sprawdzona marka wiec nikt nie powinien miec problemów. Polecę !!',
          'id': 619
        }, {
          'rating': 6,
          'authorName': 'Wiktoria',
          'date': '2018-08-17T13:56:00.000Z',
          'content': 'Procesor w jak najlepszym porządku jak najbardziej polecam',
          'id': 620
        }, {
          'rating': 6,
          'authorName': 'Melons',
          'date': '2018-08-29T20:03:00.000Z',
          'content': 'Stosunek ceny do jakości po prostu miażdży. Procek radzi sobie świetnie. Poprawił wydajność kompa o 100%. Polecam',
          'id': 621
        }],
        'attributes': [{
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '4 MB', 'id': 512}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 3 2200G', 'id': 525}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.5 GHz (3.7 GHz w trybie turbo)', 'id': 526}]
        }, {'name': 'Zintegrowany układ graficzny', 'filterable': false, 'id': 52, 'options': [{'name': 'Radeon RX Vega 8', 'id': 527}]}]
      }, {
        'id': 62,
        'name': 'Intel i5-8600 3.10GHz BOX',
        'price': 989,
        'quantity': 34,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-8600-310ghz-box-421239,2018/4/pr_2018_4_4_12_49_28_733_00.jpg',
          'main': true,
          'id': 302
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Herflik',
          'date': '2018-05-14T19:24:00.000Z',
          'content': 'Świetna moc, ale cena mogła by być niższa.',
          'id': 622
        }, {
          'rating': 6,
          'authorName': 'Nikodem',
          'date': '2018-09-06T20:00:00.000Z',
          'content': 'Super opcja dla ludzi nie mających ochoty na OC. Fabrycznie daje radę w połączeniu z 8GB DDR4 i 1060.',
          'id': 623
        }],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '6 wątków', 'id': 478}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '9 MB', 'id': 479}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-8600', 'id': 528}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.1 GHz (4.3 GHz w trybie turbo)', 'id': 529}]
        }]
      }, {
        'id': 63,
        'name': 'AMD Ryzen 7 2700X',
        'price': 1439,
        'quantity': 23,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-7-2700x-421246,2018/4/pr_2018_4_10_13_1_49_119_00.jpg',
          'main': true,
          'id': 303
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-7-2700x-421246,2018/4/pr_2018_4_10_13_1_52_119_01.jpg',
          'main': false,
          'id': 304
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Helder',
          'date': '2018-04-24T12:29:00.000Z',
          'content': 'Trafiłem na dobrą sztukę podkręciłem ją na mobasie X470. Przy renderowaniu mam kopa jak Toyota Supra na torze :)',
          'id': 624
        }, {
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2018-05-17T13:35:00.000Z',
          'content': 'Jestem bardzo zadowolony z procka.Stockowe chłodzenie bez problemu sb daje rade z chłodzeniem tego procka.Co do samego podkręcania radzę wam samemu podkręcić procek na 4.1 GHz i dać napięcie 1.38v gdyż oprogramowanie tak czy siak nie podkręci go więcej niż 4.05ghz a czasami z głupia podkręca jeden do 2rdzeni na 4.40ghz i 1.53v co go masakrycznie grzeje.Jezeli podkręcicie go na 4.1 1.38v bądź 4.2 1.41v będziecie mieć lepsza wydajność.polecam wziasc szybsze ramy do niego osobiście mam 3600mhz.',
          'id': 625
        }, {
          'rating': 6,
          'authorName': 'Dziki',
          'date': '2018-07-11T22:23:00.000Z',
          'content': 'po zmianie 8 letniego PC skok wydajnościowy jest ogromny, x-kom przy kupnie płyty głównej i procka zaktualizował BIOS za darmo (fajne sprawa). Procesor spoko, póki co nawet chłodzenie boxowe daje radę, działam z RAM\'em F4-3200C16D-16GVKB i wszystko śmiga. Świetny sprzęt pod granie i streamowanie.',
          'id': 626
        }, {
          'rating': 6,
          'authorName': 'Rayner',
          'date': '2018-07-13T20:17:00.000Z',
          'content': 'Przesiadłem się z legendarnego i5 2500k bo potrzebowałem wsparcia w streamingu. Procek na prawdę daje rade. Całkowicie zniknął problem ze streamowanym obrazem podczas dynamicznych akcji, gdzie pojawiały się po prostu dziwne piksele/artefakty, które nie były poprawnie kodowane. Obraz przy 60 klatkach jest po prostu igła w każdej sytuacji. No i co najważniejsze. Renderowanie animacji w After Effect to teraz bajka. 8 rdzeniów robi swoje :) i gwarantuje, że proc może robić wiele rzeczy naraz.',
          'id': 627
        }, {
          'rating': 6,
          'authorName': 'Kuba',
          'date': '2018-07-16T18:02:00.000Z',
          'content': 'Jedyny słuszny procesor w tej cenie.',
          'id': 628
        }, {
          'rating': 6,
          'authorName': 'BIZU',
          'date': '2018-08-19T18:47:00.000Z',
          'content': 'Zgadzam się,że w tej cenie to najlepszy wybór. Komp dostał kopa i nowe życie. AMD to wydajnościowy gigant. Polecam zakup bo warto.&nbsp;',
          'id': 629
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2933', 'id': 523}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '12 nm', 'id': 524}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 7 2700X', 'id': 530}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.7 GHz (4.3 GHz w trybie turbo)', 'id': 531}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '8 rdzeni', 'id': 532}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '16 wątków', 'id': 533}]
        }, {'name': 'Pamięć podręczna', 'filterable': false, 'id': 51, 'options': [{'name': '20 MB', 'id': 534}]}, {
          'name': 'TDP',
          'filterable': false,
          'id': 55,
          'options': [{'name': '105 W', 'id': 535}]
        }]
      }, {
        'id': 64,
        'name': 'Intel  i5-7400 3.00GHz 6MB BOX',
        'price': 769,
        'quantity': 63,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-7400-300ghz-6mb-box-340960,2017/7/pr_2017_7_27_13_28_59_996.jpg',
          'main': true,
          'id': 305
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-7400-300ghz-6mb-box-340960,pr_2017_1_2_9_52_6_251.jpg',
          'main': false,
          'id': 306
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Klopsik',
          'date': '2017-01-22T09:27:00.000Z',
          'content': 'Świetny, kupiłem go do grania w nowe gry i wymiata! Jestem zadowolony!',
          'id': 630
        }, {
          'rating': 6,
          'authorName': 'Dragon',
          'date': '2017-03-16T00:09:00.000Z',
          'content': 'Naprawdę Bardzo wydajny procesor, godny polecenia. Daje sobie spokojnie radę w grach jak i w programach graficznych. Polecam.',
          'id': 631
        }, {
          'rating': 6,
          'authorName': 'terminator ',
          'date': '2017-05-25T17:48:00.000Z',
          'content': 'Lepiej i taniej być nie mogło, myślałem, że aby pograć będę musiał wydać tysiaka albo lepiej, a tak jeszcze starczyło na słuchawki.',
          'id': 632
        }, {
          'rating': 6,
          'authorName': 'sebcia ',
          'date': '2017-07-17T17:05:00.000Z',
          'content': '3.0 na dzień dobry i to w tej cenie - super. Były to chyba najrozsądniej wydane pieniądze, uważam, że nie ma co przepłacać w wyższe procki bo nawet jak ma 3.3 GHz to osiągi aż tak widoczne nie są, poza tym planuję podkręcić, a parę groszy w kieszeni zostanie. Kolejna kwestia, nie zależy mi tylko na grach bo gram tylko w wolnych chwilach czy dla relaksu, a mało takich chwil jest, bardziej zależy mi na wydajności przy wymagających programach i przy nich Intel i5-7400 radzi sobie bezbłędnie.',
          'id': 633
        }, {'rating': 6, 'authorName': 'PAWEL', 'date': '2017-07-25T18:07:00.000Z', 'content': 'NAJLEPSZY', 'id': 634}, {
          'rating': 6,
          'authorName': 'Filip',
          'date': '2017-09-20T05:50:00.000Z',
          'content': 'Dobry proceaor w dobrej cenie',
          'id': 635
        }, {
          'rating': 5,
          'authorName': 'Petek',
          'date': '2017-09-24T22:51:00.000Z',
          'content': 'Wydajny i niedrogi, a zintegrowany układ graficzny daje radę nawet przy filmach w 4K :)',
          'id': 636
        }, {
          'rating': 6,
          'authorName': 'Tomasz',
          'date': '2017-09-29T22:13:00.000Z',
          'content': 'Świetny procesor. W zupełności wystarcza do wszystkiego',
          'id': 637
        }, {
          'rating': 6,
          'authorName': 'Radosław',
          'date': '2017-10-01T20:43:00.000Z',
          'content': 'Rewelacyjny procesor za nieduże pieniądze. W tej cenie nie znajdziemy nic lepszego. Polecam.',
          'id': 638
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2018-01-22T14:17:00.000Z',
          'content': '10/10 Extra wydajność za tą cene',
          'id': 639
        }, {
          'rating': 6,
          'authorName': 'krzysiek',
          'date': '2018-01-22T15:23:00.000Z',
          'content': 'w tej cenie super a z gtx 1050 TI WOT na maxa idzie',
          'id': 640
        }, {
          'rating': 6,
          'authorName': 'Jakub',
          'date': '2018-02-05T21:10:00.000Z',
          'content': 'Cztery rdzenie robią robotę ;) Po przesiadce z 2-rdzeniowego Pentium G4400 komputer zyskał nowe życie! Gry chodzą płynnie, praca graficzna i biurowa stała się płynniejsza, z multimediami nie ma najmniejszych problemów. Z chłodzeniem SilentiumPC Fera 3 procesor ma poniżej 35 stopni w spoczynku. Jeśli ktoś nie chce inwestować w ósmą generację procesorów i nową płytę główną, to i5-7400 jest propozycją o świetnym stosunku ceny do możliwości. Polecam!',
          'id': 641
        }, {
          'rating': 6,
          'authorName': 'MissinK',
          'date': '2018-03-09T20:22:00.000Z',
          'content': 'sprawny szybki nowej generacji',
          'id': 642
        }, {
          'rating': 6,
          'authorName': 'tomaszpecak',
          'date': '2018-03-18T15:47:00.000Z',
          'content': 'Procesor spełnia moje oczekiwania. Nadaje się do obsługi gier jak i innych zadań. Wystarczający dla przeciętnego użytkownika. Oferuje jednak znacznie więcej niż modele AMD.',
          'id': 643
        }, {'rating': 6, 'authorName': 'Marcin', 'date': '2018-07-15T16:09:00.000Z', 'content': 'Polecam', 'id': 644}],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '6 MB', 'id': 491}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-7400', 'id': 536}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.0 GHz (3.5 GHz w trybie turbo)', 'id': 538}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 630', 'id': 539}]
        }]
      }, {
        'id': 65,
        'name': 'Intel i3-7100 3.90GHz 3MB BOX',
        'price': 539,
        'quantity': 61,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i3-7100-390ghz-3mb-box-343478,2017/7/pr_2017_7_14_11_21_2_158.jpg',
          'main': true,
          'id': 307
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i3-7100-390ghz-3mb-box-343478,pr_2017_1_12_11_37_1_469.jpg',
          'main': false,
          'id': 308
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i3-7100-390ghz-3mb-box-343478,pr_2017_1_12_11_37_4_407.jpg',
          'main': false,
          'id': 309
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Deil',
          'date': '2017-01-31T21:45:00.000Z',
          'content': 'Sporo mocy za naprawdę niewiele. 4x 3.9Ghz\ndużo jak na jednego z najtańszych.\nZaskakująco dobrze radzi sobie w grach dzięki ~4ghz na rdzeń. Budżetowe zestawy polecają go zamiast i5 i I7, \'a zaoszczędzonego tysiaka włóż w kartę graficzną\'',
          'id': 645
        }, {
          'rating': 4,
          'authorName': 'mateusz93',
          'date': '2017-09-01T14:30:00.000Z',
          'content': 'Deil,\nPo pierwsze to jest 2 rdzeniowy procesor z HT, a nie 4x3.9 ghz. Jeszcze najlepiej zsumuj zegary i napisz 15.6 ghz.\nSzybki taktowany rdzeń to jedyne co mądrze napisałeś.',
          'id': 646
        }, {
          'rating': 5,
          'authorName': 'Paweł',
          'date': '2017-09-18T09:06:00.000Z',
          'content': 'Sprzęt do zastosowań biurowych zamknięty w małej obudowie. Polecam jeśli szukacie takiego rozwiązania.',
          'id': 647
        }, {
          'rating': 6,
          'authorName': 'Oleg',
          'date': '2018-01-10T18:40:00.000Z',
          'content': 'Wszystko bardzo dobrze',
          'id': 648
        }, {
          'rating': 6,
          'authorName': 'Tomasz',
          'date': '2018-01-25T11:09:00.000Z',
          'content': 'Bardzo dobry procesor jak na swoją cenę sprawdza się bardzo dobrze (GTA V Wiedźmin Watchdogs 60fps wysokie + GTX 750 Ti',
          'id': 649
        }, {
          'rating': 6,
          'authorName': 'Siver',
          'date': '2018-02-24T13:02:00.000Z',
          'content': 'Bardzo dobrze sie sprawdza z GTX 1050Ti',
          'id': 650
        }],
        'attributes': [{
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i3', 'id': 486}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '2 rdzenie', 'id': 511}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Hyper-Threading', 'id': 515}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 630', 'id': 539}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i3-7100', 'id': 543}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.9 GHz', 'id': 544}]
        }, {'name': 'Pamięć podręczna', 'filterable': false, 'id': 51, 'options': [{'name': '3 MB', 'id': 545}]}, {
          'name': 'TDP',
          'filterable': false,
          'id': 55,
          'options': [{'name': '51 W', 'id': 546}]
        }]
      }, {
        'id': 66,
        'name': 'AMD Ryzen 5 2600X',
        'price': 949,
        'quantity': 44,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-2600x-421253,2018/4/pr_2018_4_10_12_42_32_136_00.jpg',
          'main': true,
          'id': 310
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-2600x-421253,2018/4/pr_2018_4_10_12_42_35_199_01.jpg',
          'main': false,
          'id': 311
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Michał',
          'date': '2018-05-07T06:50:00.000Z',
          'content': 'Boxowe chłodzenie jest niewystarczające na\nbazowych ustawieniach, temperatury bez undervoltingu przekraczają często 85 stopni. Po undervoltingu na 1.2 V i skręceniu zegarów na 3.6 GHz bez możliwości boost, temperatury nie przekraczają 80 stopni. Najlepiej dokupić jakiegoś Fortisa.',
          'id': 651
        }, {
          'rating': 6,
          'authorName': 'Mikołaj',
          'date': '2018-05-26T08:06:00.000Z',
          'content': 'Procesor bardzo dobry, przy maksymalnym obciążeniu i najwyższym taktowaniu 4,2 GHz, temperatura nie przekracza 70°.',
          'id': 652
        }, {
          'rating': 6,
          'authorName': 'ptak',
          'date': '2018-06-03T17:36:00.000Z',
          'content': 'Bardzo fajny, szybki i wydajny procesor. Do moich zastosowań wystarczający. \n\nMoje drugie AMD po 965BE - polecam zdecydowanie. Razem z kartą 1050TI - procesor praktycznie się nudzi przy grach.',
          'id': 653
        }, {
          'rating': 6,
          'authorName': 'The Gay',
          'date': '2018-07-17T17:25:00.000Z',
          'content': 'świetny procek, fajna obsługa (darmowa aktualizacja BIOS przy kupieniu procka i płyty głównej). Ryzen 2600X lepszy od i5-8 generacji, jeżeli chodzi o multitasking. Polecam też zmienić chłodzenie na grandis 2. lepsza kultura pracy niż na boxowym',
          'id': 654
        }, {'rating': 5, 'authorName': 'Marek', 'date': '2018-08-25T12:47:00.000Z', 'content': 'Polecam', 'id': 655}, {
          'rating': 6,
          'authorName': 'Jacek',
          'date': '2018-08-26T16:16:00.000Z',
          'content': 'AMD Ryzen 5 2600X to zdecydowanie dobry sprzęt, jestem usatysfakcjonowany zakupem,  Dzięki X!',
          'id': 656
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '95 W', 'id': 496}]}, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '19 MB', 'id': 522}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2933', 'id': 523}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '12 nm', 'id': 524}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 5 2600X', 'id': 547}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.6 GHz (4.2 GHz w trybie turbo)', 'id': 548}]
        }]
      }, {
        'id': 67,
        'name': 'AMD Ryzen 5 2400G 3,6GHz',
        'price': 679,
        'quantity': 67,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-2400g-36ghz-407886,2018/4/pr_2018_4_3_15_45_39_516_00.jpg',
          'main': true,
          'id': 312
        }],
        'reviews': [{
          'rating': 3,
          'authorName': 'Blaster',
          'date': '2018-03-20T21:06:00.000Z',
          'content': 'Procek wydajny może i jest. Chłodny może i jest. Natomiast grafika to padaka totalna. Nie wiem czym się testerzy zachwycają jak co chwilę BSOD ze strony karty graficznej. Szczerze Ryzena polecam ale nie z integrą. W tej cenie lepiej kupić 1500x do tego jakaś karta pokroju 1030 minimum. Dziękuję za zwrot 15 dniowy salonowi X-KOM Gliwice fajne chopy tam pracują.',
          'id': 657
        }, {
          'rating': 6,
          'authorName': 'Misiek',
          'date': '2018-03-23T12:00:00.000Z',
          'content': 'Mój Ryzen 2400G także miał BSOD (po wgraniu sterowników od AMD), jak i przedmówcy. Wystarczyło jednak wgrać następny BIOS dla MSI B350 Tomahawk z 12.03.2018r. i śmiga wspaniale. Proc @ 3.8 Ghz 1.4v grafa @1500Mhz 1.2v dzięki czemi SW:Battlefront 2 na niskich średnio 54 FPSy, a grafika jest wspaniała :) Temperatury z Bequiet! Dark Rock 3 nie przekraczają 73*C podczas grania. W razie problemów będę kontaktować się z X-kom :) \nProcek szczerze polecam!',
          'id': 658
        }, {
          'rating': 4,
          'authorName': 'krecik',
          'date': '2018-04-08T08:25:00.000Z',
          'content': 'Zakupiłem ten procesor wraz z płytą Gigabyte GA-AB350N-GAMING-WIFI, RAM 3200 MHz (G.Skill 16 GB). Na tym zestawie Wiedźmin 3 działa płynnie na średnich detalach i w rozdzielczości Full HD (nie wiem, na ilu klatkach, ale nic nie klatkuje). Jak komuś wywala BSOD, niech wyłączy w BIOS IOMMU dla pamięci (u mnie to rozwiązało problem).\nWadą procesora jest niewątpliwie brak sterowników kary graficznej dla Win 7. Poza tą wadą, procesor jest na prawdę dobry.',
          'id': 659
        }, {
          'rating': 3,
          'authorName': 'Pio',
          'date': '2018-04-18T10:05:00.000Z',
          'content': 'Jako procesor może i bym polecił, ale jako APU - nie. AMD kompletnie pokpiło sprawę sterowników, najnowszy dostępny to 17.7, podczas gdy dla kart dedykowanych są już 18.3.4. Efekt jest taki, że wiele gier domaga się aktualizacji sterownika, uznając ten zainstalowany za przestarzały (a nowszego do APU fizycznie nie ma). Nowe gry w 720p chodzą dobrze, ale do czasu, gdy ze strony karty graficznej nie poleci BSOD, albo funkcja oszczędzania energii nie uzna, że trzeba na chwilę zrzucić taktowanie...',
          'id': 660
        }, {
          'rating': 5,
          'authorName': 'Mz',
          'date': '2018-05-07T09:15:00.000Z',
          'content': 'Procesor dobry, jednak cena z premiery, którą zapłaciłem zbyt wygórowana. Używałem przez tydzień integry na 1600MHz, później przez BSODy sterowników zacząłem używać GTXa 960 2GB. Po problemach, fpsy lepsze. Sam CPU podkręcony na 3.9GHz przy 1.34V, na 4.0GHz szedł ale przy 1.44V dopiero. Temperatury poniżej 67° przy Deepcool Gammaxx GT RGB. Ogólnie za 619zł jest opłacalny, ale nie więcej.',
          'id': 661
        }, {
          'rating': 6,
          'authorName': 'Celek',
          'date': '2018-06-22T18:18:00.000Z',
          'content': 'wydajny procek i rewelacyjna  grafika',
          'id': 662
        }, {
          'rating': 6,
          'authorName': 'Robo1992',
          'date': '2018-07-03T09:26:00.000Z',
          'content': 'Jest ok. Spełnia swoje zadanie bardzo dobrze, wydajnie.Śmiga jak tralala.Paczkę otrzymałem bardzo sprawnie i bez żadnych problemów.Z tej polki cenowej moim zdaniem faworyt.CPU jest zaskakująco dobre,w takich pieniądzach sie tego nie spodziewałem wiec pozytywne zaskocznenie.Polecam zakupienie tego Ryzena',
          'id': 663
        }, {
          'rating': 4,
          'authorName': 'phoenix',
          'date': '2018-08-05T05:16:00.000Z',
          'content': 'Niestety po miesiącu chcę już sprzedać CPU i płytę główną :(\nKrótko mówiąc - do pracy - tak. Do gier - Ryzeny nie.\n8 wątków dają radę, karta graficzna Vega 11 jest super. Ale tyle BSOD-ów pod czas grania nie widziałem chyba od czasów Windows 98. Na przykład Battlefield 1 z Ryzenami w ogóle ma wielki problem, 10-15 minut grania i koniec, możecie sprawdzić w Internecie. Inne gry powodują BSOD co raz z nowymi błędami. Sterowniki i BIOS (PC Mate B350) są zaktualizowane.',
          'id': 664
        }, {
          'rating': 6,
          'authorName': 'Timson',
          'date': '2018-08-19T14:39:00.000Z',
          'content': 'Niezły niezły. Musze przyznać ze osiągi bardzo mnie zaskoczyły. zintegrowana grafika po prostu wymiata. Trzeba to przetestować zeby wiedziec o czym mówię. W tej cenie KOZAK!',
          'id': 665
        }],
        'attributes': [{
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '4 MB', 'id': 512}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 5 2400G', 'id': 549}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.6 GHz (3.9 GHz w trybie turbo)', 'id': 550}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '8 wątków', 'id': 551}]
        }, {'name': 'Zintegrowany układ graficzny', 'filterable': false, 'id': 52, 'options': [{'name': 'Radeon RX Vega 11', 'id': 552}]}]
      }, {
        'id': 68,
        'name': 'AMD Ryzen 3 1200 3.1GHz',
        'price': 379,
        'quantity': 9,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-3-1200-31ghz-376832,2017/7/pr_2017_7_26_14_46_29_135.jpg',
          'main': true,
          'id': 313
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Przemek',
          'date': '2017-08-14T12:47:00.000Z',
          'content': 'Posiadam ten procesor od paru dni i jestem z niego bardzo zadowolony. Już na standardowych ustawieniach jest dość szybki a bez problemu można go podkręcać. U mnie obecnie pracuje na 3,6 GHz na chłodzeniu silentiumpc spartan pro. Nawet po podkręceniu procesora do 3,6 GHz nie przekracza 55 stopni a na standardowych 3,1 procesor nie przekracza 42 stopni.',
          'id': 666
        }, {
          'rating': 6,
          'authorName': 'Tomek',
          'date': '2017-09-02T17:56:00.000Z',
          'content': 'Posiadam ten procesor od miesiąca. Jestem mega zadowolony. Zmieniłem procesor z fx-6300 na ten i po podkręceniu na 3.7 Ghz bez zmieniania napięcia, system działa cudownie. Na boxowym chłodzeniu 75 stopni po 1h w prime95 small FTT 75 stopni. a po zmianie chłodzenia na moje stare od fxa silentumpc (HE924) i temperatury spadły do max 60 stopni.',
          'id': 667
        }, {
          'rating': 6,
          'authorName': 'arci',
          'date': '2017-12-05T05:02:00.000Z',
          'content': 'Bardzo dobry i udany procesor. Długo się zastanawiałem czy zaryzykować bo nie miałem dobrych doświadczeń z seria AMD FX a przez ostatnie 5 lat pracowałem na i3. Nie żałuję wyboru, procek nie grzeje się, pobiera mało prądu, chłodzenie chodzi bardzo cicho i jest szybki.',
          'id': 668
        }, {
          'rating': 6,
          'authorName': 'Skiusz94',
          'date': '2018-01-20T19:14:00.000Z',
          'content': 'Procesor bardzo dobry bez najmniejszego problemu podkręciłem go do 3,9GHz na fabrycznej paście termoprzewodzącej i chłodzeniu (w stresie nie przekracza 55 stopni celsjusza)\nBF1 działa w minimum 42FPS a GTA V w minimum 54FPS więc gorąco polecam!',
          'id': 669
        }, {
          'rating': 4,
          'authorName': 'Foia',
          'date': '2018-05-12T09:38:00.000Z',
          'content': 'Na chłodzeniu był  pypeć, ale sam procesor polecam :)',
          'id': 670
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 3 1200', 'id': 553}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.1 GHz (3.4 GHz w trybie turbo)', 'id': 554}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '8 MB', 'id': 555}]
        }, {'name': 'Rodzaj obsługiwanej pamięci', 'filterable': false, 'id': 53, 'options': [{'name': 'DDR4-2667', 'id': 556}]}]
      }, {
        'id': 69,
        'name': 'Intel i5-7600K 3.80GHz 6MB BOX',
        'price': 979,
        'quantity': 57,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-7600k-380ghz-6mb-box-340963,2017/7/pr_2017_7_14_11_13_36_943.jpg',
          'main': true,
          'id': 314
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-7600k-380ghz-6mb-box-340963,pr_2017_1_2_10_15_28_249.jpg',
          'main': false,
          'id': 315
        }],
        'reviews': [{'rating': 6, 'authorName': 'kosa', 'date': '2017-02-06T21:21:00.000Z', 'content': 'świetny.', 'id': 671}, {
          'rating': 6,
          'authorName': 'Piotr',
          'date': '2017-03-03T08:28:00.000Z',
          'content': 'Szybki w dobrej cenie i zostawiający sporo miejsca do zabawy w podkręcanie. Jak dla mnie świetny procesor.',
          'id': 672
        }, {
          'rating': 6,
          'authorName': 'tgmaverick',
          'date': '2017-03-14T19:50:00.000Z',
          'content': 'Procek się sprawdza. Daje radę pod stresem zarówno w grach jak i obróbce 3D. W tej chwili an Grandis 2 pracuje na 4,8GHz.',
          'id': 673
        }, {
          'rating': 6,
          'authorName': 'Ibanezos',
          'date': '2017-03-22T18:37:00.000Z',
          'content': 'Hmm... Co tu dużo mówić. Procesor perfekcyjny, polecam każdemu kto chce zbudować kompa do gierek ;)',
          'id': 674
        }, {
          'rating': 4,
          'authorName': 'Dawid',
          'date': '2017-04-03T12:10:00.000Z',
          'content': '4.8 GHz przy 1.36v - w grach jest ok (kilka godzin w BF4/1), ale o testach stabilności w OCCT i Prime95 zapomnijcie. Cinebench przechodzi bez problemu. \n\n4.9 Ghz - komputer uruchamia się ale przy 1.4v. Jak zamierzacie kupić ten procesor bo wierzycie że on " może osiągnąć taktowanie 5.0 GHz przy napięciu poniżej 1,35 V, co stawia go w czołówce procesorów dedykowanych overclockerom." to dajcie sobie spokój. \n\nJak szukacie cepa do gier i tylko w nich liczy się dla Was stabilność, to kupujcie. :)',
          'id': 675
        }, {
          'rating': 6,
          'authorName': 'Patryk',
          'date': '2017-05-31T06:09:00.000Z',
          'content': 'Procesor super bardzo wydajny nawet na 3.80GHz pokazuje swoją moc',
          'id': 676
        }, {
          'rating': 6,
          'authorName': 'Wojciech',
          'date': '2017-09-01T11:31:00.000Z',
          'content': 'Procesor bardzo dobry. Bez problemu idzie na 4,9 GHz na napięciu 1,235 V. Temp. w stresie z Silentum Fortis 3 nie osiągają 80 stopni.',
          'id': 677
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2017-09-22T09:19:00.000Z',
          'content': 'W 100% stabilny na 4.6GHz (wyszystko klikane w biosie Asus Z270-A)\nMultitasking daje radę, render video ok.',
          'id': 678
        }, {
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2017-09-25T12:31:00.000Z',
          'content': 'Procesor jest wydajny i naprawdę nie, aż taki drogi.',
          'id': 679
        }, {
          'rating': 6,
          'authorName': 'Bogdan',
          'date': '2017-09-29T08:36:00.000Z',
          'content': 'W tej cenie najlepszy i wydajny produkt',
          'id': 680
        }, {
          'rating': 6,
          'authorName': 'Tekken',
          'date': '2017-10-14T09:08:00.000Z',
          'content': 'Procesor bardzo dobry, moj podkreca sie do max 4.6 ghz, powyzej komputer sie wylacza',
          'id': 681
        }, {
          'rating': 6,
          'authorName': 'Cyrkiel',
          'date': '2017-10-14T19:31:00.000Z',
          'content': 'Procesor idealny!',
          'id': 682
        }, {'rating': 6, 'authorName': 'Jakub', 'date': '2017-12-24T08:37:00.000Z', 'content': 'Gorąco polecam', 'id': 683}, {
          'rating': 6,
          'authorName': 'MI8008',
          'date': '2018-01-17T10:44:00.000Z',
          'content': 'Skutecznie ogarnia pracę na dużych plikach graficznych. I tylko tego typu opinię mogę napisać. Łatwe podkręcanie na Płycie MSI 270Z-A PRO',
          'id': 684
        }, {'rating': 6, 'authorName': 'Guru', 'date': '2018-01-19T04:25:00.000Z', 'content': 'Wydajny procek', 'id': 685}, {
          'rating': 6,
          'authorName': 'miniol77',
          'date': '2018-03-23T21:35:00.000Z',
          'content': 'Miałem kupić I5 8400 8 generacji ale w ostatniej chwili zdecydowałem się na I5 7600k, głównie z uwagi na odblokowany mnożnik. Procesor zastąpił wysłużone I5 760 i jestem z niego bardzo zadowolony. Mi starcza na razie standardowe 4.3 GHz więc o OC nie będę pisał. Póki co porównując  go do starego I5 760 3.8 GHz widzę dużą różnicę. Teoretycznie nowa 4 rdzeniowa tańsza I3 wydajnościowo jest taka sama, ale wolę mieć jednak pole manewru że zmianą mnożnika na przyszłość. Polecam.',
          'id': 686
        }, {
          'rating': 6,
          'authorName': 'ice',
          'date': '2018-04-05T05:42:00.000Z',
          'content': 'Mam od roku, procesor idealny - gry, rendering, obróbka zdjęć, grafiki. Bezproblemowy',
          'id': 687
        }, {
          'rating': 6,
          'authorName': 'SJ',
          'date': '2018-05-26T17:41:00.000Z',
          'content': 'Świetnie sprawdza się przy GTA V czy Forzie Motorsport 5. Do obróbki filmów i zdjęć też się sprawdza idealnie. Polecam.',
          'id': 688
        }, {
          'rating': 6,
          'authorName': 'Mikey',
          'date': '2018-08-19T17:56:00.000Z',
          'content': 'Bardzo dobry procesor, dobrze sobie radzi pod stresem. W połączeniu z Dark Rock 3 dobrze utrzymuje temperaturę. Polecam',
          'id': 689
        }],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '6 MB', 'id': 491}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Odblokowany mnożnik', 'id': 497}, {'name': 'Wersja BOX (brak wentylatora w zestawie)', 'id': 498}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 630', 'id': 539}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-7600K', 'id': 557}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.8 GHz (4.2 GHz w trybie turbo)', 'id': 558}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '91 W', 'id': 559}]}]
      }, {
        'id': 70,
        'name': 'AMD Ryzen 5 1600 3.2GHz',
        'price': 745,
        'quantity': 93,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-1600-32ghz-359914,2017/7/pr_2017_7_14_13_48_31_245.jpg',
          'main': true,
          'id': 316
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-1600-32ghz-359914,pr_2017_4_6_11_46_28_496.png',
          'main': false,
          'id': 317
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'pyrol',
          'date': '2017-05-03T22:04:00.000Z',
          'content': 'Najlepszy procesor AMD względem swojej ceny. Polecam wszystkim, bo Intel w tej cenie jest dużo mniej opłacalnym zakupem ze względu na ledwie 4 rdzenie.',
          'id': 690
        }, {
          'rating': 6,
          'authorName': 'Wojtek',
          'date': '2017-07-09T14:51:00.000Z',
          'content': 'Kupiłem go zaraz po premierze i działa świetnie. Na początku miałem składać opierając się na sercu i5, ale jak usłyszałem, że AMD ma w planach wprowadzić procesory ZEN to przemęczyłem się kilka msc-y na AMD Athlonie :D. Kupiłem go z myślą o programowaniu wszystko ładnie śmiga. BOXowe chłodzenie też git na taniej paście za 10 zyla nawet nie przekracza 50°C (też go nie obciążam zbytnio, bo właściwie nie mam czym :P).',
          'id': 691
        }, {
          'rating': 6,
          'authorName': 'ToMi',
          'date': '2017-07-10T11:24:00.000Z',
          'content': 'Najlepszy stosunek cena/wydajność. U mnie płyta sama "podkręciła" na 3.4ghz do tego tryb turbo i nie ma sensu się bawić w OC. Daje rade z i7@4.3ghz w większości zadań. Dodatkowo bardzo dobry i solidny cooler w zestawie.',
          'id': 692
        }, {
          'rating': 6,
          'authorName': 'Krisu',
          'date': '2017-07-29T13:59:00.000Z',
          'content': 'W zastosowania profesjonalnych wciągaja i7 a jest w cenie i5, które miażdży w grach. Stosunek cena/wydajność jest niesamowicie pozytywny. Dodajcie przyszłościową platforme am4 + z tygodnia na tydzień coraz lepszą optymalizacje a wybór jest prosty. Koniec monopolu intela.',
          'id': 693
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2017-08-27T12:05:00.000Z',
          'content': 'Po OC małym do 3.7Ghz bez podnoszenia napięcia temperatura na 100% obciążeniu na załączonym chłodzeniu poniżej 65 stopni cudo.',
          'id': 694
        }, {
          'rating': 6,
          'authorName': 'kikutixxx',
          'date': '2017-08-30T13:31:00.000Z',
          'content': 'Świetny procesor w tej cenie. Kupiłem, gdy okazało się, że Z370 nie będzie kompatybilny z cannon lake. Zakup uważam za przyszłościowy, mimo tego, że w grach odstaje od 7600/7700 to więcej rdzeni w przyszłości na pewno da przewagę.',
          'id': 695
        }, {
          'rating': 6,
          'authorName': 'Tym',
          'date': '2017-09-25T04:15:00.000Z',
          'content': 'Rewelacja AMD znowu w grze 😗',
          'id': 696
        }, {
          'rating': 6,
          'authorName': 'Bartosz',
          'date': '2017-09-25T05:30:00.000Z',
          'content': 'Idealne rozwiązanie do budżetowej stacji roboczej. Wydajność podczas renderingu znakomita. W tej cenie jedyny słuszny wybór.',
          'id': 697
        }, {
          'rating': 6,
          'authorName': 'Dominik',
          'date': '2017-09-25T12:35:00.000Z',
          'content': 'Bardzo dobry produkt',
          'id': 698
        }, {
          'rating': 6,
          'authorName': 'Leszek',
          'date': '2017-09-26T14:09:00.000Z',
          'content': 'bez problemu podkrecony do 3.9 na Stock owym chłodzeniu i płycie asus prime x370\nświetny stosunek ceny do jakosci',
          'id': 699
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2017-09-26T17:59:00.000Z',
          'content': 'Bardzo dobry procesor, fantastycznie sie podkreca.',
          'id': 700
        }, {
          'rating': 6,
          'authorName': 'jan',
          'date': '2017-09-26T20:08:00.000Z',
          'content': 'Super sprzęt za niewielką kasę',
          'id': 701
        }, {
          'rating': 6,
          'authorName': 'Konrad',
          'date': '2017-09-29T05:02:00.000Z',
          'content': 'Najlepszy w swoim przedziale cenowym. Polecam!',
          'id': 702
        }, {
          'rating': 6,
          'authorName': 'Piter',
          'date': '2017-09-30T09:23:00.000Z',
          'content': 'Bardzo dobry i wydajny procesor (mam go od 06-2017). Niskie temperatury - średnia w grach to 45 stopni. Ciche i wydajne chłodzenie od AMD - średnie obroty to 1100 (niestety bez podświetlania RGB). Łatwy do podkręcania (na stockowym chłodzeniu pracuje stabilnie na 3.8 GHz, bez podnoszenia napięcia na płycie MSI Tomahawk). Moc wystarczająca do grania we wszystkie nowości. Bardzo dobry do grafiki 3D (12 wątków robi robotę).',
          'id': 703
        }, {
          'rating': 6,
          'authorName': 'RM',
          'date': '2017-09-30T11:25:00.000Z',
          'content': 'Bardzo dobry stosunek ceny do jakości. Procesor radzi sobie we wszystkich grach z dobrym skutkiem. Polecam gorąco.',
          'id': 704
        }, {
          'rating': 5,
          'authorName': 'Małgorzata',
          'date': '2017-10-01T13:57:00.000Z',
          'content': 'Bardzo dobry procesor w stosunku ceny do jakości.',
          'id': 705
        }, {
          'rating': 6,
          'authorName': 'Konrad',
          'date': '2017-10-02T10:39:00.000Z',
          'content': 'Bardzo wydajny procesor w swietnej cenie. Kupilem niedawno i jestem bardzo zadowolony :-)',
          'id': 706
        }, {
          'rating': 6,
          'authorName': 'Bartosz',
          'date': '2017-10-10T07:21:00.000Z',
          'content': 'Stosunek ceny do oferowanych możliwości świetny.\nW aplikacjach wielowątkowych wydajny lub wydajniejszy od konkurencyjnych i droższych procesorów.\nW grach że względu na słabszy jeden wątek trochę mniej wydajny jak konkurencyjny i5.\nOgólnie jest to bardzo udany i opłacalny zakup do pracy czy ogólnego zastosowania.',
          'id': 707
        }, {
          'rating': 6,
          'authorName': 'Edene',
          'date': '2017-10-12T07:21:00.000Z',
          'content': 'Procesor godny polecenia wszystkim którzy maja dość zagrywek Intela. Idealnie sprawdza się w grach jak i zastosowaniach profesjonalnych (programowanie, obróbka wideo, itp.). Polecam tym którzy jeszcze się wahają;)',
          'id': 708
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2017-10-27T10:43:00.000Z',
          'content': 'Kupiłem w sierpniu 2017 roku i od tego PieC nabrał mocy, wszystkie gry chodzą bardzo dobrze i to przy temperaturze 50 stopni na 3,7 GHz i napięciu 1.23v, dalej nie podkręcam bo nie widzę potrzeby.',
          'id': 709
        }, {
          'rating': 6,
          'authorName': 'Jakub Lekstan',
          'date': '2017-11-03T10:10:00.000Z',
          'content': '6 rdzeni 12 wątków daje rade \nJestem z niego bardzo zadowolony \nPodkreciłem go do 3.9 ghz z chłodzeniem spc Spartan 3 pro utrzymuje się 57 stopni stres',
          'id': 710
        }, {
          'rating': 6,
          'authorName': 'r1me',
          'date': '2017-11-15T16:11:00.000Z',
          'content': 'Do codziennej pracy właściwie nie trzeba go podkręcać. Jedyny minus to boxowe chłodzenie, większość recenzji przedstawia dołączony wentylator jako naprawdę udany i cichy, w rzeczywistości przy większych obrotach staje się on dokuczliwy. Zdecydowałem się na Arctic Freezer 33 AM4 i mam ciszę (+ niskie temperatury).',
          'id': 711
        }, {
          'rating': 6,
          'authorName': 'Ziomek',
          'date': '2017-11-20T20:32:00.000Z',
          'content': 'Rewelacyjny procesor :)',
          'id': 712
        }, {
          'rating': 6,
          'authorName': 'PixelPox',
          'date': '2017-12-03T12:20:00.000Z',
          'content': 'Bardzo łatwe podkręcanie na płycie msi b350 PC mate, z miejsca do 3,8 GHz, temperatura na załączonym wentylatorze nie wzrasta powyżej 70 stopni. Wentylator na stockowej krzywej pracy nie przekracza 50% swojej przepustowości. Najlepszy stosunek ceny do jakości obecnie na rynku procesorów. (12/2017).',
          'id': 713
        }, {
          'rating': 6,
          'authorName': 'Pinool',
          'date': '2017-12-09T18:21:00.000Z',
          'content': 'Ciekawy produkt , miło zaskakuje swoją pracą. Przy dobrym chłodzeniu komputer bezszelestny. Polecam',
          'id': 714
        }, {
          'rating': 6,
          'authorName': 'Wojciu',
          'date': '2017-12-18T11:33:00.000Z',
          'content': 'Małe szczęśliwe cudo. Działa szybko i sprawnie. Gry chodzą jak powinny. Renderowanie w blenderze nie zajmuje już całej nocy :3',
          'id': 715
        }, {
          'rating': 6,
          'authorName': 'Tymoteusz',
          'date': '2018-01-09T17:08:00.000Z',
          'content': 'Super procesor, z miejsca podkręciłem do 4GHz   z chłodzeniem od Silentium PC. Żadnych problemów, najlepszy stosunek jakość/cena.',
          'id': 716
        }, {
          'rating': 6,
          'authorName': 'Kuba',
          'date': '2018-01-26T11:41:00.000Z',
          'content': 'Spełnia swoją role',
          'id': 717
        }, {
          'rating': 6,
          'authorName': 'raf',
          'date': '2018-02-02T19:49:00.000Z',
          'content': 'Intel wypuścił w niemal jednocześnie 7 i 8 generację z chipsetami niekompatybilnymi ze sobą. Okazuje się że nawet 8 generacja to technicznie nadal 6 generacja, nawet obsługa DDR3 została i da się ją odblokować. AMD razem z Ryzenami nie tylko dogoniło Intela, ale wyprzedziło go pod względem stosunku wydajności do ceny, kultury pracy i uczciwości w podejściu do klientów. Obecnie mój Ryzen 1600 się nudzi :)',
          'id': 718
        }, {
          'rating': 5,
          'authorName': 'ftereska',
          'date': '2018-03-01T16:45:00.000Z',
          'content': 'najlepszy wybor w tej cenie',
          'id': 719
        }, {
          'rating': 6,
          'authorName': 'Koru',
          'date': '2018-04-03T16:05:00.000Z',
          'content': 'wymieniłem fx na ryzena jak narazie jestem zadowolony zobaczymy jak rachunki za prąd',
          'id': 720
        }, {
          'rating': 6,
          'authorName': 'qiuku',
          'date': '2018-04-12T16:19:00.000Z',
          'content': 'Procek niczego sobie. Nie wymaga dodatkowego chłodzenia. Radzi sobie świetnie i nie muli. Jestem pewien, że trochę posłuży. POLECAM',
          'id': 721
        }, {
          'rating': 6,
          'authorName': 'Exteasy',
          'date': '2018-05-30T00:16:00.000Z',
          'content': 'Genialny procesor w tym segmencie cenowym. Na stockowym coolerze i płycie B350 podkręcony do stabilnych 3,7Ghz osiąga w stresie maksymalnie 65 stopni, a w spoczynku 40-45 stopni. W grach jak i programach sprawdza się genialnie. Z pewnością nie będzie on "wąskim gardłem" dla karty w najnowszych grach na ustawieniach ultra w 1080P.',
          'id': 722
        }, {
          'rating': 6,
          'authorName': 'Jakub',
          'date': '2018-07-25T17:14:00.000Z',
          'content': 'Świetny stosunek cena - wydajność',
          'id': 723
        }, {
          'rating': 6,
          'authorName': 'Patryk',
          'date': '2018-08-14T16:17:00.000Z',
          'content': 'Ten procesor posiadam już od dłuższego czasu i jestem bardzo zadowolony. Jest szybki i stabilny. Polecam!',
          'id': 724
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 5 1600', 'id': 560}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.2 GHz (3.6 GHz w trybie turbo)', 'id': 561}]
        }, {'name': 'Pamięć podręczna', 'filterable': false, 'id': 51, 'options': [{'name': '16 MB', 'id': 562}]}]
      }, {
        'id': 71,
        'name': 'AMD Ryzen 5 1600X 3.6GHz',
        'price': 705,
        'quantity': 41,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-1600x-36ghz-357760,2017/7/pr_2017_7_14_13_38_24_270.jpg',
          'main': true,
          'id': 318
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-1600x-36ghz-357760,2017/11/pr_2017_11_29_16_13_53_485_00.jpg',
          'main': false,
          'id': 319
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Nbolt',
          'date': '2017-04-13T10:56:00.000Z',
          'content': 'Procesor naprawdę fajny i przyszłościowy.\nNajwiększa wada że sprzedawany jes bez coolera, a duże coolery niereferencyjne nie mają w zestawach odpowiednich adapterów do montażu, trzeba je zamawiać osobno u producentów...',
          'id': 725
        }, {
          'rating': 6,
          'authorName': 'Maks',
          'date': '2017-05-19T21:25:00.000Z',
          'content': 'Dostatecznie dużo wątków i wysokie taktowanie. Na prawdę wydajny i komfortowy procesor.\nBrak schładzacza nie przeszkadzał i tak zamierzałem kupić coś większego i cichszego. Wybór padła na Arctic Freezer 33 (mocowanie jest w zestawie).',
          'id': 726
        }, {
          'rating': 6,
          'authorName': 'Reylegh',
          'date': '2017-06-05T14:01:00.000Z',
          'content': 'Procesor świetnie się podkręcił na chłodzeniu corsair H60 do 4GHz.\nW grach ma wyniki świetnie wbrew niektórym benchmarkom nie straszny mu nawet 7700k.',
          'id': 727
        }, {
          'rating': 6,
          'authorName': 'Piter',
          'date': '2017-09-26T18:40:00.000Z',
          'content': 'Bardzo dobry i wydajny procesor (mam go od 3 m-cy):\n+ niskie temperatury - średnia w grach to 45 stopni\n+ ciche i wydajne chłodzenie od AMD - średnie obroty to 1100 (niestety bez podświetlania RGB)\n+ łatwy do podkręcania (na stockowym chłodzeniu pracuje stabilnie na 3.9 GHz, bez podnoszenia napięcia)\n+ moc wystarczająca do grania we wszystkie nowości\n+ bardzo dobry do grafiki 3D (12 wątków robi robotę)',
          'id': 728
        }, {
          'rating': 6,
          'authorName': 'Damian',
          'date': '2017-10-09T08:56:00.000Z',
          'content': 'polecam wszystkie gry na ultra i zużycie procesora to max 40% przy streamowaniu dochodzi do 50% w 1080 i 60fpsach :)',
          'id': 729
        }, {
          'rating': 6,
          'authorName': 'Jarek',
          'date': '2018-01-16T16:01:00.000Z',
          'content': 'Mega procek;) sprawuje się idealnie.',
          'id': 730
        }, {
          'rating': 6,
          'authorName': 'mungo28',
          'date': '2018-03-27T15:43:00.000Z',
          'content': 'Procesor stabilnie podkręcony na 4GHz:)',
          'id': 731
        }, {
          'rating': 6,
          'authorName': 'Hubert',
          'date': '2018-04-06T08:52:00.000Z',
          'content': 'Trochę już przeżyłem z tym prockiem i śmiało mogę go polecić, szczególnie fanom strategii (Total War i gry Paradoxu) jak i również osobom korzystających z wszelkich programów obciążających procesory. Ponadto świetnie współpracuje z GPU RX 580. Dla mnie najlepszy zakup na x-komie',
          'id': 732
        }, {
          'rating': 6,
          'authorName': 'karolek',
          'date': '2018-06-13T15:17:00.000Z',
          'content': 'dobry i wydajny procek w korzystnej cenie. sprawdza się na codzien i z grami tez sobie radzi, zastanawiałem się nad intelem, ale nie ma co ryzen wymiata :D polecam&nbsp;',
          'id': 733
        }, {
          'rating': 6,
          'authorName': 'Special',
          'date': '2018-09-04T16:09:00.000Z',
          'content': 'Procesor jest dla mnie idealny. Daje on możliwość całkiem niezłego OC nie grzeje się a działa wręcz idealnie. 6 rdzeni oraz 12 wątków w zupełności wystarczą na długie lata do tego w porównaniu do konkurentów nie jest on drogi. Jest to idealny procesor jeżeli chcecie na nim i pograć i popracować nic dodać nic ująć polecam na 110%.',
          'id': 734
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '95 W', 'id': 496}]}, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Odblokowany mnożnik', 'id': 497}, {'name': 'Wersja BOX (brak wentylatora w zestawie)', 'id': 498}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '12 wątków', 'id': 502}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '16 MB', 'id': 562}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 5 1600X', 'id': 563}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.6 GHz (4.0 GHz w trybie turbo)', 'id': 564}]
        }]
      }, {
        'id': 72,
        'name': 'Intel i7-7700K 4.20GHz 8MB BOX',
        'price': 1489,
        'quantity': 26,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-7700k-420ghz-8mb-box-340965,2017/7/pr_2017_7_14_8_23_17_858.jpg',
          'main': true,
          'id': 320
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-7700k-420ghz-8mb-box-340965,pr_2017_1_2_10_5_6_727.jpg',
          'main': false,
          'id': 321
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i7-7700k-420ghz-8mb-box-340965,pr_2017_1_2_10_5_9_368.jpg',
          'main': false,
          'id': 322
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Rataj26',
          'date': '2017-01-11T18:21:00.000Z',
          'content': 'Dobry procesor ale drogi ale dobre musi swoje kosztować',
          'id': 735
        }, {
          'rating': 6,
          'authorName': 'Marcinplayer',
          'date': '2017-01-21T10:45:00.000Z',
          'content': 'Procesor klasa sama w sobie i teraz czekamy na ósmą generację.Polecam.',
          'id': 736
        }, {
          'rating': 6,
          'authorName': 'Cichy',
          'date': '2017-02-17T00:01:00.000Z',
          'content': 'Nigdy nie lubiłem modernizacji komputera... Sumy zawsze przerażają gdy naszą krwawicę wydajemy w kilka sekund więc troszkę ta cena boli, no boli do momentu odpalenia przykładowo Wiedźmina 3 na "full" i juz wiemy że chyba jednak opłacało się zainwestować. W zestawie z chłodzeniem od Silentium PC pod obciążeniem trzyma temp ok 60 stopni, żadnych spadków wydajności bez powodu itp jeszcze nie odnotowałem więc śmiało mogę polecić. PS!! Mało płyt głównych go obsługuje, więc sprawdźcie płytę 2x!!!!!',
          'id': 737
        }, {
          'rating': 6,
          'authorName': 'kesiek',
          'date': '2017-02-19T17:00:00.000Z',
          'content': 'Procesor super. Kupiłem go prawie miesiąc temu. Jestem zadowolony. Wszystko chodzi jak sobie zaplanowałem. Dodatkowo podkręcony do 4,5 Ghz. Może i cena jest duża, ale starcza na parę lat. Cichy Jeżeli chodzi o kompatybilność płyt, to wszystkie na  chipchecie np: z270.',
          'id': 738
        }, {
          'rating': 5,
          'authorName': 'Artur',
          'date': '2017-03-05T16:41:00.000Z',
          'content': 'Przesiadka z i7 4771 - Wydajność rewelacja ale intel jak zwykle dał ciała nic nie poprawił syf glut pod IHS. Temperatury w stresie przy turbo 4.6 GHz po 5 minutach 72 stopnie max 82 stopni na chłodzeniu -be quiet! Pure Rock 120mm.\nA więc o wyższym OC niż 46 GHz bez chłodzenia wodnego możemy zapomnieć.\n- 1 gwiazdka za temperatury a ogólnie procesor super nawet przy standardowym turbo 4.5 GHz na wiele lat zaspokoi potrzeby GRACZA Polecam wart swojej ceny.',
          'id': 739
        }, {'rating': 6, 'authorName': 'satvic', 'date': '2017-03-09T16:21:00.000Z', 'content': 'Procesor super.', 'id': 740}, {
          'rating': 4,
          'authorName': 'Kriss',
          'date': '2017-03-25T00:42:00.000Z',
          'content': 'Procesor jak procesor wcześniej pracowałem na 6700K naprawdę fajna wydajność, niskie temperatury. 7700K wysoka wydajność + wysokie temp. W czasie spoczynku (normalnego użytkowania) z coolerem Be quiet Pure Rock i dobrze wentylowaną obudową średnia temp. około 32 *C. Pod obciążeniem niezależnie czy to gra czy też bardziej wymagający program średnia temp. sięgała 60 *C.',
          'id': 741
        }, {
          'rating': 6,
          'authorName': 'Oskar',
          'date': '2017-03-27T06:10:00.000Z',
          'content': 'Procesor sprawuje się świetnie! Intel to jednak intel nie jakieś tam ryzen ;p',
          'id': 742
        }, {
          'rating': 6,
          'authorName': 'Pawelo99',
          'date': '2017-03-29T15:28:00.000Z',
          'content': 'Zamieniłem  i7 2600K na i7700K i różnica jest, ponieważ pracuje na profesjonalnym oprogramowaniu Adobe i Edius mogę stwierdzić że przyrost jest w granicach 30-35 %. Mocno poprawiono technologie QuickSync, rendering 1 minuty  FullHD z filtrami i koloryzacją trwa około 20-25 sekund. Co do temperatur to chyba zależy od konkretnego egzemplarza. Na podkręconym do 4.9 Ghz temp sięga 74 stopni, chłodzenie Silentium PC za 109 zł. Teraz jednak kupiłbym  AMD Ryzena 7, możliwości i cena jedna biją Intela.',
          'id': 743
        }, {
          'rating': 6,
          'authorName': 'pawel',
          'date': '2017-04-02T05:30:00.000Z',
          'content': 'świetny procesor. bez problemu podkręca się do 5GHz',
          'id': 744
        }, {'rating': 6, 'authorName': 'totik', 'date': '2017-04-05T14:42:00.000Z', 'content': 'Bajka!', 'id': 745}, {
          'rating': 6,
          'authorName': 'Wojtek',
          'date': '2017-05-08T22:52:00.000Z',
          'content': 'Super procesor, wart ceny. W najnowszych grach na najwyższych możliwych ustawieniach (np. Ghost recon, Gta V, Battlefield 1) obciążony tylko w 50%... Komputer uruchamia się w 15 sekund...raj :D',
          'id': 746
        }, {'rating': 6, 'authorName': 'Adam', 'date': '2017-05-22T19:11:00.000Z', 'content': 'OK', 'id': 747}, {
          'rating': 5,
          'authorName': 'HACEK',
          'date': '2017-06-13T07:41:00.000Z',
          'content': 'FATALNE TEMPERATURY ALE POLECAM BO TO NAJNOWSZA I7 !',
          'id': 748
        }, {
          'rating': 6,
          'authorName': 'hugo',
          'date': '2017-07-11T17:41:00.000Z',
          'content': 'Procesor jest git. ale największym problemem jest pasta pod IHS... bez delidu ciężko podnieść taktowania do 5GHz w bezpiecznej temperaturze mimo naprawdę wydajnego chłodzenia, ale rzadko zdarzają się wyjątki co osiągają prawie 5.0 przy max 90 C pod pełnym obciążeniem.. Mimo wszystko przeważnie jest 4.7 GHz i i 90-93 C w stresie gdzie występuje throttling, więc lepiej zostawić 4.5 GHz przy "normalnym" chłodzniu...',
          'id': 749
        }, {
          'rating': 5,
          'authorName': 'Ataniel',
          'date': '2017-07-13T14:54:00.000Z',
          'content': 'jedyne co mozna ocenic to OC. Poczatkowo z plyta MSI M5 max co osiagnalem to 4800 ale zdazalys ie dziwne zachowania. Po upgradzie BIOSU plyty 5000 dziala ale niektore gry (The Division) kompletnie nie startuja bez jakiegokolwiek powodu. Oczywiscie na pewno mozna temu zaradzic ale nie spodziewalem sie problemow przy tak niskim OC, ktore rzekomo bylo wrecz automatyczne przy tej plycie. Tak ze nieco rozczarowanie, ale na standardowym taktowaniu absolutnie zero problemu.',
          'id': 750
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2017-07-17T09:24:00.000Z',
          'content': 'Bardzo prosimy o kontakt, postaramy się rozwiązać problemy z Twoim procesorem. Jesteśmy do twojej dyspozycji: serwis@x-kom.pl lub pod numerem 34 377 00 30.',
          'id': 751
        }, {
          'rating': 5,
          'authorName': 'Kamil',
          'date': '2017-08-09T21:13:00.000Z',
          'content': 'Wydajność jest świetna, jak na razie jest to właściwie najszybszy procesor do gier\n\nW starych tytułach miażdzy wydajnością jednordzeniową, a ja lubię modować. W grach AAA nie jest nawet blisko pełnego wykorzystania\n\nWiele osób uważa że nie warto pchać się już w quady. Jestem odmiennego zdania :)\n\nNie mogę się rozpisać przez gówno limit na znaki.\n\nMinus za to że grzeje się jak moja pała podczas polerowania. Ponad 85 C na stocku, co prawda mam na razie gówno chłodzenie, ale to i tak dużo za dużo.',
          'id': 752
        }, {
          'rating': 6,
          'authorName': 'Brutalus',
          'date': '2017-09-11T16:40:00.000Z',
          'content': '...może i tylko 4 rdzenie ale złożyłem właśnie klientowi "zabawkę" na 7700K (nomen omen z łatwością poszedł na 5GHz) i jakiego tytułu bym nie uruchomił to miażdży mojgo Ryżego 7 1800X w każdej grze. nie zależnie czy zapnę CF 2xRX470 czy GTX1070. Jeśli nie oszukujecie się sami przed sobą i komputer ma być w 90% maszynką do grania to ten Intelek jest po prostu dogmatem konfiguracji!',
          'id': 753
        }, {
          'rating': 5,
          'authorName': 'Kanar',
          'date': '2017-09-18T05:15:00.000Z',
          'content': 'Fantastyczny procesor dla ludzi kochających wydajność!',
          'id': 754
        }, {
          'rating': 6,
          'authorName': 'Bosak',
          'date': '2017-09-24T12:01:00.000Z',
          'content': 'Procesor bardzo dobry, nic sie z nim nie dzieje, temperatury przy graniu w 2k przy chlodzeniu ciecza siegaja 65-70 stopni max, polecam!',
          'id': 755
        }, {
          'rating': 5,
          'authorName': 'RaV',
          'date': '2017-09-25T05:28:00.000Z',
          'content': 'Bardzo wydajny, tylko bardzo się nagrzewa',
          'id': 756
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2017-09-25T08:10:00.000Z',
          'content': 'Procek najlepszy z możliwym dla tego socketu. Faktem jest że lubi się wygrzewać. Polecam duży cooler i wtedy realnie 40-55C. Technologicznie bez zarzutów.',
          'id': 757
        }, {
          'rating': 6,
          'authorName': 'Rafał',
          'date': '2017-09-25T18:03:00.000Z',
          'content': 'Super sprzęt, wydajny, odblokowany zegar. Warty ceny.',
          'id': 758
        }, {
          'rating': 6,
          'authorName': 'Kordian',
          'date': '2017-09-26T05:37:00.000Z',
          'content': 'Dobra wydajność.',
          'id': 759
        }, {
          'rating': 6,
          'authorName': 'Tomasz',
          'date': '2017-09-26T16:16:00.000Z',
          'content': 'Bardzo dobry procesor. Idealny dla grania w wymagające gry.',
          'id': 760
        }, {
          'rating': 6,
          'authorName': 'Yuriy',
          'date': '2017-10-01T15:10:00.000Z',
          'content': 'Jest warty , polecam :)\nDziękuję x - kom',
          'id': 761
        }, {
          'rating': 6,
          'authorName': 'Vinni_Biesiu',
          'date': '2017-10-29T18:48:00.000Z',
          'content': 'Procesor bezkonkurencyjny :) Nie zdarzyło mi się jeszcze wbić go na \'100%\' obrotów, zawsze pozostaje duży zapas. Pod obciążeniem nie grzeje się tak jak poprzednicy. Dzięki X-Kom za kolejne udane zakupy :)',
          'id': 762
        }, {
          'rating': 6,
          'authorName': 'Greg',
          'date': '2017-11-13T13:22:00.000Z',
          'content': 'Porządny procesor, zostawiłem sobie furtkę na OC stąd wybór wersji K, ale narazie na standardowym taktowaniu jeszcze nic go nie wykorzystało na 100% nawet najnowsze tytuły. Temperatury ma wyższe 0 15 stopni w stosunku do mojego poprzedniego 4770 - 65 stopni pod obciążeniem na wiatraku Pure Rock, a wzrost wydajności nie jest porażający, wbrew słupkom producenta, plus za mniejsze zużycie prądu. Zysk już za 10 lat.',
          'id': 763
        }, {
          'rating': 6,
          'authorName': 'Kukuszkin',
          'date': '2018-01-08T20:25:00.000Z',
          'content': 'Mimo że procesor posiadam od dziś, śmiało stwierdzam że wart był wydania każdej złotówki. Przesiadka z młodszego brata i5 7500 zauważalna, temperatury wcale nie wyższe od poprzednika. Kolejne udane zakupy w x-kom 👍',
          'id': 764
        }, {
          'rating': 4,
          'authorName': 'KfadratT',
          'date': '2018-01-11T11:29:00.000Z',
          'content': 'Procesor jak najbardziej cena=wydajność ale z tym wiążą się problemy temperatur jednak intel nie postarał się z glutem na tych układach. Zalecam przed zakupem przemyśleć czy nie będziemy chcieli obniżyć temp z 90C~~ przy renderowaniu skalpowaniem, w moim przypadku pomogło to obniżyć temperaturę oc 20C~! @5.2ghz',
          'id': 765
        }, {
          'rating': 6,
          'authorName': 'Paweł',
          'date': '2018-02-25T17:00:00.000Z',
          'content': 'Procesor bardzo wydajny komputer sie wlacza blyskawicznie, w programach bestia w grach takze bardzo szybki.\nAle minus za gluta trafila mi sie bardzo goraca sztuka fakt ze chlodzenie mam marne, ale temp powinny byc nizsze bez oc w gtaV po 50min gry 95 97stopni w mniej wymagajacych grach 72 73 w spoczynku 39 41, takze czeka skalp i aio nzxt kraken nie widze tego cpu bez wody',
          'id': 766
        }, {
          'rating': 6,
          'authorName': 'rozr111',
          'date': '2018-04-11T09:56:00.000Z',
          'content': 'trafiła mi się dobra sztuką pod kątem temp i OC. Przy chłodzeniu Fractal Celsius S36 (ustawienia domyślne) 5.0Ghz przy napięciu 1,22V maks temp w syntetykach 67-68st,  w grach 3-4stopnie mnije. Choć cena mogłaby być niższy biorąć pod uwagę że jest to procesor z poprzedniej generacji.',
          'id': 767
        }],
        'attributes': [{
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}, {'name': 'Intel Hyper-Threading', 'id': 515}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Odblokowany mnożnik', 'id': 497}, {'name': 'Wersja BOX (brak wentylatora w zestawie)', 'id': 498}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i7', 'id': 499}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 630', 'id': 539}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '8 wątków', 'id': 551}]
        }, {'name': 'Pamięć podręczna', 'filterable': false, 'id': 51, 'options': [{'name': '8 MB', 'id': 555}]}, {
          'name': 'TDP',
          'filterable': false,
          'id': 55,
          'options': [{'name': '91 W', 'id': 559}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i7-7700K', 'id': 565}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '4.2 GHz (4.5 GHz w trybie turbo)', 'id': 566}]
        }]
      }, {
        'id': 73,
        'name': 'Intel G5400 3.70GHz 4MB + B360M PRO-VD + SOFTWARE PACK',
        'price': 529,
        'quantity': 59,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g5400-370ghz-4mb-b360m-pro-vd-software-pack-444677,2018/8/pr_2018_8_14_12_59_18_699_00.jpg',
          'main': true,
          'id': 323
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g5400-370ghz-4mb-b360m-pro-vd-software-pack-444677,2018/8/pr_2018_8_14_12_59_22_27_01.jpg',
          'main': false,
          'id': 324
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g5400-370ghz-4mb-b360m-pro-vd-software-pack-444677,2018/8/pr_2018_8_14_12_59_25_136_02.jpg',
          'main': false,
          'id': 325
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g5400-370ghz-4mb-b360m-pro-vd-software-pack-444677,2018/8/pr_2018_8_14_12_59_31_386_04.jpg',
          'main': false,
          'id': 326
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g5400-370ghz-4mb-b360m-pro-vd-software-pack-444677,2018/8/pr_2018_8_14_12_59_28_417_03.jpg',
          'main': false,
          'id': 327
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g5400-370ghz-4mb-b360m-pro-vd-software-pack-444677,2018/8/pr_2018_8_14_12_59_34_604_05.jpg',
          'main': false,
          'id': 328
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Wiedźmin',
          'date': '2018-08-21T15:28:00.000Z',
          'content': 'Super zestawik idealna podstawa na taniego PC.',
          'id': 768
        }, {
          'rating': 6,
          'authorName': 'Maxmor',
          'date': '2018-08-24T06:29:00.000Z',
          'content': 'Polecam. PC złożony dla syna pod Miecrafta i inne mniej wymagjaące gierki. Zasilacz, stary HDDi obudowę miałem ze starego, który padł. Do tego poza zestawem dokupiłem SSD BIWIN 120 GB, RAM 2x4 GB(GOODRAM 8GB 2133MHz IRIDIUM Black) + używana grafika GTX 1050. Dobry PC złożony za 1500 PLN. Gierki śmigają, działa elegancko. Polecam promkę, cena korzystna.',
          'id': 769
        }, {
          'rating': 6,
          'authorName': 'Dragonus Pierwszy',
          'date': '2018-08-27T20:09:00.000Z',
          'content': 'Super. Dobra płyta główna do Intel G5400. Kompik świeżo złożony dziś. Śmiga jak miło, aktualny BIOS. polecam.',
          'id': 770
        }],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Pentium', 'id': 508}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'G5400', 'id': 509}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.7 GHz', 'id': 510}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '2 rdzenie', 'id': 511}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '4 MB', 'id': 512}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 610', 'id': 513}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '54 W', 'id': 514}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Hyper-Threading', 'id': 515}]
        }]
      }, {
        'id': 74,
        'name': 'AMD FX-8350 4.00GHz 8MB BOX 125W',
        'price': 345,
        'quantity': 3,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-8350-400ghz-8mb-box-125w-116377,2017/9/pr_2017_9_26_14_56_36_226_00.jpg',
          'main': true,
          'id': 329
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-8350-400ghz-8mb-box-125w-116377,pr_2016_4_12_13_6_57_152.jpg',
          'main': false,
          'id': 330
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-8350-400ghz-8mb-box-125w-116377,72284_43.jpg',
          'main': false,
          'id': 331
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-8350-400ghz-8mb-box-125w-116377,72284_2.jpg',
          'main': false,
          'id': 332
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-8350-400ghz-8mb-box-125w-116377,72284_3.jpg',
          'main': false,
          'id': 333
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-8350-400ghz-8mb-box-125w-116377,72284_4.jpg',
          'main': false,
          'id': 334
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Gość serwisu x-kom',
          'date': '2012-11-21T10:23:00.000Z',
          'content': 'zmiencie proszę opis produktu ponieważ to nie jest bulldozer a vishera',
          'id': 771
        }, {
          'rating': 6,
          'authorName': 'Gość serwisu x-kom',
          'date': '2012-11-24T21:14:00.000Z',
          'content': '...w rzeczy samej, Firma AMD oficjalnie stosuje taką oto nomenklaturę słowną  - AMD FX',
          'id': 772
        }, {
          'rating': 5,
          'authorName': 'Gość serwisu x-kom',
          'date': '2013-12-22T08:03:00.000Z',
          'content': 'W tym przedziale cenowym..warto.',
          'id': 773
        }, {
          'rating': 6,
          'authorName': 'grid',
          'date': '2014-04-07T11:16:00.000Z',
          'content': 'Za tą cenę to potwór, z zapasem mocy!',
          'id': 774
        }, {
          'rating': 6,
          'authorName': 'Tom',
          'date': '2014-08-12T13:01:00.000Z',
          'content': 'Procesor igła. Musiałem brać coś pod port AM3 żeby nie zmieniać całego komputera więc wybór był oczywisty że padnie na 8350. Jednak mój następny komputer to będzie i7 4gen.',
          'id': 775
        }, {
          'rating': 6,
          'authorName': 'Andrzej',
          'date': '2014-12-12T09:37:00.000Z',
          'content': 'Polecam. Działa z Gigabyte GA-970A-DS3P i radeonem R9 270 MSI - kupiony dla Dragon Age Inkwizycja. Działa rewelacyjnie',
          'id': 776
        }, {
          'rating': 6,
          'authorName': 'Sosinsky',
          'date': '2015-01-19T20:58:00.000Z',
          'content': 'Najlepszy wybór jeśli chodzi o AMD. Na stockowym chłodzeniu można zapomnieć o jakichkolwiek grach(przy 60-70* procek zaczyna się dusić). Przy customowym SilentiumPC Fera2 daje radę, lecz warto zamontować drugi wiatrak i...kilka w obudowę. To jest AMD, jak się go ma to wiadomo, że będzie pożar ;) Oprócz tego to moc tego procka jest ogromna!',
          'id': 777
        }, {
          'rating': 5,
          'authorName': 'x',
          'date': '2015-04-07T14:27:00.000Z',
          'content': 'niestety cena procka poszła w górę i już jest nieopłacalnym wyborem. Kiedyś była cena 650 wtedy było trochę warto, ale teraz już niestety nie, i5 w grach generuje 30 procent więcej fps a kosztuje już tylko 250 złoty więcej.',
          'id': 778
        }, {'rating': 1, 'authorName': 'kolo', 'date': '2016-01-20T06:01:00.000Z', 'content': 'bo tak', 'id': 779}, {
          'rating': 6,
          'authorName': 'krauziak',
          'date': '2016-01-31T10:47:00.000Z',
          'content': 'mam go prawie 2 lata i mogę z czystym sumieniem polecić',
          'id': 780
        }, {'rating': 6, 'authorName': ':)', 'date': '2016-05-15T16:21:00.000Z', 'content': 'Polecam', 'id': 781}, {
          'rating': 6,
          'authorName': 'zbyszek',
          'date': '2016-05-27T12:36:00.000Z',
          'content': 'używam go 2 lata i nadal ma wystarczający zapas mocy...',
          'id': 782
        }, {
          'rating': 6,
          'authorName': 'cfh',
          'date': '2016-08-29T16:03:00.000Z',
          'content': 'mam go również juz prawie 2 lata i jestem bardzo zadowolony. Dopiero teraz kiedy coraz wiecej gier i programów zaczyna wykorzystywac technologie multicore ten procek pokazuje pazur. Ale warto miec dobra płyte główną z mocną sekcją zasilającą. U mnie ten FX lata na ASUS 970 Pro Gaming/Aura.',
          'id': 783
        }, {
          'rating': 5,
          'authorName': 'Madraszynko',
          'date': '2016-11-12T10:33:00.000Z',
          'content': 'Procesor działa dosyć szybko, pierwszorzędne tytuły na optymalnych ustawieniach działają płynnie. Polecam kupić nowe chłodzenie procesora bo fabryczne nie daje rady i procesor przy maksymalnym obciążeniu osiąga temperatury ok 70-80 stopni (czyli mała elektrownia atomowa w pudełku ;) ). Z procesorem nie ma problemów...z wyjątkiem temperatury.',
          'id': 784
        }, {
          'rating': 6,
          'authorName': 'Grzegorz',
          'date': '2017-01-12T15:47:00.000Z',
          'content': 'Mam od dwóch lat i nie narzekam. W połączeniu z Radeonem 280x z 3GB RAMu gry chodzą płynnie na wysokich lub ultra',
          'id': 785
        }, {
          'rating': 5,
          'authorName': 'Ramzes',
          'date': '2017-03-14T08:57:00.000Z',
          'content': '1 Trzeba dokupić nowe chłodzenie  !!! Potem ma się rakietę  nie komputer :)',
          'id': 786
        }, {
          'rating': 6,
          'authorName': 'genezis',
          'date': '2017-03-21T18:06:00.000Z',
          'content': 'Zatrzymałem się na starszych, kultowych tytułach i tu procek bez problemu daje radę w fHD. Cenowo jestem zadowolony, z wydajności również, na moje potrzeby okej.',
          'id': 787
        }, {
          'rating': 6,
          'authorName': 'Senior',
          'date': '2017-04-03T18:03:00.000Z',
          'content': 'Montaż banalny, najlepszy procesor w tej cenie z rodzinki AMD. Możliwości podkręcania, cicha praca, dobre chłodzenie, pociągnie bardziej wymagające tytuły. Tanio, a dobrze, można brać.',
          'id': 788
        }, {
          'rating': 6,
          'authorName': 'tomrud',
          'date': '2017-12-10T21:48:00.000Z',
          'content': 'Wszystko w porządku.',
          'id': 789
        }, {
          'rating': 5,
          'authorName': 'Jacek',
          'date': '2018-01-12T12:57:00.000Z',
          'content': 'Procesor naprawdę dobry. Tylko chlodzenie Bardzo\n glosne. Momentami buczy jak odkurzacz. Proponuje wymienic na inne i bedzie super. Sam procesor oceniam na 5+',
          'id': 790
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '8 rdzeni', 'id': 532}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR3-1600 (PC3-12800)', 'id': 541}, {
            'name': 'DDR3-1333 (PC3-10600)',
            'id': 542
          }, {'name': 'DDR3-1866 (PC3-15000)', 'id': 571}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '8 MB', 'id': 555}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD FX', 'id': 567}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'FX-8350', 'id': 568}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM3+', 'id': 569}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '4.0 GHz (4.2 GHz w trybie turbo)', 'id': 570}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '32 nm', 'id': 572}]
        }, {'name': 'Dodatkowe technologie', 'filterable': false, 'id': 56, 'options': [{'name': 'AMD Turbo Core', 'id': 573}]}]
      }, {
        'id': 75,
        'name': 'Intel G4560 3.50GHz 3MB BOX',
        'price': 269,
        'quantity': 76,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g4560-350ghz-3mb-box-343473,pr_2017_1_12_11_27_20_773.jpg',
          'main': true,
          'id': 335
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g4560-350ghz-3mb-box-343473,pr_2017_1_12_11_27_17_585.jpg',
          'main': false,
          'id': 336
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'zomeer',
          'date': '2017-02-05T21:11:00.000Z',
          'content': 'To jest najlepszy pentium jaki wyszedł.\n\nNie bierzcie żadnego innego bo tylko te nowe mają HT. To praktycznie I3 tylko o 50% tańszy. \nA osiągi są świetne jak za 270 zł. \nProcesor spisuje się rewelacyjnie. Chłodzenie ciche nawet pod obciążeniem. Jeśli kogoś stać to i5 jeśli nie to tylko ten pentium bo i3 przestało mieć znaczenie',
          'id': 791
        }, {
          'rating': 6,
          'authorName': 'Gracz',
          'date': '2017-02-12T05:31:00.000Z',
          'content': 'Rewelacyjny stosunek ceny do jakości, zdecydowanie polecam. 2 rdzenie 4 wątki, po prostu i3 tylko 2 razy tańsza.',
          'id': 792
        }, {
          'rating': 6,
          'authorName': 'ŁW',
          'date': '2017-03-06T08:38:00.000Z',
          'content': 'Relacja wydajność/cena na ta chwilę bezkonkurencyjna. Nie warto dopłacać do i3. \nWzorowa obsługa w xkom Opole.',
          'id': 793
        }, {
          'rating': 6,
          'authorName': 'Arkadiusz',
          'date': '2017-03-09T18:24:00.000Z',
          'content': 'To jest taki i3-4170 albo i3-6100!!! Tylko, że nie kosztuje 500zł tylko ok.280zł! W sumie to nie wiem skąd taka różnica cenowa... Bo nie ma kodeków, które w takim CPU i tak nie są potrzebne? W KAŻDYM RAZIE GORĄCO POLECAM, TO JEST HIT!!!',
          'id': 794
        }, {
          'rating': 6,
          'authorName': 'Tequila994',
          'date': '2017-03-21T10:26:00.000Z',
          'content': 'W duecie z GTX1060 6GB daje stabilne 60FPS w większości nowych gier. W BF1 średnio 60-70FPS czasem tylko dropy do 54FPS kiedy na serwerze jest 64 graczy i dzieje się masakra na monitorze. Detale na Ultra. Killer wszystkich procków do 500zł. Nawet te starsze i5 wciąga nosem. Polecam!',
          'id': 795
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2017-03-23T15:55:00.000Z',
          'content': 'Bardzo dobry stosunek ceny do jakości! :) W tym przedziale cenowym na obecną chwile nie ma lepszego procka.',
          'id': 796
        }, {
          'rating': 6,
          'authorName': 'Venre',
          'date': '2017-03-25T19:02:00.000Z',
          'content': 'To jest nowy król budżetowego gamingu! Granie na Wysokich to dla niego pestka, z resztą, robiłem testy na GTA V i Wiedźminie 3 na łamach portalu MMO24.',
          'id': 797
        }, {
          'rating': 3,
          'authorName': 'me',
          'date': '2017-04-06T15:13:00.000Z',
          'content': 'Nie ocenię możliwości procesora, ale przy zakupie radzę zwrócić uwagę na dołączone chłodzenie. Komputer na szczęście nie był dla mnie, ale chłodzenie pracuje, potem nagle przez moment zaczyna hałasować, potem znowu normalnie pracuje i tak w kółko. I to wszystko jedynie przy włączonym systemie, bez robienia czegokolwiek, więc może co niektórzy muszą rozważyć inne chłodzenie niż dorzucane przez producenta.',
          'id': 798
        }, {
          'rating': 6,
          'authorName': 'Piotrek1375',
          'date': '2017-04-07T01:37:00.000Z',
          'content': 'Coż tu duzo pisac, najlepszy procesor w swojej klasie, za nie duze pieniądze.  Przegania i3 w praktycznie kazdych warunkach,  gorąco polecam :)',
          'id': 799
        }, {
          'rating': 6,
          'authorName': 'Alpha',
          'date': '2017-04-24T17:16:00.000Z',
          'content': 'I tutaj się zgodzę , procesor robi robotę ;)',
          'id': 800
        }, {
          'rating': 6,
          'authorName': 'Tomek',
          'date': '2017-05-09T19:18:00.000Z',
          'content': 'Procesor w tej cenie rewelacja, tym bardziej, że przesiadłem się z Q6600.',
          'id': 801
        }, {
          'rating': 6,
          'authorName': 'mario375',
          'date': '2017-05-19T22:53:00.000Z',
          'content': 'nie widzę w grach wiekszej  rożnicy niż I5 4460 którą miałem wcześniej, a teraz mogłem sobie pozwolić na lepszą grafikę',
          'id': 802
        }, {
          'rating': 6,
          'authorName': 'andrzej.tubiz',
          'date': '2017-06-02T05:09:00.000Z',
          'content': 'Kupiłem ten CPU jakiś czas temu w xkom i wiecie co Wam powiem? To jest średniej klasy i5 z napisem Pentium. Pracuję jako programista (C,CPP,JAVA,C#). Bardzo często też siedzę w Adobe CS-LS (Premiere i Photoshop). Ten malec (G4560) bije wszystkie tanie i3 na tym samym sockecie które mam w pracy. Niczym nie rózni się od i5-6600k i podobnych a jest przecież ponad 2 razy tańszy. Wydaje mi się że Intel by załatać dziurę w miejscu tanich procków - po prostu zmienił nazwę jakiegoś i5!',
          'id': 803
        }, {
          'rating': 4,
          'authorName': 'rich',
          'date': '2017-06-07T11:37:00.000Z',
          'content': 'procesor dobry do taniego pc-ta , niestety strasznie waha się cena na x-kom jeszcze wczoraj kosztował 40 zł mniej',
          'id': 804
        }, {
          'rating': 6,
          'authorName': 'szyder',
          'date': '2017-06-07T16:22:00.000Z',
          'content': 'potrzebowałem złożyć komputer na potrzeby domowe... miałem ograniczony budżet. Dysk ssd 128gb, karta graficzna od msi 1050Ti w połączeniu z tym procesorem daje ZNAKOMITE OSIĄGI w grach a także w codziennym użytku. Jestem wręcz zachwycony tym pentiumem 7 generacji.',
          'id': 805
        }, {
          'rating': 6,
          'authorName': 'Rapophis',
          'date': '2017-06-07T18:06:00.000Z',
          'content': 'Kupiłem około miesiąc temu za 239 zł i naprawdę okazał się trafionym zakupem. W zamian za taką kwotę otrzymujemy 4 rdzenie logiczne, które nie ustępują zbytnio średniej półce procesorów pokroju i5, a których cena sięga często 800-900 zł. \n\nWcześniej posiadałem procesor i5-2500, ale w żadnym stopniu nie odczuwam spadku wydajności związanej z przesiadką na ten procesor, ponieważ we wszelkich benchmarkach traci on do i5 2500 jedynie 10-20% mocy. \n\nDobry procesor do domowych zastosowań.',
          'id': 806
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2017-06-14T12:15:00.000Z',
          'content': 'Udało się kupić w dobrej cenie, do zastosowań domowych i w budżetowych zestawach gamingowych najlepszy możliwy wybór.',
          'id': 807
        }, {
          'rating': 6,
          'authorName': 'lukasis',
          'date': '2017-06-16T13:12:00.000Z',
          'content': 'W tej cenie nie ma nic lepszego!',
          'id': 808
        }, {
          'rating': 5,
          'authorName': 'Piotr',
          'date': '2017-07-22T20:14:00.000Z',
          'content': 'Klocek jest naprawde dobry szczerze polecam obsługa sklepu jest fachowa',
          'id': 809
        }, {
          'rating': 5,
          'authorName': 'Kris',
          'date': '2017-09-13T11:23:00.000Z',
          'content': 'Kupiony za ok. 250 zł w tej cenie bez konkurencji, co do chłodzenia box to głośny traktor( przy obudowie na biurku nie do wytrzymania)  - zmieniłem na Fortis 3 - temp w idle to 43 i ok540 obr/min przy maksymalnym obciążeniu samego procesora temp do 75 prz obrotach Ok 600/min w obudowie ITX w obecnej cenie 320 zł chyba lepiej dopłacić do Ryzen 3',
          'id': 810
        }, {
          'rating': 6,
          'authorName': 'Szymon',
          'date': '2017-09-24T22:02:00.000Z',
          'content': 'Bardzo dobry procesor godny polecenia',
          'id': 811
        }, {
          'rating': 5,
          'authorName': 'Maciej',
          'date': '2017-09-25T05:17:00.000Z',
          'content': 'Za tą cenę nie ma nic lepszego',
          'id': 812
        }, {
          'rating': 6,
          'authorName': 'Mateusz',
          'date': '2017-09-25T15:41:00.000Z',
          'content': 'Idealny za tą cenę. Po pół roku jeszcze mnie nie zawiódł',
          'id': 813
        }, {
          'rating': 6,
          'authorName': 'Filip',
          'date': '2017-09-25T17:40:00.000Z',
          'content': 'W tej cenie nic lepszego nie ma',
          'id': 814
        }, {
          'rating': 5,
          'authorName': 'Tomasz',
          'date': '2017-09-25T19:54:00.000Z',
          'content': 'W tej cenie najlepszy.Komp śmiga aż miło ;)',
          'id': 815
        }, {
          'rating': 6,
          'authorName': 'Rafał',
          'date': '2017-09-27T09:09:00.000Z',
          'content': 'Świetny procesor. Nie ma co dopłacać do i5',
          'id': 816
        }, {
          'rating': 6,
          'authorName': 'Barbara',
          'date': '2017-09-30T22:07:00.000Z',
          'content': 'Najlepszy stosunek\nCeny do wydajnosci.. Procesor kupiony za 220zl w lipcu.. teraz niestety niedostepny..',
          'id': 817
        }, {
          'rating': 6,
          'authorName': 'Bartosz',
          'date': '2017-10-28T11:38:00.000Z',
          'content': 'Cóż można powiedzieć, praca biurowa i podstawowe gierki jak np Leage of legends śmigają! na tym procesorze i zintegrowanej grafice. Dodatkowo kupiłem za 249zł. To była świetna cena!',
          'id': 818
        }, {
          'rating': 6,
          'authorName': 'Adam',
          'date': '2017-12-24T03:18:00.000Z',
          'content': 'Zdecydowanie najlepszy stosunek ceny do wydajności. Hyper Threading w procesorze za 300zł robi robotę. Dodatkowo pobiera bardzo mało prądu, jest podatny na dodatkowy (nie mały!) undervolting, a co za tym idzie jeszcze niższe temperatury i zmniejszony pobór prądu! Zdecydowanie polecam.',
          'id': 819
        }, {
          'rating': 6,
          'authorName': 'Marek Sz.',
          'date': '2018-04-04T19:16:00.000Z',
          'content': 'Procesor sprawuje się bardzo dobrze zarówno w pracy biurowej jak i przy wieczornym relaksie przy grach. Chodzi cicho i nie przegrzewa się nawet po kilkugodzinnej pracy na komputerze. Zdecydowanie polecam w tej cenie.&nbsp;',
          'id': 820
        }, {
          'rating': 6,
          'authorName': 'Dawid',
          'date': '2018-06-15T20:37:00.000Z',
          'content': 'Bardzo dobry procesor jak za swoją cenę.Tani i stosunkowo wydajny (moc porównywalna z i3 6100 lub i5 2400) .AMD może ze swoim nowym APU A8 który jest w podobnej cenie się schować.W grach wydajność bardziej niż zadawalająca w gta V na full hd z gtx 1050 i 8 gb ramu 50-60 klatek na mieszance wysokich i ultra,w Kingdom Come Deliverance na średnich w full hd  40-60 klatek a w nowej dość wymagającej Forzy Horizon 3 okolice średnich w 60 klatkach ale trzeba było zejść na rozdzielczością do 1600x900',
          'id': 821
        }, {
          'rating': 6,
          'authorName': 'tata',
          'date': '2018-07-11T16:43:00.000Z',
          'content': 'Najlepszy procesor do prostego desktopa pod gry takie jak Maincraft albo Fifa dla dzieci. Mają 10-12 lat i na początek złożyliśmy coś prostego. Razem mieliśmy dużo zabawy. Procesor dobry, chłodzenie wydajne. Nie ma żadnych problemów.',
          'id': 822
        }, {
          'rating': 6,
          'authorName': 'Altres',
          'date': '2018-07-15T08:28:00.000Z',
          'content': 'Bardzo dobry procesor, w tej cenie najlepszy.',
          'id': 823
        }],
        'attributes': [{
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Pentium', 'id': 508}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '2 rdzenie', 'id': 511}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '54 W', 'id': 514}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Hyper-Threading', 'id': 515}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '3 MB', 'id': 545}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'G4560', 'id': 574}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.5 GHz', 'id': 575}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 610', 'id': 576}]
        }]
      }, {
        'id': 76,
        'name': 'Intel i5-7500 3.40GHz 6MB BOX',
        'price': 920,
        'quantity': 74,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-7500-340ghz-6mb-box-340961,2017/7/pr_2017_7_14_9_48_47_879.jpg',
          'main': true,
          'id': 337
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-i5-7500-340ghz-6mb-box-340961,pr_2017_1_2_10_14_40_527.jpg',
          'main': false,
          'id': 338
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'maciek',
          'date': '2017-01-26T15:57:00.000Z',
          'content': 'swietny jak za taka cene, mi pasuje',
          'id': 824
        }, {
          'rating': 6,
          'authorName': 'KabyLejk',
          'date': '2017-02-12T05:34:00.000Z',
          'content': 'Zdecydowanie bardziej opłacalny niż jego słabszy brat i5 7400.',
          'id': 825
        }, {
          'rating': 6,
          'authorName': 'b_Art',
          'date': '2017-03-08T01:25:00.000Z',
          'content': 'Świetnie sprawdza się zarówno w grach jak i montażu zdjęć oraz wideo.\n\nMaksymalna temperaturą jaką procesor osiągnął na stockowym chłodzeniu, to 54 stopnie.',
          'id': 826
        }, {
          'rating': 6,
          'authorName': 'Przemek',
          'date': '2017-04-05T13:06:00.000Z',
          'content': 'Najlepsza i5 jakiej używałem chłodzenie box jest ciche i wydajne. Procesor potwór nic nie wytacza z niego 100%',
          'id': 827
        }, {
          'rating': 6,
          'authorName': 'fajfik',
          'date': '2017-04-14T20:23:00.000Z',
          'content': 'Procesor bardzo dobrze sprawdza się przy najnowszych tytułach...w połączeniu z GeForce 1060 6gb ... Dobry stosunek ceny do jakości... polecam',
          'id': 828
        }, {
          'rating': 6,
          'authorName': 'Piotr W.',
          'date': '2017-05-05T09:07:00.000Z',
          'content': 'Solidny, dobry procesor, który ze względu na swoją cenę i wydajność cieszy się ogromną popularnością. Obsługa X-Kom jak zwykle na najwyższym poziomie. Zdecydowanie polecam!',
          'id': 829
        }, {
          'rating': 6,
          'authorName': 'wrenio',
          'date': '2017-05-15T17:05:00.000Z',
          'content': 'Najlepszy procesor za tą cenę. Naprawdę warto.',
          'id': 830
        }, {
          'rating': 6,
          'authorName': 'Jacek',
          'date': '2017-06-01T07:45:00.000Z',
          'content': 'Procesor dobry do gier.Na identycznych ustawieniach wentylatórów chłodniejszy od i3 6300.Na obniżonych ustawieniach obrotów   po  ponad  \npółgodzinnym teście na occt pod ferą2 ledwo osiagnął 60C (temperatura w pokoju 24C) Drobne problemy na windows7(nie przyjmuje aktualizacji) Działa na płycie MSI B150M PRO-VDH-D3. Polecam.',
          'id': 831
        }, {
          'rating': 6,
          'authorName': 'Krzysztof',
          'date': '2017-06-18T11:29:00.000Z',
          'content': 'Dzień Dobry. Procesor w połączeniu z płytą główną Asus TUF Z170 MARK 2 z Geforce 1060 Asus Strix 6 GB i 2x 4 GB RAMU wymiata. Obecnie testuje w tej konfiguracje Crysisa 3. Minimalny liczba klatek na sekunde to 70 przy maksymalnych detalach graficznych.',
          'id': 832
        }, {'rating': 6, 'authorName': 'Patryk', 'date': '2017-07-30T09:56:00.000Z', 'content': 'Dobra i5 ;)', 'id': 833}, {
          'rating': 5,
          'authorName': 'gadzisko18',
          'date': '2017-08-09T06:57:00.000Z',
          'content': 'Zadowolony, cena i parametry. Bardzo mi pasują.',
          'id': 834
        }, {
          'rating': 5,
          'authorName': 'rafaello50',
          'date': '2017-08-16T19:22:00.000Z',
          'content': 'Procesor raczej pod win 10 zeby mógł sie wykazac.Po zainstalowaniu na win 7 brak kompatybilnosci z pewnymi aplikacjami. Zadowolony z zakupu i polecam.',
          'id': 835
        }, {
          'rating': 6,
          'authorName': 'Daniel',
          'date': '2017-09-03T12:17:00.000Z',
          'content': 'Dobry procek do gier polecam.',
          'id': 836
        }, {
          'rating': 6,
          'authorName': 'Krystian',
          'date': '2017-09-25T23:59:00.000Z',
          'content': 'Póki co głównie użytkowany na platformie mini itx głównie do Wiedźmina 3 i obróbki zdjęć w Ligtroomie i w tych zadaniach spisuje się bardzo dobrze. Temperaturowe też nie trzeba się za bardzo martwic. Maksymalne temperatury podczas pracy w okolicach 60 stopni w niewielkiej obudowie. Jak najbardziej na plus',
          'id': 837
        }, {
          'rating': 6,
          'authorName': 'Patryk',
          'date': '2017-09-28T22:57:00.000Z',
          'content': 'Mam i polecam. Wszystkie giery śmigają!',
          'id': 838
        }, {
          'rating': 6,
          'authorName': 'Neo',
          'date': '2018-03-14T15:17:00.000Z',
          'content': 'no to teraz giercownia na całego, pc nabrał skrzydeł, extra procesor z moją kartą jazda bez trzymanki',
          'id': 839
        }],
        'attributes': [{
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '6 MB', 'id': 491}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 630', 'id': 539}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-7500', 'id': 577}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.4 GHz (3.8 GHz w trybie turbo)', 'id': 578}]
        }]
      }, {
        'id': 77,
        'name': 'AMD Ryzen 7 2700',
        'price': 1279,
        'quantity': 58,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-7-2700-421250,2018/4/pr_2018_4_10_12_46_48_934_00.jpg',
          'main': true,
          'id': 339
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-7-2700-421250,2018/4/pr_2018_4_10_12_46_52_903_01.jpg',
          'main': false,
          'id': 340
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Kaneki',
          'date': '2018-04-27T20:35:00.000Z',
          'content': 'Spoczynek 35 a stres max 52 przy spc lt 922. 2 gry plus streamowanie z jedej i żadnych strat :) na 1v napięciu do 3.6 stabilnie',
          'id': 840
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2933', 'id': 523}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '12 nm', 'id': 524}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '8 rdzeni', 'id': 532}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '16 wątków', 'id': 533}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '20 MB', 'id': 534}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'Ryzen 7 2700', 'id': 579}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.2 GHz (4.1 GHz w trybie turbo)', 'id': 580}]
        }]
      }, {
        'id': 78,
        'name': 'Intel G3900 2.80GHz 2MB BOX 47W',
        'price': 169,
        'quantity': 27,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g3900-280ghz-2mb-box-47w-285461,2017/7/pr_2017_7_14_15_6_29_272.jpg',
          'main': true,
          'id': 341
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-g3900-280ghz-2mb-box-47w-285461,pr_2016_1_29_7_33_58_933.jpg',
          'main': false,
          'id': 342
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'Komputeroiwy Wariacina123',
          'date': '2017-12-21T11:25:00.000Z',
          'content': 'Procesor wgnaita w kule ziemska kzoak widzmin na full hd max 450fps polecam do gierek na 10 lat starczy na full hd kozak',
          'id': 841
        }],
        'attributes': [{
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja producenta)', 'id': 406}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '2.8 GHz', 'id': 476}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '2 rdzenie', 'id': 511}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151', 'id': 537}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {
            'name': 'DDR3-1600 (PC3-12800)',
            'id': 541
          }, {'name': 'DDR3-1333 (PC3-10600)', 'id': 542}, {'name': 'DDR4-1866', 'id': 586}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '51 W', 'id': 546}]}, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Celeron', 'id': 581}]
        }, {'name': 'Model procesora', 'filterable': false, 'id': 46, 'options': [{'name': 'G3900', 'id': 582}]}, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '2 wątki', 'id': 583}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '2 MB', 'id': 584}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel HD Graphics 510', 'id': 585}]
        }]
      }, {
        'id': 79,
        'name': 'AMD Ryzen 5 1500X 3.5GHz',
        'price': 629,
        'quantity': 38,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-1500x-35ghz-359917,2017/7/pr_2017_7_14_13_52_43_478.jpg',
          'main': true,
          'id': 343
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-ryzen-5-1500x-35ghz-359917,pr_2017_4_6_11_46_28_496.png',
          'main': false,
          'id': 344
        }],
        'reviews': [{
          'rating': 6,
          'authorName': 'ChrissWhite',
          'date': '2017-05-03T06:36:00.000Z',
          'content': 'Produkt czerwonych AMD RYZEN 1500X jest wyśmienitym wyborem dla osób które bawią się profesjonalnie lub mniej grafiką komputerową i obróbką filmów, mogę z czystym sumieniem stwierdzić że wymienione wyżej zadania to przyjemność z RYZEN\'em. Co do gier również jestem zadowolony, 1500X sprawuje się lepiej niż podejrzewałem (0 problemów w najnowszych tytułach). Kultura pracy tego procesora stoi na naprawdę wysokim poziomie.',
          'id': 842
        }, {
          'rating': 6,
          'authorName': 'Piotrek',
          'date': '2017-09-27T16:33:00.000Z',
          'content': 'Super procek, najlepszy chyba w tej kwocie wedlug mnie 4 rdzenie 4 watki. Mam go swoim komputerze i jedyny maly minus to troche glosny wentylator od chlodzenia. Ale i tak 6 gwiazdek. Polecam',
          'id': 843
        }, {
          'rating': 6,
          'authorName': 'Miesiu',
          'date': '2017-10-05T23:47:00.000Z',
          'content': 'Super szybki processor.  \nW zestawie cichutki wiatrak, do tego chłodny.',
          'id': 844
        }, {
          'rating': 6,
          'authorName': 'Piotrek',
          'date': '2018-03-12T16:58:00.000Z',
          'content': 'Witam kupiłem ten procesor niedawno jest świetny na boxowym chodzeniu 35 stopni w spoczynku przy graniu w UHD 57 stopni więc temperatury świetne polecam',
          'id': 845
        }, {
          'rating': 6,
          'authorName': 'Michał',
          'date': '2018-05-19T10:47:00.000Z',
          'content': 'Ogólnie procesor mam od około miesiąca wszystko jest super. Jedynym minusem który zauważyłem a dokładnie usłyszałem to to że wentylator dziwnie chrobocze ale nie jest to żaden większy problem w moim przypadku wystarczyło go przeczyścić i nasmarować. Dużym plusem przy połączeniu r5 z płyta msi a320 granade jest to iż przy użyciu aplikacji w telefonie mozemy zmieniać różne parametry takie jak: częstotliwość procesora, prędkość wentylatorów.',
          'id': 846
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {'name': 'TDP', 'filterable': false, 'id': 55, 'options': [{'name': '65 W', 'id': 483}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '4 rdzenie', 'id': 489}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}, {'name': 'Odblokowany mnożnik', 'id': 497}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD Ryzen', 'id': 518}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM4', 'id': 520}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.5 GHz (3.7 GHz w trybie turbo)', 'id': 526}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '8 wątków', 'id': 551}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '16 MB', 'id': 562}]
        }, {'name': 'Model procesora', 'filterable': false, 'id': 46, 'options': [{'name': 'Ryzen 5 1500X', 'id': 587}]}]
      }, {
        'id': 80,
        'name': 'Intel Pentium Gold G5500 3.80GHz BOX',
        'price': 359,
        'quantity': 74,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,intel-pentium-gold-g5500-380ghz-box-421232,2018/4/pr_2018_4_4_12_29_27_925_00.jpg',
          'main': true,
          'id': 345
        }],
        'reviews': [],
        'attributes': [{
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '4 wątki', 'id': 490}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2400', 'id': 492}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Pentium', 'id': 508}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '2 rdzenie', 'id': 511}]
        }, {'name': 'Pamięć podręczna', 'filterable': false, 'id': 51, 'options': [{'name': '4 MB', 'id': 512}]}, {
          'name': 'TDP',
          'filterable': false,
          'id': 55,
          'options': [{'name': '54 W', 'id': 514}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Hyper-Threading', 'id': 515}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'G5500', 'id': 588}]
        }, {'name': 'Taktowanie rdzenia', 'filterable': false, 'id': 48, 'options': [{'name': '3.8 GHz', 'id': 589}]}]
      }, {
        'id': 81,
        'name': 'AMD FX-6300 3.50GHz 6MB BOX 95W',
        'price': 329,
        'quantity': 90,
        'category': {'name': 'Processors', 'id': 3},
        'photos': [{
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-6300-350ghz-6mb-box-95w-116734,2017/7/pr_2017_7_14_12_47_13_270.jpg',
          'main': true,
          'id': 346
        }, {
          'url': 'https://cdn.x-kom.pl/i/setup/images/prod/big/product-large,amd-fx-6300-350ghz-6mb-box-95w-116734,pr_2015_2_16_8_35_55_798.jpg',
          'main': false,
          'id': 347
        }],
        'reviews': [{
          'rating': 5,
          'authorName': 'Gość serwisu x-kom',
          'date': '2013-11-06T14:51:00.000Z',
          'content': 'dobry procesor !',
          'id': 847
        }, {
          'rating': 6,
          'authorName': 'Gość serwisu x-kom',
          'date': '2013-11-09T22:02:00.000Z',
          'content': 'Polecam ten procesor każdemu do gier i programow.',
          'id': 848
        }, {
          'rating': 6,
          'authorName': 'Gość serwisu x-kom',
          'date': '2013-11-25T12:59:00.000Z',
          'content': 'procesor jest bardzo dobry, polecam :)',
          'id': 849
        }, {
          'rating': 6,
          'authorName': 'Gość serwisu x-kom',
          'date': '2014-01-08T18:22:00.000Z',
          'content': 'Ogólnie tak, chwilkę temu przesiadłem sie z PhenomII x4 965\ni różnica w wydajności jest taka że przy aplikacjach jednojajowych wypadają podobnie z minimalna korzyścią na stronę x4 965. Natomiast należy pamiętać ze fx-6300 ma dwa rdzenie więcej i ze 965 obsługuje pamięci 1333 na niektórych płytach oc 1600 natomiast  FX śmiga na 1866 +OC do 2400Mhy należy mieć tylko odpowiednie moduły.\nKolejnym aspektem są temperatury przy phenomie podkręconym na 4GHz miałem 27-30 stopni w spoczynku tu natomiast na stokowych zegarach mam 8 stopni sam w to nie wierze. Procesor jest chłodzony tym samym coolerem co jego poprzednik SilentiumPC Fortis HE1225 którego cena oscyluje w okolicach 120 złotych, wart każdej złotówki.\nNie podkręcałem jeszcze ale sądząc po temperaturach będzie szatan ;) Na ten moment uważam ze ten procek jest wart ~400zł które kosztuje. Jak podkręcę i protestuje dopiszę coś więcej.',
          'id': 850
        }, {
          'rating': 6,
          'authorName': 'Mynio',
          'date': '2014-06-20T18:27:00.000Z',
          'content': 'Świetny procesor... bez problemu można grać w Battlefield 4 na ustawieniach ultra z technologią AMD Mantle mając 100+fps. Aplikacje takie jak Photohop czy Sony Vegas Pro funkcjonują bezproblemowo, a to wszystko na stockowych zegarach. Teraz zamierzam kupić chłodzenie wodne i podkręcić tą bestię! Zdecydowanie polecam!',
          'id': 851
        }, {
          'rating': 6,
          'authorName': 'miszal5',
          'date': '2014-07-06T16:33:00.000Z',
          'content': 'dobry procesor, można grać w watchdogs i inne gry prawie na maxa.Tylko trzeba zmienić te chłodzenie boxowe w zestawie.Tyle lat od 2000 roku a AMD I INTEL używają takie same.ŚMIESZNE',
          'id': 852
        }, {
          'rating': 6,
          'authorName': 'Turok93',
          'date': '2014-07-16T02:21:00.000Z',
          'content': 'Procek zamierzam kupić mam tylko pytanko do znawców.jaką płyte i karte dobrą dobrać żeby móc popykać w world of tanks i company of heroes 2 w 1280x1024 medium/ultra min 30 fps .mój budżet na płyte to 200 - 220 zł i karta myśle do 350 - max 400 zł.myślałem o gigabyte am3 ga 970 a ud3 i karcie gigabyte gtx 650 lub 550 ti .prosze o porady',
          'id': 853
        }, {
          'rating': 6,
          'authorName': 'Turok93',
          'date': '2014-07-16T02:21:00.000Z',
          'content': 'Procek zamierzam kupić mam tylko pytanko do znawców.jaką płyte i karte dobrą dobrać żeby móc popykać w world of tanks i company of heroes 2 w 1280x1024 medium/ultra min 30 fps .mój budżet na płyte to 200 - 220 zł i karta myśle do 350 - max 400 zł.myślałem o gigabyte am3 ga 970 a ud3 i karcie gigabyte gtx 650 lub 550 ti .prosze o porady',
          'id': 854
        }, {
          'rating': 0,
          'authorName': 'x-ko',
          'date': '2014-07-16T08:29:00.000Z',
          'content': 'Z kart graficznych do 400 zł polecamy Ci ASUS GeForce GTX750 1024MB, a z płyt głównych do 220 zł Gigabyte GA-970A-DS3P. Jeśli potrzebujesz dodatkowych informacji lub bardziej szczegółowej pomocy, prosimy o wiadomość pod adresem kontakt@x-kom.pl',
          'id': 855
        }, {
          'rating': 6,
          'authorName': 'Marcin',
          'date': '2014-11-14T14:34:00.000Z',
          'content': 'Bardzo wydajny procesor o rewelacyjnym stosunku wydajności do ceny. Przy tym ma bardzo umiarkowany apetyt na energie. W trybie energooszczędnym (1,4GHz) na płycie Asus M5A97 EVO R2.0 pobiera kilka watów. Przy maksymalnym podkręceniu (FX-6300@4,9GHz) cały komputer z grafiką Asus Radeon R7 260X w 3DMark11 w szczycie nie przekracza 240W (b sprawny zasilacz)\nDo gier lepiej sprawdza się z grafiką GeForce. Jeśli chcemy dobrą płytę główną którą można nabyć poniżej 200zł warto zwrócić uwagę na Asusa M5A78L-M/USB3.',
          'id': 856
        }, {
          'rating': 4,
          'authorName': 'Adrian Łęgowski',
          'date': '2014-11-22T07:16:00.000Z',
          'content': 'Procesor bardzo fajny. Przesiadce z C2D e8200 skok wydajności na jednym rdzeniu zerowy(duże OC), ale podczas obliczeń mogę spokojnie rozsiąść się na 6 wątkach, co daje ogromnego kopa. Standardowe chłodzenie to dramat dla uszu. Mały zestaw wprawdzie daje radę, ale jego brzmienie jest bardzo nieprzyjemne.',
          'id': 857
        }, {
          'rating': 6,
          'authorName': 'Łukasz',
          'date': '2014-12-03T17:44:00.000Z',
          'content': 'Super wybór za te pieniądze. Przesiadłem się z 4-rdzeniowego Athlon 640 i skok wydajnościowy jest odczuwalny. Wg benchmarków wydajność do 40% wyższa. Procesor zaskakująco chłodny. Wprawdzie nie używam dołączonego chłodzenia, tylko Arctic Freezer, ale na boxowym coolerze na pewno też nie jest źle. Wg mnie nie ma sensu dopłacać do Intela. Najtańszy 4-rdzeniowy i5 kosztuje prawie dwa razy więcej, a wg benchmarków jest ok. 50% wydajniejszy.\nPolecam, a sklepowi dziękuję za ekspresową wysyłkę!',
          'id': 858
        }, {
          'rating': 4,
          'authorName': 'Patryk S',
          'date': '2015-03-07T07:50:00.000Z',
          'content': 'Witam jestem zwolennikiem  AMD  aktualnie używam procesora od AMD akurat właśnie tego modelu procesor wart tej ceny .  Jestem osobą która ceni sobie wydajność  lecz na tym procesorze ciężko powiedzieć iż by miał zapewniać super wydajność w najnowszych grach . Używam procesora około 3 miesięcy jest podkrecony do 4.5GHz na płycie GIGABYTE  990XA-UD3  i pracuje z karta graficzna  GTX970  G1  GAMINNG  4GB  GIGABYTE  niestety do wczoraj ponieważ kupiłem i7 4790K  ponieważ miałem straszne spadki na AMD. Ogólnie procesor jest dobry wiec osoby które zamierzają kupić ten model niech się pożądanie zastanowią dlatego,  ze nadaje się on tylko do kart graficznych  pokroju  GTX 750 1GB lub RADEON R9 270 powyżej tych kart robi się wąskim  gardłem nie jest w stanie utrzymywać klatek nawet powyżej 30 fps  z wydajna grafika nawet są spadki do 10 FPS w grze np CRYSIS  3 . Procek dobry jak za te pieniądze lecz naprawdę tylko dla średniego niższego segmentu grafik i dla graczy  którzy raczej żadko grają w gry na ultra i w rozdzielczości  Full HD .',
          'id': 859
        }, {
          'rating': 6,
          'authorName': 'szyy',
          'date': '2015-05-14T11:46:00.000Z',
          'content': 'Świetny procesor mam chłodnie wodne Cooler Master 120mm i spokojnie wyciągam 4,5 ghz przy minimalnej prędkości wentylatorów temp w gtaV i bf4 nieprzekraczań 68 stopni. Nie próbuje bardziej podkręcić ponieważ używam zbyt słabej płyty asus m5a97 r2.0 ma jedynie faze zasilającą 4+2',
          'id': 860
        }, {
          'rating': 6,
          'authorName': 'Kleryk',
          'date': '2015-06-09T19:09:00.000Z',
          'content': 'Procesor jak na swoją cenę jest wydajny. Niestety jak każdemu procesorowi z serii FX ujmuję gwiazdkę za pobór mocy a co za tym idzie ilość wydzielanego ciepła. Niestety boxowe chłodzenie kiepsko z tymi dwuma minusami sobie radzi przez co jest bardzo głośne.',
          'id': 861
        }, {
          'rating': 6,
          'authorName': 'jm',
          'date': '2015-10-21T19:25:00.000Z',
          'content': 'rewelacja po przesiadce z Athlon II x3 460 mega moc!!! polecam procesor i sklep',
          'id': 862
        }, {
          'rating': 6,
          'authorName': 'Rysiek K.',
          'date': '2015-10-27T00:28:00.000Z',
          'content': 'Procesor godny polecenia. Dobrze się podkręca, choć robiłem to tylko na próbę, nie lubię męczyć sprzętu :).\nJeśli chodzi o gry to za tą cenę nie ma lepszego. Polecam.',
          'id': 863
        }, {
          'rating': 3,
          'authorName': 'Joe',
          'date': '2015-11-20T22:32:00.000Z',
          'content': 'Cena do papierowych parametrów 5/5, Parametry na papierze do osiągów w grach 2/5. Chcesz grać to tylko Intel!!! chcesz cos obrabiac- wielozadaniowość to procek OK- daje rade. Ogólnie 3/5',
          'id': 864
        }, {
          'rating': 6,
          'authorName': 'Esseyer',
          'date': '2016-01-23T23:41:00.000Z',
          'content': 'Kapitalny procesor! Różnica ogromna po przesiadce z phenoma 955.',
          'id': 865
        }, {
          'rating': 5,
          'authorName': 'pawjan111',
          'date': '2016-05-20T12:12:00.000Z',
          'content': 'Procek ok ale macie go w złym dziale to nie buldożer tylko Vishiera',
          'id': 866
        }, {
          'rating': 5,
          'authorName': 'Micro',
          'date': '2016-08-03T15:39:00.000Z',
          'content': 'Poza chłodzeniem boxowym ktore wymienilem - nie radzil sobie przy wyzszych temp. i glosno pracował procek jest genialny!  Stabilny, szybki, wydajny lekko sie kreci cena/jakosc super.',
          'id': 867
        }, {
          'rating': 6,
          'authorName': 'Lukol',
          'date': '2016-11-23T19:33:00.000Z',
          'content': 'Jak za ta kwotę, mega wydajny procek, niskie temperatury w stresie, na ten moment ok 50st, także wersja BOX z wentylatorem sprawdza się w 100%. Przy mniej wymagających grach na wysokich ustawieniach spokojnie 100 i więcej fps.',
          'id': 868
        }, {
          'rating': 6,
          'authorName': 'mimi',
          'date': '2017-01-11T13:59:00.000Z',
          'content': 'To już drugi FX-6300 (nowy PC). Poprzedni testowałem, bo zastanawiałem się nad 6350 (+0,1GHz), dlatego podkręciłem 6300 do 4,3GHz i czuć wzrost, ale nie adekwatny do +100zł za 6350-kę. Radiator box to porażka. Wystarczy najtańszy Silentum (nawet HE-1024) i w spoczynku 3,5GHz',
          'id': 869
        }, {
          'rating': 6,
          'authorName': 'Jacek ',
          'date': '2017-03-04T12:10:00.000Z',
          'content': 'Myślałem, że trzeba będzie kupić nowy komputer, ale na szczęście AMD FX-6300 rozwiązał problem. Procesor polecił znajomy, mówił, że jak do podstawowych zadań, internetu i multimedia to jest okej. Cieszę się, że mu zaufałem, niskim kosztem, a komputer jak nowy.',
          'id': 870
        }, {
          'rating': 5,
          'authorName': 'superman',
          'date': '2018-07-01T20:07:00.000Z',
          'content': 'W tej cenie i jeśli na podstawkę AM3+ nic innego nie polecam. Łatwe OC do 3.5-3.8GHz. Na lepszej płycie lekko 4.0-4.3. Na wodnym chlodzeniu tak jak ja miałem plus zmiana vcore +  lekko 4.5 - 4.7GHz.\nJa osiągałem 5.0Ghz zdjecia też mam :) Procek Jesli cean i możliwości 5-',
          'id': 871
        }, {
          'rating': 6,
          'authorName': 'Alek',
          'date': '2018-07-16T15:12:00.000Z',
          'content': 'Procek w sam raz do grania w CS. Chłodzenie boksowe jest dobre nawet przy OC, socket AM3+ ma też na rynku jeszcze dużo możliwości.Sam procek można śmiało przyrównać do pentium G4560. Polecam.',
          'id': 872
        }, {
          'rating': 6,
          'authorName': 'WWW',
          'date': '2018-08-23T14:18:00.000Z',
          'content': 'spoko procek, wielozadaniowy i wysoko reaktywny, 36 miesięcy gwarancji od SteelSeries rewelka, użytkuję od pół roku, nie mam uwag',
          'id': 873
        }],
        'attributes': [{
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}]
        }, {'name': 'Liczba wątków', 'filterable': true, 'id': 50, 'options': [{'name': '6 wątków', 'id': 478}]}, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Wersja BOX (wentylator w zestawie)', 'id': 493}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR3-1600 (PC3-12800)', 'id': 541}, {
            'name': 'DDR3-1333 (PC3-10600)',
            'id': 542
          }, {'name': 'DDR3-1866 (PC3-15000)', 'id': 571}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '8 MB', 'id': 555}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'AMD FX', 'id': 567}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket AM3+', 'id': 569}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '32 nm', 'id': 572}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'AMD Turbo Core', 'id': 573}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'FX-6300', 'id': 590}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '3.5 GHz (4.1 GHz w trybie turbo)', 'id': 591}]
        }]
      }
    ];
    /* tslint:enable:max-line-length */

    const categories = [
      {
        'name': 'Smartphones',
        'id': 1,
        'attributes': [{
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{
            'name': 'Samsung Exynos 9810 (4 rdzenie, 2.7 GHz + 4 rdzenie, 1.7 GHz)',
            'id': 1
          }, {
            'name': 'Qualcomm Snapdragon 425 (4 rdzenie, 1.40 GHz, Cortex A53)',
            'id': 58
          }, {
            'name': 'Qualcomm Snapdragon 450 (8 rdzeni, 1.80 GHz, A53)',
            'id': 78
          }, {
            'name': 'Qualcomm Snapdragon 636 (4 rdzenie, 1,80 GHz, Kryo + 4 rdzenie, 1.60 GHz, Kryo)',
            'id': 93
          }, {
            'name': 'Qualcomm Snapdragon 625 (8 rdzeni, 2.0 GHz, Cortex A53)',
            'id': 120
          }, {
            'name': 'Qualcomm Snapdragon 430 (4 rdzenie, 1.40 GHz, A53 + 4 rdzenie, 1.1 GHz, A53)',
            'id': 136
          }, {
            'name': 'Qualcomm Snapdragon 821 (2 rdzenie, 2.35 GHz, Kryo + 2 rdzenie, 1.6 GHz, Kryo)',
            'id': 147
          }, {
            'name': 'Samsung Exynos 8890 (4 rdzenie, 2.3 GHz, ARMv8 + 4 rdzenie, 1.6 GHz, A53)',
            'id': 167
          }, {
            'name': 'MediaTek MT6750 (4 rdzenie, 1.5 GHz, Cortex A53+4 rdzenie, 1.0 GHz, Cortex A53)',
            'id': 177
          }, {
            'name': 'Samsung Exynos 7570 (4 rdzenie, 1.40 GHz, A53)',
            'id': 184
          }, {
            'name': 'Qualcomm Snapdragon 630 (8 rdzeni, 2.20 GHz, Cortex A53)',
            'id': 193
          }, {
            'name': 'Samsung Exynos 7870 (8 rdzeni, 1.60 GHz, A53)',
            'id': 199
          }, {
            'name': 'Samsung Exynos 8895 (4 rdzenie, 2.3 GHz + 4 rdzenie, 1.7 GHz)',
            'id': 209
          }, {'name': 'HiSilicon Kirin 659 (4 rdzenie, 2.36 GHz, A53 + 4 rdzenie, 1.70 GHz, A53)', 'id': 215}]
        }, {
          'name': 'Układ graficzny',
          'filterable': false,
          'id': 2,
          'options': [{'name': 'Mali-G72 MP18', 'id': 2}, {'name': 'Adreno 308', 'id': 59}, {
            'name': 'Adreno 506',
            'id': 79
          }, {'name': 'Adreno 509', 'id': 94}, {'name': 'Adreno 505', 'id': 137}, {
            'name': 'Adreno 530',
            'id': 148
          }, {'name': 'Mali-T880 MP12', 'id': 168}, {'name': 'Mali-T860', 'id': 178}, {
            'name': 'Mali-T720',
            'id': 185
          }, {'name': 'Adreno 508', 'id': 194}, {'name': 'Mali-T830 MP2', 'id': 200}, {'name': 'Mali-G71 MP20', 'id': 210}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '6 GB', 'id': 3}, {'name': '2 GB', 'id': 60}, {'name': '3 GB', 'id': 80}, {'name': '4 GB', 'id': 95}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}, {'name': '16 GB', 'id': 61}, {'name': '32 GB', 'id': 81}, {'name': '64 GB', 'id': 96}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}, {'name': 'Dotykowy, IPS', 'id': 62}, {
            'name': 'Dotykowy, TFT LCD',
            'id': 186
          }]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '6,4"', 'id': 6}, {'name': '5"', 'id': 63}, {'name': '5,7"', 'id': 82}, {
            'name': '6"',
            'id': 97
          }, {'name': '5,99"', 'id': 121}, {'name': '5,84"', 'id': 127}, {'name': '5,2"', 'id': 163}, {
            'name': '5,1"',
            'id': 169
          }, {'name': '5,5"', 'id': 179}, {'name': '5,8"', 'id': 211}, {'name': '5,93"', 'id': 216}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2960 x 1440', 'id': 7}, {'name': '1280 x 720', 'id': 64}, {
            'name': '1440 x 720',
            'id': 83
          }, {'name': '2160 x 1080', 'id': 98}, {'name': '2280 x 1080', 'id': 128}, {
            'name': '1920 x 1080',
            'id': 138
          }, {'name': '2880 x 1440', 'id': 149}, {'name': '2560 x 1440', 'id': 170}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {'name': 'NFC', 'id': 11}]
        }, {
          'name': 'System nawigacji satelitarnej',
          'filterable': false,
          'id': 9,
          'options': [{'name': 'GPS', 'id': 12}, {'name': 'A-GPS, GLONASS', 'id': 13}, {'name': 'Beidou', 'id': 84}]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {
            'name': 'Micro USB - 1 szt.',
            'id': 65
          }, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}, {
            'name': 'Gniazdo kart nanoSIM - 1 szt.',
            'id': 150
          }, {'name': 'Gniazdo kart microSIM - 1 szt.', 'id': 187}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 4000 mAh', 'id': 18}, {
            'name': 'Litowo-jonowa 3020 mAh',
            'id': 66
          }, {'name': 'Litowo-jonowa 3300 mAh', 'id': 85}, {
            'name': 'Litowo-jonowa 3000 mAh',
            'id': 99
          }, {'name': 'Litowo-polimerowa 3000 mAh', 'id': 113}, {
            'name': 'Litowo-polimerowa 4000 mAh',
            'id': 129
          }, {'name': 'Litowo-jonowa 2800 mAh', 'id': 139}, {
            'name': 'Litowo-polimerowa 3300 mAh',
            'id': 151
          }, {'name': 'Litowo-polimerowa 4500 mAh', 'id': 180}, {
            'name': 'Litowo-jonowa 3600 mAh',
            'id': 201
          }, {'name': 'Litowo-polimerowa 3340 mAh', 'id': 217}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 8.1 Oreo', 'id': 19}, {'name': 'Android 7.0 Nougat', 'id': 67}, {
            'name': 'Android 7.1 Nougat',
            'id': 86
          }, {'name': 'Android 6.0 Marshmallow', 'id': 171}]
        }, {
          'name': 'Rozdzielczość aparatu',
          'filterable': false,
          'id': 13,
          'options': [{'name': '12.0 Mpix - tył (podwójny)', 'id': 20}, {'name': '8.0 Mpix - przód', 'id': 21}, {
            'name': '13.0 Mpix - tył',
            'id': 68
          }, {'name': '5.0 Mpix - przód', 'id': 69}, {'name': '12.0 Mpix - tył', 'id': 87}, {
            'name': '12.0 + 5.0 Mpix - tył',
            'id': 100
          }, {'name': '13.0 Mpix - tył (podwójny)', 'id': 152}, {'name': '16.0 Mpix - tył', 'id': 164}, {
            'name': '13.0 Mpix - przód',
            'id': 202
          }, {'name': '16.0 + 2.0 Mpix - tył', 'id': 218}]
        }, {
          'name': 'Przysłona obiektywu',
          'filterable': false,
          'id': 14,
          'options': [{'name': 'f/1.7 - przedni obiektyw', 'id': 22}, {
            'name': 'f/1.5 - tylny obiektyw',
            'id': 23
          }, {'name': 'f/2.4 - tylny teleobiektyw', 'id': 24}, {
            'name': 'f/2.2 - tylny obiektyw',
            'id': 88
          }, {'name': 'f/2.0 - przedni obiektyw', 'id': 101}, {
            'name': 'f/1.7 - tylny obiektyw',
            'id': 102
          }, {'name': 'f/2.2 - przedni obiektyw', 'id': 140}, {
            'name': 'f/2.0 - tylny obiektyw',
            'id': 141
          }, {'name': 'f/2.2 - przedni obiektyw szerokokątny', 'id': 153}, {
            'name': 'f/1.8 - tylny obiektyw',
            'id': 154
          }, {'name': 'f/2.4 - tylny obiektyw szerokokątny', 'id': 155}, {
            'name': 'f/1.9 - tylny obiektyw',
            'id': 188
          }, {'name': 'f/1.9 - przedni obiektyw', 'id': 203}]
        }, {
          'name': 'Lampa błyskowa',
          'filterable': false,
          'id': 15,
          'options': [{'name': 'Wbudowana', 'id': 25}]
        }, {
          'name': 'Rozdzielczość nagrywania wideo',
          'filterable': false,
          'id': 16,
          'options': [{'name': '4K (do 60 kl./s)', 'id': 26}, {'name': 'FullHD 1080p', 'id': 70}, {'name': '4K', 'id': 103}]
        }, {
          'name': 'Dual SIM',
          'filterable': false,
          'id': 17,
          'options': [{
            'name': 'Dual SIM Active LTE - Obsługa dwóch kart SIM w technologii aktywny Dual SIM',
            'id': 27
          }, {'name': 'Dual SIM - Obsługa dwóch kart SIM', 'id': 71}, {'name': 'Nie', 'id': 156}]
        }, {
          'name': 'Grubość',
          'filterable': false,
          'id': 18,
          'options': [{'name': '8,8 mm', 'id': 28}, {'name': '8,1 mm', 'id': 72}, {'name': '7,7 mm', 'id': 89}, {
            'name': '6,7 mm',
            'id': 104
          }, {'name': '8,4 mm', 'id': 114}, {'name': '8,05 mm', 'id': 122}, {'name': '8,75 mm', 'id': 130}, {
            'name': '9,5 mm',
            'id': 142
          }, {'name': '7,9 mm', 'id': 157}, {'name': '6,9 mm', 'id': 172}, {'name': '9,7 mm', 'id': 189}, {
            'name': '8,6 mm',
            'id': 195
          }, {'name': '8,0 mm', 'id': 204}, {'name': '7,6 mm', 'id': 219}]
        }, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '76,3 mm', 'id': 29}, {'name': '71 mm', 'id': 73}, {'name': '72,8 mm', 'id': 90}, {
            'name': '76,5 mm',
            'id': 105
          }, {'name': '70,1 mm', 'id': 115}, {'name': '74,45 mm', 'id': 123}, {'name': '71,68 mm', 'id': 131}, {
            'name': '73 mm',
            'id': 143
          }, {'name': '71,9 mm', 'id': 158}, {'name': '73,6 mm', 'id': 165}, {'name': '70,8 mm', 'id': 173}, {
            'name': '78,4 mm',
            'id': 181
          }, {'name': '73,3 mm', 'id': 190}, {'name': '75,8 mm', 'id': 196}, {'name': '74,8 mm', 'id': 205}, {
            'name': '68,2 mm',
            'id': 212
          }, {'name': '75,3 mm', 'id': 220}]
        }, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '161,9 mm', 'id': 30}, {'name': '143,5 mm', 'id': 74}, {'name': '151,8 mm', 'id': 91}, {
            'name': '156,5 mm',
            'id': 106
          }, {'name': '140 mm', 'id': 116}, {'name': '158,5 mm', 'id': 124}, {'name': '149,33 mm', 'id': 132}, {
            'name': '144,4 mm',
            'id': 144
          }, {'name': '149 mm', 'id': 159}, {'name': '150 mm', 'id': 166}, {'name': '155 mm', 'id': 182}, {
            'name': '146,3 mm',
            'id': 191
          }, {'name': '148,8 mm', 'id': 197}, {'name': '152,5 mm', 'id': 206}]
        }, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '204 g', 'id': 31}, {'name': '145 g', 'id': 75}, {'name': '158 g', 'id': 92}, {
            'name': '156 g',
            'id': 107
          }, {'name': '137 g', 'id': 117}, {'name': '180 g', 'id': 125}, {'name': '175 g', 'id': 133}, {
            'name': '153 g',
            'id': 145
          }, {'name': '163 g', 'id': 160}, {'name': '165 g', 'id': 183}, {'name': '172 g', 'id': 192}, {
            'name': '181 g',
            'id': 207
          }, {'name': '152 g', 'id': 213}]
        }, {
          'name': 'Kolor',
          'filterable': false,
          'id': 22,
          'options': [{'name': 'Czarny - Midnight Black', 'id': 32}, {'name': 'Czarny', 'id': 76}, {
            'name': 'Szary',
            'id': 146
          }, {'name': 'Srebrny', 'id': 161}, {'name': 'Niebieski', 'id': 198}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Obudowa wykonana ze szkła i aluminium', 'id': 33}, {
            'name': 'Funkcja ładowania bezprzewodowego',
            'id': 34
          }, {'name': 'Pyłoszczelność i wodoszczelność (IP68)', 'id': 35}, {'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Żyroskop',
            'id': 37
          }, {'name': 'Magnetometr', 'id': 38}, {'name': 'Barometr', 'id': 39}, {'name': 'Pulsometr', 'id': 40}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Czujnik Halla', 'id': 43}, {
            'name': 'Wbudowane głośniki stereo',
            'id': 44
          }, {'name': 'Czytnik linii papilarnych', 'id': 45}, {'name': 'Skaner twarzy', 'id': 46}, {
            'name': 'Skaner tęczówki oka',
            'id': 47
          }, {'name': 'Funkcja szybkiego ładowania Quick Charge', 'id': 48}, {
            'name': 'USB OTG',
            'id': 49
          }, {'name': 'Szkło Corning Gorilla Glass 3', 'id': 108}, {'name': 'Czujnik podczerwieni', 'id': 118}, {
            'name': 'Obsługa radia FM',
            'id': 119
          }, {'name': 'Metalowa obudowa', 'id': 134}, {
            'name': 'Odporność na wstrząsy i upadki',
            'id': 162
          }, {'name': 'Szkło Corning Gorilla Glass 4', 'id': 174}, {
            'name': 'Funkcja latarki',
            'id': 175
          }, {'name': 'Szkło Corning Gorilla Glass 5', 'id': 214}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel USB typ C', 'id': 51}, {
            'name': 'Przejściówka USB-C -&gt; micro USB',
            'id': 52
          }, {'name': 'Przejściówka USB-C -&gt; USB', 'id': 53}, {'name': 'Słuchawki', 'id': 54}, {
            'name': 'Rysik',
            'id': 55
          }, {'name': 'Instrukcja szybkiego uruchomienia telefonu', 'id': 56}, {
            'name': 'Kabel microUSB',
            'id': 77
          }, {'name': 'Ładowarka TurboPower™ 18 W', 'id': 109}, {
            'name': 'Przejściówka USB-C -&gt; Jack 3.5 mm',
            'id': 110
          }, {'name': 'Moto Power Pack', 'id': 111}, {'name': 'Silikonowe plecki', 'id': 135}, {
            'name': 'Kabel USB',
            'id': 176
          }, {'name': 'Zasilacz', 'id': 208}, {'name': 'Etui', 'id': 221}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}, {'name': '24 miesiące (gwarancja sprzedawcy)', 'id': 126}]
        }]
      }, {
        'name': 'Notebooks',
        'id': 2,
        'attributes': [{
          'name': 'Procesor',
          'filterable': false,
          'id': 1,
          'options': [{
            'name': 'Samsung Exynos 9810 (4 rdzenie, 2.7 GHz + 4 rdzenie, 1.7 GHz)',
            'id': 1
          }, {
            'name': 'Qualcomm Snapdragon 425 (4 rdzenie, 1.40 GHz, Cortex A53)',
            'id': 58
          }, {
            'name': 'Qualcomm Snapdragon 450 (8 rdzeni, 1.80 GHz, A53)',
            'id': 78
          }, {
            'name': 'Qualcomm Snapdragon 636 (4 rdzenie, 1,80 GHz, Kryo + 4 rdzenie, 1.60 GHz, Kryo)',
            'id': 93
          }, {
            'name': 'Qualcomm Snapdragon 625 (8 rdzeni, 2.0 GHz, Cortex A53)',
            'id': 120
          }, {
            'name': 'Qualcomm Snapdragon 430 (4 rdzenie, 1.40 GHz, A53 + 4 rdzenie, 1.1 GHz, A53)',
            'id': 136
          }, {
            'name': 'Qualcomm Snapdragon 821 (2 rdzenie, 2.35 GHz, Kryo + 2 rdzenie, 1.6 GHz, Kryo)',
            'id': 147
          }, {
            'name': 'Samsung Exynos 8890 (4 rdzenie, 2.3 GHz, ARMv8 + 4 rdzenie, 1.6 GHz, A53)',
            'id': 167
          }, {
            'name': 'MediaTek MT6750 (4 rdzenie, 1.5 GHz, Cortex A53+4 rdzenie, 1.0 GHz, Cortex A53)',
            'id': 177
          }, {
            'name': 'Samsung Exynos 7570 (4 rdzenie, 1.40 GHz, A53)',
            'id': 184
          }, {
            'name': 'Qualcomm Snapdragon 630 (8 rdzeni, 2.20 GHz, Cortex A53)',
            'id': 193
          }, {
            'name': 'Samsung Exynos 7870 (8 rdzeni, 1.60 GHz, A53)',
            'id': 199
          }, {
            'name': 'Samsung Exynos 8895 (4 rdzenie, 2.3 GHz + 4 rdzenie, 1.7 GHz)',
            'id': 209
          }, {
            'name': 'HiSilicon Kirin 659 (4 rdzenie, 2.36 GHz, A53 + 4 rdzenie, 1.70 GHz, A53)',
            'id': 215
          }, {
            'name': 'Intel Core i7-8750H (6 rdzeni, od 2.20 GHz do 4.10 GHz, 9 MB cache)',
            'id': 222
          }, {
            'name': 'Intel Core i5 (2 rdzenie, od 1.8 GHz do 2.9 GHz, 3 MB cache)',
            'id': 264
          }, {
            'name': 'Intel Core i3-7100U (2 rdzenie, 2.40 GHz, 3 MB cache)',
            'id': 283
          }, {
            'name': 'Intel Core i5-8250U (4 rdzenie, od 1.6 GHz do 3.4 GHz, 6MB cache)',
            'id': 294
          }, {
            'name': 'Intel Core i3-6006U (2 rdzenie, 2.00 GHz, 3 MB cache)',
            'id': 310
          }, {
            'name': 'Intel Core i5-7300HQ (4 rdzenie, od 2.5 GHz do 3.5 GHz, 6MB cache)',
            'id': 334
          }, {
            'name': 'Intel Core i3-8130U (2 rdzenie, od 2.2 GHz do 3.4 GHz, 4MB cache)',
            'id': 346
          }, {
            'name': 'Intel Core i7-7700HQ (4 rdzenie, od 2.80 GHz do 3.80 GHz, 6 MB cache)',
            'id': 356
          }, {
            'name': 'Intel Core i5-7200U (2 rdzenie, od 2.5 GHz do 3.1 GHz, 3MB cache)',
            'id': 401
          }, {
            'name': 'Intel Core i5-7300U (2 rdzenie, od 2.6 GHz do 3.5 GHz, 3MB cache)',
            'id': 407
          }, {
            'name': 'Intel Core i3-7130U (2 rdzenie, 2.70 GHz, 3 MB cache)',
            'id': 419
          }, {
            'name': 'Intel Core i7-8550U (4 rdzenie, od 1.80 GHz do 4.00 GHz, 8 MB cache)',
            'id': 451
          }, {'name': 'Intel Core i5 (2 rdzenie, od 2.3 GHz do 3.6 GHz, 4 MB cache)', 'id': 460}]
        }, {
          'name': 'Pamięć RAM',
          'filterable': true,
          'id': 3,
          'options': [{'name': '6 GB', 'id': 3}, {'name': '2 GB', 'id': 60}, {'name': '3 GB', 'id': 80}, {
            'name': '4 GB',
            'id': 95
          }, {'name': '16 GB (SO-DIMM DDR4, 2666MHz)', 'id': 223}, {
            'name': '8 GB (SO-DIMM DDR3, 1600 MHz)',
            'id': 265
          }, {'name': '4 GB (SO-DIMM DDR4, 2133MHz)', 'id': 284}, {
            'name': '8 GB (SO-DIMM DDR3, 1866 MHz)',
            'id': 295
          }, {'name': '8 GB (SO-DIMM DDR4, 2133MHz)', 'id': 311}, {
            'name': '8 GB (SO-DIMM DDR4, 2400MHz)',
            'id': 324
          }, {'name': '8 GB (SO-DIMM DDR4, 2666MHz)', 'id': 336}, {
            'name': '16 GB (SO-DIMM DDR4, 2133MHz)',
            'id': 357
          }, {'name': '16 GB (SO-DIMM DDR3, 1866 MHz)', 'id': 452}, {'name': '8 GB (SO-DIMM DDR3, 2133 MHz)', 'id': 461}]
        }, {
          'name': 'Pamięć wbudowana',
          'filterable': true,
          'id': 4,
          'options': [{'name': '128 GB', 'id': 4}, {'name': '16 GB', 'id': 61}, {'name': '32 GB', 'id': 81}, {'name': '64 GB', 'id': 96}]
        }, {
          'name': 'Typ ekranu',
          'filterable': false,
          'id': 5,
          'options': [{'name': 'Dotykowy, Super AMOLED', 'id': 5}, {'name': 'Dotykowy, IPS', 'id': 62}, {
            'name': 'Dotykowy, TFT LCD',
            'id': 186
          }, {'name': 'Matowy, LED, IPS', 'id': 228}, {'name': 'Błyszczący, LED', 'id': 267}, {
            'name': 'Błyszczący, LED, IPS',
            'id': 298
          }, {'name': 'Matowy, LED', 'id': 314}, {'name': 'Matowy, LED, EWV', 'id': 364}, {
            'name': 'Błyszczący, LED, IPS, dotykowy',
            'id': 377
          }, {'name': 'Błyszczący, LED, IPS, Retina', 'id': 462}]
        }, {
          'name': 'Przekątna ekranu',
          'filterable': false,
          'id': 6,
          'options': [{'name': '6,4"', 'id': 6}, {'name': '5"', 'id': 63}, {'name': '5,7"', 'id': 82}, {
            'name': '6"',
            'id': 97
          }, {'name': '5,99"', 'id': 121}, {'name': '5,84"', 'id': 127}, {'name': '5,2"', 'id': 163}, {
            'name': '5,1"',
            'id': 169
          }, {'name': '5,5"', 'id': 179}, {'name': '5,8"', 'id': 211}, {'name': '5,93"', 'id': 216}, {
            'name': '15,6"',
            'id': 229
          }, {'name': '13,3"', 'id': 268}, {'name': '14,0"', 'id': 365}, {'name': '12,3"', 'id': 408}]
        }, {
          'name': 'Rozdzielczość ekranu',
          'filterable': true,
          'id': 7,
          'options': [{'name': '2960 x 1440', 'id': 7}, {'name': '1280 x 720', 'id': 64}, {
            'name': '1440 x 720',
            'id': 83
          }, {'name': '2160 x 1080', 'id': 98}, {'name': '2280 x 1080', 'id': 128}, {
            'name': '1920 x 1080',
            'id': 138
          }, {'name': '2880 x 1440', 'id': 149}, {'name': '2560 x 1440', 'id': 170}, {
            'name': '1920 x 1080 (FullHD)',
            'id': 230
          }, {'name': '1440 x 900 (WXGA+)', 'id': 269}, {'name': '2736 x 1824', 'id': 409}, {'name': '2560 x 1600 (WQXGA)', 'id': 463}]
        }, {
          'name': 'Łączność',
          'filterable': false,
          'id': 8,
          'options': [{'name': '4G (LTE)', 'id': 8}, {'name': 'Wi-Fi', 'id': 9}, {'name': 'Bluetooth', 'id': 10}, {
            'name': 'NFC',
            'id': 11
          }, {'name': 'LAN 10/100/1000 Mbps', 'id': 237}, {'name': 'Wi-Fi 802.11 a/b/g/n/ac', 'id': 238}, {
            'name': 'Moduł Bluetooth',
            'id': 239
          }, {'name': 'Wi-Fi 802.11 b/g/n/ac', 'id': 273}, {'name': 'LAN 10/100 Mbps', 'id': 395}, {
            'name': 'Wi-Fi 802.11 b/g/n',
            'id': 426
          }]
        }, {
          'name': 'Złącza',
          'filterable': false,
          'id': 10,
          'options': [{'name': 'USB Typu-C - 1 szt.', 'id': 14}, {
            'name': 'Czytnik kart pamięci - 1 szt.',
            'id': 15
          }, {
            'name': 'Gniazdo kart nanoSIM - 2 szt. (Drugi slot wspólny z czytnikiem kart pamięci)',
            'id': 16
          }, {'name': 'Wyjście słuchawkowe/głośnikowe - 1 szt.', 'id': 17}, {
            'name': 'Micro USB - 1 szt.',
            'id': 65
          }, {'name': 'Gniazdo kart nanoSIM - 2 szt.', 'id': 112}, {
            'name': 'Gniazdo kart nanoSIM - 1 szt.',
            'id': 150
          }, {'name': 'Gniazdo kart microSIM - 1 szt.', 'id': 187}]
        }, {
          'name': 'Bateria',
          'filterable': false,
          'id': 11,
          'options': [{'name': 'Litowo-jonowa 4000 mAh', 'id': 18}, {
            'name': 'Litowo-jonowa 3020 mAh',
            'id': 66
          }, {'name': 'Litowo-jonowa 3300 mAh', 'id': 85}, {
            'name': 'Litowo-jonowa 3000 mAh',
            'id': 99
          }, {'name': 'Litowo-polimerowa 3000 mAh', 'id': 113}, {
            'name': 'Litowo-polimerowa 4000 mAh',
            'id': 129
          }, {'name': 'Litowo-jonowa 2800 mAh', 'id': 139}, {
            'name': 'Litowo-polimerowa 3300 mAh',
            'id': 151
          }, {'name': 'Litowo-polimerowa 4500 mAh', 'id': 180}, {
            'name': 'Litowo-jonowa 3600 mAh',
            'id': 201
          }, {'name': 'Litowo-polimerowa 3340 mAh', 'id': 217}, {
            'name': '4-komorowa, 3500 mAh, Li-Ion',
            'id': 247
          }, {'name': '4-komorowa, 6793 mAh, Li-Ion', 'id': 303}, {
            'name': '4-komorowa, 2800 mAh, Li-Ion',
            'id': 317
          }, {'name': '3-komorowa, 3800 mAh, Li-Ion', 'id': 330}, {
            'name': '4-komorowa, 4550 mAh, Li-Ion',
            'id': 341
          }, {'name': '3-komorowa, 3727 mAh, Li-Ion', 'id': 348}, {
            'name': '4-komorowa, 3220 mAh, Li-Ion',
            'id': 358
          }, {'name': '3-komorowa, 3653 mAh, Li-Ion', 'id': 366}, {
            'name': '3-komorowa, 3615 mAh, Li-Ion',
            'id': 378
          }, {'name': '4-komorowa, 2750 mAh, Li-Ion', 'id': 384}, {
            'name': '3-komorowa, 3800 mAh, Li-Polymer',
            'id': 388
          }, {'name': '4-komorowa, 2700 mAh, Li-Ion', 'id': 396}, {
            'name': '2-komorowa, 4000 mAh, Li-Ion',
            'id': 398
          }, {'name': '3-komorowa, 3500 mAh, Li-Ion', 'id': 402}, {
            'name': '3-komorowa, 3166 mAh, Li-Ion',
            'id': 421
          }, {'name': '3-komorowa, 3350 mAh, Li-Ion', 'id': 427}, {
            'name': '3-komorowa, 4211 mAh, Li-Ion',
            'id': 436
          }, {'name': '3-komorowa, 3470 mAh, Li-Ion', 'id': 443}, {
            'name': '2-komorowa, 4940 mAh, Li-Polymer',
            'id': 447
          }, {'name': '3-komorowa, 4335 mAh, Li-Polymer', 'id': 455}]
        }, {
          'name': 'Zainstalowany system operacyjny',
          'filterable': true,
          'id': 12,
          'options': [{'name': 'Android 8.1 Oreo', 'id': 19}, {'name': 'Android 7.0 Nougat', 'id': 67}, {
            'name': 'Android 7.1 Nougat',
            'id': 86
          }, {'name': 'Android 6.0 Marshmallow', 'id': 171}, {
            'name': 'Microsoft Windows 10 Pro PL (wersja 64-bitowa)',
            'id': 248
          }, {'name': 'macOS Sierra', 'id': 276}, {'name': 'Microsoft Windows 10 Home PL (wersja 64-bitowa)', 'id': 289}]
        }, {
          'name': 'Szerokość',
          'filterable': false,
          'id': 19,
          'options': [{'name': '76,3 mm', 'id': 29}, {'name': '71 mm', 'id': 73}, {'name': '72,8 mm', 'id': 90}, {
            'name': '76,5 mm',
            'id': 105
          }, {'name': '70,1 mm', 'id': 115}, {'name': '74,45 mm', 'id': 123}, {'name': '71,68 mm', 'id': 131}, {
            'name': '73 mm',
            'id': 143
          }, {'name': '71,9 mm', 'id': 158}, {'name': '73,6 mm', 'id': 165}, {'name': '70,8 mm', 'id': 173}, {
            'name': '78,4 mm',
            'id': 181
          }, {'name': '73,3 mm', 'id': 190}, {'name': '75,8 mm', 'id': 196}, {'name': '74,8 mm', 'id': 205}, {
            'name': '68,2 mm',
            'id': 212
          }, {'name': '75,3 mm', 'id': 220}, {'name': '389 mm', 'id': 251}, {'name': '325 mm', 'id': 278}, {
            'name': '308 mm',
            'id': 291
          }, {'name': '305,4 mm', 'id': 305}, {'name': '380 mm', 'id': 319}, {'name': '358 mm', 'id': 331}, {
            'name': '361 mm',
            'id': 350
          }, {'name': '390 mm', 'id': 360}, {'name': '326 mm', 'id': 368}, {'name': '382 mm', 'id': 373}, {
            'name': '335 mm',
            'id': 380
          }, {'name': '359 mm', 'id': 391}, {'name': '378 mm', 'id': 400}, {'name': '292 mm', 'id': 413}, {
            'name': '324 mm',
            'id': 423
          }, {'name': '381 mm', 'id': 429}, {'name': '323 mm', 'id': 437}, {'name': '305 mm', 'id': 467}]
        }, {
          'name': 'Wysokość',
          'filterable': false,
          'id': 20,
          'options': [{'name': '161,9 mm', 'id': 30}, {'name': '143,5 mm', 'id': 74}, {'name': '151,8 mm', 'id': 91}, {
            'name': '156,5 mm',
            'id': 106
          }, {'name': '140 mm', 'id': 116}, {'name': '158,5 mm', 'id': 124}, {'name': '149,33 mm', 'id': 132}, {
            'name': '144,4 mm',
            'id': 144
          }, {'name': '149 mm', 'id': 159}, {'name': '150 mm', 'id': 166}, {'name': '155 mm', 'id': 182}, {
            'name': '146,3 mm',
            'id': 191
          }, {'name': '148,8 mm', 'id': 197}, {'name': '152,5 mm', 'id': 206}, {'name': '25 mm', 'id': 250}, {
            'name': 'Od 3 do 17 mm',
            'id': 277
          }, {'name': '16,9 mm', 'id': 290}, {'name': '13,9 mm', 'id': 304}, {'name': '24 mm', 'id': 318}, {
            'name': '24,8 mm',
            'id': 342
          }, {'name': '19,5 mm', 'id': 349}, {'name': '26,6 mm', 'id': 359}, {'name': '19 mm', 'id': 367}, {
            'name': '22 mm',
            'id': 372
          }, {'name': '19,9 mm', 'id': 379}, {'name': '23,5 mm', 'id': 385}, {'name': '22,9 mm', 'id': 399}, {
            'name': '19,2 mm',
            'id': 403
          }, {'name': '8,5 mm', 'id': 412}, {'name': '15,9 mm', 'id': 422}, {'name': '27,7 mm', 'id': 428}, {
            'name': '24,1 mm',
            'id': 433
          }, {'name': '23,2 mm', 'id': 448}, {'name': '14,9 mm', 'id': 466}]
        }, {
          'name': 'Waga',
          'filterable': false,
          'id': 21,
          'options': [{'name': '204 g', 'id': 31}, {'name': '145 g', 'id': 75}, {'name': '158 g', 'id': 92}, {
            'name': '156 g',
            'id': 107
          }, {'name': '137 g', 'id': 117}, {'name': '180 g', 'id': 125}, {'name': '175 g', 'id': 133}, {
            'name': '153 g',
            'id': 145
          }, {'name': '163 g', 'id': 160}, {'name': '165 g', 'id': 183}, {'name': '172 g', 'id': 192}, {
            'name': '181 g',
            'id': 207
          }, {'name': '152 g', 'id': 213}, {'name': '2,75 kg (z baterią)', 'id': 253}, {
            'name': '1,35 kg (z baterią)',
            'id': 280
          }, {'name': '1,20 kg (z baterią)', 'id': 293}, {'name': '1,32 kg (z baterią)', 'id': 307}, {
            'name': '2,10 kg (z baterią)',
            'id': 321
          }, {'name': '1,86 kg (z baterią)', 'id': 333}, {'name': '2,62 kg (z baterią)', 'id': 344}, {
            'name': '1,62 kg (z baterią)',
            'id': 352
          }, {'name': '1,55 kg (z baterią)', 'id': 354}, {'name': '2,70 kg (z baterią)', 'id': 362}, {
            'name': '1,43 kg (z baterią)',
            'id': 370
          }, {'name': '2,20 kg (z baterią)', 'id': 375}, {'name': '1,64 kg (z baterią)', 'id': 381}, {
            'name': '2,30 kg (z baterią)',
            'id': 387
          }, {'name': '1,74 kg (z baterią)', 'id': 389}, {'name': '1,90 kg (z baterią)', 'id': 393}, {
            'name': '2,22 kg (z baterią)',
            'id': 397
          }, {'name': '1,98 kg (z baterią)', 'id': 405}, {'name': '0,77 kg (z baterią)', 'id': 415}, {
            'name': '1,42 kg (z baterią)',
            'id': 425
          }, {'name': '2,00 kg (z baterią)', 'id': 431}, {'name': '2,26 kg (z baterią)', 'id': 434}, {
            'name': '1,56 kg (z baterią)',
            'id': 445
          }, {'name': '1,67 kg (z baterią)', 'id': 446}, {'name': '2,07 kg (z baterią)', 'id': 450}, {
            'name': '1,30 kg (z baterią)',
            'id': 457
          }, {'name': '1,38 kg (z baterią)', 'id': 469}]
        }, {
          'name': 'Dodatkowe informacje',
          'filterable': false,
          'id': 23,
          'options': [{'name': 'Obudowa wykonana ze szkła i aluminium', 'id': 33}, {
            'name': 'Funkcja ładowania bezprzewodowego',
            'id': 34
          }, {'name': 'Pyłoszczelność i wodoszczelność (IP68)', 'id': 35}, {'name': 'Akcelerometr', 'id': 36}, {
            'name': 'Żyroskop',
            'id': 37
          }, {'name': 'Magnetometr', 'id': 38}, {'name': 'Barometr', 'id': 39}, {'name': 'Pulsometr', 'id': 40}, {
            'name': 'Czujnik światła',
            'id': 41
          }, {'name': 'Czujnik zbliżenia', 'id': 42}, {'name': 'Czujnik Halla', 'id': 43}, {
            'name': 'Wbudowane głośniki stereo',
            'id': 44
          }, {'name': 'Czytnik linii papilarnych', 'id': 45}, {'name': 'Skaner twarzy', 'id': 46}, {
            'name': 'Skaner tęczówki oka',
            'id': 47
          }, {'name': 'Funkcja szybkiego ładowania Quick Charge', 'id': 48}, {
            'name': 'USB OTG',
            'id': 49
          }, {'name': 'Szkło Corning Gorilla Glass 3', 'id': 108}, {'name': 'Czujnik podczerwieni', 'id': 118}, {
            'name': 'Obsługa radia FM',
            'id': 119
          }, {'name': 'Metalowa obudowa', 'id': 134}, {
            'name': 'Odporność na wstrząsy i upadki',
            'id': 162
          }, {'name': 'Szkło Corning Gorilla Glass 4', 'id': 174}, {
            'name': 'Funkcja latarki',
            'id': 175
          }, {'name': 'Szkło Corning Gorilla Glass 5', 'id': 214}, {
            'name': 'Aluminiowe wnętrze laptopa',
            'id': 254
          }, {'name': 'Podświetlana klawiatura', 'id': 255}, {
            'name': 'Białe podświetlenie klawiatury',
            'id': 256
          }, {'name': 'Wydzielona klawiatura numeryczna', 'id': 257}, {
            'name': 'Wielodotykowy, intuicyjny touchpad',
            'id': 258
          }, {'name': 'Możliwość zabezpieczenia linką (port Noble Wedge)', 'id': 259}, {
            'name': 'Wbudowany czytnik linii papilarnych',
            'id': 260
          }, {'name': 'Szyfrowanie TPM', 'id': 261}, {
            'name': 'Aluminiowa obudowa',
            'id': 281
          }, {'name': 'Możliwość zabezpieczenia linką (port Kensington Lock)', 'id': 322}, {
            'name': 'Czerwone podświetlenie klawiatury',
            'id': 345
          }, {'name': 'Magnezowa pokrywa matrycy', 'id': 416}, {
            'name': 'Corning Gorilla Glass 4',
            'id': 417
          }, {
            'name': 'Dostępne sterowniki tylko dla systemu Windows 10',
            'id': 432
          }, {'name': 'Ochrona dostępu do dysków twardych za pomocą hasła', 'id': 458}, {
            'name': 'Wielodotykowy gładzik Force Touch',
            'id': 470
          }, {'name': 'Klawiatura w układzie UK', 'id': 471}]
        }, {
          'name': 'Dołączone akcesoria',
          'filterable': false,
          'id': 24,
          'options': [{'name': 'Ładowarka', 'id': 50}, {'name': 'Kabel USB typ C', 'id': 51}, {
            'name': 'Przejściówka USB-C -&gt; micro USB',
            'id': 52
          }, {'name': 'Przejściówka USB-C -&gt; USB', 'id': 53}, {'name': 'Słuchawki', 'id': 54}, {
            'name': 'Rysik',
            'id': 55
          }, {'name': 'Instrukcja szybkiego uruchomienia telefonu', 'id': 56}, {
            'name': 'Kabel microUSB',
            'id': 77
          }, {'name': 'Ładowarka TurboPower™ 18 W', 'id': 109}, {
            'name': 'Przejściówka USB-C -&gt; Jack 3.5 mm',
            'id': 110
          }, {'name': 'Moto Power Pack', 'id': 111}, {'name': 'Silikonowe plecki', 'id': 135}, {
            'name': 'Kabel USB',
            'id': 176
          }, {'name': 'Zasilacz', 'id': 208}, {'name': 'Etui', 'id': 221}, {
            'name': 'Kabel zasilający',
            'id': 308
          }, {'name': 'Bateria (podstawowa)', 'id': 323}, {'name': 'Klawiatura', 'id': 418}, {
            'name': 'Firmowe etui na laptopa',
            'id': 439
          }, {'name': 'Firmowa mysz', 'id': 440}, {
            'name': 'Przejściówka HDMI -&gt; VGA',
            'id': 441
          }, {'name': 'Przejściówka USB -&gt; RJ-45', 'id': 442}, {'name': 'Przejściówka micro HDMI -&gt; HDMI', 'id': 459}]
        }, {
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}, {
            'name': '24 miesiące (gwarancja sprzedawcy)',
            'id': 126
          }, {'name': '36 miesięcy (gwarancja x-kom sp. z o.o.)', 'id': 263}, {
            'name': '12 miesięcy (gwarancja producenta)',
            'id': 282
          }, {'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}, {'name': '36 miesięcy (gwarancja producenta)', 'id': 406}]
        }, {
          'name': 'Ilość gniazd pamięci (ogółem / wolne)',
          'filterable': false,
          'id': 27,
          'options': [{'name': '2/0', 'id': 224}, {'name': '0/0 (pamięć wlutowana)', 'id': 266}, {'name': '1/0', 'id': 312}, {
            'name': '2/1',
            'id': 325
          }, {'name': '1/1', 'id': 435}]
        }, {
          'name': 'Dysk SSD M.2',
          'filterable': false,
          'id': 28,
          'options': [{'name': '240 GB', 'id': 225}, {'name': '512 GB', 'id': 453}]
        }, {
          'name': 'Dysk HDD SATA 5400 obr.',
          'filterable': false,
          'id': 29,
          'options': [{'name': '1000 GB', 'id': 226}]
        }, {
          'name': 'Wbudowane napędy optyczne',
          'filterable': false,
          'id': 30,
          'options': [{'name': 'Brak', 'id': 227}, {'name': 'Nagrywarka DVD+/-RW', 'id': 313}, {
            'name': 'Nagrywarka DVD+/-RW DualLayer',
            'id': 383
          }]
        }, {
          'name': 'Karta graficzna',
          'filterable': false,
          'id': 31,
          'options': [{'name': 'NVIDIA GeForce GTX 1050Ti', 'id': 231}, {
            'name': '+ Intel UHD Graphics 630',
            'id': 232
          }, {'name': 'Intel HD Graphics 6000', 'id': 270}, {'name': 'Intel HD Graphics 620', 'id': 285}, {
            'name': 'Intel UHD Graphics 620',
            'id': 299
          }, {'name': 'Intel HD Graphics 520', 'id': 315}, {'name': 'NVIDIA GeForce MX150', 'id': 326}, {
            'name': '+ Intel UHD Graphics 620',
            'id': 327
          }, {'name': 'NVIDIA GeForce GTX 1050', 'id': 337}, {
            'name': '+ Intel HD Graphics 630',
            'id': 338
          }, {'name': 'NVIDIA GeForce 940MX', 'id': 390}, {'name': 'AMD Radeon 520', 'id': 394}, {
            'name': 'NVIDIA GeForce MX130',
            'id': 444
          }, {'name': 'Intel Iris Graphics 640', 'id': 464}, {'name': 'AMD Radeon 530', 'id': 472}]
        }, {
          'name': 'Wielkość pamięci karty graficznej',
          'filterable': false,
          'id': 32,
          'options': [{'name': '4096 MB GDDR5 (pamięć własna)', 'id': 233}, {
            'name': 'Pamięć współdzielona',
            'id': 271
          }, {'name': '2048 MB GDDR5 (pamięć własna)', 'id': 328}]
        }, {
          'name': 'Dźwięk',
          'filterable': false,
          'id': 33,
          'options': [{
            'name': 'Wbudowany mikrofon',
            'id': 234
          }, {'name': 'Zintegrowana karta dźwiękowa zgodna z Intel High Definition Audio', 'id': 235}, {
            'name': 'Wbudowane cztery głośniki',
            'id': 300
          }, {'name': 'Wbudowane dwa mikrofony', 'id': 301}, {'name': 'Dolby Atmos Sound System', 'id': 329}]
        }, {
          'name': 'Kamera internetowa',
          'filterable': false,
          'id': 34,
          'options': [{'name': '1.0 Mpix', 'id': 236}, {'name': 'FaceTime HD', 'id': 272}, {
            'name': '0.3 Mpix',
            'id': 316
          }, {'name': '5.0 Mpix', 'id': 410}]
        }, {
          'name': 'Rodzaje wejść / wyjść',
          'filterable': false,
          'id': 35,
          'options': [{'name': 'USB 3.1 Gen. 1 (USB 3.0) - 3 szt.', 'id': 240}, {
            'name': 'HDMI - 1 szt.',
            'id': 241
          }, {'name': 'Thunderbolt 3 - 1 szt.', 'id': 242}, {'name': 'VGA (D-sub) - 1 szt.', 'id': 243}, {
            'name': 'RJ-45 (LAN) - 1 szt.',
            'id': 244
          }, {'name': 'Wyjście słuchawkowe/wejście mikrofonowe - 1 szt.', 'id': 245}, {
            'name': 'DC-in (wejście zasilania) - 1 szt.',
            'id': 246
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 2 szt.', 'id': 274}, {
            'name': 'Thunderbolt - 1 szt.',
            'id': 275
          }, {'name': 'USB 3.1 Gen. 1 (USB 3.0) - 1 szt.', 'id': 286}, {
            'name': 'Czytnik kart pamięci microSD - 1 szt.',
            'id': 287
          }, {'name': 'USB 2.0 - 1 szt.', 'id': 288}, {'name': 'USB Typu-C - 2 szt.', 'id': 302}, {
            'name': 'Mini Display Port - 1 szt.',
            'id': 339
          }, {'name': 'Wejście mikrofonowe - 1 szt.', 'id': 340}, {
            'name': 'USB 2.0 - 2 szt.',
            'id': 347
          }, {'name': 'Złącze stacji dokującej - 1 szt.', 'id': 411}, {
            'name': 'Noble Lock - 1 szt.',
            'id': 420
          }, {'name': 'Micro HDMI - 1 szt.', 'id': 454}, {'name': 'USB Typu-C (z Thunderbolt) - 2 szt.', 'id': 465}]
        }, {
          'name': 'Dołączone oprogramowanie',
          'filterable': false,
          'id': 36,
          'options': [{'name': 'Partycja recovery (opcja przywrócenia systemu z dysku)', 'id': 249}]
        }, {
          'name': 'Głębokość',
          'filterable': false,
          'id': 37,
          'options': [{'name': '270 mm', 'id': 252}, {'name': '227 mm', 'id': 279}, {'name': '211 mm', 'id': 292}, {
            'name': '215,6 mm',
            'id': 306
          }, {'name': '254 mm', 'id': 320}, {'name': '239 mm', 'id': 332}, {'name': '278 mm', 'id': 343}, {
            'name': '243 mm',
            'id': 351
          }, {'name': '266 mm', 'id': 361}, {'name': '226 mm', 'id': 369}, {'name': '263 mm', 'id': 374}, {
            'name': '260 mm',
            'id': 386
          }, {'name': '247 mm', 'id': 392}, {'name': '253 mm', 'id': 404}, {'name': '201 mm', 'id': 414}, {
            'name': '220 mm',
            'id': 424
          }, {'name': '252 mm', 'id': 430}, {'name': '224 mm', 'id': 438}, {'name': '251 mm', 'id': 449}, {
            'name': '225 mm',
            'id': 456
          }, {'name': '212 mm', 'id': 468}]
        }, {
          'name': 'Komponenty rozszerzone',
          'filterable': false,
          'id': 38,
          'options': [{
            'name': 'Dysk i pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom',
            'id': 262
          }, {
            'name': 'Pamięć RAM rozszerzona na profesjonalnej linii montażowej x-kom',
            'id': 355
          }, {'name': 'Dysk rozszerzony na profesjonalnej linii montażowej x-kom', 'id': 382}]
        }]
      }, {
        'name': 'Processors',
        'id': 3,
        'attributes': [{
          'name': 'Gwarancja',
          'filterable': false,
          'id': 25,
          'options': [{'name': '24 miesiące (gwarancja producenta)', 'id': 57}, {
            'name': '24 miesiące (gwarancja sprzedawcy)',
            'id': 126
          }, {'name': '36 miesięcy (gwarancja x-kom sp. z o.o.)', 'id': 263}, {
            'name': '12 miesięcy (gwarancja producenta)',
            'id': 282
          }, {'name': '24 miesiące (gwarancja x-kom sp. z o.o.)', 'id': 309}, {
            'name': '36 miesięcy (gwarancja producenta)',
            'id': 406
          }, {'name': '36 miesięcy (gwarancja sprzedawcy)', 'id': 485}]
        }, {
          'name': 'Rodzina procesorów',
          'filterable': true,
          'id': 45,
          'options': [{'name': 'Intel Core i5', 'id': 473}, {'name': 'Intel Core i3', 'id': 486}, {
            'name': 'Intel Core i7',
            'id': 499
          }, {'name': 'Intel Pentium', 'id': 508}, {'name': 'AMD Ryzen', 'id': 518}, {
            'name': 'AMD FX',
            'id': 567
          }, {'name': 'Intel Celeron', 'id': 581}]
        }, {
          'name': 'Model procesora',
          'filterable': false,
          'id': 46,
          'options': [{'name': 'i5-8400', 'id': 474}, {'name': 'i3-8100', 'id': 487}, {'name': 'i5-8600K', 'id': 494}, {
            'name': 'i7-8700',
            'id': 500
          }, {'name': 'i7-8700K', 'id': 504}, {'name': 'i5-8500', 'id': 506}, {'name': 'G5400', 'id': 509}, {
            'name': 'i7-8086K',
            'id': 516
          }, {'name': 'Ryzen 5 2600', 'id': 519}, {'name': 'Ryzen 3 2200G', 'id': 525}, {
            'name': 'i5-8600',
            'id': 528
          }, {'name': 'Ryzen 7 2700X', 'id': 530}, {'name': 'i5-7400', 'id': 536}, {'name': 'i3-7100', 'id': 543}, {
            'name': 'Ryzen 5 2600X',
            'id': 547
          }, {'name': 'Ryzen 5 2400G', 'id': 549}, {'name': 'Ryzen 3 1200', 'id': 553}, {
            'name': 'i5-7600K',
            'id': 557
          }, {'name': 'Ryzen 5 1600', 'id': 560}, {'name': 'Ryzen 5 1600X', 'id': 563}, {'name': 'i7-7700K', 'id': 565}, {
            'name': 'FX-8350',
            'id': 568
          }, {'name': 'G4560', 'id': 574}, {'name': 'i5-7500', 'id': 577}, {'name': 'Ryzen 7 2700', 'id': 579}, {
            'name': 'G3900',
            'id': 582
          }, {'name': 'Ryzen 5 1500X', 'id': 587}, {'name': 'G5500', 'id': 588}, {'name': 'FX-6300', 'id': 590}]
        }, {
          'name': 'Gniazdo procesora (socket)',
          'filterable': true,
          'id': 47,
          'options': [{'name': 'Socket 1151 (Intel Core 8 gen. Coffee Lake)', 'id': 475}, {
            'name': 'Socket AM4',
            'id': 520
          }, {'name': 'Socket 1151', 'id': 537}, {'name': 'Socket AM3+', 'id': 569}]
        }, {
          'name': 'Taktowanie rdzenia',
          'filterable': false,
          'id': 48,
          'options': [{'name': '2.8 GHz', 'id': 476}, {'name': '3.6 GHz', 'id': 488}, {
            'name': '3.6 GHz (4.3 GHz w trybie turbo)',
            'id': 495
          }, {'name': '3.2 GHz (4.6 GHz w trybie turbo)', 'id': 501}, {
            'name': '3.7 GHz (4.7 GHz w trybie turbo)',
            'id': 505
          }, {'name': '3.0 GHz (4.1 GHz w trybie turbo)', 'id': 507}, {
            'name': '3.7 GHz',
            'id': 510
          }, {'name': '4.0 GHz (5.0 GHz w trybie turbo)', 'id': 517}, {
            'name': '3.4 GHz (3.9 GHz w trybie turbo)',
            'id': 521
          }, {'name': '3.5 GHz (3.7 GHz w trybie turbo)', 'id': 526}, {
            'name': '3.1 GHz (4.3 GHz w trybie turbo)',
            'id': 529
          }, {'name': '3.7 GHz (4.3 GHz w trybie turbo)', 'id': 531}, {
            'name': '3.0 GHz (3.5 GHz w trybie turbo)',
            'id': 538
          }, {'name': '3.9 GHz', 'id': 544}, {
            'name': '3.6 GHz (4.2 GHz w trybie turbo)',
            'id': 548
          }, {'name': '3.6 GHz (3.9 GHz w trybie turbo)', 'id': 550}, {
            'name': '3.1 GHz (3.4 GHz w trybie turbo)',
            'id': 554
          }, {'name': '3.8 GHz (4.2 GHz w trybie turbo)', 'id': 558}, {
            'name': '3.2 GHz (3.6 GHz w trybie turbo)',
            'id': 561
          }, {'name': '3.6 GHz (4.0 GHz w trybie turbo)', 'id': 564}, {
            'name': '4.2 GHz (4.5 GHz w trybie turbo)',
            'id': 566
          }, {'name': '4.0 GHz (4.2 GHz w trybie turbo)', 'id': 570}, {
            'name': '3.5 GHz',
            'id': 575
          }, {'name': '3.4 GHz (3.8 GHz w trybie turbo)', 'id': 578}, {
            'name': '3.2 GHz (4.1 GHz w trybie turbo)',
            'id': 580
          }, {'name': '3.8 GHz', 'id': 589}, {'name': '3.5 GHz (4.1 GHz w trybie turbo)', 'id': 591}]
        }, {
          'name': 'Liczba rdzeni fizycznych',
          'filterable': true,
          'id': 49,
          'options': [{'name': '6 rdzeni', 'id': 477}, {'name': '4 rdzenie', 'id': 489}, {
            'name': '2 rdzenie',
            'id': 511
          }, {'name': '8 rdzeni', 'id': 532}]
        }, {
          'name': 'Liczba wątków',
          'filterable': true,
          'id': 50,
          'options': [{'name': '6 wątków', 'id': 478}, {'name': '4 wątki', 'id': 490}, {
            'name': '12 wątków',
            'id': 502
          }, {'name': '16 wątków', 'id': 533}, {'name': '8 wątków', 'id': 551}, {'name': '2 wątki', 'id': 583}]
        }, {
          'name': 'Pamięć podręczna',
          'filterable': false,
          'id': 51,
          'options': [{'name': '9 MB', 'id': 479}, {'name': '6 MB', 'id': 491}, {'name': '12 MB', 'id': 503}, {
            'name': '4 MB',
            'id': 512
          }, {'name': '19 MB', 'id': 522}, {'name': '20 MB', 'id': 534}, {'name': '3 MB', 'id': 545}, {
            'name': '8 MB',
            'id': 555
          }, {'name': '16 MB', 'id': 562}, {'name': '2 MB', 'id': 584}]
        }, {
          'name': 'Zintegrowany układ graficzny',
          'filterable': false,
          'id': 52,
          'options': [{'name': 'Intel UHD Graphics 630', 'id': 480}, {
            'name': 'Intel UHD Graphics 610',
            'id': 513
          }, {'name': 'Radeon RX Vega 8', 'id': 527}, {'name': 'Intel HD Graphics 630', 'id': 539}, {
            'name': 'Radeon RX Vega 11',
            'id': 552
          }, {'name': 'Intel HD Graphics 610', 'id': 576}, {'name': 'Intel HD Graphics 510', 'id': 585}]
        }, {
          'name': 'Rodzaj obsługiwanej pamięci',
          'filterable': false,
          'id': 53,
          'options': [{'name': 'DDR4-2666', 'id': 481}, {'name': 'DDR4-2400', 'id': 492}, {
            'name': 'DDR4-2933',
            'id': 523
          }, {'name': 'DDR4-2133 (PC4-17000)', 'id': 540}, {'name': 'DDR3-1600 (PC3-12800)', 'id': 541}, {
            'name': 'DDR3-1333 (PC3-10600)',
            'id': 542
          }, {'name': 'DDR4-2667', 'id': 556}, {'name': 'DDR3-1866 (PC3-15000)', 'id': 571}, {'name': 'DDR4-1866', 'id': 586}]
        }, {
          'name': 'Technologia produkcji procesora',
          'filterable': false,
          'id': 54,
          'options': [{'name': '14 nm', 'id': 482}, {'name': '12 nm', 'id': 524}, {'name': '32 nm', 'id': 572}]
        }, {
          'name': 'TDP',
          'filterable': false,
          'id': 55,
          'options': [{'name': '65 W', 'id': 483}, {'name': '95 W', 'id': 496}, {'name': '54 W', 'id': 514}, {
            'name': '105 W',
            'id': 535
          }, {'name': '51 W', 'id': 546}, {'name': '91 W', 'id': 559}]
        }, {
          'name': 'Dodatkowe technologie',
          'filterable': false,
          'id': 56,
          'options': [{'name': 'Intel Turbo Boost 2.0', 'id': 484}, {'name': 'Intel Hyper-Threading', 'id': 515}, {
            'name': 'AMD Turbo Core',
            'id': 573
          }]
        }]
      }
    ];

    const transport: Array<TransportResponseInterface> = [
      {
        id: 1,
        name: 'DHL',
        cost: 9.99
      },
      {
        id: 2,
        name: 'UPC',
        cost: 10.99
      },
      {
        id: 3,
        name: 'FedEx',
        cost: 7.99
      }
    ];

    const payment: Array<PaymentResponseInterface> = [
      {
        id: 1,
        name: 'Standard transfer (12h)',
        cost: 0.09
      },
      {
        id: 2,
        name: 'Fast transfer (15m)',
        cost: 1.99
      },
      {
        id: 3,
        name: 'Super, hyper, extra transfer (-0.1s)',
        cost: 19.99
      }
    ];

    return {
      product: products,
      category: categories,
      transport,
      payment
    };
  }
}
