import {Theme} from '../models/theme';
import {Injectable} from '@angular/core';
import {ThemeFactory} from '../factory/theme.factory';

@Injectable()
export class ThemeService {
  private readonly LS_KEY: string = 'app-theme';
  private _themes: Array<Theme> = [];
  private propertyMap = {
    primaryColor: '--c-primary',
    primaryColor1: '--c-primary-1',
    primaryColor2: '--c-primary-2',
  };

  public constructor(private themeFactory: ThemeFactory) {
    this._themes.push(this.themeFactory.create('light-blue', 'Light blue'));
    this._themes.push(this.themeFactory.create('light-green', 'Light green'));
    this._themes.push(this.themeFactory.create('light-orange', 'Light orange'));

    this.loadThemeSchema();
  }

  public changeThemeSchema(name: string): void {
    const theme = this.getThemeByName(name);

    if (theme) {
      for (const property in this.propertyMap) {
        if (this.propertyMap.hasOwnProperty(property) && theme.hasOwnProperty(property)) {
          document.documentElement.style.setProperty(this.propertyMap[property], theme[property]);
        }
      }

      this.saveThemeSchema(theme.name);
    }
  }

  public get themes(): Array<Theme> {
    return this._themes;
  }

  private loadThemeSchema(): void {
    const themeName = localStorage.getItem(this.LS_KEY);

    if (themeName) {
      this.changeThemeSchema(themeName);
    }
  }

  private saveThemeSchema(name: string): void {
    localStorage.setItem(this.LS_KEY, name);
  }

  private getThemeByName(name: string): Theme | undefined {
    return this.themes.find((theme: Theme) => {
      return theme.name === name;
    });
  }
}
