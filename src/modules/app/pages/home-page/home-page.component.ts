import {Component} from '@angular/core';
import {Breadcrumb} from '../../../shared/models/breadcrumb';
import {SwiperConfigInterface} from 'ngx-swiper-wrapper';

@Component({
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {
  public sliderConfig: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    centeredSlides: true,
    navigation: {
      prevEl: '.button-prev',
      nextEl: '.button-next'
    },
    simulateTouch: false,
    autoplay: true
  };

  public get breadcrumbsItems(): Array<Breadcrumb> {
    return [
      new Breadcrumb('Home')
    ];
  }
}
