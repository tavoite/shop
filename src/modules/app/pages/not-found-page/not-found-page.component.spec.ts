import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NotFoundPageComponent} from './not-found-page.component';

describe('NotFoundComponent', () => {
  let component: NotFoundPageComponent;
  let fixture: ComponentFixture<NotFoundPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotFoundPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have message', () => {
    const notFound = fixture.debugElement.componentInstance;

    expect(notFound.message).toEqual('Requested page cannot be found :(');
  });

  it('should display message', () => {
    const notFound = fixture.debugElement.nativeElement;

    expect(notFound.querySelector('.message').textContent).toEqual('Requested page cannot be found :(');
  });
});
