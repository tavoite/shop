import {Injectable} from '@angular/core';

@Injectable()
export class TouchDetectorService {
  public isTouchEnabled(): boolean {
    try {
      document.createEvent('TouchEvent');

      return true;
    } catch (e) {
      return false;
    }
  }

  public isTouchDisabled(): boolean {
    return !this.isTouchEnabled();
  }
}
