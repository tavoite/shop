import {Component, Input} from '@angular/core';
import {Breadcrumb} from '../../models/breadcrumb';

@Component({
  selector: 'shared-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent {
  @Input()
  public items: Array<Breadcrumb> = [];
}
