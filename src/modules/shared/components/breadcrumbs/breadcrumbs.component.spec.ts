import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BreadcrumbsComponent} from './breadcrumbs.component';
import {Breadcrumb} from '../../models/breadcrumb';
import {RouterTestingModule} from '@angular/router/testing';

describe('BreadcrumbsComponent', () => {
  let component: BreadcrumbsComponent;
  let fixture: ComponentFixture<BreadcrumbsComponent>;
  let native;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BreadcrumbsComponent
      ],
      imports: [
        RouterTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have text item', () => {
    component.items = [
      new Breadcrumb('Test')
    ];
    fixture.detectChanges();

    expect(native.querySelector('.item-text').textContent).toEqual('Test');
  });

  it('should have link item', () => {
    component.items = [
      new Breadcrumb('Test', ['/'])
    ];
    fixture.detectChanges();

    expect(native.querySelector('.item-link').textContent).toEqual('Test');
    expect(native.querySelector('.item-link').getAttribute('href')).toEqual('/');
  });
});
