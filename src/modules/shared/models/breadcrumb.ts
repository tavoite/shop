export class Breadcrumb {
  public constructor(private _text: string, private _url?: Array<any>) {
  }

  get text(): string {
    return this._text;
  }

  get url(): Array<any> {
    return this._url;
  }
}
