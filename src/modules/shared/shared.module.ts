import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TitleComponent} from './components/title/title.component';
import {TouchDetectorService} from './services/touch-detector.service';
import {BreadcrumbsComponent} from './components/breadcrumbs/breadcrumbs.component';
import {RouterModule} from '@angular/router';
import {DropdownComponent} from './components/dropdown/dropdown.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbDropdownModule,
    FontAwesomeModule
  ],
  exports: [
    TitleComponent,
    BreadcrumbsComponent,
    DropdownComponent
  ],
  providers: [
    TouchDetectorService
  ],
  declarations: [
    TitleComponent,
    BreadcrumbsComponent,
    DropdownComponent
  ],
})
export class SharedModule {
}
