import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductRoutingModule} from './product-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {ProductClient} from './client/product.client';
import {ProductFactory} from './factory/product.factory';
import {SharedModule} from '../shared/shared.module';
import {PhotoFactory} from './factory/photo.factory';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {ProductRepository} from './repositories/product.repository';
import {CategoryFactory} from './factory/category.factory';
import {ProductOnListComponent} from './components/product-on-list/product-on-list.component';
import {CategoryRepository} from './repositories/category.repository';
import {CategoryNavigationComponent} from './components/category-navigation/category-navigation.component';
import {CategoryClient} from './client/category.client';
import {ProductReviewsComponent} from './components/product-reviews/product-reviews.component';
import {ReviewFactory} from './factory/review.factory';
import {FormsModule} from '@angular/forms';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {ReviewRepository} from './repositories/review.repository';
import {AttributeFactory} from './factory/attribute.factory';
import {AttributeOptionFactory} from './factory/attribute-option.factory';
import {ProductSearchComponent} from './components/product-search/product-search.component';
import {BuyButtonComponent} from './components/buy-button/buy-button.component';
import {ProductListComponent} from './components/product-list/product-list.component';
import {ProductDetailPageComponent} from './pages/product-detail-page/product-detail-page.component';
import {ProductListPageComponent} from './pages/product-list-page/product-list-page.component';
import {SearchListPageComponent} from './pages/search-list-page/search-list-page.component';
import {ProductPhotoComponent} from './components/product-photo/product-photo.component';
import {MatProgressSpinnerModule} from '@angular/material';
import {ProductListFilterManagerComponent} from './components/product-list-filter-manager/product-list-filter-manager.component';
import {FilterFactory} from './factory/filter.factory';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FilterService} from './services/filter.service';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

@NgModule({
  imports: [
    CommonModule,
    SwiperModule,
    FormsModule,
    NgbRatingModule.forRoot(),
    MatProgressSpinnerModule,
    HttpClientModule,
    FontAwesomeModule,
    SharedModule,
    ProductRoutingModule
  ],
  exports: [
    ProductSearchComponent,
    ProductPhotoComponent
  ],
  entryComponents: [
    ProductListFilterManagerComponent
  ],
  declarations: [
    ProductListPageComponent,
    ProductDetailPageComponent,
    SearchListPageComponent,
    ProductOnListComponent,
    CategoryNavigationComponent,
    ProductReviewsComponent,
    ProductSearchComponent,
    BuyButtonComponent,
    ProductListComponent,
    ProductPhotoComponent,
    ProductListFilterManagerComponent
  ],
  providers: [
    ProductClient,
    ProductFactory,
    ProductRepository,
    PhotoFactory,
    CategoryClient,
    CategoryFactory,
    CategoryRepository,
    ReviewFactory,
    ReviewRepository,
    AttributeFactory,
    AttributeOptionFactory,
    FilterFactory,
    FilterService,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class ProductModule {
}
