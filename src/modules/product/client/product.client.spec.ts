import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ProductClient} from './product.client';
import {ProductResponseInterface} from '../interfaces/product-response.interface';
import {FilterService} from '../services/filter.service';

describe('ProductClient', () => {
  let injector: TestBed;
  let service: ProductClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ProductClient,
        FilterService
      ]
    });
    injector = getTestBed();
    service = injector.get(ProductClient);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getOne', () => {
    it('should return an Observable<ProductResponseInterface>', () => {
      const dummyResponse = <ProductResponseInterface>{
        id: 1,
        name: 'Test',
        price: 10
      };

      service.getOne(1).subscribe(data => {
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(`${ProductClient.URL}/${dummyResponse.id}`);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });

  describe('#getAllByCategoryId', () => {
    it('should return an Observable<ProductResponseInterface[]>', () => {
      const dummyResponse = [
        <ProductResponseInterface>{
          id: 1,
          name: 'Test',
          price: 10,
          photos: [],
          category: {
            id: 1,
            name: 'test'
          }
        }
      ];

      service.getAllByCategoryId(1).subscribe(data => {
        expect(data.length).toBe(1);
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(ProductClient.URL);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });
});
