import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {CategoryResponseInterface} from '../interfaces/category-response.interface';

@Injectable()
export class CategoryClient {
  public static readonly URL: string = '/api/category';

  public constructor(private httpClient: HttpClient) {
  }

  public getOne(categoryId: number): Observable<CategoryResponseInterface> {
    return this.httpClient.get<CategoryResponseInterface>(`${CategoryClient.URL}/${categoryId}`);
  }

  public getAll(): Observable<CategoryResponseInterface[]> {
    return this.httpClient.get<CategoryResponseInterface[]>(CategoryClient.URL);
  }
}
