import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductResponseInterface} from '../interfaces/product-response.interface';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Filter} from '../models/filter';
import {FilterService} from '../services/filter.service';

@Injectable()
export class ProductClient {
  public static readonly URL: string = '/api/product';

  public constructor(private httpClient: HttpClient, private filterService: FilterService) {
  }

  public getOne(id: number): Observable<ProductResponseInterface> {
    return this.httpClient.get<ProductResponseInterface>(`${ProductClient.URL}/${id}`);
  }

  public getAllByCategoryId(categoryId: number): Observable<ProductResponseInterface[]> {
    return this.httpClient.get<ProductResponseInterface[]>(ProductClient.URL).pipe(
      map((data: ProductResponseInterface[]): ProductResponseInterface[] => {
        return data.filter((product: ProductResponseInterface): boolean => {
          return product.category.id === categoryId;
        });
      })
    );
  }

  public getAllByCategoryIdWithFilters(categoryId: number, filters: Array<Filter>): Observable<ProductResponseInterface[]> {
    return this.getAllByCategoryId(categoryId).pipe(
      map((data: ProductResponseInterface[]): ProductResponseInterface[] => {
        return this.filterService.filterResponse(data, filters);
      })
    );
  }

  public getAllByPhrase(phrase: string): Observable<ProductResponseInterface[]> {
    return this.httpClient.get<ProductResponseInterface[]>(`${ProductClient.URL}?name=\\b${phrase}\\b`);
  }

  public getAllByIds(ids: Array<number>): Observable<ProductResponseInterface[]> {
    const idsAsString = '(' + ids.join('|') + ')';

    return this.httpClient.get<ProductResponseInterface[]>(`${ProductClient.URL}?id=\\b${idsAsString}\\b`);
  }
}
