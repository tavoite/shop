import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {CategoryClient} from './category.client';
import {CategoryResponseInterface} from '../interfaces/category-response.interface';

describe('CategoryClient', () => {
  let injector: TestBed;
  let service: CategoryClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CategoryClient]
    });
    injector = getTestBed();
    service = injector.get(CategoryClient);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getOne', () => {
    it('should return an Observable<CategoryResponseInterface>', () => {
      const dummyResponse = <CategoryResponseInterface>{
        id: 1,
        name: 'Test',
      };

      service.getOne(1).subscribe(data => {
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(`${CategoryClient.URL}/${dummyResponse.id}`);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });

  describe('#getAll', () => {
    it('should return an Observable<CategoryResponseInterface[]>', () => {
      const dummyResponse = [
        <CategoryResponseInterface>{
          id: 1,
          name: 'Test',
        },
        <CategoryResponseInterface>{
          id: 2,
          name: 'Test 2',
        }
      ];

      service.getAll().subscribe(data => {
        expect(data.length).toBe(2);
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(CategoryClient.URL);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });
});
