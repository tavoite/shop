import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductDetailPageComponent} from './pages/product-detail-page/product-detail-page.component';
import {ProductListPageComponent} from './pages/product-list-page/product-list-page.component';
import {SearchListPageComponent} from './pages/search-list-page/search-list-page.component';

const routes: Routes = [
  {
    path: 'product-list/:categoryId',
    component: ProductListPageComponent
  },
  {
    path: 'product-detail/:productId',
    component: ProductDetailPageComponent
  },
  {
    path: 'search/:phrase',
    component: SearchListPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule {
}
