import {ReviewResponseInterface} from '../interfaces/review-response.interface';
import {Review} from '../models/review';
import {Injectable} from '@angular/core';

@Injectable()
export class ReviewFactory {
  public create(data: ReviewResponseInterface): Review {
    return new Review(
      data.id,
      data.rating,
      data.authorName,
      new Date(data.date),
      data.content
    );
  }

  public createCollection(data: ReviewResponseInterface[]): Review[] {
    const collection: Array<Review> = [];

    for (const reviewData of data) {
      collection.push(this.create(reviewData));
    }

    return collection;
  }
}
