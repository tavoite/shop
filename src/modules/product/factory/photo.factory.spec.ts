import {PhotoFactory} from './photo.factory';
import {PhotoResponseInterface} from '../interfaces/photo-response.interface';
import {Photo} from '../models/photo';

describe('PhotoFactory', () => {
  let factory: PhotoFactory;

  beforeEach(() => {
    factory = new PhotoFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Photo object', function () {
    /* tslint:disable:max-line-length */
    const data = <PhotoResponseInterface>{
      id: 1,
      url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Taylor_Momsen_-_Warped_Tour_Kickoff_%285%29.jpg/1200px-Taylor_Momsen_-_Warped_Tour_Kickoff_%285%29.jpg'
    };
    /* tslint:enable:max-line-length */

    const photo = factory.create(data);

    expect(photo instanceof Photo).toBeTruthy();
    expect(photo.url).toBe(data.url);
  });

  it('should return array of Photo objects', function () {
    /* tslint:disable:max-line-length */
    const data = [
      <PhotoResponseInterface>{
        id: 1,
        url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Taylor_Momsen_-_Warped_Tour_Kickoff_%285%29.jpg/1200px-Taylor_Momsen_-_Warped_Tour_Kickoff_%285%29.jpg'
      },
      <PhotoResponseInterface>{
        id: 2,
        url: 'https://i.pinimg.com/originals/01/6d/74/016d74dcde6a55a0622b205da6c99a44.jpg'
      }
    ];
    /* tslint:enable:max-line-length */

    const products = factory.createCollection(data);

    expect(products.length).toBe(data.length);
  });
});
