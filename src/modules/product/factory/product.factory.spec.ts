import {ProductFactory} from './product.factory';
import {ProductResponseInterface} from '../interfaces/product-response.interface';
import {Product} from '../models/product';
import {PhotoFactory} from './photo.factory';
import {CategoryFactory} from './category.factory';
import {ReviewFactory} from './review.factory';
import {AttributeFactory} from './attribute.factory';
import {AttributeOptionFactory} from './attribute-option.factory';

describe('ProductFactory', () => {
  let factory: ProductFactory;

  beforeEach(() => {
    factory = new ProductFactory(
      new PhotoFactory(),
      new CategoryFactory(new AttributeFactory(new AttributeOptionFactory())),
      new ReviewFactory(),
      new AttributeFactory(new AttributeOptionFactory())
    );
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Product object', () => {
    const data = <ProductResponseInterface>{
      id: 1,
      name: 'Test',
      price: 69,
      quantity: 100,
      category: {
        id: 1,
        name: 'test',
        attributes: []
      },
      photos: [
        {
          id: 5000,
          url: 'https://i.pinimg.com/originals/01/6d/74/016d74dcde6a55a0622b205da6c99a44.jpg',
          main: true
        }
      ],
      reviews: [
        {
          id: 2000,
          rating: 5,
          authorName: 'Jan Kowalski',
          date: '2017-10-10 18:02:34',
          content: 'Great console! Best on the market.'
        },
        {
          id: 2001,
          rating: 1,
          authorName: 'Jan Kowalski',
          date: '2017-10-10 18:02:34',
          content: 'Great console! Best on the market.'
        }
      ],
      attributes: []
    };

    const product = factory.create(data);

    expect(product instanceof Product).toBeTruthy();
    expect(product.id).toBe(data.id);
    expect(product.name).toBe(data.name);
    expect(product.price).toBe(data.price);
    expect(product.photos.length).toEqual(1);
    expect(product.reviews.length).toEqual(2);
  });

  it('should return array of Product objects', () => {
    const data = [
      <ProductResponseInterface>{
        id: 1,
        name: 'Test',
        price: 69,
        quantity: 100,
        category: {
          id: 1,
          name: 'test',
          attributes: []
        },
        photos: [],
        reviews: [],
        attributes: []
      },
      <ProductResponseInterface>{
        id: 2,
        name: 'Test 2',
        price: 22,
        quantity: 1,
        category: {
          id: 2,
          name: 'test 2',
          attributes: []
        },
        photos: [],
        reviews: [],
        attributes: []
      }
    ];

    const products = factory.createCollection(data);

    expect(products.length).toBe(data.length);
  });
});
