export class Photo {
  public constructor(
    private id: number,
    private _url: string,
    private _main: boolean = false) {
  }

  public get url(): string {
    return this._url;
  }

  public get main(): boolean {
    return this._main;
  }

  public toString(): string {
    return this.url;
  }
}
