import {Photo} from './photo';
import {Category} from './category';
import {Review} from './review';
import {Attribute} from './attribute';

export class Product {
  public constructor(
    private _id: number,
    private _name: string,
    private _price: number,
    private _category: Category,
    private _photos: Array<Photo> = [],
    private _reviews: Array<Review> = [],
    private _attributes: Array<Attribute> = []) {
  }

  public get id(): number {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get price(): number {
    return this._price;
  }

  public get category(): Category {
    return this._category;
  }

  public get photos(): Array<Photo> {
    return this._photos;
  }

  public get reviews(): Array<Review> {
    return this._reviews;
  }

  public get attributes(): Array<Attribute> {
    return this._attributes;
  }

  public get mainPhoto(): Photo | undefined {
    return this.photos.find((photo: Photo): boolean => {
        return photo.main;
    });
  }

  public toString(): string {
    return this.name;
  }
}
