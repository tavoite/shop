export class Review {
  public constructor(
    private _id: number,
    private _rating: number,
    private _authorName: string,
    private _date: Date,
    private _content: string) {
  }

  public get id(): number {
    return this._id;
  }

  public get rating(): number {
    return this._rating;
  }

  public get authorName(): string {
    return this._authorName;
  }

  public get date(): Date {
    return this._date;
  }

  public get content(): string {
    return this._content;
  }
}
