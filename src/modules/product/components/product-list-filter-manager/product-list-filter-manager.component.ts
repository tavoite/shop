import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../../models/category';
import {Attribute} from '../../models/attribute';
import {AttributeOption} from '../../models/attribute-option';
import {Filter} from '../../models/filter';
import {FilterFactory} from '../../factory/filter.factory';
import {ActivatedRoute} from '@angular/router';
import {AttributeWithCheckStatusInterface} from '../../interfaces/attribute-with-check-status.interface';

@Component({
  selector: 'product-list-filter-manager',
  templateUrl: './product-list-filter-manager.component.html',
  styleUrls: ['./product-list-filter-manager.component.scss']
})
export class ProductListFilterManagerComponent implements OnInit {
  @Input()
  public category: Category | undefined;

  @Output()
  public filterListChange = new EventEmitter<Array<Filter>>();

  public initialized: boolean = false;
  private filterList: Array<Filter> = [];

  public constructor(private route: ActivatedRoute, private filterFactory: FilterFactory) {
  }

  public ngOnInit(): void {
    this.route.queryParams.subscribe(() => {
      if (this.route.snapshot.queryParams['filters']) {
        this.filterList = this.filterFactory.createCollection(this.route.snapshot.queryParams['filters']);
      }

      if (!this.initialized) {
        this.initialized = true;
      }
    });
  }

  public onCheckboxClick(attribute: Attribute, attributeOption: AttributeOption): void {
    const existingFilter = this.filterList.find((filter: Filter) => {
      return filter.attributeId === attribute.id && filter.attributeOptionId === attributeOption.id;
    });

    if (existingFilter) {
      this.filterList.splice(this.filterList.indexOf(existingFilter), 1);
    } else {
      this.filterList.push(this.filterFactory.create(attribute.id, attributeOption.id));
    }

    this.filterListChange.emit(this.filterList);
  }

  private shouldBeChecked(attribute: Attribute, attributeOption: AttributeOption): boolean {
    for (const filter of this.filterList) {
      if (filter.attributeId === attribute.id && filter.attributeOptionId === attributeOption.id) {
        return true;
      }
    }

    return false;
  }

  public get attributes(): Array<AttributeWithCheckStatusInterface> {
    if (!this.category) {
      return [];
    }

    return this.category.attributes.filter((attribute: Attribute) => {
      return attribute.filterable;
    }).map((attribute: Attribute) => {
      const optionsWithCheckedStatus: Array<{
        option: AttributeOption,
        checked: boolean
      }> = [];

      for (const attributeOption of attribute.options) {
        optionsWithCheckedStatus.push({
          option: attributeOption,
          checked: this.shouldBeChecked(attribute, attributeOption)
        });
      }

      return {
        attribute,
        options: optionsWithCheckedStatus
      };
    });
  }
}
