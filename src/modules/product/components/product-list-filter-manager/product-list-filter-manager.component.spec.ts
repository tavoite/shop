import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductListFilterManagerComponent} from './product-list-filter-manager.component';
import {RouterTestingModule} from '@angular/router/testing';
import {FilterFactory} from '../../factory/filter.factory';
import {SharedModule} from '../../../shared/shared.module';
import {MatProgressSpinnerModule} from '@angular/material';

describe('ProductListFilterManagerComponent', () => {
  let component: ProductListFilterManagerComponent;
  let fixture: ComponentFixture<ProductListFilterManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductListFilterManagerComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        MatProgressSpinnerModule
      ],
      providers: [
        FilterFactory
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListFilterManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
