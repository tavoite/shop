import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CategoryNavigationComponent} from './category-navigation.component';
import {Category} from '../../models/category';
import {SharedModule} from '../../../shared/shared.module';
import {CategoryRepository} from '../../repositories/category.repository';
import {CategoryClient} from '../../client/category.client';
import {CategoryFactory} from '../../factory/category.factory';
import {AttributeFactory} from '../../factory/attribute.factory';
import {AttributeOptionFactory} from '../../factory/attribute-option.factory';
import {RouterTestingModule} from '@angular/router/testing';
import {MatProgressSpinnerModule} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';

describe('CategoryNavigationComponent', () => {
  let component: CategoryNavigationComponent;
  let fixture: ComponentFixture<CategoryNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CategoryNavigationComponent
      ],
      imports: [
        SharedModule,
        HttpClientModule,
        RouterTestingModule,
        MatProgressSpinnerModule
      ],
      providers: [
        CategoryRepository,
        CategoryClient,
        CategoryFactory,
        AttributeFactory,
        AttributeOptionFactory
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should display three links',  () => {
  //   component.categories = [
  //     new Category(1, 'Test 1'),
  //     new Category(2, 'Test 2'),
  //     new Category(3, 'Test 3'),
  //   ];
  //
  //   const native = fixture.debugElement.nativeElement;
  //
  //   expect(native.querySelectorAll('.category-item').length).toEqual(3);
  // });
});
