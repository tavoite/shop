import {Component, OnInit} from '@angular/core';
import {CategoryRepository} from '../../repositories/category.repository';
import {Category} from '../../models/category';

@Component({
  selector: 'product-category-navigation',
  templateUrl: './category-navigation.component.html',
  styleUrls: ['./category-navigation.component.scss']
})
export class CategoryNavigationComponent implements OnInit {
  public categories: Array<Category>;

  public constructor(private categoryRepository: CategoryRepository) {
  }

  public ngOnInit(): void {
    this.categoryRepository.getAll().subscribe((categoryCollection: Array<Category>) => {
      this.categories = categoryCollection;
    });
  }
}
