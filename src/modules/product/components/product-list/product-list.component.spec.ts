import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductListComponent} from './product-list.component';
import {ProductClient} from '../../client/product.client';
import {ProductFactory} from '../../factory/product.factory';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from '../../../shared/shared.module';
import {PhotoFactory} from '../../factory/photo.factory';
import {CategoryFactory} from '../../factory/category.factory';
import {ProductRepository} from '../../repositories/product.repository';
import {ProductOnListComponent} from '../product-on-list/product-on-list.component';
import {ProductListPageComponent} from '../../pages/product-list-page/product-list-page.component';
import {ProductDetailPageComponent} from '../../pages/product-detail-page/product-detail-page.component';
import {SearchListPageComponent} from '../../pages/search-list-page/search-list-page.component';
import {CategoryNavigationComponent} from '../category-navigation/category-navigation.component';
import {ProductListFilterManagerComponent} from '../product-list-filter-manager/product-list-filter-manager.component';
import {MatProgressSpinnerModule} from '@angular/material';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {BuyButtonComponent} from '../buy-button/buy-button.component';
import {ProductReviewsComponent} from '../product-reviews/product-reviews.component';
import {ProductPhotoComponent} from '../product-photo/product-photo.component';
import {FormsModule} from '@angular/forms';
import {PRODUCT_MODULE_GLOBAL_CONFIG} from '../../consts/product-module-global-config.const';
import {OrderItemFactory} from '../../../checkout/factory/order-item.factory';
import {OrderService} from '../../../checkout/services/order.service';
import {AttributeFactory} from '../../factory/attribute.factory';
import {AttributeOptionFactory} from '../../factory/attribute-option.factory';
import {FilterFactory} from '../../factory/filter.factory';
import {ReviewRepository} from '../../repositories/review.repository';
import {ReviewFactory} from '../../factory/review.factory';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderProvider} from '../../../checkout/provider/order.provider';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductListPageComponent,
        ProductDetailPageComponent,
        SearchListPageComponent,
        ProductListComponent,
        ProductOnListComponent,
        CategoryNavigationComponent,
        ProductListFilterManagerComponent,
        BuyButtonComponent,
        ProductReviewsComponent,
        ProductPhotoComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule,
        SharedModule,
        MatProgressSpinnerModule,
        SwiperModule,
        NgbRatingModule.forRoot(),
        FontAwesomeModule,
        RouterTestingModule
      ],
      providers: [
        ProductRepository,
        ProductClient,
        ProductFactory,
        PhotoFactory,
        CategoryFactory,
        AttributeFactory,
        AttributeOptionFactory,
        FilterFactory,
        ReviewRepository,
        ReviewFactory,
        OrderItemFactory,
        OrderService,
        OrderProvider,
        {
          provide: PRODUCT_MODULE_GLOBAL_CONFIG,
          useValue: {
            checkoutUrl: '/checkout'
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
