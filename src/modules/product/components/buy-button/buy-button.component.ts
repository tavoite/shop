import {Component, Inject, Input, Optional} from '@angular/core';
import {Product} from '../../models/product';
import {ProductModuleConfigInterface} from '../../interfaces/product-module-config.interface';
import {OrderItemDTO} from '../../../checkout/dto/order-item.dto';
import {Router} from '@angular/router';
import {PRODUCT_MODULE_GLOBAL_CONFIG} from '../../consts/product-module-global-config.const';
import {OrderService} from '../../../checkout/services/order.service';

@Component({
  selector: 'product-buy-button',
  templateUrl: './buy-button.component.html',
  styleUrls: ['./buy-button.component.scss']
})
export class BuyButtonComponent {
  @Input()
  public product: Product;

  public constructor(
    @Optional() @Inject(PRODUCT_MODULE_GLOBAL_CONFIG) private config: ProductModuleConfigInterface,
    private orderService: OrderService,
    private router: Router) {
  }

  public onClick(event: Event): void {
    event.preventDefault();

    this.orderService.addOrderItem(this.getNewOrderItemDTO());

    this.router.navigate([this.config.checkoutUrl]);
  }

  public get shouldBeVisible(): boolean {
    return !!(this.config.checkoutUrl && this.orderService);
  }

  private getNewOrderItemDTO(): OrderItemDTO {
    const orderItemDTO = new OrderItemDTO();

    orderItemDTO.product = this.product;
    orderItemDTO.quantity = 1;
    orderItemDTO.cost = this.product.price;

    return orderItemDTO;
  }
}
