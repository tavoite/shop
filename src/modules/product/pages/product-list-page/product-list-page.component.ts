import {Component, OnInit} from '@angular/core';
import {Product} from '../../models/product';
import {ProductRepository} from '../../repositories/product.repository';
import {Category} from '../../models/category';
import {ActivatedRoute, ParamMap, Params, Router} from '@angular/router';
import {CategoryRepository} from '../../repositories/category.repository';
import {Breadcrumb} from '../../../shared/models/breadcrumb';
import {Filter} from '../../models/filter';
import {FilterFactory} from '../../factory/filter.factory';

@Component({
  templateUrl: './product-list-page.component.html',
  styleUrls: ['./product-list-page.component.scss']
})
export class ProductListPageComponent implements OnInit {
  public filterManagerExpanded: boolean = false;
  public productCollection: Array<Product> | undefined;
  public contentLoading: boolean = false;
  public category: Category;

  public constructor(
    private productRepository: ProductRepository,
    private categoryRepository: CategoryRepository,
    private filterFactory: FilterFactory,
    private route: ActivatedRoute,
    private router: Router) {
  }

  public ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const categoryId = +params.get('categoryId');

      this.contentLoading = true;
      this.filterManagerExpanded = false;

      this.categoryRepository.getOne(categoryId).subscribe((category: Category | undefined) => {
        if (category) {
          if (this.route.snapshot.queryParams['filters']) {
            const filters = this.filterFactory.createCollection(this.route.snapshot.queryParams['filters']);

            this.productRepository.getAllByCategoryIdWithFilters(categoryId, filters).subscribe((productCollection: Array<Product>) => {
              this.assignContent(category, productCollection);
            });
          } else {
            this.productRepository.getAllByCategoryId(categoryId).subscribe((productCollection: Array<Product>) => {
              this.assignContent(category, productCollection);
            });
          }
        } else {
          this.assignContent(category, undefined);
        }
      });
    });
  }

  public onFilterListChange(filterList: Array<Filter>): void {
    const queryParams: Params = Object.assign({}, this.route.snapshot.queryParams);
    const filtersString = filterList.map((filter: Filter) => {
      return `${filter.attributeId}-${filter.attributeOptionId}`;
    }).join(',');

    if (!filtersString.length) {
      queryParams['filters'] = null;
    } else {
      queryParams['filters'] = filtersString;
    }

    this.productRepository.getAllByCategoryIdWithFilters(this.category.id, filterList).subscribe((productCollection: Array<Product>) => {
      this.productCollection = productCollection;
      this.router.navigate(['/product-list', this.category.id], {queryParams: queryParams, replaceUrl: true});
    });
  }

  public toggleFilterList(): void {
    this.filterManagerExpanded = !this.filterManagerExpanded;

    if (this.filterManagerExpanded) {
      window.scrollTo({left: 0, top: 0, behavior: 'smooth'});
    }
  }

  public get breadcrumbsItems(): Array<Breadcrumb> {
    return [
      new Breadcrumb('Home', ['/']),
      new Breadcrumb(this.category.name),
    ];
  }

  private assignContent(category: Category, productCollection: Array<Product>): void {
    this.category = category;
    this.productCollection = productCollection;
    this.contentLoading = false;
  }
}
