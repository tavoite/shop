export enum FormStatus {
  Initial,
  Sending,
  Success,
  Fail
}
