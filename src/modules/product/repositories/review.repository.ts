import {ReviewDTO} from '../dto/review.dto';
import {Injectable} from '@angular/core';
import {Observable, Observer} from 'rxjs';

@Injectable()
export class ReviewRepository {
  public add(reviewData: ReviewDTO): Observable<boolean> {
    // Here should be call  to client, which will save review in db.
    return Observable.create((observer: Observer<boolean>) => {
      setTimeout(() => {
        observer.next(true);
      }, 2000);
    });
  }
}
