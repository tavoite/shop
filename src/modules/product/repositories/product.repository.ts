import {ProductResponseInterface} from '../interfaces/product-response.interface';
import {ProductFactory} from '../factory/product.factory';
import {Observable, Observer} from 'rxjs';
import {ProductClient} from '../client/product.client';
import {Product} from '../models/product';
import {Injectable} from '@angular/core';
import {Filter} from '../models/filter';

@Injectable()
export class ProductRepository {
  public constructor(private client: ProductClient, private factory: ProductFactory) {
  }

  public getOne(id: number): Observable<Product | undefined> {
    return Observable.create((observer: Observer<Product>) => {
      this.client.getOne(id).subscribe((data: ProductResponseInterface) => {
        observer.next(this.factory.create(data));
      }, () => {
        observer.next(undefined);
      });
    });
  }

  public getAllByCategoryId(categoryId: number): Observable<Product[]> {
    return Observable.create((observer: Observer<Product[]>) => {
      this.client.getAllByCategoryId(categoryId).subscribe((data: ProductResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }

  public getAllByCategoryIdWithFilters(categoryId: number, filters: Array<Filter>): Observable<Product[]> {
    return Observable.create((observer: Observer<Product[]>) => {
      this.client.getAllByCategoryIdWithFilters(categoryId, filters).subscribe((data: ProductResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }

  public getAllByIds(ids: Array<number>): Observable<Product[]> {
    return Observable.create((observer: Observer<Product[]>) => {
      this.client.getAllByIds(ids).subscribe((data: ProductResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }

  public getAllByPhrase(phrase: string): Observable<Product[]> {
    return Observable.create((observer: Observer<Product[]>) => {
      this.client.getAllByPhrase(phrase).subscribe((data: ProductResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }
}
