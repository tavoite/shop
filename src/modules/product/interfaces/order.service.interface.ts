import {OrderItemDTO} from '../../checkout/dto/order-item.dto';

export interface OrderServiceInterface {
  addOrderItem(orderItemDTO: OrderItemDTO): void;
}
