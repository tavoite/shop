import {PhotoResponseInterface} from './photo-response.interface';
import {CategoryResponseInterface} from './category-response.interface';
import {ReviewResponseInterface} from './review-response.interface';
import {AttributeResponseInterface} from './attribute-response.interface';

export interface ProductResponseInterface {
  id: number;
  name: string;
  price: number;
  quantity: number;
  category: CategoryResponseInterface;
  photos: Array<PhotoResponseInterface>;
  attributes: Array<AttributeResponseInterface>;
  reviews: Array<ReviewResponseInterface>;
}
