import {AttributeResponseInterface} from './attribute-response.interface';

export interface CategoryResponseInterface {
  id: number;
  name: string;
  attributes?: Array<AttributeResponseInterface>;
}
