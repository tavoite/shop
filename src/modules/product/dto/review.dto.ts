export class ReviewDTO {
  public rating: number;
  public authorName: string;
  public date: Date;
  public content: string;
  public productId: number;
}
