import {PaymentResponseInterface} from '../interfaces/payment-response.interface';
import {Payment} from '../models/payment';
import {PaymentFactory} from './payment.factory';

describe('PaymentFactory', () => {
  let factory: PaymentFactory;

  beforeEach(() => {
    factory = new PaymentFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Payment object', () => {
    const data = <PaymentResponseInterface>{
      id: 1,
      name: 'Test',
      cost: 9.99
    };

    const payment = factory.create(data);

    expect(payment instanceof Payment).toBeTruthy();
    expect(payment.id).toBe(data.id);
    expect(payment.name).toBe(data.name);
    expect(payment.cost).toBe(data.cost);
  });

  it('should return array of Payment objects', () => {
    const data = [
      <PaymentResponseInterface>{
        id: 1,
        name: 'Test',
        cost: 9.99
      },
      <PaymentResponseInterface>{
        id: 2,
        name: 'Test 2',
        cost: 10.65
      }
    ];

    const paymentCollection = factory.createCollection(data);

    expect(paymentCollection.length).toBe(data.length);
  });
});
