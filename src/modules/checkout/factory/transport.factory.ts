import {Injectable} from '@angular/core';
import {TransportResponseInterface} from '../interfaces/transport-response.interface';
import {Transport} from '../models/transport';

@Injectable()
export class TransportFactory {
  public create(data: TransportResponseInterface): Transport {
    return new Transport(data.id, data.name, data.cost);
  }

  public createCollection(data: TransportResponseInterface[]): Transport[] {
    const transportCollection: Array<Transport> = [];

    for (const transportData of data) {
      transportCollection.push(this.create(transportData));
    }

    return transportCollection;
  }
}
