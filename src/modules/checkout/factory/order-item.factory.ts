import {Injectable} from '@angular/core';
import {OrderItemDTO} from '../dto/order-item.dto';
import {OrderItem} from '../models/order-item';
import {v4 as uuid} from 'uuid';

@Injectable()
export class OrderItemFactory {
  public create(orderItemDTO: OrderItemDTO): OrderItem {
    return new OrderItem(uuid(), orderItemDTO.quantity, orderItemDTO.cost, orderItemDTO.product);
  }
}
