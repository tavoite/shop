import {OrderItemFactory} from './order-item.factory';
import {OrderItemDTO} from '../dto/order-item.dto';
import {Product} from '../../product/models/product';
import {Category} from '../../product/models/category';

describe('OrderItemFactory', () => {
  let factory: OrderItemFactory;

  beforeEach(() => {
    factory = new OrderItemFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return OrderItemFactory object', function () {
    const dto = new OrderItemDTO();

    dto.product = new Product(1, 'Test', 10, new Category(1, 'Test category'));
    dto.cost = 10;
    dto.quantity = 1;

    const orderItem = factory.create(dto);

    expect(orderItem.product).toBe(dto.product);
    expect(orderItem.cost).toBe(dto.cost);
    expect(orderItem.quantity).toBe(dto.quantity);
  });
});
