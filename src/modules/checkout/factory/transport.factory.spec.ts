import {TransportResponseInterface} from '../interfaces/transport-response.interface';
import {Transport} from '../models/transport';
import {TransportFactory} from './transport.factory';

describe('TransportFactory', () => {
  let factory: TransportFactory;

  beforeEach(() => {
    factory = new TransportFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Transport object', () => {
    const data = <TransportResponseInterface>{
      id: 1,
      name: 'Test',
      cost: 9.99
    };

    const transport = factory.create(data);

    expect(transport instanceof Transport).toBeTruthy();
    expect(transport.id).toBe(data.id);
    expect(transport.name).toBe(data.name);
    expect(transport.cost).toBe(data.cost);
  });

  it('should return array of Transport objects', () => {
    const data = [
      <TransportResponseInterface>{
        id: 1,
        name: 'Test',
        cost: 9.99
      },
      <TransportResponseInterface>{
        id: 2,
        name: 'Test 2',
        cost: 10.65
      }
    ];

    const transportCollection = factory.createCollection(data);

    expect(transportCollection.length).toBe(data.length);
  });
});
