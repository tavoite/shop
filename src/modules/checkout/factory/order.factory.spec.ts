import {OrderFactory} from './order.factory';

describe('OrderFactory', () => {
  let factory: OrderFactory;

  beforeEach(() => {
    factory = new OrderFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return empty Order object', function () {
    const order = factory.createEmpty();

    expect(order.id).not.toBe(undefined);
    expect(order.items.length).toBe(0);
    expect(order.address).toBe(undefined);
    expect(order.payment).toBe(undefined);
    expect(order.transport).toBe(undefined);
    expect(order.state.initialized).toBe(true);
  });
});
