import {Injectable} from '@angular/core';
import {Order} from '../models/order';
import {OrderItem} from '../models/order-item';
import {OrderItemMinimalDTO} from '../dto/order-item-minimal.dto';
import {OrderDTO} from '../dto/order.dto';
import {PaymentRepository} from '../repositories/payment.repository';
import {TransportRepository} from '../repositories/transport.repository';
import {ProductRepository} from '../../product/repositories/product.repository';
import {AddressFactory} from '../../client/factory/address.factory';
import {OrderItemFactory} from '../factory/order-item.factory';
import {OrderMinimalDTO} from '../dto/order-minimal.dto';
import {AddressDTO} from '../../client/dto/address.dto';
import {OrderFactory} from '../factory/order.factory';
import {Payment} from '../models/payment';
import {Transport} from '../models/transport';
import {BehaviorSubject, Observable} from 'rxjs';
import {OrderItemDTO} from '../dto/order-item.dto';
import {Product} from '../../product/models/product';

@Injectable()
export class LocalOrderService {
  private readonly LS_ORDER_KEY: string = 'order';

  public constructor(
    private paymentRepository: PaymentRepository,
    private transportRepository: TransportRepository,
    private productRepository: ProductRepository,
    private addressFactory: AddressFactory,
    private orderItemFactory: OrderItemFactory,
    private orderFactory: OrderFactory) {
  }

  public saveOrder(order: Order): void {
    this.saveToStorage(this.createOrderMinimalDTO(order));
  }

  public reCreateOrder(): Observable<Order | undefined> {
    const orderMinimalDTO = this.readFromStorage();

    if (orderMinimalDTO) {
      const orderDTO = new OrderDTO(orderMinimalDTO.id),
        orderSubject = new BehaviorSubject(undefined),
        productIds = [];

      orderDTO.state = orderMinimalDTO.state;

      if (orderMinimalDTO.address) {
        orderDTO.address = this.addressFactory.create(orderMinimalDTO.address);
      }

      if (orderMinimalDTO.items.length) {
        for (const orderItemMinimalDTO of orderMinimalDTO.items) {
          productIds.push(orderItemMinimalDTO.productId);
        }

        this.productRepository.getAllByIds(productIds).subscribe((products: Array<Product>) => {
          for (const product of products) {
            const orderItemMinimalDTO = orderMinimalDTO.items.find((item: OrderItemMinimalDTO) => {
              return item.productId === product.id;
            });

            if (orderItemMinimalDTO) {
              orderDTO.items.push(this.orderItemFactory.create(new OrderItemDTO(
                product,
                orderItemMinimalDTO.quantity,
                orderItemMinimalDTO.cost
              )));
            }
          }

          this.checkAndSend(orderMinimalDTO, orderDTO, orderSubject);
        });
      }

      if (orderMinimalDTO.transportId) {
        this.transportRepository.getOne(orderMinimalDTO.transportId).subscribe((transport?: Transport) => {
          orderDTO.transport = transport;

          this.checkAndSend(orderMinimalDTO, orderDTO, orderSubject);
        });
      }

      if (orderMinimalDTO.paymentId) {
        this.paymentRepository.getOne(orderMinimalDTO.paymentId).subscribe((payment?: Payment) => {
          orderDTO.payment = payment;

          this.checkAndSend(orderMinimalDTO, orderDTO, orderSubject);
        });
      }

      this.checkAndSend(orderMinimalDTO, orderDTO, orderSubject);

      return orderSubject.asObservable();
    }

    return undefined;
  }

  public hasOrderInStorage(): boolean {
    return !!localStorage.getItem(this.LS_ORDER_KEY);
  }

  private checkAndSend(orderMinimalDTO: OrderMinimalDTO, orderDTO: OrderDTO, orderSubject: BehaviorSubject<Order | undefined>): boolean {
    if (orderMinimalDTO.id !== orderDTO.id) {
      return false;
    }

    if (orderMinimalDTO.address && !orderDTO.address) {
      return false;
    }

    if (orderMinimalDTO.transportId && !orderDTO.transport) {
      return false;
    }

    if (orderMinimalDTO.paymentId && !orderDTO.payment) {
      return false;
    }

    if (orderMinimalDTO.items.length !== orderDTO.items.length) {
      return false;
    }

    orderSubject.next(this.orderFactory.create(orderDTO));

    return true;
  }

  private saveToStorage(orderMinimalDTO: OrderMinimalDTO): void {
    localStorage.setItem(this.LS_ORDER_KEY, JSON.stringify(orderMinimalDTO));
  }

  private readFromStorage(): OrderMinimalDTO | undefined {
    if (this.hasOrderInStorage()) {
      return <OrderMinimalDTO>JSON.parse(localStorage.getItem(this.LS_ORDER_KEY));
    }

    return undefined;
  }

  private createOrderMinimalDTO(order: Order): OrderMinimalDTO {
    const orderMinimalDTO = new OrderMinimalDTO(order.id);

    orderMinimalDTO.state = order.state;

    if (order.address) {
      orderMinimalDTO.address = new AddressDTO(
        order.address.id,
        order.address.name,
        order.address.surname,
        order.address.street,
        order.address.buildingNumber,
        order.address.city,
        order.address.zip,
      );
    }

    orderMinimalDTO.items = order.items.map((orderItem: OrderItem) => {
      return new OrderItemMinimalDTO(
        orderItem.id,
        orderItem.product.id,
        orderItem.quantity,
        orderItem.cost
      );
    });

    if (order.transport) {
      orderMinimalDTO.transportId = order.transport.id;
    }

    if (order.payment) {
      orderMinimalDTO.paymentId = order.payment.id;
    }

    return orderMinimalDTO;
  }
}
