import {Component, OnInit} from '@angular/core';
import {Order} from '../../models/order';
import {Breadcrumb} from '../../../shared/models/breadcrumb';
import {Router} from '@angular/router';
import {OrderProvider} from '../../provider/order.provider';
import {OrderService} from '../../services/order.service';
import {CheckoutStep} from '../../enums/checkout-step.enum';

@Component({
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.scss']
})
export class CheckoutPageComponent implements OnInit {
  public order: Order;
  public CheckoutStep = CheckoutStep;

  public constructor(
    private orderProvider: OrderProvider,
    private orderService: OrderService,
    private router: Router) {
  }

  public ngOnInit(): void {
    this.orderProvider.orderObservable.subscribe((order: Order) => {
      this.order = order;
    });
  }

  public shouldSummaryButtonBeDisabled(): boolean {
    return !this.orderService.isReadyForSummary();
  }

  public goToSummary(): void {
    this.router.navigate(['/checkout/summary']);
  }

  public hasOrderItems(): boolean {
    return !!this.order.items.length;
  }

  public get breadcrumbsItems(): Array<Breadcrumb> {
    return [
      new Breadcrumb('Home', ['/']),
      new Breadcrumb('Checkout')
    ];
  }
}
