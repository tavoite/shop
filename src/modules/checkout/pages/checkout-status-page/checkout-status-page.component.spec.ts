import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CheckoutStatusPageComponent} from './checkout-status-page.component';
import {OrderItemFactory} from '../../factory/order-item.factory';
import {Order} from '../../models/order';
import {Observable, Observer} from 'rxjs';
import {OrderProvider} from '../../provider/order.provider';
import {OrderService} from '../../services/order.service';
import {OrderState} from '../../value/order-state.value';
import {OrderItem} from '../../models/order-item';
import {Product} from '../../../product/models/product';
import {Category} from '../../../product/models/category';
import {Address} from '../../../client/models/address';
import {Transport} from '../../models/transport';
import {Payment} from '../../models/payment';
import {RouterTestingModule} from '@angular/router/testing';

describe('CheckoutStatusPageComponent', () => {
  let component: CheckoutStatusPageComponent;
  let fixture: ComponentFixture<CheckoutStatusPageComponent>;
  let native: HTMLElement;

  const order = new Order('o1', [
      new OrderItem('oi1', 1, 100, new Product(1, 'Test', 100, new Category(1, 'Test')))
    ], new Address('a1', 'test', 'test', 'test', '1', 'test', '00-000'),
    new Transport(1, 'Test', 100),
    new Payment(1, 'Test', 9),
    new OrderState(true, true, true, true, true, true)
  );
  const orderProviderStub = {
    orderObservable: Observable.create((observer: Observer<Order>) => {observer.next(order); })
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CheckoutStatusPageComponent
      ],
      imports: [
        RouterTestingModule
      ],
      providers: [
        OrderItemFactory,
        OrderService,
        {
          provide: OrderProvider,
          useValue: orderProviderStub
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutStatusPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display successful message', () => {
    component.isSaved = true;

    fixture.detectChanges();

    expect(native.querySelector('.message span').textContent).toEqual(
      'Your order is saved and will be prepared within 24h.'
    );
  });

  it('should display failed message', () => {
    component.isSaved = false;

    fixture.detectChanges();

    expect(native.querySelector('.message span').textContent).toEqual(
      'Sorry, but we can\'t save your order. Please contact with someone from our staff.'
    );
  });
});
