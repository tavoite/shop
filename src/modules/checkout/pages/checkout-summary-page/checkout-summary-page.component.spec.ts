import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CheckoutSummaryPageComponent} from './checkout-summary-page.component';
import {OrderItemFactory} from '../../factory/order-item.factory';
import {SharedModule} from '../../../shared/shared.module';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductModule} from '../../../product/product.module';
import {FormsModule} from '@angular/forms';
import {OrderItemListComponent} from '../../components/order-item-list/order-item-list.component';
import {Observable, Observer} from 'rxjs';
import {OrderItem} from '../../models/order-item';
import {Order} from '../../models/order';
import {Product} from '../../../product/models/product';
import {Category} from '../../../product/models/category';
import {Address} from '../../../client/models/address';
import {Transport} from '../../models/transport';
import {Payment} from '../../models/payment';
import {OrderProvider} from '../../provider/order.provider';
import {OrderService} from '../../services/order.service';
import {OrderState} from '../../value/order-state.value';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

describe('CheckoutSummaryPageComponent', () => {
  let component: CheckoutSummaryPageComponent;
  let fixture: ComponentFixture<CheckoutSummaryPageComponent>;

  const product = new Product(1, 'Test product', 51, new Category(1, 'Test category'));
  const order = new Order('o1', [
      new OrderItem('oi1', 100, 50, product)
    ],
    new Address('a1', 'Name', 'Surname', 'Street', '22', 'City', '33-333'),
    new Transport(1, 'Transport', 9),
    new Payment(1, 'Payment', 5),
    new OrderState(true, true, true, true)
  );
  const orderProviderStub = {
    orderObservable: Observable.create((observer: Observer<Order>) => {observer.next(order); })
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CheckoutSummaryPageComponent,
        OrderItemListComponent
      ],
      imports: [
        SharedModule,
        ProductModule,
        FormsModule,
        FontAwesomeModule,
        RouterTestingModule,
      ],
      providers: [
        OrderItemFactory,
        OrderService,
        {
          provide: OrderProvider,
          useValue: orderProviderStub
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutSummaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
