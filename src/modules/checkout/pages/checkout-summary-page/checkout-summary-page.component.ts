import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Order} from '../../models/order';
import {Breadcrumb} from '../../../shared/models/breadcrumb';
import {OrderProvider} from '../../provider/order.provider';
import {OrderService} from '../../services/order.service';

@Component({
  templateUrl: './checkout-summary-page.component.html',
  styleUrls: ['./checkout-summary-page.component.scss']
})
export class CheckoutSummaryPageComponent implements OnInit {
  public order: Order;

  public constructor(
    private orderProvider: OrderProvider,
    private orderService: OrderService,
    private router: Router) {
  }

  public ngOnInit(): void {
    this.orderProvider.orderObservable.subscribe((order: Order) => {
      this.order = order;
    });
  }

  public saveOrder(): void {
    this.orderService.saveOrder();
    this.router.navigate(['/checkout/status']);
  }

  public get breadcrumbsItems(): Array<Breadcrumb> {
    return [
      new Breadcrumb('Home', ['/']),
      new Breadcrumb('Checkout', ['/checkout']),
      new Breadcrumb('Summary')
    ];
  }
}
