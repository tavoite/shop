import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {Order} from '../models/order';
import {OrderFactory} from '../factory/order.factory';
import {LocalOrderService} from '../services/local-order.service';

@Injectable()
export class OrderProvider {
  private reCreateSubscription: Subscription;
  private orderSubject: Subject<Order>;
  private readonly _orderObservable: Observable<Order>;

  public constructor(private orderFactory: OrderFactory, private localOrderService: LocalOrderService) {
    this.orderSubject = new BehaviorSubject(this.orderFactory.createEmpty());
    this._orderObservable = this.orderSubject.asObservable();

    if (this.localOrderService.hasOrderInStorage()) {
      this.reCreateSubscription = this.localOrderService.reCreateOrder().subscribe((order: Order | undefined) => {
        if (order) {
          this.orderSubject.next(order);

          if (this.reCreateSubscription) {
            this.reCreateSubscription.unsubscribe();
          }
        }
      });
    }
  }

  public refresh(order?: Order): void {
    if (!order) {
      order = this.orderFactory.createEmpty();
    }

    this.orderSubject.next(order);

    this.localOrderService.saveOrder(order);
  }

  public get orderObservable(): Observable<Order> {
    return this._orderObservable;
  }
}
