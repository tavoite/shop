import {Product} from '../../product/models/product';

export class OrderItemDTO {
  public constructor(
    public product?: Product,
    public quantity?: number,
    public cost?: number) {
  }
}
