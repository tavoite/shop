import {OrderState} from '../value/order-state.value';
import {Transport} from '../models/transport';
import {Payment} from '../models/payment';
import {Address} from '../../client/models/address';
import {OrderItem} from '../models/order-item';

export class OrderDTO {
  public constructor(
    public id?: string,
    public items: Array<OrderItem> = [],
    public address?: Address,
    public transport?: Transport,
    public payment?: Payment,
    public state?: OrderState
  ) {
  }
}
