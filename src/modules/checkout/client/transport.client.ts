import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {TransportResponseInterface} from '../interfaces/transport-response.interface';

@Injectable()
export class TransportClient {
  public static readonly URL: string = '/api/transport';

  public constructor(private httpClient: HttpClient) {
  }

  public getOne(transportId: number): Observable<TransportResponseInterface> {
    return this.httpClient.get<TransportResponseInterface>(`${TransportClient.URL}/${transportId}`);
  }

  public getAll(): Observable<TransportResponseInterface[]> {
    return this.httpClient.get<TransportResponseInterface[]>(TransportClient.URL);
  }
}
