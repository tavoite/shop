import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {PaymentResponseInterface} from '../interfaces/payment-response.interface';

@Injectable()
export class PaymentClient {
  public static readonly URL: string = '/api/payment';

  public constructor(private httpClient: HttpClient) {
  }

  public getOne(paymentId: number): Observable<PaymentResponseInterface> {
    return this.httpClient.get<PaymentResponseInterface>(`${PaymentClient.URL}/${paymentId}`);
  }

  public getAll(): Observable<PaymentResponseInterface[]> {
    return this.httpClient.get<PaymentResponseInterface[]>(PaymentClient.URL);
  }
}
