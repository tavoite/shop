export class OrderState {
  public constructor(
    public initialized?: boolean | undefined,
    public orderItemsProcessed?: boolean | undefined,
    public addressProcessed?: boolean | undefined,
    public paymentProcessed?: boolean | undefined,
    public transportProcessed?: boolean | undefined,
    public saved?: boolean | undefined
  ) {
  }
}
