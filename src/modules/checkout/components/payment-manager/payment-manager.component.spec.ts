import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {PaymentManagerComponent} from './payment-manager.component';
import {FormsModule} from '@angular/forms';
import {PaymentRepository} from '../../repositories/payment.repository';
import {PaymentClient} from '../../client/payment.client';
import {PaymentFactory} from '../../factory/payment.factory';
import {HttpClientModule} from '@angular/common/http';
import {OrderService} from '../../services/order.service';

describe('PaymentManagerComponent', () => {
  let component: PaymentManagerComponent;
  let fixture: ComponentFixture<PaymentManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PaymentManagerComponent
      ],
      imports: [
        FormsModule,
        HttpClientModule
      ],
      providers: [
        PaymentRepository,
        PaymentClient,
        PaymentFactory,
        {
          provide: OrderService,
          useValue: {
            updateOrder: () => {}
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
