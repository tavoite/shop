import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CheckoutInfoComponent} from './checkout-info.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../services/order.service';
import {OrderItemFactory} from '../../factory/order-item.factory';
import {OrderProvider} from '../../provider/order.provider';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TransportFactory} from '../../factory/transport.factory';
import {TransportClient} from '../../client/transport.client';
import {PaymentRepository} from '../../repositories/payment.repository';
import {LocalOrderService} from '../../services/local-order.service';
import {TransportRepository} from '../../repositories/transport.repository';
import {PaymentFactory} from '../../factory/payment.factory';
import {OrderFactory} from '../../factory/order.factory';
import {PaymentClient} from '../../client/payment.client';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ProductModule} from '../../../product/product.module';
import {ClientModule} from '../../../client/client.module';
import {Observable, Observer} from 'rxjs';
import {Order} from '../../models/order';

describe('CheckoutInfoComponent', () => {
  let component: CheckoutInfoComponent;
  let fixture: ComponentFixture<CheckoutInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CheckoutInfoComponent
      ],
      imports: [
        FontAwesomeModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: OrderProvider,
          useValue: {
            orderObservable: Observable.create((observer: Observer<Order | undefined>) => {
              observer.next(undefined);
            })
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
