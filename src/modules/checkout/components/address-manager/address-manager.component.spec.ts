import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AddressManagerComponent} from './address-manager.component';
import {FormsModule} from '@angular/forms';
import {ClientModule} from '../../../client/client.module';
import {OrderService} from '../../services/order.service';
import {OrderItemFactory} from '../../factory/order-item.factory';
import {OrderProvider} from '../../provider/order.provider';
import {OrderFactory} from '../../factory/order.factory';
import {LocalOrderService} from '../../services/local-order.service';
import {PaymentRepository} from '../../repositories/payment.repository';
import {TransportRepository} from '../../repositories/transport.repository';
import {ProductModule} from '../../../product/product.module';
import {PaymentClient} from '../../client/payment.client';
import {TransportClient} from '../../client/transport.client';
import {PaymentFactory} from '../../factory/payment.factory';
import {TransportFactory} from '../../factory/transport.factory';

describe('AddressManagerComponent', () => {
  let component: AddressManagerComponent;
  let fixture: ComponentFixture<AddressManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddressManagerComponent
      ],
      imports: [
        FormsModule,
        ClientModule,
        ProductModule
      ],
      providers: [
        OrderService,
        OrderProvider,
        OrderItemFactory,
        OrderFactory,
        LocalOrderService,
        PaymentRepository,
        TransportRepository,
        PaymentClient,
        TransportClient,
        PaymentFactory,
        TransportFactory
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
