import {Component, Input} from '@angular/core';
import {OrderItem} from '../../models/order-item';
import {OrderService} from '../../services/order.service';
import {Order} from '../../models/order';
import {CheckoutSectionComponentInterface} from '../../interfaces/checkout-section-component.interface';

@Component({
  selector: 'checkout-order-item-list',
  templateUrl: './order-item-list.component.html',
  styleUrls: ['./order-item-list.component.scss']
})
export class OrderItemListComponent implements CheckoutSectionComponentInterface {
  @Input()
  public order: Order;

  @Input()
  public readOnlyMode: boolean = false;

  public errorMessage: boolean = false;
  public readonly ORDER_ITEM_QUANTITY_MIN: number = 1;
  public readonly ORDER_ITEM_QUANTITY_MAX: number = 10;

  public constructor(private orderService: OrderService) {
  }

  public removeOrderItem(orderItem: OrderItem): void {
    this.orderService.removeOrderItem(orderItem);

    if (!this.order.items.length) {
      this.orderService.clearOrder();
    }
  }

  public onButtonClick(): void {
    this.order.state.orderItemsProcessed = true;
    this.orderService.updateOrder(this.order);
  }

  public onOrderItemQuantityInput(orderItem: OrderItem, eventTarget: HTMLInputElement): void {
    const inputValue = parseInt(eventTarget.value, 10);

    if (Number.isNaN(inputValue) || inputValue < this.ORDER_ITEM_QUANTITY_MIN || inputValue > this.ORDER_ITEM_QUANTITY_MAX) {
      this.order.state.orderItemsProcessed = false;
      this.errorMessage = true;
    } else {
      this.order.state.orderItemsProcessed = undefined;
      this.errorMessage = false;

      orderItem.quantity = inputValue;

      this.orderService.updateOrder(this.order);
    }
  }

  public shouldButtonBeDisabled(): boolean {
    return !this.order.items || !this.order.items.length || this.state === false;
  }

  public shouldBeVisible(): boolean {
    return this.state === undefined || this.state === false;
  }

  public get state(): boolean | undefined {
    return this.order.state.orderItemsProcessed;
  }
}
