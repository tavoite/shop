import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {OrderItemListComponent} from './order-item-list.component';
import {ProductModule} from '../../../product/product.module';
import {FormsModule} from '@angular/forms';
import {OrderService} from '../../services/order.service';
import {Order} from '../../models/order';
import {OrderItem} from '../../models/order-item';
import {Product} from '../../../product/models/product';
import {Category} from '../../../product/models/category';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

describe('OrderItemListComponent', () => {
  let component: OrderItemListComponent;
  let fixture: ComponentFixture<OrderItemListComponent>;
  let native: HTMLElement;

  const product =  new Product(1, 'Test product', 51, new Category(1, 'Test category'));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrderItemListComponent
      ],
      imports: [
        ProductModule,
        FontAwesomeModule,
        FormsModule
      ],
      providers: [
        {
          provide: OrderService,
          useValue: {
            removeOrderItem: () => {},
            clearOrder: () => {},
            updateOrder: () => {}
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemListComponent);
    component = fixture.componentInstance;

    component.order = new Order('o1', [
      new OrderItem('oi1', 100, 50, product)
    ]);

    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have one order item on list', () => {
    expect(native.querySelector('.order-item .order-item-name').textContent).toEqual('Test product');
  });

  it('should order item have read-only-mode class', () => {
    component.readOnlyMode = true;

    fixture.detectChanges();

    expect(native.querySelector('.order-item').classList.contains('read-only-mode')).toBeTruthy();
  });

  it('should display error message', () => {
    component.errorMessage = true;

    fixture.detectChanges();

    expect(native.querySelector('.error-message')).toBeTruthy();
  });

  it('should display no order items message and no order item list', () => {
    component.order = new Order('o1');

    fixture.detectChanges();

    expect(native.querySelector('.empty-basket-message')).toBeTruthy();
    expect(native.querySelector('.order-item-list')).toBeFalsy();
  });
});
