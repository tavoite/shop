import {Component, ComponentFactoryResolver, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Order} from '../../models/order';
import {OrderItemListComponent} from '../order-item-list/order-item-list.component';
import {AddressManagerComponent} from '../address-manager/address-manager.component';
import {PaymentManagerComponent} from '../payment-manager/payment-manager.component';
import {TransportManagerComponent} from '../transport-manager/transport-manager.component';
import {CheckoutStep} from '../../enums/checkout-step.enum';
import {CheckoutSectionComponentInterface} from '../../interfaces/checkout-section-component.interface';

@Component({
  selector: 'checkout-section',
  templateUrl: './checkout-section.component.html',
  styleUrls: ['./checkout-section.component.scss'],
  entryComponents: [
    OrderItemListComponent,
    AddressManagerComponent,
    PaymentManagerComponent,
    TransportManagerComponent
  ],
})
export class CheckoutSectionComponent implements OnInit {
  @ViewChild('componentContainer', {read: ViewContainerRef})
  public componentContainer: ViewContainerRef;

  @Input()
  public step: CheckoutStep;

  @Input()
  public sectionTitle: string;

  @Input()
  public alwaysOpen: boolean = false;

  @Input()
  public set order(order: Order | undefined) {
    this._order = order;

    if (this.componentInstance) {
      this.componentInstance.order = order;
    }
  }

  public get order(): Order | undefined {
    return this._order;
  }

  public componentInstance: CheckoutSectionComponentInterface;
  private _order: Order | undefined;
  private open: boolean = false;
  private componentMap: Array<{
    checkoutStep: CheckoutStep,
    component: any
  }> = [
    {
      checkoutStep: CheckoutStep.OrderItemListStep,
      component: OrderItemListComponent
    },
    {
      checkoutStep: CheckoutStep.AddressStep,
      component: AddressManagerComponent
    },
    {
      checkoutStep: CheckoutStep.PaymentStep,
      component: PaymentManagerComponent
    },
    {
      checkoutStep: CheckoutStep.TransportStep,
      component: TransportManagerComponent
    }
  ];

  public constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }

  public ngOnInit(): void {
    if (this.step !== undefined) {
      this.loadComponent();
    }
  }

  public toggleSection(): void {
    this.open = !this.open;
  }

  public shouldSectionBeOpen(): boolean {
    return this.componentInstance && (this.componentInstance.shouldBeVisible() || this.open || this.alwaysOpen);
  }

  private loadComponent() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.componentMap[this.step].component);

    this.componentContainer.clear();

    const componentRef = this.componentContainer.createComponent(componentFactory);

    this.componentInstance = (
      <CheckoutSectionComponentInterface>componentRef.instance
    );

    this.componentInstance.order = this.order;

    if (this.componentInstance.visibility) {
      this.componentInstance.visibility.subscribe((value: boolean) => {
        this.open = value;
      });
    }
  }
}
