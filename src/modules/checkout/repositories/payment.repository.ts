import {Observable, Observer} from 'rxjs';
import {Injectable} from '@angular/core';
import {PaymentClient} from '../client/payment.client';
import {PaymentFactory} from '../factory/payment.factory';
import {Payment} from '../models/payment';
import {PaymentResponseInterface} from '../interfaces/payment-response.interface';

@Injectable()
export class PaymentRepository {
  public constructor(private client: PaymentClient, private factory: PaymentFactory) {
  }

  public getOne(id: number): Observable<Payment> {
    return Observable.create((observer: Observer<Payment>) => {
      this.client.getOne(id).subscribe((data: PaymentResponseInterface) => {
        observer.next(this.factory.create(data));
      });
    });
  }

  public getAll(): Observable<Payment[]> {
    return Observable.create((observer: Observer<Payment[]>) => {
      this.client.getAll().subscribe((data: PaymentResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }
}
