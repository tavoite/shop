import {Observable, Observer} from 'rxjs';
import {Injectable} from '@angular/core';
import {TransportClient} from '../client/transport.client';
import {TransportFactory} from '../factory/transport.factory';
import {TransportResponseInterface} from '../interfaces/transport-response.interface';
import {Transport} from '../models/transport';

@Injectable()
export class TransportRepository {
  public constructor(private client: TransportClient, private factory: TransportFactory) {
  }

  public getOne(id: number): Observable<Transport> {
    return Observable.create((observer: Observer<Transport>) => {
      this.client.getOne(id).subscribe((data: TransportResponseInterface) => {
        observer.next(this.factory.create(data));
      });
    });
  }

  public getAll(): Observable<Transport[]> {
    return Observable.create((observer: Observer<Transport[]>) => {
      this.client.getAll().subscribe((data: TransportResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }
}
