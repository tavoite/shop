export enum CheckoutStep {
  OrderItemListStep,
  AddressStep,
  PaymentStep,
  TransportStep
}
