import {Injectable} from '@angular/core';
import {CheckoutGuard} from './checkout.guard';
import {OrderState} from '../value/order-state.value';

@Injectable()
export class CheckoutSummaryGuard extends CheckoutGuard {
  protected isAcceptableState(orderState: OrderState): boolean {
    return this.orderService.isReadyForSummary();
  }
}
