import {CanActivate, Router} from '@angular/router';
import {Order} from '../models/order';
import {OrderProvider} from '../provider/order.provider';
import {Injectable} from '@angular/core';
import {OrderState} from '../value/order-state.value';
import {OrderService} from '../services/order.service';

@Injectable()
export abstract class CheckoutGuard implements CanActivate {
  private order: Order;

  public constructor(private orderProvider: OrderProvider, protected orderService: OrderService, private router: Router) {
    this.orderProvider.orderObservable.subscribe((order: Order) => {
      this.order = order;
    });
  }

  public canActivate(): boolean {
    if (!this.isAcceptableState(this.order.state)) {
      this.router.navigate(['/checkout']);

      return false;
    }

    return true;
  }

  protected abstract isAcceptableState(orderState: OrderState): boolean;
}
