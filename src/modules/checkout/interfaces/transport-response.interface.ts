export interface TransportResponseInterface {
  id: number;
  name: string;
  cost: number;
}
