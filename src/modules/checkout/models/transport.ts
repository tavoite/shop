export class Transport {
  public constructor(private _id: number, private _name: string, private _cost: number) {
  }

  public get id(): number {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get cost(): number {
    return this._cost;
  }
}
