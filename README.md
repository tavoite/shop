# Angular Shop

### System Requirements
- node >= 8.9.1
- npm >= 5.6.0

### Installation
- npm i

### Browser support
- Last two versions of Chrome, Firefox, Opera, Edge, Safari, Samsung Browser
- IE is not supported (desktop and mobile), Opera Mini as well

### Additional
- To test production build (with sw), install http-server globally, run `ng build --prod` and run `http-server -p 4020` in dist directory.
