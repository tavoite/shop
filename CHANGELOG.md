### 1.0.1
- change search logic
- fix unhandled exception, when product with given id does not exist
- fixes in styling e.g not found page
- change filter logic (move most of it to separate service)
- refactor

### 1.0.2
- add themes
- change scss properties to css variables
- order can be saved in local storage from now

### 1.0.3
- project name change
- logo change

### 1.0.4
- app version change

### 1.0.5
- add almost 100 new products
- update forgotten changelog
- update dependencies, angular version is now 6.1
- refresh reviews view in product detail
